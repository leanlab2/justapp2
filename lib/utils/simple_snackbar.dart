import 'package:flutter/material.dart';

void simpleSnackBar(BuildContext context, String s){
  Scaffold.of(context).showSnackBar(SnackBar(content: Text(s),duration: Duration(milliseconds: 500),));
}

void simpleLongSnackBar(BuildContext context, String s){
  Scaffold.of(context).showSnackBar(SnackBar(content: Text(s),duration: Duration(milliseconds: 800),));
}

void simpleLongLongSnackBar(BuildContext context, String s){
  Scaffold.of(context).showSnackBar(SnackBar(content: Text(s),duration: Duration(milliseconds: 1500),));
}