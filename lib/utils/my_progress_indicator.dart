
import 'package:flutter/material.dart';
import 'package:justapp/app/const/constants.dart';

class MyProgressIndicator extends StatelessWidget {

  final double containerSize;
  final double progressSize;

  const MyProgressIndicator({required this.containerSize, this.progressSize = 60});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: containerSize,
      height: containerSize,
      child: Center(
        child: SizedBox(
          height: progressSize,
          width: progressSize,
          child: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(Color(Constants.defaultColor)),
          ),
        ),
      ),
    );
  }
}
