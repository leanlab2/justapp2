import 'dart:io';

import 'package:flutter/material.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/admin_user_state.dart';
import 'package:justapp/app/models/firebase_auth_state.dart';
import 'package:justapp/app/models/firestore/admin_user_model.dart';
import 'package:justapp/app/models/firestore/setting/entire_model.dart';
import 'package:justapp/app/models/firestore/setting/string_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_board_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_category_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_favorite_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_made_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_profile_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_receive_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_send_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_web_model.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/repo/admin_user_network_repository.dart';
import 'package:justapp/app/repo/appinfo_network_repository.dart';
import 'package:justapp/app/repo/board_network_repository.dart';
import 'package:justapp/app/repo/custom_app_network_repository.dart';
import 'package:justapp/app/repo/setting_network_repository.dart';
import 'package:justapp/app/repo/user_network_repository.dart';
import 'package:justapp/app/viewer/app/tabs/viewer_tabs.dart';
import 'package:package_info/package_info.dart';
import 'package:flutter/foundation.dart' as foundation;

Future<void> initSettingModel(SettingState settingState, String userMade) async {
  print('4_initSettingModel');
  //stream listen
  settingNetworkRepository.selectGetTabStream(userMade, KEY_CUSTOMAPP_SETTINGS_ENTIRE, 'entire').listen((entireModel) async {
    settingState.entireModel = entireModel;
  });
  //string stream listen
  settingNetworkRepository.selectGetTabStream(userMade, KEY_CUSTOMAPP_SETTINGS_STRING, 'string').listen((stringModel) async {
    settingState.stringModel = stringModel;
  });
  //tab stream listen
  settingNetworkRepository.selectGetTabStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, 'tab').listen((tab1Model) async {
    settingState.tab1Model = tab1Model;
  });
  settingNetworkRepository.selectGetTabStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, 'tab').listen((tab2Model) async {
    settingState.tab2Model = tab2Model;
  });
  settingNetworkRepository.selectGetTabStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, 'tab').listen((tab3Model) async {
    settingState.tab3Model = tab3Model;
  });
  settingNetworkRepository.selectGetTabStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, 'tab').listen((tab4Model) async {
    settingState.tab4Model = tab4Model;
  });
  settingNetworkRepository.selectGetTabStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, 'tab').listen((tab5Model) async {
    settingState.tab5Model = tab5Model;
  });
  //tab board stream listen
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, KEY_CUSTOMAPP_SETTINGS_BOARD).listen((tab1BoardModel) async {
    settingState.tab1BoardModel = tab1BoardModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, KEY_CUSTOMAPP_SETTINGS_BOARD).listen((tab2BoardModel) async {
    settingState.tab2BoardModel = tab2BoardModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, KEY_CUSTOMAPP_SETTINGS_BOARD).listen((tab3BoardModel) async {
    settingState.tab3BoardModel = tab3BoardModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, KEY_CUSTOMAPP_SETTINGS_BOARD).listen((tab4BoardModel) async {
    settingState.tab4BoardModel = tab4BoardModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, KEY_CUSTOMAPP_SETTINGS_BOARD).listen((tab5BoardModel) async {
    settingState.tab5BoardModel = tab5BoardModel;
  });
  //tab favorite stream listen
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, KEY_CUSTOMAPP_SETTINGS_FAVORITE).listen((tab1FavoriteModel) async {
    settingState.tab1FavoriteModel = tab1FavoriteModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, KEY_CUSTOMAPP_SETTINGS_FAVORITE).listen((tab2FavoriteModel) async {
    settingState.tab2FavoriteModel = tab2FavoriteModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, KEY_CUSTOMAPP_SETTINGS_FAVORITE).listen((tab3FavoriteModel) async {
    settingState.tab3FavoriteModel = tab3FavoriteModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, KEY_CUSTOMAPP_SETTINGS_FAVORITE).listen((tab4FavoriteModel) async {
    settingState.tab4FavoriteModel = tab4FavoriteModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, KEY_CUSTOMAPP_SETTINGS_FAVORITE).listen((tab5FavoriteModel) async {
    settingState.tab5FavoriteModel = tab5FavoriteModel;
  });
  //tab category stream listen
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, KEY_CUSTOMAPP_SETTINGS_CATEGORY).listen((tab1CategoryModel) async {
    settingState.tab1CategoryModel = tab1CategoryModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, KEY_CUSTOMAPP_SETTINGS_CATEGORY).listen((tab2CategoryModel) async {
    settingState.tab2CategoryModel = tab2CategoryModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, KEY_CUSTOMAPP_SETTINGS_CATEGORY).listen((tab3CategoryModel) async {
    settingState.tab3CategoryModel = tab3CategoryModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, KEY_CUSTOMAPP_SETTINGS_CATEGORY).listen((tab4CategoryModel) async {
    settingState.tab4CategoryModel = tab4CategoryModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, KEY_CUSTOMAPP_SETTINGS_CATEGORY).listen((tab5CategoryModel) async {
    settingState.tab5CategoryModel = tab5CategoryModel;
  });
  //tab made stream listen
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, KEY_CUSTOMAPP_SETTINGS_MADE).listen((tab1MadeModel) async {
    settingState.tab1MadeModel = tab1MadeModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, KEY_CUSTOMAPP_SETTINGS_MADE).listen((tab2MadeModel) async {
    settingState.tab2MadeModel = tab2MadeModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, KEY_CUSTOMAPP_SETTINGS_MADE).listen((tab3MadeModel) async {
    settingState.tab3MadeModel = tab3MadeModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, KEY_CUSTOMAPP_SETTINGS_MADE).listen((tab4MadeModel) async {
    settingState.tab4MadeModel = tab4MadeModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, KEY_CUSTOMAPP_SETTINGS_MADE).listen((tab5MadeModel) async {
    settingState.tab5MadeModel = tab5MadeModel;
  });
  //tab send stream listen
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, KEY_CUSTOMAPP_SETTINGS_SEND).listen((tab1SendModel) async {
    settingState.tab1SendModel = tab1SendModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, KEY_CUSTOMAPP_SETTINGS_SEND).listen((tab2SendModel) async {
    settingState.tab2SendModel = tab2SendModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, KEY_CUSTOMAPP_SETTINGS_SEND).listen((tab3SendModel) async {
    settingState.tab3SendModel = tab3SendModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, KEY_CUSTOMAPP_SETTINGS_SEND).listen((tab4SendModel) async {
    settingState.tab4SendModel = tab4SendModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, KEY_CUSTOMAPP_SETTINGS_SEND).listen((tab5SendModel) async {
    settingState.tab5SendModel = tab5SendModel;
  });
  //tab receive stream listen
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, KEY_CUSTOMAPP_SETTINGS_RECEIVE).listen((tab1ReceiveModel) async {
    settingState.tab1ReceiveModel = tab1ReceiveModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, KEY_CUSTOMAPP_SETTINGS_RECEIVE).listen((tab2ReceiveModel) async {
    settingState.tab2ReceiveModel = tab2ReceiveModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, KEY_CUSTOMAPP_SETTINGS_RECEIVE).listen((tab3ReceiveModel) async {
    settingState.tab3ReceiveModel = tab3ReceiveModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, KEY_CUSTOMAPP_SETTINGS_RECEIVE).listen((tab4ReceiveModel) async {
    settingState.tab4ReceiveModel = tab4ReceiveModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, KEY_CUSTOMAPP_SETTINGS_RECEIVE).listen((tab5ReceiveModel) async {
    settingState.tab5ReceiveModel = tab5ReceiveModel;
  });
  //tab web stream listen
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, KEY_CUSTOMAPP_SETTINGS_WEB).listen((tab1WebModel) async {
    settingState.tab1WebModel = tab1WebModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, KEY_CUSTOMAPP_SETTINGS_WEB).listen((tab2WebModel) async {
    settingState.tab2WebModel = tab2WebModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, KEY_CUSTOMAPP_SETTINGS_WEB).listen((tab3WebModel) async {
    settingState.tab3WebModel = tab3WebModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, KEY_CUSTOMAPP_SETTINGS_WEB).listen((tab4WebModel) async {
    settingState.tab4WebModel = tab4WebModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, KEY_CUSTOMAPP_SETTINGS_WEB).listen((tab5WebModel) async {
    settingState.tab5WebModel = tab5WebModel;
  });
  //tab profile stream listen
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, KEY_CUSTOMAPP_SETTINGS_PROFILE).listen((tab1ProfileModel) async {
    settingState.tab1ProfileModel = tab1ProfileModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, KEY_CUSTOMAPP_SETTINGS_PROFILE).listen((tab2ProfileModel) async {
    settingState.tab2ProfileModel = tab2ProfileModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, KEY_CUSTOMAPP_SETTINGS_PROFILE).listen((tab3ProfileModel) async {
    settingState.tab3ProfileModel = tab3ProfileModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, KEY_CUSTOMAPP_SETTINGS_PROFILE).listen((tab4ProfileModel) async {
    settingState.tab4ProfileModel = tab4ProfileModel;
  });
  settingNetworkRepository.selectGetTabFuncStream(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, KEY_CUSTOMAPP_SETTINGS_PROFILE).listen((tab5ProfileModel) async {
    settingState.tab5ProfileModel = tab5ProfileModel;
  });


  settingState.entireModel = await settingNetworkRepository.selectGetTab(userMade, KEY_CUSTOMAPP_SETTINGS_ENTIRE, EntireModel);
  settingState.stringModel = await settingNetworkRepository.selectGetTab(userMade, KEY_CUSTOMAPP_SETTINGS_STRING, StringModel);
  settingState.tab1Model = await settingNetworkRepository.selectGetTab(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, TabModel);
  settingState.tab2Model = await settingNetworkRepository.selectGetTab(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, TabModel);
  settingState.tab3Model = await settingNetworkRepository.selectGetTab(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, TabModel);
  settingState.tab4Model = await settingNetworkRepository.selectGetTab(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, TabModel);
  settingState.tab5Model = await settingNetworkRepository.selectGetTab(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, TabModel);

  settingState.tab1BoardModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, KEY_CUSTOMAPP_SETTINGS_BOARD, TabBoardModel);
  settingState.tab2BoardModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, KEY_CUSTOMAPP_SETTINGS_BOARD, TabBoardModel);
  settingState.tab3BoardModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, KEY_CUSTOMAPP_SETTINGS_BOARD, TabBoardModel);
  settingState.tab4BoardModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, KEY_CUSTOMAPP_SETTINGS_BOARD, TabBoardModel);
  settingState.tab5BoardModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, KEY_CUSTOMAPP_SETTINGS_BOARD, TabBoardModel);

  settingState.tab1FavoriteModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, KEY_CUSTOMAPP_SETTINGS_FAVORITE, TabFavoriteModel);
  settingState.tab2FavoriteModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, KEY_CUSTOMAPP_SETTINGS_FAVORITE, TabFavoriteModel);
  settingState.tab3FavoriteModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, KEY_CUSTOMAPP_SETTINGS_FAVORITE, TabFavoriteModel);
  settingState.tab4FavoriteModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, KEY_CUSTOMAPP_SETTINGS_FAVORITE, TabFavoriteModel);
  settingState.tab5FavoriteModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, KEY_CUSTOMAPP_SETTINGS_FAVORITE, TabFavoriteModel);

  settingState.tab1CategoryModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, KEY_CUSTOMAPP_SETTINGS_CATEGORY, TabCategoryModel);
  settingState.tab2CategoryModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, KEY_CUSTOMAPP_SETTINGS_CATEGORY, TabCategoryModel);
  settingState.tab3CategoryModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, KEY_CUSTOMAPP_SETTINGS_CATEGORY, TabCategoryModel);
  settingState.tab4CategoryModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, KEY_CUSTOMAPP_SETTINGS_CATEGORY, TabCategoryModel);
  settingState.tab5CategoryModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, KEY_CUSTOMAPP_SETTINGS_CATEGORY, TabCategoryModel);

  settingState.tab1MadeModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, KEY_CUSTOMAPP_SETTINGS_MADE, TabMadeModel);
  settingState.tab2MadeModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, KEY_CUSTOMAPP_SETTINGS_MADE, TabMadeModel);
  settingState.tab3MadeModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, KEY_CUSTOMAPP_SETTINGS_MADE, TabMadeModel);
  settingState.tab4MadeModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, KEY_CUSTOMAPP_SETTINGS_MADE, TabMadeModel);
  settingState.tab5MadeModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, KEY_CUSTOMAPP_SETTINGS_MADE, TabMadeModel);

  settingState.tab1SendModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, KEY_CUSTOMAPP_SETTINGS_SEND, TabSendModel);
  settingState.tab2SendModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, KEY_CUSTOMAPP_SETTINGS_SEND, TabSendModel);
  settingState.tab3SendModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, KEY_CUSTOMAPP_SETTINGS_SEND, TabSendModel);
  settingState.tab4SendModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, KEY_CUSTOMAPP_SETTINGS_SEND, TabSendModel);
  settingState.tab5SendModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, KEY_CUSTOMAPP_SETTINGS_SEND, TabSendModel);

  settingState.tab1ReceiveModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, KEY_CUSTOMAPP_SETTINGS_RECEIVE, TabReceiveModel);
  settingState.tab2ReceiveModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, KEY_CUSTOMAPP_SETTINGS_RECEIVE, TabReceiveModel);
  settingState.tab3ReceiveModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, KEY_CUSTOMAPP_SETTINGS_RECEIVE, TabReceiveModel);
  settingState.tab4ReceiveModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, KEY_CUSTOMAPP_SETTINGS_RECEIVE, TabReceiveModel);
  settingState.tab5ReceiveModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, KEY_CUSTOMAPP_SETTINGS_RECEIVE, TabReceiveModel);

  settingState.tab1WebModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, KEY_CUSTOMAPP_SETTINGS_WEB, TabWebModel);
  settingState.tab2WebModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, KEY_CUSTOMAPP_SETTINGS_WEB, TabWebModel);
  settingState.tab3WebModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, KEY_CUSTOMAPP_SETTINGS_WEB, TabWebModel);
  settingState.tab4WebModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, KEY_CUSTOMAPP_SETTINGS_WEB, TabWebModel);
  settingState.tab5WebModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, KEY_CUSTOMAPP_SETTINGS_WEB, TabWebModel);


  settingState.tab1ProfileModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB1, KEY_CUSTOMAPP_SETTINGS_PROFILE, TabProfileModel);
  settingState.tab2ProfileModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB2, KEY_CUSTOMAPP_SETTINGS_PROFILE, TabProfileModel);
  settingState.tab3ProfileModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB3, KEY_CUSTOMAPP_SETTINGS_PROFILE, TabProfileModel);
  settingState.tab4ProfileModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB4, KEY_CUSTOMAPP_SETTINGS_PROFILE, TabProfileModel);
  settingState.tab5ProfileModel = await settingNetworkRepository.selectGetTabFunc(userMade, KEY_CUSTOMAPP_SETTINGS_TAB5, KEY_CUSTOMAPP_SETTINGS_PROFILE, TabProfileModel);
  print("initSettingModel_end");
}


Future<AdminUserModel> initAdminUserModel(FirebaseAuthState firebaseAuthState, AdminUserState adminUserState) async {
  print('1_initAdminUserModel');
  adminUserNetworkRepository.getAdminUserModelStream(firebaseAuthState.firebaseUser.uid).listen((userAdminModel) {
    adminUserState.adminUserModel = userAdminModel;
  });

  //future await
  adminUserState.adminUserModel = await adminUserNetworkRepository.getAdminUserModel(firebaseAuthState.firebaseUser.uid);
  return adminUserState.adminUserModel;
}

Future<void> initViewerModel(ViewerState viewerState, String userMade, String userKey) async{

  print('1_initAppInfo');
  viewerState.appInfo = await appInfoNetworkRepository.getAppInfo(userMade);

  if(Constants.isMoblie){
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    switch(foundation.defaultTargetPlatform) {
      case foundation.TargetPlatform.iOS:
        if (int.parse(packageInfo.buildNumber) < viewerState.appInfo.versionIos) {
          popUpdate();
        }
        break;
      case foundation.TargetPlatform.android:
        if (int.parse(packageInfo.buildNumber) < viewerState.appInfo.versionAndroid) {
          popUpdate();
        }
        break;
    }
  }

  print('2_initCustomBuildAppModel');
  viewerState.customAppModel = await customAppNetworkRepository.getCustomAppModel(userMade);


  print('3_initUserModel');
  userNetworkRepository.getUserModelStream(userMade, userKey).listen((userModel) {
    viewerState.userModel = userModel;
  });
  viewerState.userModel = await userNetworkRepository.getUserModel(userMade, userKey);


  print('4_initUserModelMap');
  await userNetworkRepository.getUserModelMap(userMade, viewerState);
  // userNetworkRepository.getUserModelMapStream(userMade).listen((userModelMap) {
  //   viewerState.userModelMap = userModelMap;
  // });
  // viewerState.userModelMap = await userNetworkRepository.getUserMapModel(userMade);


  print('5_initBoardModelMap');
  await boardNetworkRepository.getBoardModelMap(userMade, viewerState);

}


void popUpdate() {
  showDialog(
      context: ViewerTabsState.tabContext,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)), //this right here
          backgroundColor: Colors.blueGrey,
          child: Container(
            height: 200,
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: double.infinity,
                    child: Text(
                      '업데이트',
                      style: TextStyle(color: Colors.blueGrey, fontSize: 18),
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 15),
                    width: double.infinity,
                    child: Text(
                      '새로운 업데이트가 있습니다. 지금 업데이트를 하셔서 더 나은 환경을 경험해 보세요.',
                      style: TextStyle(color: Colors.blueGrey, fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  FlatButton(
                    minWidth: double.infinity,
                    child: Text('업데이트', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                    onPressed: () async {
                      // await Constants.openWithStore();
                    },
                    color: Colors.blueGrey,
                    textColor: Colors.blueGrey,
                    padding: EdgeInsets.symmetric(vertical: 22),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                  )
                ],
              ),
            ),
          ),
        );
      });
}

MaterialColor createMaterialColor(Color color) {
  List strengths = <double>[.05];
  var swatch = <int, Color>{};
  final int r = color.red, g = color.green, b = color.blue;

  for (int i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }
  strengths.forEach((strength) {
    final double ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  });
  return MaterialColor(color.value, swatch);
}