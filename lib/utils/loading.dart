// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:justapp/const/constants.dart';
// import 'package:progress_dialog/progress_dialog.dart';
//
// Future<ProgressDialog> showLoading(BuildContext context, String text) async {
//
//   ProgressDialog pr = new ProgressDialog(context, isDismissible: false);
//   pr.style(
//       message: ' ' + text,
//       borderRadius: 10.0,
//       backgroundColor: Constants.color121530,
//       progressWidget: Padding(
//         padding: const EdgeInsets.all(8.0),
//         child: CircularProgressIndicator(backgroundColor: Constants.color116126175, valueColor: new AlwaysStoppedAnimation<Color>(Constants.color121530),),
//       ),
//       insetAnimCurve: Curves.easeInOut,
//       progressTextStyle: TextStyle(
//           color: Constants.color116126175, fontSize: 13.0, fontWeight: FontWeight.w400),
//       messageTextStyle: TextStyle(
//           color: Constants.color116126175, fontSize: 19.0, fontWeight: FontWeight.w600)
//   );
//
//   await pr.show();
//
//   return pr;
// }
//
// void dismissLoading(ProgressDialog pr){
//
//   if(pr != null){
//     pr.hide().then((isHidden) {
//       print(isHidden);
//     });
//   }
// }