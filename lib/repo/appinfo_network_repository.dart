import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/const/firestore_keys.dart';
import 'package:justapp/models/firestore/app_info.dart';

class AppInfoNetworkRepository{
  Future<AppInfo> getAppInfo() async {
    final DocumentReference userRef = FirebaseFirestore.instance.collection(COLLECTION_APPINFO).doc(KEY_APPINFO);
    DocumentSnapshot snapshot = await userRef.get();
    print("getAppInfo");
    print(snapshot.data());
    return AppInfo.fromSnapshot(snapshot);

  }
}

AppInfoNetworkRepository appInfoNetworkRepository = AppInfoNetworkRepository();
