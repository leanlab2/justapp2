import 'dart:io';

import 'package:flutter/material.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/main_view.dart';
import 'package:justapp/app/viewer/viewer_app_view.dart';
import 'package:justapp/models/app_info_state.dart';
import 'package:justapp/repo/appinfo_network_repository.dart';
import 'package:justapp/screens/splash_view.dart';
import 'package:justapp/utils/init_model.dart';
import 'package:provider/provider.dart';
import 'package:justapp/app/const/constants.dart';

class AppView extends StatefulWidget {
  const AppView({Key? key}) : super(key: key);

  @override
  _AppViewState createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  bool isComplete = false;

  @override
  void initState() {
    super.initState();
  }

  late Future _init1;
  bool isa = true;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AppInfoState>(
          create: (_) => AppInfoState(),
        ),
      ],
      child: MaterialApp(
        theme: ThemeData(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          hoverColor: Colors.transparent,
          focusColor: Colors.transparent,
          primarySwatch: createMaterialColor(Color(Constants.defaultColor)),
        ),
        home: Consumer<AppInfoState>(builder: (context,
            AppInfoState appInfoState, child) {
          if(isa) {
            _init1 = _initAppInfo(appInfoState);
          }
          return FutureBuilder(
              future: Future.wait([
                _init1,
              ]),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (!isComplete) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                      return SplashView();
                    case ConnectionState.waiting:
                      return SplashView();
                    default:
                      isComplete = true;
                      return MainView();
                      // return ViewerAppView(userMade: "맛집", userKey: KEY_CUSTOMAPP_USERS_PRODUCER);
                  }
                } else {
                  return MainView();
                  // return ViewerAppView(userMade: "맛집", userKey: KEY_CUSTOMAPP_USERS_PRODUCER);
                }
              });
        }),
      ),
    );
  }

  Future<void> _initAppInfo(AppInfoState appInfoState) async {
    isa = false;
    //future await
    appInfoState.appInfo = await appInfoNetworkRepository.getAppInfo();
    // PackageInfo packageInfo = await PackageInfo.fromPlatform();
    //
    // if (Platform.isAndroid) {
    //   if (int.parse(packageInfo.buildNumber) < appInfoState.appInfo.versionAndroid) {
    //     popUpdate();
    //   }
    // } else if (Platform.isIOS) {
    //   if (int.parse(packageInfo.buildNumber) < appInfoState.appInfo.versionIos) {
    //     popUpdate();
    //   }
    // }
  }

  void popUpdate() {
    showDialog(
        context: context,//TabsState.tabContext,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)), //this right here
            backgroundColor: Colors.white,
            child: Container(
              height: 200,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: double.infinity,
                      child: Text(
                        '업데이트',
                        style: TextStyle(color: Colors.black, fontSize: 18),
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 15),
                      width: double.infinity,
                      child: Text(
                        '새로운 업데이트가 있습니다. 지금 업데이트를 하셔서 더 나은 환경을 경험해 보세요.',
                        style: TextStyle(color: Colors.black, fontSize: 16),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    FlatButton(
                      minWidth: double.infinity,
                      child: Text('업데이트', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                      onPressed: () async {
                        // await Constants.openWithStore();
                      },
                      color: Colors.white,
                      textColor: Colors.black,
                      padding: EdgeInsets.symmetric(vertical: 22),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
}
