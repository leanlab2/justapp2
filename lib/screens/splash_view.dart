import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:justapp/app/const/constants.dart';

class SplashView extends StatefulWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  Widget build(BuildContext context) {
    Constants.currentSize(context);
    return Container(
      width: Constants.size.width,
      child: Stack(
        children: [
          //background image
          SvgPicture.asset(
            'assets/auth/images/splashBg.svg',
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.cover,
          ),

          Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text("메인페이지 스플레쉬 입니다.",style: TextStyle(fontSize: 50,color: Colors.red)),
          ]),
        ],
      ));
  }
}
