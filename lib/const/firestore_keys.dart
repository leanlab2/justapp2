//AppInfo
const COLLECTION_APPINFO = 'AppInfo';
const KEY_APPINFO = 'Justapp';
const KEY_VERSIONANDROID = 'version_android';
const KEY_VERSIONIOS = 'version_ios';
const KEY_VERSIONAPP = 'version_app';

//amdin_user
const COLLECTION_USERS = 'Users';
const KEY_USERKEY = 'user_key';
const KEY_USERIMG = 'image';
const KEY_USEREMAIL = 'email';
const KEY_USERNAME = 'name';
const KEY_USERBELONG = 'belong';
const KEY_USERMADE = 'made';