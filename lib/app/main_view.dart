import 'package:flutter/material.dart';
import 'package:justapp/app/builder/builder_app_view.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/viewer/screens/search_viewer_app_view.dart';
import 'package:justapp/app/viewer/viewer_app_view.dart';

class MainView extends StatefulWidget {
  MainView();

  @override
  _MainViewState createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: false,
        body: ListView(
          children: [
            Column(
              children: [
                //logo
                Container(
                  padding: EdgeInsets.only(top: 50),
                  child: Image.asset(
                    'assets/auth/images/appLogo.png',
                    width: Constants.size.width>300? 300: Constants.size.width * 0.8,
                    color: Colors.black,
                  ),
                ),

                ConstrainedBox(
                  constraints: new BoxConstraints(maxWidth: Constants.size.width * 0.8),
                  child:Text(
                    "앱을 만들고 싶으시면 '빌더' 만든앱을 사용하고 싶으시면 '뷰어'를 클릭해 주세요.",
                    style: TextStyle(color: Colors.black, fontSize: 12),
                    textAlign: TextAlign.center,
                    // overflow: TextOverflow.ellipsis,
                  ),
                ),

                //button
                Container(
                  width: Constants.size.width>400? 400: Constants.size.width * 0.9,
                  padding: EdgeInsets.fromLTRB(30, 50, 30, 10),
                  child: FlatButton(
                    minWidth: double.infinity,
                    child: Text("빌더", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                        return BuilderAppView();
                      }));
                    },
                    color: Color(Constants.defaultColor),
                    textColor: Colors.white,
                    padding: EdgeInsets.symmetric(vertical: 22),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                  ),
                ),

                //button
                Container(
                  width: Constants.size.width>400? 400: Constants.size.width * 0.9,
                  padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                  child: FlatButton(
                    minWidth: double.infinity,
                    child: Text("뷰어", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                        return SearchViewerAppView();
                      }));
                    },
                    color: Color(Constants.defaultColor),
                    textColor: Colors.white,
                    padding: EdgeInsets.symmetric(vertical: 22),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                  ),
                ),
              ],
            ),
          ],
        ),
        );
  }
}
