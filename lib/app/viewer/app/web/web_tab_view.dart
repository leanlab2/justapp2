import 'package:flutter/material.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/viewer/app/others/notification_list_view.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:justapp/utils/transitions.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebTabView extends StatefulWidget {
  final GlobalKey<NavigatorState> navigatorKey;
  final tabNum;
  final userMade;

  WebTabView({required this.navigatorKey, required this.tabNum, required this.userMade});

  @override
  _WebTabViewState createState() => _WebTabViewState();
}

class _WebTabViewState extends State<WebTabView> {
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
        key: widget.tabNum == 1 ? null : widget.navigatorKey,
        onGenerateRoute: (routeSettings) {
          return MaterialPageRoute(
              builder: (context) => Consumer<SettingState>(
                      builder: (BuildContext context, SettingState settingState, child) {
                    return Scaffold(
                      backgroundColor: selectTabBgColor(widget.tabNum, settingState),
                      appBar: AppBar(
                        title: Text('이벤트', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
                        centerTitle: true,
                        actions:[
                          IconButton(icon: Icon(Icons.notifications_none_outlined,color:Colors.black),
                              onPressed: () =>{
                                Navigator.of(context).push(
                                  Transitions(
                                    transitionType: TransitionType.slideLeft,
                                    duration: Duration(milliseconds: 200),
                                    curve: Curves.bounceInOut,
                                    reverseCurve: Curves.fastOutSlowIn,
                                    widget: NotificationListView(),
                                  ),
                                ),
                              }),
                        ],
                        backgroundColor: Colors.white,
                      ),
                      body: SafeArea(
                        child: Stack(
                          children: [
                            WebView(
                              initialUrl: widget.tabNum == 1? settingState.tab1WebModel.url:
                              widget.tabNum == 2? settingState.tab2WebModel.url:
                              widget.tabNum == 3? settingState.tab3WebModel.url:
                              widget.tabNum == 4? settingState.tab4WebModel.url:settingState.tab5WebModel.url,
                              javascriptMode: JavascriptMode.unrestricted,
                              onPageFinished: (finish) {
                                setState(() {
                                  isLoading = false;
                                });
                              },
                            ),
                            isLoading
                                ? Center(
                                    child: CircularProgressIndicator(),
                                  )
                                : SizedBox.shrink(),
                          ],
                        ),
                      ),
                    );
                  }));
        });
  }
}
