import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/others/notification_list_view.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:justapp/utils/transitions.dart';
import 'package:provider/provider.dart';

class FavoriteTabListView extends StatefulWidget {
  final GlobalKey<NavigatorState> navigatorKey;
  final tabNum;
  final userMade;

  FavoriteTabListView({required this.navigatorKey,required this.tabNum,required this.userMade});

  @override
  _FavoriteTabListViewState createState() => _FavoriteTabListViewState();
}

class _FavoriteTabListViewState extends State<FavoriteTabListView> {
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
        key: widget.tabNum == 1 ? null : widget.navigatorKey,
        onGenerateRoute: (routeSettings) {
          return MaterialPageRoute(
              builder: (context) => Consumer2<ViewerState, SettingState>(
              builder: (BuildContext context, ViewerState viewerState, SettingState settingState, child) {
                return Scaffold(
                  backgroundColor: selectTabBgColor(widget.tabNum, settingState),
                  appBar: AppBar(
                    title: Text('관심목록', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
                    centerTitle: true,
                    actions:[
                      IconButton(icon: Icon(Icons.notifications_none_outlined,color:Colors.black),
                          onPressed: () =>{
                            Navigator.of(context).push(
                              Transitions(
                                transitionType: TransitionType.slideLeft,
                                duration: Duration(milliseconds: 200),
                                curve: Curves.bounceInOut,
                                reverseCurve: Curves.fastOutSlowIn,
                                widget: NotificationListView(),
                              ),
                            ),
                          }),
                    ],
                    backgroundColor: Colors.white,
                  ),
                  body: Column(
                    children: [
                      Expanded(
                        child: selectBoardList(
                            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_FAVORITE, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE),
                            settingState, viewerState, viewerState.userModel.favoriteMap, widget.userMade,
                            KEY_CUSTOMAPP_SETTINGS_FAVORITE, widget.tabNum))
                    ],
                  )
                );
              }));
        });
  }
}
