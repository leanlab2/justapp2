import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/board/board_tab_list_view.dart';
import 'package:justapp/app/viewer/app/board/read/read_board_view.dart';
import 'package:justapp/app/viewer/app/board/widgets/board_grid_item.dart';
import 'package:justapp/app/viewer/app/board/widgets/board_image_item.dart';
import 'package:justapp/app/viewer/app/board/widgets/board_item.dart';
import 'package:justapp/app/viewer/app/category/category_tab_view.dart';
import 'package:justapp/app/viewer/app/category/widgets/category_grid_item.dart';
import 'package:justapp/app/viewer/app/category/widgets/category_list_item.dart';
import 'package:justapp/app/viewer/app/favorite/favorite_tab_list_view.dart';
import 'package:justapp/app/viewer/app/others/receive_user_list_view.dart';
import 'package:justapp/app/viewer/app/others/widgets/receive_message_item.dart';
import 'package:justapp/app/viewer/app/profile/profile_tab_view.dart';
import 'package:justapp/app/viewer/app/web/web_tab_view.dart';
import 'package:justapp/utils/transitions.dart';

selectTabBgColor(int tabNum, SettingState settingState) {
  return tabNum == 1
      ? Color(settingState.tab1Model.bgColor)
      : tabNum == 2
          ? Color(settingState.tab2Model.bgColor)
          : tabNum == 3
              ? Color(settingState.tab3Model.bgColor)
              : tabNum == 4
                  ? Color(settingState.tab4Model.bgColor)
                  : Color(settingState.tab5Model.bgColor);
}

selectTabCategory(categoryNum, SettingState settingState) {
  return categoryNum == 1
      ? settingState.entireModel.category1 == {}? "":settingState.entireModel.category1
      : categoryNum == 2
          ? settingState.entireModel.category2 == {}? "":settingState.entireModel.category2
          : categoryNum == 3
              ? settingState.entireModel.category3 == {}? "":settingState.entireModel.category3
              : categoryNum == 4
                  ? settingState.entireModel.category4 == {}? "":settingState.entireModel.category4
                  : categoryNum == 5
                      ? settingState.entireModel.category5 == {}? "":settingState.entireModel.category5
                      : categoryNum == 6
                          ? settingState.entireModel.category6 == {}? "":settingState.entireModel.category6
                          : categoryNum == 7
                              ? settingState.entireModel.category7 == {}? "":settingState.entireModel.category7
                              : settingState.entireModel.category8 == {}? "":settingState.entireModel.category8;
}

//board
selectBoardList(String listType, SettingState settingState, ViewerState viewerState, standardContainer, String userMade, String funcType, int tabNum) {
  // viewerState.currentMarkerId = "";
  if (standardContainer.length == 0) {
    return Center(child: Text("등록된 리스트가 없습니다.", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)));
  } else {
    if (listType == "list") {
      return ListView.builder(
          shrinkWrap: true,
          itemCount: standardContainer.length,
          itemBuilder: (BuildContext context, int index) {
            return
              viewerState.boardModelMap[standardContainer.keys.elementAt(index)]!.isHidden && funcType != KEY_CUSTOMAPP_SETTINGS_MADE?SizedBox.shrink():
              Column(
              children: [
                BoardItem(
                    boardKey: standardContainer.keys.elementAt(index),
                    userMade: userMade,
                    funcType: funcType,
                    tabNum: tabNum),
                funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE?
                ListTile(
                  leading: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text('지원한 사람', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                  ),
                  trailing: Container(width: 30, height: 30, child: SvgPicture.asset('assets/profile/icons/profileArrow.svg')),
                  onTap: () => {
                    Navigator.of(context).push(
                      Transitions(
                        transitionType: TransitionType.slideLeft,
                        duration: Duration(milliseconds: 200),
                        curve: Curves.bounceInOut,
                        reverseCurve: Curves.fastOutSlowIn,
                        widget: ReceiveUserListView(boardKey: standardContainer.keys.elementAt(index), tabNum: tabNum, userMade: userMade),
                      ),
                    ),
                  },
                ): SizedBox.shrink(),
                selectGetFuncAllType(settingState, tabNum, funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION)
                    ? Divider(
                        color: Colors.black54.withOpacity(0.1),
                        thickness: 1,
                      ): SizedBox.shrink()
              ],
            );
          });
    } else if (listType == "image") {
      return ListView.builder(
          shrinkWrap: true,
          itemCount: standardContainer.length,
          itemBuilder: (BuildContext context, int index) {
            return
              viewerState.boardModelMap[standardContainer.keys.elementAt(index)]!.isHidden && funcType != KEY_CUSTOMAPP_SETTINGS_MADE?SizedBox.shrink():
              Column(
              children: [
                BoardImageItem(
                    boardKey: standardContainer.keys.elementAt(index),
                    userMade: userMade,
                    funcType: funcType,
                    tabNum: tabNum),
                funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE?
                ListTile(
                  leading: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text('지원한 사람', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                  ),
                  trailing: Container(width: 30, height: 30, child: SvgPicture.asset('assets/profile/icons/profileArrow.svg')),
                  onTap: () => {
                    Navigator.of(context).push(
                      Transitions(
                        transitionType: TransitionType.slideLeft,
                        duration: Duration(milliseconds: 200),
                        curve: Curves.bounceInOut,
                        reverseCurve: Curves.fastOutSlowIn,
                        widget: ReceiveUserListView(boardKey: standardContainer.keys.elementAt(index), tabNum: tabNum, userMade: userMade),
                      ),
                    ),
                  },
                ): SizedBox.shrink(),
                selectGetFuncAllType(settingState, tabNum, funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION)
                    ? Divider(
                        color: Colors.black54.withOpacity(0.1),
                        thickness: 1,
                      )
                    : SizedBox.shrink()
              ],
            );
            // return Text('결과가 없습니다.');
          });
    }else if (listType == "collection") {
      return GridView.count(
        crossAxisCount: 2,
        childAspectRatio: 0.5 / 0.65,
        mainAxisSpacing: 4.0,
        crossAxisSpacing: 4.0,
        children: List.generate(standardContainer.length, (index) {
          return viewerState.boardModelMap[standardContainer.keys.elementAt(index)]!.isHidden && funcType != KEY_CUSTOMAPP_SETTINGS_MADE?SizedBox.shrink():
          Column(
            children: [
              BoardGridItem(
                  boardKey: standardContainer.keys.elementAt(index),
                  userMade: userMade,
                  funcType: funcType,
                  tabNum: tabNum),
            ],
          );
          // return Text('결과가 없습니다.');
        }),
      );
    } else {}
  }
}

//category
selectCategoryList(String listType, SettingState settingState, standardContainer, int tabNum, String userMade) {
  if (standardContainer.length == 0) {
    return Center(child: Text("등록된 리스트가 없습니다.", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)));
  } else {
    if (listType == "list") {
      return ListView.builder(
          shrinkWrap: true,
          itemCount: standardContainer.length,
          itemBuilder: (BuildContext context, int index) {
            return CategoryListItem(index: index, boardKey: standardContainer.keys.elementAt(index), tabNum: tabNum, userMade: userMade);
            // return Text('결과가 없습니다.');
          });
    }
    else if(listType == "collection"){
      return Padding(
        padding: const EdgeInsets.only(top:10, left: 10, right: 10),
        child: GridView.count(
          crossAxisCount: 3,
          childAspectRatio: 1,
          children: List.generate(standardContainer.length, (index) {
            return Padding(
              padding: const EdgeInsets.only(top: 10, bottom: 20),
              child: CategoryGridItem(index: index, boardKey: standardContainer.keys.elementAt(index), tabNum: tabNum, userMade: userMade),
            );
          }),
        ),
      );
    }
    else{
    }
  }
}

//user
selectUserList(SettingState settingState, standardContainer, String userMade, String funcType, int tabNum) {
  if (standardContainer.length == 0) {
    return Center(child: Text("등록된 리스트가 없습니다.", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)));
  } else {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: standardContainer.length,
        itemBuilder: (BuildContext context, int index) {
          return
            Column(
              children: [
                ReceiveMessageItem(
                  boardKey: standardContainer.keys.elementAt(index),
                  valueMap: standardContainer.values.elementAt(index),
                  userMade: userMade,
                  funcType: funcType,
                  tabNum: tabNum),
                Divider(
                color: Colors.black54.withOpacity(0.1),
                thickness: 1),
                // selectGetFuncAllType(settingState, tabNum, funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) //변경해야함
                //     ? Divider(
                //   color: Colors.black54.withOpacity(0.1),
                //   thickness: 1,
                // ): SizedBox.shrink()
              ],
            );
        });
  }
}



selectTabFunc(
    tabCount,
    String tab1FucPage,
    String tab2FucPage,
    String tab3FucPage,
    String tab4FucPage,
    String tab5FucPage,
    String userMade,
    GlobalKey<NavigatorState> tab1NavigatorKey,
    GlobalKey<NavigatorState> tab2NavigatorKey,
    GlobalKey<NavigatorState> tab3NavigatorKey,
    GlobalKey<NavigatorState> tab4NavigatorKey,
    GlobalKey<NavigatorState> tab5NavigatorKey) {
  switch (tabCount) {
    case 2:
      {
        return [
          tab1FucPage == "Board"
              ? BoardTabListView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : tab1FucPage == "Favorite"
              ? FavoriteTabListView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : tab1FucPage == "Category"
              ? CategoryTabView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : tab1FucPage == "Profile"
              ? ProfileTabView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : tab1FucPage == "WebView"
              ? WebTabView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : WebTabView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade), //reservation
          tab2FucPage == "Board"
              ? BoardTabListView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : tab2FucPage == "Favorite"
              ? FavoriteTabListView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : tab2FucPage == "Category"
              ? CategoryTabView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : tab2FucPage == "Profile"
              ? ProfileTabView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : tab2FucPage == "WebView"
              ? WebTabView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : WebTabView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade),
        ];
      }
    case 3:
      {
        return [
          tab1FucPage == "Board"
              ? BoardTabListView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : tab1FucPage == "Favorite"
              ? FavoriteTabListView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : tab1FucPage == "Category"
              ? CategoryTabView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : tab1FucPage == "Profile"
              ? ProfileTabView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : tab1FucPage == "WebView"
              ? WebTabView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : WebTabView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade), //reservation
          tab2FucPage == "Board"
              ? BoardTabListView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : tab2FucPage == "Favorite"
              ? FavoriteTabListView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : tab2FucPage == "Category"
              ? CategoryTabView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : tab2FucPage == "Profile"
              ? ProfileTabView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : tab2FucPage == "WebView"
              ? WebTabView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : WebTabView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade),
          tab3FucPage == "Board"
              ? BoardTabListView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade)
              : tab3FucPage == "Favorite"
              ? FavoriteTabListView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade)
              : tab3FucPage == "Category"
              ? CategoryTabView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade)
              : tab3FucPage == "Profile"
              ? ProfileTabView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade)
              : tab3FucPage == "WebView"
              ? WebTabView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade)
              : WebTabView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade),
        ];
      }
    case 4:
      {
        return [
          tab1FucPage == "Board"
              ? BoardTabListView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : tab1FucPage == "Favorite"
              ? FavoriteTabListView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : tab1FucPage == "Category"
              ? CategoryTabView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : tab1FucPage == "Profile"
              ? ProfileTabView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : tab1FucPage == "WebView"
              ? WebTabView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : WebTabView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade), //reservation
          tab2FucPage == "Board"
              ? BoardTabListView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : tab2FucPage == "Favorite"
              ? FavoriteTabListView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : tab2FucPage == "Category"
              ? CategoryTabView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : tab2FucPage == "Profile"
              ? ProfileTabView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : tab2FucPage == "WebView"
              ? WebTabView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : WebTabView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade),
          tab3FucPage == "Board"
              ? BoardTabListView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade)
              : tab3FucPage == "Favorite"
              ? FavoriteTabListView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade)
              : tab3FucPage == "Category"
              ? CategoryTabView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade)
              : tab3FucPage == "Profile"
              ? ProfileTabView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade)
              : tab3FucPage == "WebView"
              ? WebTabView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade)
              : WebTabView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade),
          tab4FucPage == "Board"
              ? BoardTabListView(navigatorKey: tab4NavigatorKey, tabNum: 4, userMade: userMade)
              : tab4FucPage == "Favorite"
              ? FavoriteTabListView(navigatorKey: tab4NavigatorKey, tabNum: 4, userMade: userMade)
              : tab4FucPage == "Category"
              ? CategoryTabView(navigatorKey: tab4NavigatorKey, tabNum: 4, userMade: userMade)
              : tab4FucPage == "Profile"
              ? ProfileTabView(navigatorKey: tab4NavigatorKey, tabNum: 4, userMade: userMade)
              : tab4FucPage == "WebView"
              ? WebTabView(navigatorKey: tab4NavigatorKey, tabNum: 4, userMade: userMade)
              : WebTabView(navigatorKey: tab4NavigatorKey, tabNum: 4, userMade: userMade),
        ];
      }
    case 5:
      {
        return [
          tab1FucPage == "Board"
              ? BoardTabListView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : tab1FucPage == "Favorite"
              ? FavoriteTabListView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : tab1FucPage == "Category"
              ? CategoryTabView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : tab1FucPage == "Profile"
              ? ProfileTabView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : tab1FucPage == "WebView"
              ? WebTabView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade)
              : WebTabView(navigatorKey: tab1NavigatorKey, tabNum: 1, userMade: userMade), //reservation
          tab2FucPage == "Board"
              ? BoardTabListView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : tab2FucPage == "Favorite"
              ? FavoriteTabListView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : tab2FucPage == "Category"
              ? CategoryTabView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : tab2FucPage == "Profile"
              ? ProfileTabView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : tab2FucPage == "WebView"
              ? WebTabView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade)
              : WebTabView(navigatorKey: tab2NavigatorKey, tabNum: 2, userMade: userMade),
          tab3FucPage == "Board"
              ? BoardTabListView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade)
              : tab3FucPage == "Favorite"
              ? FavoriteTabListView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade)
              : tab3FucPage == "Category"
              ? CategoryTabView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade)
              : tab3FucPage == "Profile"
              ? ProfileTabView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade)
              : tab3FucPage == "WebView"
              ? WebTabView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade)
              : WebTabView(navigatorKey: tab3NavigatorKey, tabNum: 3, userMade: userMade),
          tab4FucPage == "Board"
              ? BoardTabListView(navigatorKey: tab4NavigatorKey, tabNum: 4, userMade: userMade)
              : tab4FucPage == "Favorite"
              ? FavoriteTabListView(navigatorKey: tab4NavigatorKey, tabNum: 4, userMade: userMade)
              : tab4FucPage == "Category"
              ? CategoryTabView(navigatorKey: tab4NavigatorKey, tabNum: 4, userMade: userMade)
              : tab4FucPage == "Profile"
              ? ProfileTabView(navigatorKey: tab4NavigatorKey, tabNum: 4, userMade: userMade)
              : tab4FucPage == "WebView"
              ? WebTabView(navigatorKey: tab4NavigatorKey, tabNum: 4, userMade: userMade)
              : WebTabView(navigatorKey: tab4NavigatorKey, tabNum: 4, userMade: userMade),
          tab5FucPage == "Board"
              ? BoardTabListView(navigatorKey: tab5NavigatorKey, tabNum: 5, userMade: userMade)
              : tab5FucPage == "Favorite"
              ? FavoriteTabListView(navigatorKey: tab5NavigatorKey, tabNum: 5, userMade: userMade)
              : tab5FucPage == "Category"
              ? CategoryTabView(navigatorKey: tab5NavigatorKey, tabNum: 5, userMade: userMade)
              : tab5FucPage == "Profile"
              ? ProfileTabView(navigatorKey: tab5NavigatorKey, tabNum: 5, userMade: userMade)
              : tab5FucPage == "WebView"
              ? WebTabView(navigatorKey: tab5NavigatorKey, tabNum: 5, userMade: userMade)
              : WebTabView(navigatorKey: tab5NavigatorKey, tabNum: 5, userMade: userMade),
        ];
      }
  }
}
