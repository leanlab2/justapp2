import 'dart:core';
import 'dart:io';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/firestore/app_info.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingView extends StatefulWidget {
  final tabNum;

  SettingView({required this.tabNum});

  @override
  _SettingViewState createState() => _SettingViewState();
}

class _SettingViewState extends State<SettingView> {
  bool _noti = true;

  @override
  void initState() {
    super.initState();
    _getSetting();

    if(Constants.isMoblie){
      getUpdateInfo();
    }
  }

  Future<void> _getSetting() async {
    // final SharedPreferences prefs = await _prefs;
    // _noti = prefs.getBool(BebeNotification.notiKey);

    setState(() {
      if (_noti == null) {
        _noti = true;
      }
    });
  }

  bool isUpdateAvailable = false;

  void getUpdateInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    AppInfo appInfo = Provider.of<ViewerState>(context, listen: false).appInfo;
    if (Platform.isAndroid) {
      if (int.parse(packageInfo.buildNumber) < appInfo.versionAndroid) {
        isUpdateAvailable = true;
        setState(() {});
      }
    } else if (Platform.isIOS) {
      if (int.parse(packageInfo.buildNumber) < appInfo.versionIos) {
        isUpdateAvailable = true;
        setState(() {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<SettingState>(builder: (BuildContext context, SettingState settingState, child) {
      return Scaffold(
          backgroundColor: selectTabBgColor(widget.tabNum, settingState),
          appBar: AppBar(
            title: Text(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_TITLE),
                style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
            centerTitle: true,
            leading: BackButton(
                color: Colors.black
            ),
            backgroundColor: Colors.white,
          ),
          body: Padding(
            padding: const EdgeInsets.only(top:10.0),
            child: ListView(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    children: [
                      //알림 설정
                      selectGetFuncAllType(
                              settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_ISACTIVE)
                          ? InkWell(
                              focusColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: ListTile(
                                contentPadding: EdgeInsets.only(left: 0, right: 0),
                                title: FittedBox(
                                  fit: BoxFit.scaleDown,
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE,
                                        KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_TITLE),
                                    style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                        fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                  ),
                                ),
                                trailing: CupertinoSwitch(
                                  value: _noti,
                                  activeColor: Color(settingState.entireModel.defaultColor),
                                  onChanged: (bool value) {
                                    setState(() {
                                      _noti = value;
                                      // _setSetting(BebeNotification.notiKey, value);
                                      // BebeNotification.dailyAtTimeNotification(context);
                                    });
                                  },
                                ),
                              ),
                              onTap: () {},
                            )
                          : SizedBox.shrink(),
                      // selectGetFuncAllType(
                      //         settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_ISACTIVE)
                      //     ? Divider(
                      //         color: Colors.black.withOpacity(0.1),
                      //         thickness: 1,
                      //       )
                      //     : SizedBox.shrink(),

                      //현재 버전
                      InkWell(
                        focusColor: Colors.transparent,
                        hoverColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        child: ListTile(
                          contentPadding: EdgeInsets.only(left: 0, right: 0),
                          title: FittedBox(
                            fit: BoxFit.scaleDown,
                            alignment: Alignment.centerLeft,
                            child: Text(
                              '현재 버전',
                              style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                  fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                            ),
                          ),
                          trailing: isUpdateAvailable
                              ? FlatButton(
                                  minWidth: 86,
                                  color: Color(settingState.entireModel.defaultColor),
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                                  onPressed: () {
                                    Constants.openWithStore();
                                  },
                                  child: Text(
                                    '업데이트',
                                    style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                        fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                    textAlign: TextAlign.center,
                                  ),
                                )
                              : Text(
                                  '최신버전',
                                  style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                    fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                  textAlign: TextAlign.center,
                                ),
                        ),
                        onTap: () {},
                      ),
                      // Divider(
                      //   color: Colors.black.withOpacity(0.1),
                      //   thickness: 1,
                      // ),

                      //서비스 약관
                      selectGetFuncAllType(
                              settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_ISACTIVE)
                          ? InkWell(
                              focusColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: ListTile(
                                contentPadding: EdgeInsets.only(left: 0, right: 0),
                                title: FittedBox(
                                  fit: BoxFit.scaleDown,
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE,
                                        KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_TITLE),
                                    style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                        fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                  ),
                                ),
                                trailing: Container(width: 30, height: 30, child: SvgPicture.asset('assets/profile/icons/profileArrow.svg',color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)))),
                              ),
                              onTap: () {
                                Constants.launchInWebViewOrVC(selectGetFuncAllType(
                                    settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_URL));
                              },
                            )
                          : SizedBox.shrink(),
                      // selectGetFuncAllType(
                      //         settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_ISACTIVE)
                      //     ? Divider(
                      //         color: Colors.black.withOpacity(0.1),
                      //         thickness: 1,
                      //       )
                      //     : SizedBox.shrink(),

                      //개인정보처리방침
                      selectGetFuncAllType(
                              settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_ISACTIVE)
                          ? InkWell(
                              focusColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: ListTile(
                                contentPadding: EdgeInsets.only(left: 0, right: 0),
                                title: FittedBox(
                                  fit: BoxFit.scaleDown,
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE,
                                        KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_TITLE),
                                    style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                        fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                  ),
                                ),
                                trailing: Container(width: 30, height: 30, child: SvgPicture.asset('assets/profile/icons/profileArrow.svg',color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)))),
                              ),
                              onTap: () {
                                Constants.launchInWebViewOrVC(selectGetFuncAllType(
                                    settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_URL));
                              },
                            )
                          : SizedBox.shrink(),
                      // selectGetFuncAllType(
                      //         settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_ISACTIVE)
                      //     ? Divider(
                      //         color: Colors.black.withOpacity(0.1),
                      //         thickness: 1,
                      //       )
                      //     : SizedBox.shrink(),

                      //라이센스
                      selectGetFuncAllType(
                              settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_ISACTIVE)
                          ? InkWell(
                              focusColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: ListTile(
                                contentPadding: EdgeInsets.only(left: 0, right: 0),
                                title: FittedBox(
                                  fit: BoxFit.scaleDown,
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE,
                                        KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_TITLE),
                                    style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                        fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                  ),
                                ),
                                trailing: Container(width: 30, height: 30, child: SvgPicture.asset('assets/profile/icons/profileArrow.svg',color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)))),
                              ),
                              onTap: () {
                                Constants.launchInWebViewOrVC(selectGetFuncAllType(
                                    settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_URL));
                              },
                            )
                          : SizedBox.shrink(),
                      // selectGetFuncAllType(
                      //         settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_ISACTIVE)
                      //     ? Divider(
                      //         color: Colors.black.withOpacity(0.1),
                      //         thickness: 1,
                      //       )
                      //     : SizedBox.shrink(),
                    ],
                  ),
                ),


                selectGetFuncAllType(
                        settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ISACTIVE)
                    ? Padding(
                        padding: EdgeInsets.all(30),
                        child: Row(
                          mainAxisAlignment: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE,
                                      KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ALIGN) ==
                                  'left'
                              ? MainAxisAlignment.start
                              : selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE,
                                          KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ALIGN) ==
                                      'center'
                                  ? MainAxisAlignment.center
                                  : MainAxisAlignment.end,
                          children: [
                            InkWell(
                              child: Text(
                                '로그아웃',
                                style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                        fontSize: 14),
                              ),
                              onTap: () {
                                // Provider.of<FirebaseAuthState>(context, listen: false).signOut();
                              },
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 30),
                              child: Container(
                                width: 1,
                                height: 14,
                                color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                              ),
                            ),
                            InkWell(
                              child: Text(
                                '계정삭제',
                                style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                    fontSize: 14),
                              ),
                              onTap: () async{
                                final result = await showOkCancelAlertDialog(
                                  context: context,
                                  title: '탈퇴를 하시겠습니까?',
                                  message: '정말 계정을 삭제하시겠습니까?\n해당 기능은 되돌릴 수 없습니다.',
                                  okLabel: '확인',
                                  cancelLabel: "취소",
                                  defaultType: OkCancelAlertDefaultType.cancel,
                                );
                                if(result == OkCancelResult.ok){
                                  // FirebaseAuth.instance.currentUser!.delete().catchError((error) {
                                  //   Navigator.pop(context);
                                  //   simpleLongLongSnackBar(context, '삭제를 위해 다시 로그인해주세요.');
                                  // });

                                }
                              },
                            ),
                          ],
                        ))
                    : SizedBox.shrink(),
              ],
            ),
          ));
    });
  }


}
