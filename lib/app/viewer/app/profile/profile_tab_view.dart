import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/others/other_list_view.dart';
import 'package:justapp/app/viewer/app/profile/my_profile_view.dart';
import 'package:justapp/app/viewer/app/profile/setting_view.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:provider/provider.dart';
import 'package:rate_my_app/rate_my_app.dart';
import 'package:share/share.dart';

class ProfileTabView extends StatefulWidget {
  late final GlobalKey<NavigatorState> navigatorKey;
  late final tabNum;
  late final userMade;

  ProfileTabView({required this.navigatorKey, required this.tabNum, required this.userMade});

  @override
  _ProfileTabViewState createState() => _ProfileTabViewState();
}

    class _ProfileTabViewState extends State<ProfileTabView> {
    @override
    Widget build(BuildContext context) {
    return Navigator(
    key: widget.tabNum == 1 ? null : widget.navigatorKey,
    onGenerateRoute: (routeSettings) {
    return MaterialPageRoute(
    builder: (context) => Consumer2<ViewerState, SettingState>(builder: (BuildContext context, ViewerState viewerState, SettingState settingState, child) {
                    return Scaffold(
                      appBar: AppBar(
                        title: Text('설정', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
                        centerTitle: true,
                        backgroundColor: Colors.white,
                      ),
                      backgroundColor: selectTabBgColor(widget.tabNum, settingState),
                      resizeToAvoidBottomInset: true,
                      body: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: ListView(
                          children: <Widget>[
                            //내 정보
                            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ISACTIVE)?
                            InkWell(
                              focusColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: ListTile(
                                contentPadding: EdgeInsets.only(left: 0, right: 0),
                                leading: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ICON) ==0?null:
                                  Container(
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.centerLeft,
                                    child: Icon(IconData(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ICON),
                                        fontFamily: 'MaterialIcons'), color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),),
                                  ),
                                title: Text(
                                  selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_TITLE),
                                  style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                      fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                ),
                                // trailing: Container(width: 30, height: 30, child: SvgPicture.asset('assets/profile/icons/profileArrow.svg')),
                              ),
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                  return MyProfileView(tabNum: widget.tabNum, userMade: widget.userMade);
                                }));
                              },
                            ):SizedBox.shrink(),
                            // selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ISACTIVE)?
                            // Divider(
                            //   color: Colors.black.withOpacity(0.1),
                            //   thickness: 1,
                            // ):SizedBox.shrink(),

                            //연락 받은거
                            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ISACTIVE)?
                            viewerState.userModel.userPosition == 'producer'
                                ? InkWell(
                                    focusColor: Colors.transparent,
                                    hoverColor: Colors.transparent,
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    child: ListTile(
                                      contentPadding: EdgeInsets.only(left: 0, right: 0),
                                      leading: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ICON) ==0?null:
                                        Container(
                                          width: 30,
                                          height: 30,
                                          alignment: Alignment.centerLeft,
                                        child: Icon(IconData(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ICON),
                                            fontFamily: 'MaterialIcons'), color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),),
                                      ),
                                      title: Text(
                                        selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_RECEIVE, KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE),
                                        style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                            fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                      ),
                                      // trailing: Container(width: 30, height: 30, child: SvgPicture.asset('assets/profile/icons/profileArrow.svg')),
                                    ),
                                    onTap: () {
                                      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                        return OtherListView(tabNum: widget.tabNum, userMade: widget.userMade, funcType: KEY_CUSTOMAPP_SETTINGS_RECEIVE);
                                      }));
                                    },
                                  )
                                : SizedBox.shrink():SizedBox.shrink(),
                                // selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ISACTIVE)?
                                // viewerState.userModel.userPosition == 'producer'
                                // ? Divider(
                                //     color: Colors.black.withOpacity(0.1),
                                //     thickness: 1,
                                //   )
                                // : SizedBox.shrink():SizedBox.shrink(),


                            //내가 만든거
                            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ISACTIVE)?
                            viewerState.userModel.userPosition == 'producer'
                                ? InkWell(
                              focusColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: ListTile(
                                contentPadding: EdgeInsets.only(left: 0, right: 0),
                                leading: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ICON) ==0?null:
                                Container(
                                  width: 30,
                                  height: 30,
                                  alignment: Alignment.centerLeft,
                                  child: Icon(IconData(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ICON),
                                      fontFamily: 'MaterialIcons'), color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),),
                                ),
                                title: Text(
                                  selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_MADE, KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE),
                                  style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                      fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                ),
                                // trailing: Container(width: 30, height: 30, child: SvgPicture.asset('assets/profile/icons/profileArrow.svg')),
                              ),
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                  return OtherListView(tabNum: widget.tabNum, userMade: widget.userMade, funcType: KEY_CUSTOMAPP_SETTINGS_MADE);
                                }));
                              },
                            )
                                : SizedBox.shrink():SizedBox.shrink(),
                            // selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ISACTIVE)?
                            // viewerState.userModel.userPosition == 'producer'
                            //     ? Divider(
                            //         color: Colors.black.withOpacity(0.1),
                            //         thickness: 1,
                            //       )
                            //     : SizedBox.shrink():SizedBox.shrink(),

                            //연락 보낸거
                            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ISACTIVE)?
                            InkWell(
                              focusColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: ListTile(
                                contentPadding: EdgeInsets.only(left: 0, right: 0),
                                leading: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ICON) ==0?null:
                                  Container(
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.centerLeft,
                                  child: Icon(IconData(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ICON),
                                      fontFamily: 'MaterialIcons'), color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),),
                                ),
                                title: Text(
                                  selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_SEND, KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE),
                                  style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                      fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                ),
                                // trailing: Container(width: 30, height: 30, child: SvgPicture.asset('assets/profile/icons/profileArrow.svg')),
                              ),
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                  return OtherListView(tabNum: widget.tabNum, userMade: widget.userMade, funcType: KEY_CUSTOMAPP_SETTINGS_SEND);
                                }));
                              },
                            ):SizedBox.shrink(),
                            // selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ISACTIVE)?
                            // Divider(
                            //   color: Colors.black.withOpacity(0.1),
                            //   thickness: 1,
                            // ):SizedBox.shrink(),

                            //관심목록
                            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ISACTIVE)?
                            InkWell(
                              focusColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: ListTile(
                                contentPadding: EdgeInsets.only(left: 0, right: 0),
                                leading: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ICON) ==0?null:
                                Container(
                                  width: 30,
                                  height: 30,
                                  alignment: Alignment.centerLeft,
                                  // child: Icon(IconData(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ICON),

                                  child: Icon(IconData(61515,
                                      fontFamily: 'MaterialIcons'), color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),),
                                ),
                                title: Text(
                                  "관심목록",
                                  style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                      fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                ),
                                // trailing: Container(width: 30, height: 30, child: SvgPicture.asset('assets/profile/icons/profileArrow.svg')),
                              ),
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                  return OtherListView(tabNum: widget.tabNum, userMade: widget.userMade, funcType: KEY_CUSTOMAPP_SETTINGS_FAVORITE);
                                }));
                              },
                            ):SizedBox.shrink(),
                            // selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ISACTIVE)?
                            // Divider(
                            //   color: Colors.black.withOpacity(0.1),
                            //   thickness: 1,
                            // ):SizedBox.shrink(),



                            //앱 평가하기
                            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ISACTIVE)?
                            InkWell(
                              focusColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: ListTile(
                                contentPadding: EdgeInsets.only(left: 0, right: 0),
                                leading: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ICON) ==0?null:
                                  Container(
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.centerLeft,
                                    child: Icon(IconData(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ICON),
                                        fontFamily: 'MaterialIcons'), color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),),
                                ),
                                title: FittedBox(
                                  fit: BoxFit.scaleDown,
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_TITLE),
                                    style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                        fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                  ),
                                ),
                              ),
                              onTap: () async {
                                RateMyApp rateMyApp = RateMyApp(
                                  preferencesPrefix: 'rateMyApp_',
                                  googlePlayIdentifier: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_GOOGLEPLAYID),
                                  appStoreIdentifier: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_APPSTOREID),
                                );

                                await rateMyApp.launchStore();
                              },
                            ):SizedBox.shrink(),
                            // selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ISACTIVE)?
                            // Divider(
                            //   color: Colors.black.withOpacity(0.1),
                            //   thickness: 1,
                            // ):SizedBox.shrink(),

                            //앱 공유하기
                            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ISACTIVE)?
                            InkWell(
                              focusColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: ListTile(
                                contentPadding: EdgeInsets.only(left: 0, right: 0),
                                leading: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ICON) ==0?null:
                                  Container(
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.centerLeft,
                                    child: Icon(IconData(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ICON),
                                        fontFamily: 'MaterialIcons'), color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),),
                                ),
                                title: FittedBox(
                                  fit: BoxFit.scaleDown,
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_TITLE),
                                    style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                        fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                    textAlign: TextAlign.justify,
                                  ),
                                ),
                              ),
                              onTap: () async {
                                Share.share(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_MSG));
                              },
                            ):SizedBox.shrink(),
                            // selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ISACTIVE)?
                            // Divider(
                            //   color: Colors.black.withOpacity(0.1),
                            //   thickness: 1,
                            // ):SizedBox.shrink(),

                            //버그신고 및 문의사항
                            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ISACTIVE)?
                            InkWell(
                              focusColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: ListTile(
                                contentPadding: EdgeInsets.only(left: 0, right: 0),
                                leading: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ICON) ==0?null:
                                Container(
                                  width: 30,
                                  height: 30,
                                  alignment: Alignment.centerLeft,
                                  child: Icon(IconData(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ICON),
                                      fontFamily: 'MaterialIcons'), color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),),
                                ),
                                title: FittedBox(
                                  fit: BoxFit.scaleDown,
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_TITLE),
                                    style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                        fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                  ),
                                ),
                              ),
                              onTap: () async {
                                await Constants.launchEmail(context, "magnate0@naver.com", 'support');
                              },
                            ):SizedBox.shrink(),
                            // selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ISACTIVE)?
                            // Divider(
                            //   color: Colors.black.withOpacity(0.1),
                            //   thickness: 1,
                            // ):SizedBox.shrink(),

                            //설정
                            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ISACTIVE)?
                            InkWell(
                              focusColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: ListTile(
                                contentPadding: EdgeInsets.only(left: 0, right: 0),
                                leading: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ICON) ==0?null:
                                  Container(
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.centerLeft,
                                    child: Icon(IconData(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ICON),
                                        fontFamily: 'MaterialIcons'), color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),),
                                ),
                                title: FittedBox(
                                  fit: BoxFit.scaleDown,
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_TITLE),
                                    style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                        fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                  ),
                                ),
                                // trailing: Container(width: 30, height: 30, child: SvgPicture.asset('assets/profile/icons/profileArrow.svg')),
                              ),
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                  return SettingView(tabNum: widget.tabNum);
                                }));
                              },
                            ):SizedBox.shrink(),
                            // selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ISACTIVE)?
                            // Divider(
                            //   color: Colors.black.withOpacity(0.1),
                            //   thickness: 1,
                            // ):SizedBox.shrink(),
                          ],
                        ),
                      ),
                    );
                  }));
        });
  }

// Future<void> _launchInWebViewOrVC(String url) async {
//   if (await canLaunch(url)) {
//     await launch(
//       url,
//       forceSafariVC: true,
//       forceWebView: true,
//       enableJavaScript: true,
//       headers: <String, String>{'my_header_key': 'my_header_value'},
//     );
//   } else {
//     throw 'Could not launch $url';
//   }
// }
}
