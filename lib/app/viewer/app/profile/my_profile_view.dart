import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/repo/user_network_repository.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:justapp/utils/simple_snackbar.dart';
import 'package:provider/provider.dart';

class MyProfileView extends StatefulWidget {
  final tabNum;
  final userMade;

  MyProfileView({required this.tabNum, required this.userMade});

  @override
  _MyProfileViewState createState() => _MyProfileViewState();
}

class _MyProfileViewState extends State<MyProfileView> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _phoneController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();

  late bool positionParam;
  String _position = '';
  int _phoneNumb = 010;
  late String userKey;

  @override
  void initState() {
    super.initState();
    userKey = Provider.of<ViewerState>(context, listen: false).userModel.userKey;
    _nameController.text = Provider.of<ViewerState>(context, listen: false).userModel.userName;
    _position = Provider.of<ViewerState>(context, listen: false).userModel.userPosition;
    _phoneController.text = Provider.of<ViewerState>(context, listen: false).userModel.userPhoneNum.toString();
    _emailController.text = Provider.of<ViewerState>(context, listen: false).userModel.userEmail;
    // _position == 'producer'? positionParam = false: positionParam = true;

  }

  @override
  Widget build(BuildContext context) {
    return Consumer<SettingState>(builder: (BuildContext context, SettingState settingState, child) {
    return Scaffold(
      backgroundColor: selectTabBgColor(widget.tabNum, settingState),
      appBar: AppBar(
        title: Text(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_TITLE),
            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
        centerTitle: true,
        leading: BackButton(
            color: Colors.black
        ),
        backgroundColor: Colors.white,
      ),
      resizeToAvoidBottomInset: false,
      body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Container(
                // height: Constants.sizeHeight - Constants.tabHeight,
                child: Padding(
                  padding: EdgeInsets.only(top:20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_ISACTIVE)?
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 25),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_TITLE),
                              style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                  fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                            ),
                            Container(
                              child: TextFormField(
                                controller: _nameController,
                                onChanged: (text) {
                                  setState(() {});
                                },
                                textAlignVertical: TextAlignVertical.bottom,
                                style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                    fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                cursorColor: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                textAlign: TextAlign.left,
                                keyboardType: TextInputType.text,
                                decoration: _nameController.text==""?null:InputDecoration(
                                  suffixIcon: IconButton(
                                    padding: EdgeInsets.only(top: 10,left:10),
                                    onPressed: _nameController.clear,
                                    icon: Icon(Icons.clear,size:18),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ):SizedBox.shrink(),
                      // selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_ISACTIVE)?
                      // Padding(
                      //   padding: EdgeInsets.only(left: 25, right: 25, bottom: 5),
                      //   child: Divider(
                      //     color: Colors.black.withOpacity(0.1),
                      //     thickness: 1,
                      //   ),
                      // ):SizedBox.shrink(),
                      SizedBox(height:20),



                      selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_ISACTIVE)?
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 25),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "휴대폰 번호",
                              style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                  fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                            ),
                            Container(
                              child: TextFormField(
                                readOnly: true,
                                controller: _phoneController,
                                onChanged: (text) {
                                  setState(() {});
                                },
                                textAlignVertical: TextAlignVertical.bottom,
                                style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                    fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                cursorColor: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                textAlign: TextAlign.left,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  suffix: Container(
                                    // padding: EdgeInsets.only(top: 10,left:10),
                                    width: 55,
                                    height:25,
                                    child: FlatButton(
                                      child: Text('인증', style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold, color: Colors.white)),
                                      onPressed: () async {
                                        simpleSnackBar(context, '이미 인증한 번호 입니다.');
                                      },
                                      color: Colors.black26,
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ):SizedBox.shrink(),
                      SizedBox(height:20),


                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 25),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '이메일',
                              style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                  fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                            ),
                            Container(
                              child: TextFormField(
                                controller: _emailController,
                                onChanged: (text) {
                                  setState(() {});
                                },
                                textAlignVertical: TextAlignVertical.bottom,
                                style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                    fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                                cursorColor: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                                textAlign: TextAlign.left,
                                keyboardType: TextInputType.text,
                                decoration: _emailController.text==""?null:InputDecoration(
                                  suffixIcon: IconButton(
                                    padding: EdgeInsets.only(top: 10,left:10),
                                    onPressed: _emailController.clear,
                                    icon: Icon(Icons.clear,size:18),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height:20),




                      // selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_ISACTIVE)?
                      // Container(
                      //   padding: EdgeInsets.symmetric(horizontal: 25),
                      //   child: Column(
                      //     crossAxisAlignment: CrossAxisAlignment.start,
                      //     children: [
                      //       Text(
                      //         selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_TITLE),
                      //         style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                      //             fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble()),
                      //       ),
                      //       InkWell(
                      //         focusColor: Colors.transparent,
                      //         hoverColor: Colors.transparent,
                      //         splashColor: Colors.transparent,
                      //         highlightColor: Colors.transparent,
                      //         child: Container(
                      //           width: double.infinity,
                      //           child: DropdownButton(
                      //               underline: Container(
                      //                 height: 1.0,
                      //                 decoration: const BoxDecoration(
                      //                     border: Border(bottom: BorderSide(color: Colors.black38, width: 1)
                      //                 ),
                      //               ),
                      //               ),
                      //               isExpanded: true,
                      //           value: _position,
                      //           items:Constants.valuePositionType.map(
                      //                 (value) {
                      //               return DropdownMenuItem(
                      //                 value: value,
                      //                 child: Text(value.toString(),style: TextStyle(color: Color(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR)),
                      //                     fontSize: selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE).toDouble())),
                      //               );
                      //             },
                      //           ).toList(),
                      //           onChanged: (value) {
                      //             setState(() {
                      //               _position = value.toString();
                      //             });
                      //           }),
                      //         ),
                      //         onTap: () {
                      //           FocusScope.of(context).requestFocus(new FocusNode());
                      //         },
                      //       )
                      //     ],
                      //   ),
                      // ):SizedBox.shrink(),
                      // selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_ISACTIVE)?
                      // Padding(
                      //   padding: EdgeInsets.symmetric(horizontal: 25, vertical: 5),
                      //   child: Divider(
                      //     color: Colors.black.withOpacity(0.1),
                      //     thickness: 1,
                      //   ),
                      // ):SizedBox.shrink(),


                      Container(
                        width: Constants.sizeWidth>400? 400: Constants.sizeWidth * 0.9,
                        padding: const EdgeInsets.only(left:30, right:30),
                        child: FlatButton(
                          minWidth: double.infinity,
                          child: Text(
                            '변경 완료',
                            style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white),
                          ),
                          onPressed: () async {
                            // if (_nameController.text.isEmpty) {
                            //   simpleSnackBar(context, '이름을 입력해 주세요');
                            //   return;
                            // }
                            // if(!_emailController.text.contains('@')){
                            //   simpleSnackBar(context, '올바른 이메일 주소를 입력해주세요.');
                            //   return;
                            // }
                            // else{
                              userNetworkRepository.updateUserProfile(widget.userMade, userKey,_nameController.text,_position).then((value) => {
                                simpleSnackBar(context, '저장되었습니다.')
                              });
                            // }
                          },
                          color: _nameController.text.isEmpty != true ? Color(settingState.entireModel.defaultColor) : Colors.black26,
                          padding: EdgeInsets.symmetric(vertical: 22),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                        ),
                      ),

                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
      });
  }
}
