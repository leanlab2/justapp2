import 'package:flutter/material.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/category/category_list_view.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:justapp/utils/transitions.dart';
import 'package:provider/provider.dart';

class CategoryGridItem extends StatefulWidget {
  final int index;
  final String boardKey;
  final String userMade;
  final int tabNum;

  CategoryGridItem({required this.index, required this.boardKey, required this.userMade, required this.tabNum});

  @override
  _CategoryGridItemState createState() => _CategoryGridItemState();
}

class _CategoryGridItemState extends State<CategoryGridItem> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    int categoryNum = widget.index + 1;
    return Consumer2<ViewerState, SettingState>(builder: (BuildContext context, ViewerState viewerState, SettingState settingState, child) {
      return FlatButton(
          onPressed: () {
            widget.tabNum == 0?
            Navigator.of(context).pop(selectTabCategory(categoryNum, settingState)[KEY_CUSTOMAPP_SETTINGS_CATEGORY_NAME]):
            Navigator.of(context).push(
              Transitions(
                  transitionType: TransitionType.slideLeft,
                  duration: Duration(milliseconds: 200),
                  curve: Curves.bounceInOut,
                  reverseCurve: Curves.fastOutSlowIn,
                  widget: CategoryListView(userMade: widget.userMade, boardKey: 'category_' + categoryNum.toString(), tabNum: widget.tabNum)
              ),
            );
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(IconData(selectTabCategory(categoryNum, settingState)[KEY_CUSTOMAPP_SETTINGS_CATEGORY_ICON], fontFamily: 'MaterialIcons'), size:60),
              SizedBox(height: 5),
              Text(selectTabCategory(categoryNum, settingState)[KEY_CUSTOMAPP_SETTINGS_CATEGORY_NAME],
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black)),
            ],
          )
      );
    });
  }
}
