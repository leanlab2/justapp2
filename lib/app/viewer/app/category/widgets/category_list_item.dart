import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/category/category_list_view.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:justapp/utils/transitions.dart';
import 'package:provider/provider.dart';

class CategoryListItem extends StatefulWidget {
  final int index;
  final String boardKey;
  final int tabNum;
  final String userMade;

  CategoryListItem({required this.index, required this.boardKey, required this.tabNum, required this.userMade});

  @override
  _CategoryListItemState createState() => _CategoryListItemState();
}

class _CategoryListItemState extends State<CategoryListItem> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    int categoryNum = widget.index + 1;
    return Consumer2<ViewerState, SettingState>(builder: (BuildContext context, ViewerState viewerState, SettingState settingState, child) {
      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left:15.0,right:15.0),
            child: ListTile(
              title: Text(selectTabCategory(categoryNum, settingState)[KEY_CUSTOMAPP_SETTINGS_CATEGORY_NAME],
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black)),
              trailing: widget.tabNum == 0?null:Container(width: 30, height: 30, child: SvgPicture.asset('assets/profile/icons/profileArrow.svg')),
              onTap: () =>{
                widget.tabNum == 0?
                Navigator.of(context).pop(selectTabCategory(categoryNum, settingState)[KEY_CUSTOMAPP_SETTINGS_CATEGORY_NAME]):
                Navigator.of(context).push(
                  Transitions(
                    transitionType: TransitionType.slideLeft,
                    duration: Duration(milliseconds: 200),
                    curve: Curves.bounceInOut,
                    reverseCurve: Curves.fastOutSlowIn,
                    widget: CategoryListView(userMade: widget.userMade,  boardKey: 'category_' + categoryNum.toString() , tabNum: widget.tabNum)
                  ),
                ),
              },
            ),
          ),
          Divider(
            color: Colors.black54.withOpacity(0.1),
            thickness: 1,
          )
        ],
      );
    });
  }
}
