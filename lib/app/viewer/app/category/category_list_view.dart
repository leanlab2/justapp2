import 'package:flutter/material.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:provider/provider.dart';

class CategoryListView extends StatefulWidget {
  final userMade;
  final boardKey;
  final tabNum;

  CategoryListView({required this.userMade,required this.boardKey,required this.tabNum});

  @override
  _CategoryListViewState createState() => _CategoryListViewState();
}

class _CategoryListViewState extends State<CategoryListView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Consumer2<SettingState, ViewerState>(builder: (BuildContext context, SettingState settingState, ViewerState viewerState, child) {
      return Scaffold(
          backgroundColor: selectTabBgColor(widget.tabNum, settingState),
          appBar: AppBar(
            title: Text('카테고리', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
            centerTitle: true,
            leading: BackButton(color: Colors.black, onPressed: () =>
            {
              Navigator.of(context).pop()
            }),
            backgroundColor: Colors.white,
          ),
          body:
          Column(children: [
            Expanded(
              child: widget.tabNum == 1
                  ? selectBoardList(settingState.tab1CategoryModel.listType, settingState, viewerState, viewerState.categoryModelMap[widget.boardKey],
                      widget.userMade, KEY_CUSTOMAPP_SETTINGS_CATEGORY, widget.tabNum)
                  : widget.tabNum == 2
                      ? selectBoardList(settingState.tab2CategoryModel.listType, settingState,viewerState,
                          viewerState.categoryModelMap[widget.boardKey], widget.userMade, KEY_CUSTOMAPP_SETTINGS_CATEGORY, widget.tabNum)
                      : widget.tabNum == 3
                          ? selectBoardList(
                              settingState.tab3CategoryModel.listType,
                              settingState,viewerState,
                              viewerState.categoryModelMap[widget.boardKey],
                              widget.userMade,
                              KEY_CUSTOMAPP_SETTINGS_CATEGORY,
                              widget.tabNum)
                          : widget.tabNum == 4
                              ? selectBoardList(
                                  settingState.tab4CategoryModel.listType,
                                  settingState,viewerState,
                                  viewerState.categoryModelMap[widget.boardKey],
                                  widget.userMade,
                                  KEY_CUSTOMAPP_SETTINGS_CATEGORY,
                                  widget.tabNum)
                              : selectBoardList(
                                  settingState.tab5CategoryModel.listType,
                                  settingState,viewerState,
                                  viewerState.categoryModelMap[widget.boardKey],
                                  widget.userMade,
                                  KEY_CUSTOMAPP_SETTINGS_CATEGORY,
                                  widget.tabNum),
            )
          ])
      );
    });
  }
}
