import 'package:flutter/material.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/others/notification_list_view.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:justapp/utils/transitions.dart';
import 'package:provider/provider.dart';

class CategoryTabView extends StatefulWidget {
  final GlobalKey<NavigatorState> navigatorKey;
  final tabNum;
  final userMade;

  CategoryTabView({required this.navigatorKey, required this.tabNum,required this.userMade});

  @override
  _CategoryTabViewState createState() => _CategoryTabViewState();
}

class _CategoryTabViewState extends State<CategoryTabView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
        key: widget.tabNum == 1 ? null : widget.navigatorKey,
        onGenerateRoute: (routeSettings) {
          return MaterialPageRoute(
            builder: (context) =>
                Consumer2<SettingState, ViewerState>(builder: (BuildContext context, SettingState settingState, ViewerState viewerState, child) {
                  return Scaffold(
                      appBar: AppBar(
                        title: Text('카테고리', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
                        centerTitle: true,
                        backgroundColor: Colors.white,
                        actions:[
                          IconButton(icon: Icon(Icons.notifications_none_outlined,color:Colors.black),
                              onPressed: () =>{
                                Navigator.of(context).push(
                                  Transitions(
                                    transitionType: TransitionType.slideLeft,
                                    duration: Duration(milliseconds: 200),
                                    curve: Curves.bounceInOut,
                                    reverseCurve: Curves.fastOutSlowIn,
                                    widget: NotificationListView(),
                                  ),
                                ),
                              }),
                        ],
                      ),
                      backgroundColor: selectTabBgColor(widget.tabNum, settingState),
                      body: Padding(
                        padding: EdgeInsets.only(top:10.0),
                        child: Column(children: [
                          Expanded(
                            child: selectCategoryList('collection', settingState, viewerState.categoryModelMap, widget.tabNum, widget.userMade),
                          )
                        ]),
                      ));
                }),
          );
        });
  }
}
