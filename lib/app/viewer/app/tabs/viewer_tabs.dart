import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:justapp/utils/init_model.dart';
import 'package:provider/provider.dart';

class ViewerTabs extends StatefulWidget {
  final String userMade;
  ViewerTabs({required this.userMade});

  @override
  ViewerTabsState createState() => ViewerTabsState();
}

class ViewerTabsState extends State<ViewerTabs> with TickerProviderStateMixin {
  static late PageController pageController;
  // int _page = 0;
  static late BuildContext tabContext;

  //네비게이션
  final GlobalKey<NavigatorState> tab1NavigatorKey = GlobalKey<NavigatorState>();
  final GlobalKey<NavigatorState> tab2NavigatorKey = GlobalKey<NavigatorState>();
  final GlobalKey<NavigatorState> tab3NavigatorKey = GlobalKey<NavigatorState>();
  final GlobalKey<NavigatorState> tab4NavigatorKey = GlobalKey<NavigatorState>();
  final GlobalKey<NavigatorState> tab5NavigatorKey = GlobalKey<NavigatorState>();

  void onPageChanged(int page) {
    Provider.of<SettingState>(context, listen: false).pageIndex = page;
    // setState(() {
    //   this._page = page;
    // });
  }

  void updateUI() {
    setState(() {});
  }

  @override
  void initState() {
    // _page = widget.homeScreenNum;

    checkMember();

    super.initState();
    pageController = PageController(initialPage: Provider.of<SettingState>(context, listen: false).pageIndex);

    //다른 스테이트에서 탭 유아이 업데이트할때 사용할수 있도록.
    Constants.updateUIForTab = updateUI;

    // Constants.prefsClear(); //pref초기화
  }

  void checkMember() async {
    var connectivityResult = await (Connectivity().checkConnectivity());

    //인터넷은 연결되어있는데, 유저모델이 널인건, 가입이 잘못되었다는 뜻. 이럴때는 그냥 로그아웃을 시킨다.
    // if (connectivityResult != ConnectivityResult.none) {
    //   if (!Provider.of<FirebaseAuthState>(context, listen: false).firebaseUser.isAnonymous &&
    //       Provider.of<UserModelState>(context, listen: false).userModel == null) {
    //     print('signout');
    //     Provider.of<FirebaseAuthState>(context, listen: false).signOut();
    //   }
    // }
  }

  void navigationTapped(int page) {
    print("PAGE!!!!!");
    pageController.jumpToPage(page);
    Provider.of<SettingState>(context, listen: false).pageIndex = page;
    // _pageController.animateToPage(page, duration: Duration(milliseconds: 300), curve: Curves.cu);
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    tabContext = context;

    return WillPopScope(
      onWillPop: () async {
        if (Provider.of<SettingState>(context, listen: false).pageIndex == 1) {
          //tab2
          if (tab2NavigatorKey.currentState!.canPop()) {
            tab2NavigatorKey.currentState!.pop();
          }
        } else if (Provider.of<SettingState>(context, listen: false).pageIndex == 2) {
          //tab3
          if (tab3NavigatorKey.currentState!.canPop()) {
            tab3NavigatorKey.currentState!.pop();
          }
        } else if (Provider.of<SettingState>(context, listen: false).pageIndex == 3) {
          //tab4
          if (tab4NavigatorKey.currentState!.canPop()) {
            tab4NavigatorKey.currentState!.pop();
          }
        } else if (Provider.of<SettingState>(context, listen: false).pageIndex == 4) {
          //tab5
          if (tab5NavigatorKey.currentState!.canPop()) {
            tab5NavigatorKey.currentState!.pop();
          }
        }
        return false;
      },
      child: Navigator(
          key: tab1NavigatorKey,
          onGenerateRoute: (routeSettings) {
            return MaterialPageRoute(
                builder: (context) => Consumer<SettingState>(
                        builder: (BuildContext context, SettingState settingState, child) {
                          Constants.prefs.setString(Constants.splashKey,settingState.entireModel.splashImg);
                      var tabAppBar = selectTabAppBar(settingState);
                      var tabFunc = selectTabFunc(settingState.entireModel.tabCount, settingState.tab1Model.funcPage, settingState.tab2Model.funcPage,
                          settingState.tab3Model.funcPage, settingState.tab4Model.funcPage, settingState.tab5Model.funcPage, widget.userMade, tab1NavigatorKey,
                          tab2NavigatorKey, tab3NavigatorKey, tab4NavigatorKey, tab5NavigatorKey);

                      return Scaffold(
                        resizeToAvoidBottomInset: true,
                        body: Stack(
                          children: [
                            MaterialApp(
                              theme: ThemeData(
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                focusColor: Colors.transparent,
                                primarySwatch: createMaterialColor(Color(settingState.entireModel.defaultColor)),
                              ),
                              home: PageView(
                                physics: NeverScrollableScrollPhysics(),
                                controller: pageController,
                                onPageChanged: onPageChanged,
                                children: tabFunc,
                              ),
                            ),
                          ],
                        ),
                        bottomNavigationBar: BottomAppBar(
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: tabAppBar,
                          ),
                          color: Color(settingState.entireModel.tabBgColor),
                        ),
                      );
                    }));
          }),
    );
  }

  selectTabAppBar(SettingState settingState) {
    switch (settingState.entireModel.tabCount) {
      case 2:
        {
          return [
            barIcon( 0, false, settingState),
            barIcon(1, false, settingState),
          ];
        }
      case 3:
        {
          return [
            barIcon(0, false, settingState),
            barIcon(1, false, settingState),
            barIcon(2, false, settingState),
          ];
        }
      case 4:
        {
          return [
            barIcon(0, false, settingState),
            barIcon(1, false, settingState),
            barIcon(2, false, settingState),
            barIcon(3, false, settingState),
          ];
        }
      case 5:
        {
          return [
            barIcon(0, false, settingState),
            barIcon(1, false, settingState),
            barIcon(2, false, settingState),
            barIcon(3, false, settingState),
            barIcon(4, false, settingState),
          ];
        }
    }
  }



  Widget barIcon(int page, bool badge, SettingState settingState) {
    return Padding(
      padding: EdgeInsets.only(bottom: 10, top: 5),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Stack(
            children: [
              IconButton(
                icon: Icon(IconData(page==0?settingState.tab1Model.tabIcon:page==1?settingState.tab2Model.tabIcon:page==2?settingState.tab3Model.tabIcon:
                  page==3?settingState.tab4Model.tabIcon:settingState.tab5Model.tabIcon, fontFamily: 'MaterialIcons')),
                color: settingState.pageIndex == page ? Color(settingState.entireModel.tabColor): Color(settingState.entireModel.tabColor).withOpacity(0.2),
                onPressed: () => navigationTapped(page),
                padding: EdgeInsets.all(0),
                focusColor: Colors.transparent,
                hoverColor: Colors.transparent,
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                disabledColor: Colors.transparent,
              ),
              //badge
              badge
                  ? Positioned(
                      right: 0,
                      top: 0,
                      child: Container(
                        width: 10,
                        height: 10,
                        decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.red),
                      ),
                    )
                  : SizedBox.shrink(),
            ],
          ),
          Text(
            page==0?settingState.tab1Model.tabTitle:page==1?settingState.tab2Model.tabTitle:page==2?settingState.tab3Model.tabTitle:
            page==3?settingState.tab4Model.tabTitle:settingState.tab5Model.tabTitle,
            style: TextStyle(color: settingState.pageIndex == page ? Color(settingState.entireModel.tabColor): Color(settingState.entireModel.tabColor).withOpacity(0.2), fontSize: 10),
            overflow: TextOverflow.ellipsis,
          )
        ],
      ),
    );
  }
}
