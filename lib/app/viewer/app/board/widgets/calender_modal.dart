import 'package:flutter/material.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/utils/utils.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';


class CalenderModal extends StatefulWidget {
  final String userMade;
  final SettingState settingState;

  CalenderModal({required this.userMade, required this.settingState});
  @override
  _CalenderModalState createState() => _CalenderModalState();
}

class _CalenderModalState extends State<CalenderModal> {
  CalendarFormat _calendarFormat = CalendarFormat.month;
  RangeSelectionMode _rangeSelectionMode = RangeSelectionMode.toggledOn; // Can be toggled on/off by longpressing a date
  DateTime _focusedDay = DateTime.now();
  DateTime? _selectedDay;
  DateTime? _rangeStart;
  DateTime? _rangeEnd;
  late Map<String, dynamic> reservationBoardMap;
  late Map<String, dynamic> reservationUserMap;

  @override
  Widget build(BuildContext context) {
      return Column(
        children: [
          TableCalendar(
            firstDay: kFirstDay,
            lastDay: kLastDay,
            focusedDay: _focusedDay,
            selectedDayPredicate: (day) => isSameDay(_selectedDay, day),
            rangeStartDay: _rangeStart,
            rangeEndDay: _rangeEnd,
            calendarFormat: _calendarFormat,
            rangeSelectionMode: _rangeSelectionMode,
            onDaySelected: (selectedDay, focusedDay) {
              if (!isSameDay(_selectedDay, selectedDay)) {
                setState(() {
                  _selectedDay = selectedDay;
                  _focusedDay = focusedDay;
                  _rangeStart = null; // Important to clean those
                  _rangeEnd = null;
                  _rangeSelectionMode = RangeSelectionMode.toggledOff;
                });
              }
            },
            onRangeSelected: (start, end, focusedDay) {
              setState(() {
                _selectedDay = null;
                _focusedDay = focusedDay;
                _rangeStart = start;
                _rangeEnd = end;
                _rangeSelectionMode = RangeSelectionMode.toggledOn;
              });
            },
            onFormatChanged: (format) {
              if (_calendarFormat != format) {
                setState(() {
                  _calendarFormat = format;
                });
              }
            },
            onPageChanged: (focusedDay) {
              _focusedDay = focusedDay;
            },
          ),
          Container(
            padding: const EdgeInsets.only(top: 30, bottom: 20, left: 20, right: 20),
            width: Constants.size.width * 0.8,
            child: FlatButton(
              minWidth: double.infinity,
              child: Text('선택하기', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white)),
              onPressed: () {
                List<DateTime> dateTimeList = [];
                if (_rangeStart != null) dateTimeList.add(_rangeStart!);
                if (_rangeEnd != null) dateTimeList.add(_rangeEnd!);

                if (_rangeStart != null || _rangeEnd == null)
                  Navigator.of(context).pop(dateTimeList);
              },
              color: Color(widget.settingState.entireModel.defaultColor),
              padding: EdgeInsets.symmetric(vertical: 20),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
            ),
          ),
        ],
      );
  }
}
