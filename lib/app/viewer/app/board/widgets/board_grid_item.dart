import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/board/read/read_board_view.dart';
import 'package:justapp/app/viewer/app/board/read/read_real_estate_board_view.dart';
import 'package:justapp/app/viewer/app/board/read/read_recommend_board_view.dart';
import 'package:justapp/utils/my_progress_indicator.dart';
import 'package:justapp/utils/transitions.dart';
import 'package:provider/provider.dart';

class BoardGridItem extends StatefulWidget {
  final String boardKey;
  final String userMade;
  final String funcType;
  final int tabNum;

  BoardGridItem({required this.boardKey, required this.userMade, required this.funcType, required this.tabNum});

  @override
  _BoardGridItemState createState() => _BoardGridItemState();
}

class _BoardGridItemState extends State<BoardGridItem> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<ViewerState, SettingState>(builder: (BuildContext context, ViewerState viewerState, SettingState settingState, child) {
      return Container(
          margin: const EdgeInsets.only(left: 10, right: 10,  top: 5, bottom: 5),
          child: Stack(
            children: [
              InkWell(
                focusColor: Colors.transparent,
                hoverColor: Colors.transparent,
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                child: Container(
                  height: Constants.sizeWidth * 0.6,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    shape: BoxShape.rectangle,
                    color: Color(selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR)),
                    boxShadow: [
                      selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW)?
                      BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.1), blurRadius: 2, spreadRadius: 2):
                      BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.0), blurRadius: 0, spreadRadius: 0)
                    ],
                  ),
                ),
                onTap: () {
                  Navigator.of(context).push(
                    Transitions(
                      transitionType: TransitionType.slideLeft,
                      duration: Duration(milliseconds: 200),
                      curve: Curves.bounceInOut,
                      reverseCurve: Curves.fastOutSlowIn,
                      widget:
                      selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_BOARD, KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE)=='Work'?
                      ReadBoardView(boardKey: widget.boardKey, userMade: widget.userMade, tabNum: widget.tabNum, funcType: widget.funcType):
                      selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_BOARD, KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE)=='RealEstate'?
                      ReadRealEstateBoardView(boardKey: widget.boardKey, userMade: widget.userMade, tabNum: widget.tabNum, funcType: widget.funcType):
                      selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_BOARD, KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE)=='Recommend'?
                      ReadRecommendBoardView(boardKey: widget.boardKey, userMade: widget.userMade, tabNum: widget.tabNum, funcType: widget.funcType):
                      ReadBoardView(boardKey: widget.boardKey, userMade: widget.userMade, tabNum: widget.tabNum, funcType: widget.funcType),
                    ),
                  );
                },
              ),


              Container(
                padding: const EdgeInsets.all(15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ClipRRect(
                        borderRadius: BorderRadius.circular(20.0),
                        child: CachedNetworkImage(
                            imageUrl: viewerState.boardModelMap[widget.boardKey]!.boardThumbnailImage[0],
                            fit: BoxFit.cover,
                            width: Constants.sizeWidth * 0.4,
                            height: Constants.sizeWidth * 0.3,
                            placeholder: (BuildContext context, String url) {
                              return MyProgressIndicator(containerSize: 30);
                            })),
                    SizedBox(height: 10),
                    selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_ISACTIVE]?
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          width: Constants.sizeWidth * 0.2,
                          child: Text(
                            viewerState.boardModelMap[widget.boardKey]!.boardTitle,
                            textAlign: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN] == "left"? TextAlign.left:
                            selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN] == "center"? TextAlign.center:TextAlign.right,
                            style: TextStyle(fontWeight:FontWeight.bold, fontSize: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE].toDouble(),
                                color: Color(selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR])),
                            maxLines: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_LINE],
                          ),
                        ),
                        selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_BOARD, KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE)=='Recommend'?
                        Row(
                          children: [
                            Icon(Icons.favorite),
                            Padding(
                              padding: const EdgeInsets.only(left: 5),
                              child: AutoSizeText(viewerState.boardModelMap[widget.boardKey]!.favoriteCnt.toString()),
                            ),
                          ],
                        ):SizedBox.shrink(),
                      ],
                    ):SizedBox.shrink(),
                    SizedBox(height: 5),

                    selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_ISACTIVE]?
                    Padding(
                      padding: EdgeInsets.only(bottom: 10),
                      child: SizedBox(
                        width: Constants.sizeWidth * 0.4,
                        child: AutoSizeText(
                          viewerState.boardModelMap[widget.boardKey]!.boardSubTItle,
                          textAlign: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN] == "left"? TextAlign.left:
                          selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN] == "center"? TextAlign.center:TextAlign.right,
                          style: TextStyle(fontSize: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE].toDouble(),
                              color: Color(selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR])),
                          maxLines: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_LINE],
                        ),
                      ),
                    ):SizedBox.shrink(),
                  ],
                ),
              ),
            ],
          ));
    });
  }
}
