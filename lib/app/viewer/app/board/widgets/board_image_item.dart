import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/board/read/read_board_view.dart';
import 'package:justapp/app/viewer/app/board/read/read_real_estate_board_view.dart';
import 'package:justapp/app/viewer/app/board/read/read_recommend_board_view.dart';
import 'package:justapp/utils/my_progress_indicator.dart';
import 'package:justapp/utils/transitions.dart';
import 'package:provider/provider.dart';

class BoardImageItem extends StatefulWidget {
  final String boardKey;
  final String userMade;
  final String funcType;
  final int tabNum;

  BoardImageItem({required this.boardKey, required this.userMade, required this.funcType, required this.tabNum});

  @override
  _BoardImageItemState createState() => _BoardImageItemState();
}

class _BoardImageItemState extends State<BoardImageItem> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<ViewerState, SettingState>(builder: (BuildContext context, ViewerState viewerState, SettingState settingState, child) {
      return Container(
        margin: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
        child: Stack(
          children: [
            InkWell(
              focusColor: Colors.transparent,
              hoverColor: Colors.transparent,
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  border: Border.all(
                    width: 1,
                    color: Colors.black12,
                  ),
                  // boxShadow: [
                  //   selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW)
                  //       ? BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.1), blurRadius: 5, spreadRadius: 5)
                  //       : BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.0), blurRadius: 0, spreadRadius: 0)
                  // ],
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  child: CachedNetworkImage(
                      imageUrl: viewerState.boardModelMap[widget.boardKey]!.boardThumbnailImage[0],
                      fit: BoxFit.cover,
                      width: Constants.sizeWidth,
                      height: Constants.sizeWidth * 0.7,
                      placeholder: (BuildContext context, String url) {
                        return MyProgressIndicator(containerSize: 30);
                      }),
                ),
              ),
              onTap: () {
                Navigator.of(context).push(
                  Transitions(
                    transitionType: TransitionType.slideLeft,
                    duration: Duration(milliseconds: 200),
                    curve: Curves.bounceInOut,
                    reverseCurve: Curves.fastOutSlowIn,
                    widget: selectGetFuncAllType(
                                settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_BOARD, KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) ==
                            'Work'
                        ? ReadBoardView(boardKey: widget.boardKey, userMade: widget.userMade, tabNum: widget.tabNum, funcType: widget.funcType)
                        : selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_BOARD, KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) ==
                                'RealEstate'
                            ? ReadRealEstateBoardView(
                                boardKey: widget.boardKey, userMade: widget.userMade, tabNum: widget.tabNum, funcType: widget.funcType)
                            : selectGetFuncAllType(
                                        settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_BOARD, KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) ==
                                    'Recommend'
                                ? ReadRecommendBoardView(
                                    boardKey: widget.boardKey, userMade: widget.userMade, tabNum: widget.tabNum, funcType: widget.funcType)
                                : ReadBoardView(
                                    boardKey: widget.boardKey, userMade: widget.userMade, tabNum: widget.tabNum, funcType: widget.funcType),
                  ),
                );
              },
            ),

            // color: Color(selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR)),
            // borderRadius: BorderRadius.only(topLeft:Radius.circular(20), topRight:Radius.circular(20)),
            Positioned(
              bottom: 0,
              child: Container(
                width: Constants.sizeWidth - 20,
                height: Constants.sizeWidth * 0.2,

                padding: EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 5),
                // color: Color(selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR))
                //     .withOpacity(0.2),
                // color: Colors.black.withOpacity(0.2),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20), bottomRight: Radius.circular(20)),
                  border: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW)
                      ? Border.all(
                          width: 1,
                          color: Colors.black12,
                        )
                      : null,
                  boxShadow: [
                    // BoxShadow(color: Colors.black.withOpacity(0.2))
                    BoxShadow(color: Color(selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR)))
                    // BoxShadow(color: Color(selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR)).withOpacity(0.2))
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    selectGetFuncAllType(
                            settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_ISACTIVE]
                        ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(
                                child: AutoSizeText(
                                  viewerState.boardModelMap[widget.boardKey]!.boardTitle,
                                  textAlign: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType,
                                              KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN] ==
                                          "left"
                                      ? TextAlign.left
                                      : selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[
                                                  KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN] ==
                                              "center"
                                          ? TextAlign.center
                                          : TextAlign.right,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType,
                                              KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE]
                                          .toDouble(),
                                      color: Color(selectGetFuncAllType(settingState, widget.tabNum, widget.funcType,
                                          KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR])),
                                  maxLines: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[
                                      KEY_CUSTOMAPP_SETTINGS_FUNC_LINE],
                                ),
                              ),
                              selectGetFuncAllType(
                                          settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_BOARD, KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) ==
                                      'Recommend'
                                  ? Row(
                                      children: [
                                        Icon(Icons.favorite),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 5),
                                          child: AutoSizeText(viewerState.boardModelMap[widget.boardKey]!.favoriteCnt.toString()),
                                        ),
                                      ],
                                    )
                                  : SizedBox.shrink(),
                            ],
                          )
                        : SizedBox.shrink(),
                    SizedBox(height: 5),
                    selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[
                            KEY_CUSTOMAPP_SETTINGS_FUNC_ISACTIVE]
                        ? SizedBox(
                            child: AutoSizeText(
                              viewerState.boardModelMap[widget.boardKey]!.boardSubTItle,
                              textAlign: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[
                                          KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN] ==
                                      "left"
                                  ? TextAlign.left
                                  : selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[
                                              KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN] ==
                                          "center"
                                      ? TextAlign.center
                                      : TextAlign.right,
                              style: TextStyle(
                                  fontSize: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType,
                                          KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE]
                                      .toDouble(),
                                  color: Color(selectGetFuncAllType(settingState, widget.tabNum, widget.funcType,
                                      KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR])),
                              maxLines: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[
                                  KEY_CUSTOMAPP_SETTINGS_FUNC_LINE],
                            ),
                          )
                        : SizedBox.shrink(),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    });
  }
}
