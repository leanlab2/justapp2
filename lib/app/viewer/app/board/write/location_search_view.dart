import 'package:flutter/material.dart';
import 'package:google_place/google_place.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:provider/provider.dart';

class LocationSearchView extends StatefulWidget {

  @override
  _LocationSearchViewState createState() => _LocationSearchViewState();
}

class _LocationSearchViewState extends State<LocationSearchView> {
  late GooglePlace googlePlace;
  List<AutocompletePrediction> predictions = [];
  TextEditingController _searchController = TextEditingController();

  @override
  void initState() {
    googlePlace = GooglePlace(Constants.googleApiKey);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<SettingState>(builder: (BuildContext context, SettingState settingState, child) {
      return Scaffold(
        resizeToAvoidBottomInset : false,
          appBar: AppBar(
            title: Text('위치 검색', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
            centerTitle: true,
            leading: BackButton(
                color: Colors.black
            ),
            backgroundColor: Colors.white,
          ),
          body: SafeArea(
            child: Container(
              margin: EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Stack(
                    children: [
                      TextField(
                        controller: _searchController,
                        // style: TextStyle(color: Colors.black12),
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: BorderSide(color: Colors.black, width: 0.5),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: BorderSide(color: Colors.black, width: 0.5),
                          ),
                          contentPadding: const EdgeInsets.only(top: 10.0, left: 10.0, right:50.0),
                          hintText: '도로명 또는 명칭을 입력하세요.',
                          hintStyle: TextStyle(color: Colors.black54),
                          // prefix: Padding(
                          //   padding: EdgeInsets.all(2),
                          // ),
                        ),
                        keyboardType: TextInputType.name,
                      ),
                      Container(
                        alignment: Alignment.topRight,
                        padding: const EdgeInsets.only(right:5.0),
                        child: IconButton(
                            iconSize: 25,
                            onPressed: () async{
                              if (_searchController.text.isNotEmpty) {
                                var result = await googlePlace.autocomplete.get(_searchController.text,language: 'ko');
                                if (result != null && result.predictions != null && mounted) {
                                  setState(() {
                                    predictions = result.predictions!;
                                    FocusScope.of(context).unfocus();
                                  });
                                }
                              } else {
                                if (predictions.length > 0 && mounted) {
                                  setState(() {
                                    predictions = [];
                                  });
                                }
                              }
                            },
                            icon: Icon(Icons.search_rounded),
                            color: Colors.black54),
                      ),
                    ],
                  ),
                  SizedBox( height:10),
                  predictions.length ==0?
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox( height:20),
                      Text("TIP", style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
                      SizedBox( height:20),
                      Text("도로명, 건물명, 지번 중 선택하여 입력하세요.", style: TextStyle(fontSize: 17)),
                      SizedBox( height:20),
                      Text("도로명 + 건물번호", style: TextStyle(fontSize: 15)),
                      SizedBox( height:10),
                      Text("예) 테헤란로 152", style: TextStyle(fontSize: 12, color: Colors.blue)),
                      SizedBox( height:20),
                      Text("동/읍/면/리 + 번지", style: TextStyle(fontSize: 15)),
                      SizedBox( height:10),
                      Text("예) 역삼동 757", style: TextStyle(fontSize: 12, color: Colors.blue)),
                      SizedBox( height:20),
                      Text("건물명, 아파트명", style: TextStyle(fontSize: 15)),
                      SizedBox( height:10),
                      Text("예) 삼성동 힐스테이트", style: TextStyle(fontSize: 12, color: Colors.blue)),
                    ],
                  ):
                  Expanded(
                    child: ListView.builder(
                      itemCount: predictions.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          leading: CircleAvatar(
                            child: Icon(
                              Icons.pin_drop,
                              color: Colors.white,
                            ),
                          ),
                          title: Text(predictions[index].description.toString()),
                          onTap: () async {
                            var result = await googlePlace.details.get(predictions[index].placeId.toString());
                            Navigator.of(context).pop(result!.result);
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
      );
    });
  }
}
