import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:future_progress_dialog/future_progress_dialog.dart';
import 'package:google_place/google_place.dart';
import 'package:image_picker/image_picker.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/repo/board_network_repository.dart';
import 'package:justapp/app/viewer/app/board/widgets/calender_modal.dart';
import 'package:justapp/app/viewer/app/board/write/select_category_view.dart';
import 'package:justapp/app/viewer/app/board/write/location_search_view.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:justapp/utils/my_progress_indicator.dart';
import 'package:justapp/utils/simple_snackbar.dart';
import 'package:justapp/utils/transitions.dart';
import 'package:provider/provider.dart';
import 'package:flutter/foundation.dart' as foundation;

class CreateBoardView extends StatefulWidget {
  final String userMade;
  final int tabNum;
  final String boardKey;

  CreateBoardView({required this.userMade, required this.tabNum, required this.boardKey});

  @override
  _CreateBoardViewState createState() => _CreateBoardViewState();
}

class _CreateBoardViewState extends State<CreateBoardView> {
  final ImagePicker _picker = ImagePicker();
  late List<String> optionStrList = [];
  List<XFile>? imageList= <XFile>[];
  List<String>? editImageList= <String>[];
// int count = 0;
  late String _category = 'category_1';
  String _categoryTitle = "카테고리 선택";
  String _address = "";
  Location _location = Location(lat:37.0,lng:128.0);
  dynamic detailsResult = "", dummyTitle = "";
  int categoryValue = 1;
  int titleNumLen = 0, subTitleNumLen = 0, contentNumLen = 0;
  late DateTime termStartDate = new DateTime.utc(1990,1,1),termEndDate = new DateTime.utc(1990,1,1);
  bool _isCallChecked=true, _isMessageChecked=false, _isMailChecked=false, _isLinkChecked=false, _isOnlineChecked=false;
  TextEditingController _titleController = TextEditingController();
  TextEditingController _subTitleController = TextEditingController();
  TextEditingController _timeController = TextEditingController();
  TextEditingController _priceController = TextEditingController();
  TextEditingController _phoneNumController = TextEditingController();
  TextEditingController _optionController = TextEditingController();
  TextEditingController _contentController = TextEditingController();
  TextEditingController _linkController = TextEditingController();

  Future getFuture() {
    return Future(() async {
      await Future.delayed(Duration(seconds: 5));
      return 'Hello, Future Progress Dialog!';
    });
  }

  @override
  void initState() {
    super.initState();


    if(widget.boardKey!=""){
      //image
      Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.boardImage.forEach((value){
        editImageList!.add(value);
      });

      //title
      _titleController.text = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.boardTitle;
      //subtitle
      _subTitleController.text = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.boardSubTItle;

      //detail
      _subTitleController.text = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.boardSubTItle;
      _timeController.text = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.time;
      _phoneNumController.text = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.phoneNum.toString();
      termStartDate = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.termStartDate.toDate();
      termEndDate = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.termEndDate.toDate();
      _priceController.text = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.price;

      //option
      Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.optionList.forEach((value){
        optionStrList.add(value);
      });

      //address
      _address = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.address;
      _location = Location(lat:Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.location.latitude,
          lng:Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.location.longitude);

      //content
      _contentController.text = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.boardContents.replaceAll('\\n','\n');

      //category
      _category = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.category;


      //ischecked
      _isCallChecked = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.isCallChecked;
      _isMessageChecked = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.isMessageChecked;
      _isMailChecked = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.isMailChecked;
      _isLinkChecked = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.isLinkChecked;
      _linkController.text = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.linkStr;
      _isOnlineChecked = Provider.of<ViewerState>(context, listen: false).boardModelMap[widget.boardKey]!.isOnlineChecked;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<ViewerState, SettingState>(builder: (BuildContext context, ViewerState viewerState, SettingState settingState, child) {
      return Scaffold(
          appBar: AppBar(
            title: Text(
                selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_BOARD, KEY_CUSTOMAPP_SETTINGS_BOARD_NAVI_CREATETITLE),
                style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
            centerTitle: true,
            leading: CloseButton(color: Colors.black),
            backgroundColor: Colors.white,
          ),
          backgroundColor: selectTabBgColor(widget.tabNum, settingState),
          // resizeToAvoidBottomInset: false,
          body: ListView(
            children: [
              //사진
              Padding(
                padding: const EdgeInsets.only(top: 20, left: 10),
                child: Text("사진", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
              ),
              Container(
                  padding: const EdgeInsets.only(top: 15, left: 10),
                  height: 100,
                  width: Constants.sizeWidth,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount:imageList!.length + 1,
                      itemBuilder: (BuildContext context, int index) {
                        if (index == 0) {
                          return
                            ListView.builder(
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              itemCount:editImageList!.length + 1,
                              itemBuilder: (BuildContext context, int index) {
                                if (index == 0) {
                                  return InkWell(
                                      child: Container(
                                        alignment: Alignment.center,
                                        width: 80,
                                        height: 80,
                                        padding: const EdgeInsets.all(10.0),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            width: 1,
                                            color: Colors.grey,
                                          ),
                                          borderRadius: BorderRadius.all(Radius.circular(10)),
                                          shape: BoxShape.rectangle,
                                        ),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(bottom: 5.0),
                                              child: Icon(Icons.add_a_photo, size: 20, color: Colors.grey,),
                                            ),
                                            Text((editImageList!.length + imageList!.length).toString() + "/10"),
                                          ],
                                        ),
                                      ),
                                      onTap: () async =>
                                      {
                                        await getImage(
                                          ImageSource.gallery,
                                          context: context,
                                          isMultiImage: true,
                                        )
                                      }
                                  );
                                }
                                else{
                                  return Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(left:5.0),
                                        child: Stack(
                                          children: [
                                            ClipRRect(
                                              child: CachedNetworkImage(imageUrl:editImageList![index-1],placeholder: (BuildContext context, String url) {
                                                return MyProgressIndicator(containerSize: 5);
                                              }),
                                              borderRadius: BorderRadius.all(Radius.circular(10)),
                                              // shape: BoxShape.rectangle,
                                            ),
                                            InkWell(
                                                child: Container(
                                                  width:20,
                                                  height:20,
                                                  decoration: BoxDecoration(
                                                    color: Colors.black,
                                                    shape: BoxShape.circle,
                                                  ),
                                                  child:Icon(Icons.clear,color: Colors.white,size: 15),
                                                ),
                                                onTap: () => {
                                                  setState(() { editImageList!.removeAt(index-1);
                                                  })
                                                }
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  );
                                }
                              }
                            );
                        }
                        else {
                          return
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left:5.0),
                                    child: Stack(
                                      children: [
                                        ClipRRect(
                                          child:  foundation.defaultTargetPlatform == foundation.TargetPlatform.iOS || foundation.defaultTargetPlatform == foundation.TargetPlatform.android?
                                          Image.file(File(imageList![index-1].path)):Image.network(imageList![index-1].path),
                                          borderRadius: BorderRadius.all(Radius.circular(10)),
                                            // shape: BoxShape.rectangle,
                                        ),
                                        InkWell(
                                          child: Container(
                                            width:20,
                                            height:20,
                                            decoration: BoxDecoration(
                                              color: Colors.black,
                                              shape: BoxShape.circle,
                                            ),
                                            child:Icon(Icons.clear,color: Colors.white,size: 15),
                                          ),
                                          onTap: () => {
                                            setState(() { imageList!.removeAt(index-1);
                                            })
                                          }
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                          );
                        }
                      })),
              //제목
              Padding(
                padding: const EdgeInsets.only(top: 40, left: 10),
                child: Text("제목", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
              ),
              Container(
                padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                height: 70,
                child: TextField(
                  controller: _titleController,
                  // style: TextStyle(color: Colors.black12),
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(color: Colors.black, width: 0.5),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(color: Colors.black, width: 0.5),
                    ),
                    contentPadding: const EdgeInsets.only(top: 10.0, left: 10.0),
                    hintText: '제목을 입력해 주세요. (3글자 이상)',
                    hintStyle: TextStyle(color: Colors.black54),
                    // prefix: Padding(
                    //   padding: EdgeInsets.all(2),
                    // ),
                  ),
                  keyboardType: TextInputType.name,
                  onChanged: (value) {
                    setState(() {
                      titleNumLen = value.length;
                    });
                  },
                ),
              ),


              //부제목
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text("부제목", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
              ),
              Container(
                padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                height: 70,
                child: TextField(
                  controller: _subTitleController,
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(color: Colors.black, width: 0.5),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(color: Colors.black, width: 0.5),
                    ),
                    contentPadding: const EdgeInsets.only(top: 10.0, left: 10.0),
                    hintText: '부제목을 입력해 주세요. (3글자 이상)',
                    hintStyle: TextStyle(color: Colors.black54),
                    // prefix: Padding(
                    //   padding: EdgeInsets.all(4),
                    // ),
                  ),
                  keyboardType: TextInputType.name,
                  onChanged: (value) {
                    setState(() {
                      subTitleNumLen = value.length;
                    });
                  },
                ),
              ),
              //카테고리
              Container(
                height: 50,
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: FlatButton(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(_categoryTitle,  style: TextStyle(fontSize:16, fontWeight: FontWeight.bold, color: Colors.black)),
                      Container(width: 30, height: 30, child: SvgPicture.asset('assets/profile/icons/profileArrow.svg',color: Colors.black,)),
                      // Text('>',  style: TextStyle(fontSize:20, fontWeight: FontWeight.bold, color: Colors.black)),
                    ],
                  ),
                  onPressed: () async {
                    dummyTitle = await Navigator.of(context).push(
                        Transitions(
                          transitionType: TransitionType.slideLeft,
                          duration: Duration(milliseconds: 200),
                          curve: Curves.bounceInOut,
                          reverseCurve: Curves.fastOutSlowIn,
                          widget: SelectCategoryView(),
                        ),
                      );
                      if(dummyTitle != null){
                        setState(() {
                          _categoryTitle = dummyTitle;
                        });
                      }
                  },
                  shape: RoundedRectangleBorder(
                    side: BorderSide(color:Colors.black, width: 0.5),
                    borderRadius: BorderRadius.circular(5),
                  ),
                ),
              ),


              //category
              // Container(
              //   alignment: Alignment.center,
              //   padding: const EdgeInsets.only(top: 10),
              //   child: dropDown(settingState, selectGetType(settingState, -1, KEY_CUSTOMAPP_SETTINGS_ENTIRE_CATEGORYCOUNT)),
              // ),


              //시간
              Padding(
                padding: const EdgeInsets.only(top: 40, left: 10),
                child: Text("시간", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
              ),
              Container(
                padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                height: 70,
                child: TextField(
                  controller: _timeController,
                  // style: TextStyle(color: Colors.black12),
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(color: Colors.black, width: 0.5),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(color: Colors.black, width: 0.5),
                    ),
                    contentPadding: const EdgeInsets.only(top: 10.0, left: 10.0),
                    // prefix: Padding(
                    //   padding: EdgeInsets.all(2),
                    // ),
                  ),
                  keyboardType: TextInputType.name,
                ),
              ),

              //기간
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text("기간", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
              ),
              Container(
                height: 60,
                padding: const EdgeInsets.only(top:10, left: 10, right: 10),
                child: FlatButton(
                  child: Row(
                    children: [
                      Text(termStartDate.year == 1990 && termEndDate.year == 1990?'':
                      termEndDate.year == 1990?
                      termStartDate.year.toString() + '.' + termStartDate.month.toString() + '.' + termStartDate.day.toString():
                      termStartDate.year.toString() + '.' + termStartDate.month.toString() + '.' + termStartDate.day.toString() + ' ~ '
                          + termEndDate.year.toString() + '.'+termEndDate.month.toString() + '.' + termEndDate.day.toString(), textAlign: TextAlign.left,),
                    ],
                  ),
                  onPressed: () async {
                    final detailsResult = await showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return Dialog(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)), //this right here
                        backgroundColor: Colors.white,
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: CalenderModal(userMade: widget.userMade, settingState: settingState),
                          ),
                        ),
                      );
                    });

                    setState(() {
                      print("detailsResult");
                      print(detailsResult);
                      if(detailsResult!=null){
                        if(detailsResult.length == 1){
                          termStartDate = detailsResult[0];
                          termEndDate = new DateTime.utc(1990,1,1);
                        }
                        else if(detailsResult.length == 2){
                          termStartDate = detailsResult[0];
                          termEndDate = detailsResult[1];
                        }
                        else{
                          termStartDate = new DateTime.utc(1990,1,1);
                          termEndDate = new DateTime.utc(1990,1,1);
                        }
                      }
                    });
                  },
                  shape: RoundedRectangleBorder(
                    side: BorderSide(color: Colors.black, width: 0.5),
                    borderRadius: BorderRadius.circular(5),
                  ),
                ),
              ),


              //가격 / 급여
              Padding(
                padding: const EdgeInsets.only(top:10, left: 10),
                child: Text("가격 / 급여", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
              ),
              Container(
                padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                height: 70,
                child: TextField(
                  controller: _priceController,
                  // style: TextStyle(color: Colors.black12),
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(color: Colors.black, width: 0.5),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(color: Colors.black, width: 0.5),
                    ),
                    contentPadding: const EdgeInsets.only(top: 10.0, left: 10.0),
                    // prefix: Padding(
                    //   padding: EdgeInsets.all(2),
                    // ),
                  ),
                  keyboardType: TextInputType.name,
                ),
              ),

              //전화번호
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text("연락가능 번호", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
              ),
              Container(
                padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                height: 70,
                child: TextField(
                  controller: _phoneNumController,
                  // style: TextStyle(color: Colors.black12),
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(color: Colors.black, width: 0.5),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(color: Colors.black, width: 0.5),
                    ),
                    contentPadding: const EdgeInsets.only(top: 10.0, left: 10.0),
                    // prefix: Padding(
                    //   padding: EdgeInsets.all(2),
                    // ),
                  ),
                  keyboardType: TextInputType.number,
                ),
              ),

              //위치
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text("위치", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
              ),
              Container(
                height: 60,
                padding: const EdgeInsets.only(top:10, left: 10, right: 10),
                child: FlatButton(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(_address),

                      Container(
                        width: 100,
                        child: FlatButton(
                          child: Text('위치검색', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white)),
                          onPressed: () async {
                            detailsResult = await Navigator.of(context).push(
                              Transitions(
                                transitionType: TransitionType.slideLeft,
                                duration: Duration(milliseconds: 200),
                                curve: Curves.bounceInOut,
                                reverseCurve: Curves.fastOutSlowIn,
                                widget: LocationSearchView(),
                              ),
                            );
                            setState(() {
                              if(detailsResult!=null){
                                _address = detailsResult!.name!.toString();
                                _location = detailsResult!.geometry!.location;
                              }
                            });
                          },
                          color: Colors.black54,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                        ),
                      ),
                    ],
                  ),
                  onPressed: () async {
                    detailsResult = await Navigator.of(context).push(
                      Transitions(
                        transitionType: TransitionType.slideLeft,
                        duration: Duration(milliseconds: 200),
                        curve: Curves.bounceInOut,
                        reverseCurve: Curves.fastOutSlowIn,
                        widget: LocationSearchView(),
                      ),
                    );
                    setState(() {
                      if(detailsResult!=null){
                        _address = detailsResult!.name!.toString();
                        _location = detailsResult!.geometry!.location;
                      }
                    });
                  },
                  shape: RoundedRectangleBorder(
                    side: BorderSide(color: Colors.black, width: 0.5),
                    borderRadius: BorderRadius.circular(5),
                  ),
                ),
              ),


              //옵션 정보
              Padding(
                padding: const EdgeInsets.only(top:35, left: 10),
                child: Text("옵션 정보", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
              ),
              Stack(
                children: [
                  Container(
                    padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                    height: 70,
                    child: TextField(
                      controller: _optionController,
                      // style: TextStyle(color: Colors.black12),
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(color: Colors.black, width: 0.5),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(color: Colors.black, width: 0.5),
                        ),
                        contentPadding: const EdgeInsets.only(top: 10.0, left: 10.0, right:50.0),
                        hintText: '옵션을 입력해 주세요.',
                        hintStyle: TextStyle(color: Colors.black54),
                        // prefix: Padding(
                        //   padding: EdgeInsets.all(2),
                        // ),
                      ),
                      keyboardType: TextInputType.name,
                    ),
                  ),
                  Container(
                    alignment: Alignment.topRight,
                    padding: const EdgeInsets.only(right:10.0,top:10.0),
                    child: IconButton(
                        iconSize: 25,
                        onPressed: () {
                          if(_optionController.text !="") {
                            setState(() {
                              optionStrList.add(_optionController.text);
                              _optionController.text = "";
                            });
                          }
                        },
                        icon: Icon(Icons.add),
                        color: Colors.black54),
                  ),
                ],
              ),
              ListView.builder(
                  shrinkWrap: true,
                  itemCount: optionStrList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: const EdgeInsets.only(top:5.0),
                      child: Container(
                        margin: const EdgeInsets.only(left:10.0,right:10.0),
                        height: 40,
                        decoration: BoxDecoration(
                            color: Colors.black12,
                            borderRadius: BorderRadius.circular(5)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left:20.0),
                              child: Text('+ '+optionStrList[index], style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black)),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right:20.0),
                              child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      optionStrList.removeAt(index);
                                    });
                                  },
                                  child: Container(width: 30,height:40,  child: Center(child: Text('X', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black))))
                              ),
                            ),
                          ],
                        ),
                      )
                      // child: optionList[index],
                    );
                    // return Text('결과가 없습니다.');
                  }),



              //상세 내용
              selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_BOARD, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS)[
                      KEY_CUSTOMAPP_SETTINGS_FUNC_ISACTIVE]
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top:30, left: 10),
                          child: Text("상세 내용", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
                        ),
                        Container(
                            padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                            child: TextField(
                              maxLength: 1000,
                              maxLines: null,
                              controller: _contentController,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                hintText: '내용을 입력해 주세요.(5글자 이상)',
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(color: Colors.black, width: 0.5),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(color: Colors.black, width: 0.5),
                                ),
                                // prefix: Padding(
                                //   padding: EdgeInsets.all(4),
                                // ),
                              ),
                              keyboardType: TextInputType.multiline,
                              onChanged: (value) {
                                setState(() {
                                  contentNumLen = value.length;
                                });
                              },
                            ),
                          ),
                      ],
                  )
                  : SizedBox.shrink(),


              //연락받을 방법
              Padding(
                padding: const EdgeInsets.only(top:30, left: 10, bottom:10),
                child: Text("연락받을 방법", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        padding: const EdgeInsets.only(left: 10),
                        child: Checkbox(
                          value:_isCallChecked,
                          onChanged: (value){
                            setState(() {
                              _isCallChecked = value!;
                            });
                          },
                        ),
                      ),
                      Text('전화',
                        style: TextStyle( color: Colors.black, fontSize: 15),
                      ),
                    ],
                  ),

                  Row(
                    children: [
                      Checkbox(
                        value:_isMessageChecked,
                        onChanged: (value){
                          setState(() {
                            _isMessageChecked = value!;
                          });
                        },
                      ),
                      Text('문자',
                        style: TextStyle( color: Colors.black, fontSize: 15),
                      ),
                    ],
                  ),

                  Row(
                    children: [
                      Checkbox(
                        value:_isMailChecked,
                        onChanged: (value){
                          setState(() {
                            _isMailChecked = value!;
                          });
                        },
                      ),
                      Container(
                        padding: const EdgeInsets.only(right: 20),
                        child: Text('이메일',
                          style: TextStyle( color: Colors.black, fontSize: 15),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        padding: const EdgeInsets.only(left: 10),
                        child: Checkbox(
                          value:_isLinkChecked,
                          onChanged: (value){
                            setState(() {
                              _isLinkChecked = value!;
                            });
                          },
                        ),
                      ),
                      Text('링크',
                        style: TextStyle( color: Colors.black, fontSize: 15),
                      ),
                      _isLinkChecked?Container(
                        padding: const EdgeInsets.only(left: 10),
                        width: Constants.sizeWidth * 0.45,
                        height: 30,
                        child: TextField(
                          controller: _linkController,
                          // style: TextStyle(color: Colors.black12),
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(color: Colors.black, width: 0.5),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(color: Colors.black, width: 0.5),
                            ),
                            contentPadding: const EdgeInsets.only(top: 10.0, left: 10.0),
                            hintText: 'url을 입력하세요.',
                            hintStyle: TextStyle(color: Colors.black54),
                          ),
                          keyboardType: TextInputType.name,
                          onChanged: (value) {//_linkController
                          },
                        ),
                      ):SizedBox.shrink(),
                    ],
                  ),

                  Row(
                    children: [
                      Checkbox(
                        value:_isOnlineChecked,
                        onChanged: (value){
                          setState(() {
                            _isOnlineChecked = value!;
                          });
                        },
                      ),
                      Container(
                        padding: const EdgeInsets.only(right: 20),
                        child: Text('온라인',
                          style: TextStyle( color: Colors.black, fontSize: 15),
                        ),
                      ),
                    ],
                  ),
                ],
              ),


              //완료
              Container(
                padding: const EdgeInsets.only(top: 30, bottom: 20, left: 20, right: 20),
                width: Constants.sizeWidth * 0.8,
                child: FlatButton(
                  minWidth: double.infinity,
                  child: Text('완료', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white)),
                  onPressed: () {
                      if (titleNumLen > 3 && contentNumLen > 5) {
                        if(_isCallChecked || _isMessageChecked || _isMailChecked || _isLinkChecked || _isOnlineChecked) {
                          widget.boardKey == "" ?
                          showDialog(
                            context: context,
                            builder: (context) =>
                                FutureProgressDialog(
                                  //id img
                                    boardNetworkRepository
                                        .setBoard(
                                        viewerState,
                                        widget.userMade,
                                        viewerState.userModel.userKey,
                                        DateTime.now(),
                                        imageList!,
                                        _titleController.text,
                                        _subTitleController.text,
                                        _category,
                                        _timeController.text,
                                        termStartDate,
                                        termEndDate,
                                        _priceController.text,
                                        _phoneNumController.text,
                                        _address,
                                        _location,
                                        optionStrList,
                                        _contentController.text.replaceAll('\n', '\\n'),
                                        _isCallChecked,
                                        _isMessageChecked,
                                        _isMailChecked,
                                        _isLinkChecked,
                                        _linkController.text,
                                        _isOnlineChecked,
                                        editImageList!)),
                          ).then((value) =>
                          {
                            simpleLongSnackBar(context, "게시를 했습니다."),
                            Navigator.of(context).pop(),
                          }) :
                          showDialog(
                            context: context,
                            builder: (context) =>
                                FutureProgressDialog(
                                  //id img
                                    boardNetworkRepository
                                        .updateBoard(
                                        widget.userMade,
                                        viewerState.userModel.userKey,
                                        widget.boardKey,
                                        DateTime.now(),
                                        imageList!,
                                        _titleController.text,
                                        _subTitleController.text,
                                        _category,
                                        _timeController.text,
                                        termStartDate,
                                        termEndDate,
                                        _priceController.text,
                                        _phoneNumController.text,
                                        _address,
                                        _location,
                                        optionStrList,
                                        _contentController.text.replaceAll('\n', '\\n'),
                                        _isCallChecked,
                                        _isMessageChecked,
                                        _isMailChecked,
                                        _isLinkChecked,
                                        _linkController.text,
                                        _isOnlineChecked,
                                        editImageList!)),
                          ).then((value) =>
                          {
                            simpleLongSnackBar(context, "수정을 완료 했습니다."),
                            Navigator.of(context).pop(),
                          });
                        }
                        else{
                          simpleLongSnackBar(context, "연락받을 방법은 최소 1개 이상 선택하셔야 합니다.");
                        }
                      } else {
                        simpleLongSnackBar(context, "제목은 3글자, 내용은 5글자 이상을 적어주세요.");
                      }
                  },
                  color: (titleNumLen > 3 && contentNumLen > 5) && (_isCallChecked || _isMessageChecked || _isMailChecked || _isLinkChecked || _isOnlineChecked)?
                    Color(settingState.entireModel.defaultColor) : Colors.black26,
                  padding: EdgeInsets.symmetric(vertical: 20),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                ),
              ),
            ],
          ));
    });
  }

  getImage(ImageSource source,
      {BuildContext? context, bool isMultiImage = true}) async {
    try {
      List<XFile>? pickedFileList = await _picker.pickMultiImage(
        imageQuality: 10,
      );
      setState(() {
        for (int i = 0; i < pickedFileList!.length; i++) {
          imageList!.add(pickedFileList[i]);
        }
      });
    } catch (e) {
      setState(() {
        print(e);

      });
    }
  }
}
