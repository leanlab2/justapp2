import 'package:flutter/material.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:provider/provider.dart';

class SelectCategoryView extends StatefulWidget {
  @override
  _MapViewState createState() => _MapViewState();
}

class _MapViewState extends State<SelectCategoryView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<SettingState, ViewerState>(builder: (BuildContext context, SettingState settingState, ViewerState viewerState, child) {
      return Scaffold(
          appBar: AppBar(
            title: Text('카테고리', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
            centerTitle: true,
            leading: BackButton(
                color: Colors.black
            ),
            backgroundColor: Colors.white,
          ),
          body: Padding(
            padding: EdgeInsets.only(top:10.0),
            child: Column(children: [
              Expanded(
                child: selectCategoryList('list', settingState, viewerState.categoryModelMap, 0, ""),
              )
            ]),
          ),
      );
    });
  }
}
