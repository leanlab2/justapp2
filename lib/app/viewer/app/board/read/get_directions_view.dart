import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';

class GetDirectionsView extends StatefulWidget {
  final String boardKey;

  GetDirectionsView({required this.boardKey});

  @override
  _GetDirectionsViewState createState() => _GetDirectionsViewState();
}

class _GetDirectionsViewState extends State<GetDirectionsView> {
  CameraPosition _initialLocation = CameraPosition(target: LatLng(0.0, 0.0));
  late GoogleMapController mapController;
  Set<Marker> markers = {};
  late PolylinePoints polylinePoints;
  Map<PolylineId, Polyline> polylines = {};
  List<LatLng> polylineCoordinates = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ViewerState>(builder: (BuildContext context, ViewerState viewerState, child) {
      return Scaffold(
          appBar: AppBar(
            title: Text('길 찾기', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
            centerTitle: true,
            leading: BackButton(
                color: Colors.black
            ),
            backgroundColor: Colors.white,
          ),
        resizeToAvoidBottomInset: false,
          body:Stack(
            children: <Widget>[
              // Map View
              GoogleMap(
                markers: Set<Marker>.from(markers),
                initialCameraPosition: _initialLocation,
                myLocationEnabled: true,
                myLocationButtonEnabled: false,
                mapType: MapType.normal,
                zoomGesturesEnabled: true,
                zoomControlsEnabled: false,
                polylines: Set<Polyline>.of(polylines.values),
                onMapCreated: (GoogleMapController controller) {
                  mapController = controller;
                  _currentLocationInit().then((value) => {
                    _calculateDistance(value, viewerState),
                  });
                },
              ),

              SafeArea(
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10.0, bottom: 10.0),
                    child: ClipOval(
                      child: Material(
                        color: Colors.white,  // button color
                        child: InkWell(
                          splashColor: Color(Constants.defaultColor),  // inkwell color
                          child: SizedBox(
                            width: 56,
                            height: 56,
                            child: Icon(Icons.my_location),
                          ),
                          onTap: () {
                            _currentLocation();
                          },
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),

      );
    });
  }

  Future<LocationData> _currentLocationInit() async {
    var destinationLocation = new Location();
    LocationData currentLocation = await destinationLocation.getLocation();
    return currentLocation;
  }

  _createPolylines(
      double startLatitude,
      double startLongitude,
      double destinationLatitude,
      double destinationLongitude,
      ) async {
    polylinePoints = PolylinePoints();
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      Constants.API_KEY, // Google Maps API Key
      PointLatLng(startLatitude, startLongitude),
      PointLatLng(destinationLatitude, destinationLongitude),
      travelMode: TravelMode.transit,
    );

    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }

    PolylineId id = PolylineId('poly');
    Polyline polyline = Polyline(
      polylineId: id,
      color: Colors.red,
      points: polylineCoordinates,
      width: 3,
    );
    polylines[id] = polyline;
  }

  Future<bool> _calculateDistance(LocationData startLocation, ViewerState viewerState) async {
    try {
      double startLatitude = startLocation.latitude!.toDouble();
      double startLongitude = startLocation.longitude!.toDouble();


      double destinationLatitude = viewerState.boardModelMap[widget.boardKey]!.location.latitude!.toDouble();
      double destinationLongitude = viewerState.boardModelMap[widget.boardKey]!.location.longitude!.toDouble();


      // Start Location Marker
      Marker startMarker = Marker(
        markerId: MarkerId('Start'),
        position: LatLng(startLatitude, startLongitude),
        infoWindow: InfoWindow(
          title: '시작지점',
          // snippet: _startAddress,
        ),
        icon: BitmapDescriptor.defaultMarker,
      );

      // Destination Location Marker
      Marker destinationMarker = Marker(
        markerId: MarkerId('Destination'),
        position: LatLng(destinationLatitude, destinationLongitude),
        infoWindow: InfoWindow(
          title: '목적지',
          snippet: viewerState.boardModelMap[widget.boardKey]!.address,
          // snippet: _destinationAddress,
        ),
        icon: BitmapDescriptor.defaultMarker,
      );

      // Adding the markers to the list
      markers.add(startMarker);
      markers.add(destinationMarker);


      double miny = (startLatitude <= destinationLatitude)
          ? startLatitude
          : destinationLatitude;
      double minx = (startLongitude <= destinationLongitude)
          ? startLongitude
          : destinationLongitude;
      double maxy = (startLatitude <= destinationLatitude)
          ? destinationLatitude
          : startLatitude;
      double maxx = (startLongitude <= destinationLongitude)
          ? destinationLongitude
          : startLongitude;

      double southWestLatitude = miny;
      double southWestLongitude = minx;

      double northEastLatitude = maxy;
      double northEastLongitude = maxx;

      // Accommodate the two locations within the
      // camera view of the map
      mapController.animateCamera(
        CameraUpdate.newLatLngBounds(
          LatLngBounds(
            northeast: LatLng(northEastLatitude, northEastLongitude),
            southwest: LatLng(southWestLatitude, southWestLongitude),
          ),
          100.0,
        ),
      );

      await _createPolylines(startLatitude, startLongitude, destinationLatitude,
          destinationLongitude);

      setState(() {
        // _placeDistance = totalDistance.toStringAsFixed(2);
        // print('DISTANCE: $_placeDistance km');
      });

      return true;
    } catch (e) {
      print(e);
    }
    return false;
  }

  double _coordinateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  _currentLocation() async {
    LocationData currentLocation;
    var location = new Location();
    currentLocation = await location.getLocation();

    mapController.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
        bearing: 0,
        target: LatLng(currentLocation.latitude!.toDouble(), currentLocation.longitude!.toDouble()),
        zoom: 18.0,
      ),
    ));
  }
}
