import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/repo/user_network_repository.dart';
import 'package:justapp/utils/simple_snackbar.dart';
import 'package:provider/provider.dart';

class SupportView extends StatefulWidget {
  final String boardKey;
  final String userMade;
  final String userKey;

  SupportView({required this.boardKey, required this.userMade, required this.userKey});

  @override
  _SupportViewState createState() => _SupportViewState();
}

class _SupportViewState extends State<SupportView> {
  TextEditingController _titleController = TextEditingController();
  TextEditingController _contentController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<SettingState, ViewerState>(builder: (BuildContext context, SettingState settingState, ViewerState viewerState, child) {
      return Scaffold(
          appBar: AppBar(
            title: Text('지원하기', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
            centerTitle: true,
            leading: BackButton(
                color: Colors.black
            ),
            backgroundColor: Colors.white,
          ),
        resizeToAvoidBottomInset: false,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                alignment: Alignment.topLeft,
                padding: const EdgeInsets.only(top: 15, left: 10, bottom: 10),
                child: Text("제목", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
              ),
              Container(
                padding: const EdgeInsets.only(left: 10, right: 10),
                height: 60,
                child: TextField(
                  controller: _titleController,
                  // style: TextStyle(color: Colors.black12),
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(color: Colors.black, width: 0.5),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide(color: Colors.black, width: 0.5),
                    ),
                    contentPadding: const EdgeInsets.only(top: 5.0, left: 10.0),
                    hintText: '제목을 입력해 주세요. (3글자 이상)',
                    hintStyle: TextStyle(color: Colors.black54),
                    // prefix: Padding(
                    //   padding: EdgeInsets.all(2),
                    // ),
                  ),
                  keyboardType: TextInputType.name,
                ),
              ),




              Container(
                alignment: Alignment.topLeft,
                padding: const EdgeInsets.only(left: 10),
                child: Text("전달메세지", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
              ),
              Container(
                padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                child: TextField(
                  maxLength: 1000,
                  maxLines: 4,
                  controller: _contentController,
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    hintText: '내용을 입력해 주세요.(5글자 이상)',
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(color: Colors.black, width: 0.5),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(color: Colors.black, width: 0.5),
                    ),
                    // prefix: Padding(
                    //   padding: EdgeInsets.all(4),
                    // ),
                  ),
                  keyboardType: TextInputType.multiline,
                ),
              ),

              Spacer(),


              Container(
                width: Constants.sizeWidth>400? 400: Constants.sizeWidth * 0.9,
                padding: EdgeInsets.fromLTRB(30, 30, 30, 20),
                child: Center(
                  child: FlatButton(
                    minWidth: double.infinity,
                    child: Text("지원하기", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                    onPressed: () async{
                      final result = await showOkCancelAlertDialog(
                        context: context,
                        title: '지원하기',
                        message: '정말로 지원하시겠습니까?',
                        okLabel: '확인',
                        cancelLabel: "취소",
                        defaultType: OkCancelAlertDefaultType.cancel,
                      );
                      if(result == OkCancelResult.ok){
                        Map<String, dynamic> messageMap = {};
                        messageMap[KEY_CUSTOMAPP_USERS_RECEIVE_MAP_TITLE] = _titleController.text;
                        messageMap[KEY_CUSTOMAPP_USERS_RECEIVE_MAP_MESSAGE] = _contentController.text.replaceAll('\n', '\\n');
                        messageMap[KEY_CUSTOMAPP_USERS_RECEIVE_MAP_DATE] = DateTime.now();
                        messageMap[KEY_CUSTOMAPP_USERS_RECEIVE_MAP_USERKEY] = DateTime.now();


                       userNetworkRepository.setUserOtherMap(
                            widget.userMade, widget.userKey, widget.boardKey, KEY_CUSTOMAPP_USERS_RECEIVE_MAP, messageMap).then((value) => {
                          // setState(() {
                            simpleLongSnackBar(context, "지원이 완료되었습니다."),
                            Navigator.of(context).pop(),
                          // }),
                        });
                      }
                    },
                    color: Color(settingState.entireModel.defaultColor),
                    textColor: Colors.white,
                    padding: EdgeInsets.symmetric(vertical: 22),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                  ),
                ),
              ),
            ],
          ),

      );
    });
  }
}
