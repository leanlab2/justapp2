import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/repo/board_network_repository.dart';
import 'package:justapp/app/repo/user_network_repository.dart';
import 'package:justapp/app/viewer/app/board/read/get_directions_view.dart';
import 'package:justapp/app/viewer/app/board/read/map_view.dart';
import 'package:justapp/app/viewer/app/board/read/support_view.dart';
import 'package:justapp/app/viewer/app/board/write/create_board_view.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:justapp/app/viewer/app/tabs/viewer_tabs.dart';
import 'package:justapp/utils/my_progress_indicator.dart';
import 'package:justapp/utils/simple_snackbar.dart';
import 'package:justapp/utils/transitions.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:flutter_sms/flutter_sms.dart';

class ReadRecommendBoardView extends StatefulWidget {
  final String boardKey;
  final String userMade;
  final int tabNum;
  final String funcType;

  ReadRecommendBoardView({required this.boardKey, required this.userMade, required this.tabNum, required this.funcType});

  @override
  _ReadRecommendBoardViewState createState() => _ReadRecommendBoardViewState();
}

class _ReadRecommendBoardViewState extends State<ReadRecommendBoardView> {
  bool isFavorite = false;
  String _userName = '',_userPhoneNum = '',_userEamil = '';
  late CameraPosition _initialLocation;//= CameraPosition(target: LatLng(37.0, 126.0));
  late GoogleMapController mapController;
  late List<Marker> _markers = [];
  final CarouselController planetPageController = CarouselController();
  late int _current = 0;
  late DateTime termStartDate = new DateTime.utc(1990,1,1),termEndDate = new DateTime.utc(1990,1,1);

  @override
  void initState() {
    super.initState();
    ViewerState viewerState = Provider.of<ViewerState>(context, listen: false);

    userNetworkRepository.getUserModel(widget.userMade, viewerState.boardModelMap[widget.boardKey]!.userKey).then((value) => {
      setState(() {
        _userName = value.userName;
        _userPhoneNum = value.userPhoneNum.toString();
        _userEamil = value.userEmail.toString();
        isFavorite = value.favoriteMap.containsKey(widget.boardKey);
      })
    });

    _markers.add(
        Marker(
            markerId: MarkerId(viewerState.boardModelMap[widget.boardKey]!.userKey),
            draggable: true,
            onTap: () => print("Marker!"),
            position: LatLng(viewerState.boardModelMap[widget.boardKey]!.location.latitude, viewerState.boardModelMap[widget.boardKey]!.location.longitude)
        )
    );
    _initialLocation = CameraPosition(target: LatLng(viewerState.boardModelMap[widget.boardKey]!.location.latitude, viewerState.boardModelMap[widget.boardKey]!.location.longitude),zoom: 15.0);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<ViewerState, SettingState>(
        builder: (BuildContext context, ViewerState viewerState, SettingState settingState, child) {
      return Scaffold(
          appBar: AppBar(
            title: Text(
                selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_BOARD, KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE),
                style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
            centerTitle: true,
            leading: BackButton(color: Colors.black),
            actions: [
              viewerState.boardModelMap[widget.boardKey]!.userKey == viewerState.userModel.userKey? Padding(
                  padding: EdgeInsets.only(right: 10.0),
                  child: GestureDetector(
                    onTap: () async{
                      final result = await showModalActionSheet(
                        context: ViewerTabsState.tabContext,
                        actions: <SheetAction>[
                          SheetAction(
                            icon: Icons.warning,
                            label: viewerState.boardModelMap[widget.boardKey]!.isHidden?'보여주기':'숨기기',
                            key: 'hiddenKey',
                          ),
                          const SheetAction(
                            label: '수정하기',
                            icon: Icons.info,
                            key: 'editKey',
                          ),
                          const SheetAction(
                            icon: Icons.warning,
                            label: '삭제하기',
                            key: 'deleteKey',
                            isDestructiveAction: true,
                          ),
                        ],
                      );
                      if(result == 'hiddenKey'){
                        boardNetworkRepository.updateHiddenBoard(widget.userMade,viewerState.userModel.userKey,widget.boardKey,
                            viewerState.boardModelMap[widget.boardKey]!.isHidden?false:true).then((value) => {
                          simpleLongSnackBar(context, "수정되었습니다."),
                          Navigator.of(context).pop()
                        });
                      }
                      else if(result == 'editKey'){
                          Navigator.of(ViewerTabsState.tabContext).push(
                            Transitions(
                              transitionType: TransitionType.slideUp,
                              duration: Duration(milliseconds: 200),
                              curve: Curves.bounceInOut,
                              reverseCurve: Curves.fastOutSlowIn,
                              widget: CreateBoardView(userMade: widget.userMade, tabNum: widget.tabNum, boardKey: widget.boardKey),
                            ),
                          );
                      }
                      else if(result == 'deleteKey'){
                        boardNetworkRepository.deleteBoard(widget.boardKey, widget.userMade, viewerState.boardModelMap[widget.boardKey]!.userKey,viewerState).then((value) => {
                          simpleLongSnackBar(context, "삭제되었습니다.")
                        });
                      }
                    },
                    child: Icon(
                        Icons.more_vert,
                      color: Colors.black,
                    ),
                  )
              ):SizedBox.shrink(),
            ],
            backgroundColor: Colors.white,
          ),
          backgroundColor: selectTabBgColor(widget.tabNum, settingState),
          resizeToAvoidBottomInset: false,
          body:

          ListView(
            children: [
              //Image
              Stack(
                children: [
                  Container(
                    width: Constants.sizeWidth,
                    height: Constants.sizeWidth - 150,
                    child: CarouselSlider(
                      carouselController: planetPageController,
                      options: CarouselOptions(
                        autoPlay: false,
                        aspectRatio: 1.0,
                        enlargeCenterPage: true,
                        viewportFraction: 1,

                        onPageChanged: (index,reason) {
                          setState(() {
                            _current = index;
                            // print("${_current}");
                          });
                        },
                      ),

                      items: List.generate(
                        viewerState.boardModelMap[widget.boardKey]!.boardImage.length,
                              (index) => CachedNetworkImage(imageUrl:viewerState.boardModelMap[widget.boardKey]!.boardImage[index],
                                  width: Constants.sizeWidth, height: Constants.sizeWidth,placeholder: (BuildContext context, String url) {
                                return MyProgressIndicator(containerSize: 5);
                              }),
                        //Image.network(boardState.boardModelMap[widget.boardKey]!.boardImage[index], width: Constants.sizeWidth, height: Constants.sizeWidth),

                              )),

                    // ListView.builder(
                    //     scrollDirection: Axis.horizontal,
                    //     itemCount: boardState.boardModelMap[widget.boardKey]!.boardImage.length,
                    //     itemBuilder: (BuildContext context, int index) {
                    //       return Image.network(boardState.boardModelMap[widget.boardKey]!.boardImage[index], width: Constants.sizeWidth, height: Constants.sizeWidth);
                    //     }),
                  ),
                  Positioned(
                    child: Container(
                      padding: EdgeInsets.only(top: Constants.sizeWidth - 160),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: viewerState.boardModelMap[widget.boardKey]!.boardImage.map((url) {
                          int index = viewerState.boardModelMap[widget.boardKey]!.boardImage.indexOf(url);
                          return Container(
                            width: 8.0,
                            height: 8.0,
                            margin: EdgeInsets.symmetric(vertical: 2.0, horizontal: 4.0),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: _current == index
                                  ? Colors.black
                                  : Color.fromRGBO(0, 0, 0, 0.4),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                ],
              ),
              Divider(
                color: Colors.black.withOpacity(0.1),
                thickness: 1,
              ),


              //main content
              //username date
              Container(
                padding: EdgeInsets.only(top: 5, left: 20,bottom: 10),
                child: Row(
                  children: [
                    Text(
                      _userName + "·"+
                      viewerState.boardModelMap[widget.boardKey]!.date.toDate().year.toString()+ '-'
                      +viewerState.boardModelMap[widget.boardKey]!.date.toDate().month.toString() + '-'
                      +viewerState.boardModelMap[widget.boardKey]!.date.toDate().day.toString(),
                        style: TextStyle(fontSize: 12, color: Colors.black45)
                    ),
                  ],
                ),
              ),
              //title, favorite
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  //title
                  Container(
                    padding: EdgeInsets.only(left: 20),
                    child: AutoSizeText(
                      viewerState.boardModelMap[widget.boardKey]!.boardTitle,
                      textAlign: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN] == "left"? TextAlign.left:
                      selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN] == "center"? TextAlign.center:TextAlign.right,
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE].toDouble(),
                          color: Color(selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR])),
                      maxLines: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_LINE],
                    ),
                  ),
                  selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_BOARD, KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE)=='Recommend'?
                  Row(
                    children: [
                      Icon(Icons.favorite),
                      Padding(
                        padding: const EdgeInsets.only(left:5, right: 20),
                        child: AutoSizeText(viewerState.boardModelMap[widget.boardKey]!.favoriteCnt.toString()),
                      ),
                    ],
                  ):SizedBox.shrink(),
                  //favorite
                  // Row(
                  //   children: [
                  //     Container(
                  //         child: IconButton(
                  //             icon: new Icon(Icons.share_rounded),
                  //             highlightColor: Colors.pink,
                  //             onPressed: () {
                  //               Share.share(
                  //               '업체명: '+viewerState.boardModelMap[widget.boardKey]!.boardTitle+
                  //               '\n주소: '+viewerState.boardModelMap[widget.boardKey]!.address+
                  //               '\n전화: '+viewerState.boardModelMap[widget.boardKey]!.phoneNum.toString()+
                  //               '\n영업시간: '+viewerState.boardModelMap[widget.boardKey]!.time+
                  //               '\n\n다운로드 링크' + selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_MSG)
                  //               );
                  //             })),
                  //     Container(
                  //         padding: EdgeInsets.only(right:10),
                  //         child: isFavorite
                  //             ? IconButton(
                  //             icon: new Icon(Icons.favorite),
                  //             highlightColor: Colors.pink,
                  //             onPressed: () {
                  //               userNetworkRepository.deleteUserOtherMap(
                  //                   widget.userMade, viewerState.userModel.userKey, viewerState.boardModelMap[widget.boardKey]!.boardKey,KEY_CUSTOMAPP_USERS_FAVORITE_MAP).then((value) => {
                  //                 setState(() {
                  //                   isFavorite = false;
                  //                 }),
                  //               });
                  //             }):
                  //         IconButton(
                  //             icon: new Icon(Icons.favorite_border),
                  //             highlightColor: Colors.pink,
                  //             onPressed: () {
                  //               userNetworkRepository.setUserOtherMap(
                  //                   widget.userMade, viewerState.userModel.userKey, viewerState.boardModelMap[widget.boardKey]!.boardKey,KEY_CUSTOMAPP_USERS_FAVORITE_MAP, "").then((value) => {
                  //                 setState(() {
                  //                   isFavorite = true;
                  //                 }),
                  //               });
                  //             })),
                  //   ],
                  // ),
                ],
              ),
              //subTitle
              Container(
                padding: EdgeInsets.only(top:5, left: 20, right: 20, bottom:10),
                child: AutoSizeText(
                  viewerState.boardModelMap[widget.boardKey]!.boardSubTItle,
                  textAlign: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN] == "left"? TextAlign.left:
                  selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN] == "center"? TextAlign.center:TextAlign.right,
                  style: TextStyle(fontSize: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE].toDouble(),
                      color: Color(selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR])),
                  maxLines: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_LINE],
                ),
              ),
              // Container(
              //   padding: EdgeInsets.only(top: 15, left: 20, right: 20),
              //   child: AutoSizeText(
              //     userName,
              //     textAlign: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN] == "left"? TextAlign.left:
              //     selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN] == "center"? TextAlign.center:TextAlign.right,
              //     style: TextStyle(fontSize: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE].toDouble(),
              //         color: Color(selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR])),
              //     maxLines: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE)[KEY_CUSTOMAPP_SETTINGS_FUNC_LINE],
              //   ),
              // ),
              Divider(
                color: Colors.black.withOpacity(0.1),
                thickness: 1,
              ),
              
              //datail contents


              Padding(
                padding: EdgeInsets.only(top:10, bottom:10),
                child: Row(
                  children: [
                    InkWell(
                      child: Container(
                        padding: const EdgeInsets.only(left: 10, right: 10),
                        child: Column(
                          children: [
                            Icon(isFavorite?Icons.favorite:Icons.favorite_border,size: 30),
                            SizedBox(height: 5),
                            SizedBox(
                                width: Constants.sizeWidth * 0.25-20,
                                child: Text('찜하기',textAlign: TextAlign.center,)
                            ),
                          ],
                        ),
                      ),
                      onTap: () async =>{
                        isFavorite?
                        await userNetworkRepository
                            .deleteUserOtherMap(widget.userMade, viewerState.userModel.userKey,
                            viewerState.boardModelMap[widget.boardKey]!.boardKey, KEY_CUSTOMAPP_USERS_FAVORITE_MAP)
                            .then((value) => {
                          boardNetworkRepository.updateFavoriteCntBoard(widget.userMade, viewerState.userModel.userKey,
                              viewerState.boardModelMap[widget.boardKey]!.boardKey, isFavorite).then((value) => {
                            setState(() {
                              isFavorite = false;
                            }),
                          })
                        }):
                        await userNetworkRepository
                              .setUserOtherMap(widget.userMade, viewerState.userModel.userKey,
                            viewerState.boardModelMap[widget.boardKey]!.boardKey, KEY_CUSTOMAPP_USERS_FAVORITE_MAP, "")
                                .then((value) => {
                                boardNetworkRepository.updateFavoriteCntBoard(widget.userMade, viewerState.userModel.userKey,
                                viewerState.boardModelMap[widget.boardKey]!.boardKey, isFavorite).then((value) => {
                                  setState(() {
                                    isFavorite = true;
                                  }),
                            }),
                          }),
                      },
                    ),
                    InkWell(
                      child: Container(
                        padding: const EdgeInsets.only( left: 10, right: 10),
                        child: Column(
                          children: [
                            Icon(Icons.map_outlined,size: 30),
                            SizedBox(height: 5),
                            SizedBox(
                                width: Constants.sizeWidth * 0.25-20,
                                child: Text('길찾기',textAlign: TextAlign.center,)
                            ),
                          ],
                        ),
                      ),
                      onTap: () =>{
                        Navigator.of(ViewerTabsState.tabContext).push(
                          Transitions(
                            transitionType: TransitionType.slideUp,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.bounceInOut,
                            reverseCurve: Curves.fastOutSlowIn,
                            widget: GetDirectionsView(boardKey:widget.boardKey),
                          ),
                        ),
                      },
                    ),
                    InkWell(
                      child: Container(
                        padding: const EdgeInsets.only(left: 10, right: 10),
                        child: Column(
                          children: [
                            Icon(Icons.ios_share,size: 30),
                            SizedBox(height: 5),
                            SizedBox(
                                width: Constants.sizeWidth * 0.25-20,
                                child: Text('공유하기',textAlign: TextAlign.center,)
                            ),
                          ],
                        ),
                      ),
                      onTap: () =>{
                        Share.share(
                          '업체명: '+viewerState.boardModelMap[widget.boardKey]!.boardTitle+
                          '\n주소: '+viewerState.boardModelMap[widget.boardKey]!.address+
                          '\n전화: '+viewerState.boardModelMap[widget.boardKey]!.phoneNum.toString()+
                          '\n영업시간: '+viewerState.boardModelMap[widget.boardKey]!.time+
                          '\n\n다운로드 링크' + selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_MSG)
                        ),
                      },
                    ),
                    InkWell(
                      child: Container(
                        padding: const EdgeInsets.only(left: 10, right: 10),
                        child: Column(
                          children: [
                            Icon(Icons.call_end_outlined,size: 30),
                            SizedBox(height: 5),
                            SizedBox(
                                width: Constants.sizeWidth * 0.25-20,
                                child: Text('문의하기',textAlign: TextAlign.center,)
                            ),
                          ],
                        ),
                      ),
                      onTap: () async => {
                        Constants.isMoblie?
                          await FlutterPhoneDirectCaller.callNumber(_userPhoneNum.toString()):
                          await showOkAlertDialog(
                          context: context,
                          title: '핸드폰 번호',
                          message: _userPhoneNum.toString(),
                          okLabel: '확인',
                        ),
                      },
                    ),
                  ],
                ),
              ),
              Divider(
                color: Colors.black.withOpacity(0.1),
                thickness: 1,
              ),



              viewerState.boardModelMap[widget.boardKey]!.price != ""?
              Container(
                padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('가격', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                    Text(viewerState.boardModelMap[widget.boardKey]!.price),
                  ],
                ),
              ):SizedBox.shrink(),
              viewerState.boardModelMap[widget.boardKey]!.address != ""?
              Container(
                padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('주소', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                    Text(viewerState.boardModelMap[widget.boardKey]!.address),
                  ],
                ),
              ):SizedBox.shrink(),
              viewerState.boardModelMap[widget.boardKey]!.phoneNum != 0?Container(
                padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('전화번호', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                    Text(viewerState.boardModelMap[widget.boardKey]!.phoneNum.toString()),
                  ],
                ),
              ):SizedBox.shrink(),
              viewerState.boardModelMap[widget.boardKey]!.termStartDate.toDate().year == 1990 && viewerState.boardModelMap[widget.boardKey]!.termEndDate.toDate().year == 1990?
              SizedBox.shrink():
              Container(
                padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('기간', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                    Text(viewerState.boardModelMap[widget.boardKey]!.termEndDate.toDate().year == 1990?
                    viewerState.boardModelMap[widget.boardKey]!.termStartDate.toDate().year.toString() + '.'
                        +viewerState.boardModelMap[widget.boardKey]!.termStartDate.toDate().month.toString() + '.'
                        +viewerState.boardModelMap[widget.boardKey]!.termStartDate.toDate().day.toString():
                    viewerState.boardModelMap[widget.boardKey]!.termStartDate.toDate().year.toString() + '.'
                        +viewerState.boardModelMap[widget.boardKey]!.termStartDate.toDate().month.toString() + '.'
                        +viewerState.boardModelMap[widget.boardKey]!.termStartDate.toDate().day.toString() + ' ~ '
                        +viewerState.boardModelMap[widget.boardKey]!.termEndDate.toDate().year.toString() + '.'
                        +viewerState.boardModelMap[widget.boardKey]!.termEndDate.toDate().month.toString() + '.'
                        +viewerState.boardModelMap[widget.boardKey]!.termEndDate.toDate().day.toString()),
                  ],
                ),
              ),
              viewerState.boardModelMap[widget.boardKey]!.time != ""?
              Container(
                padding: const EdgeInsets.only(top: 10, left: 20, right: 20, bottom:10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('시간', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                    Text(viewerState.boardModelMap[widget.boardKey]!.time),
                  ],
                ),
              ):SizedBox.shrink(),
              Divider(
                color: Colors.black.withOpacity(0.1),
                thickness: 1,
              ),



              //contents
              selectGetFuncAllType(
                  settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS)[KEY_CUSTOMAPP_SETTINGS_FUNC_ISACTIVE] ?
              Container(
                padding: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
                child:Text(
                  viewerState.boardModelMap[widget.boardKey]!.boardContents.replaceAll('\\n', '\n'),
                  textAlign: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS)[KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN] == "left"? TextAlign.left:
                  selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS)[KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN] == "center"? TextAlign.center:TextAlign.right,
                  style: TextStyle(fontSize: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS)[KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE].toDouble(),
                      color: Color(selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS)[KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR])),
                  // maxLines: selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS)[KEY_CUSTOMAPP_SETTINGS_FUNC_LINE],
                ),
              ): SizedBox.shrink(),
              Divider(
                color: Colors.black.withOpacity(0.1),
                thickness: 1,
              ),


              //option
              // Container(
              //   padding: EdgeInsets.only(top: 5, left: 20),
              //   child: Text('옵션', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
              // ),
              // Container(
              //   padding: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
              //   child:ListView.builder(
              //       shrinkWrap: true,
              //       itemCount: viewerState.boardModelMap[widget.boardKey]!.optionList.length,
              //       itemBuilder: (BuildContext context, int index) {
              //         return Text("+ " + viewerState.boardModelMap[widget.boardKey]!.optionList[index]);
              //         // return Text('결과가 없습니다.');
              //       }),
              // ),
              // Divider(
              //   color: Colors.black.withOpacity(0.1),
              //   thickness: 1,
              // ),


              //위치
              Container(
                padding: EdgeInsets.only(top: 5, left: 20),
                child: Text('위치', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 10, left: 20),
                    child: Text( viewerState.boardModelMap[widget.boardKey]!.address, style: TextStyle(fontSize: 14)),
                  ),
                  Row(
                    children: [
                      Container(
                        child: IconButton(
                            icon: new Icon(Icons.copy),
                            onPressed: () {
                              Clipboard.setData(new ClipboardData(text: viewerState.boardModelMap[widget.boardKey]!.address)).then((value) => {
                                simpleLongSnackBar(context, "주소가 복사되었습니다."),
                              });
                            }),
                      ),
                      // Container(
                      //   padding: EdgeInsets.only(right:10),
                      //   child: IconButton(
                      //       icon: new Icon(Icons.location_on),
                      //       onPressed: () {
                      //         Navigator.of(ViewerTabsState.tabContext).push(
                      //           Transitions(
                      //             transitionType: TransitionType.slideUp,
                      //             duration: Duration(milliseconds: 200),
                      //             curve: Curves.bounceInOut,
                      //             reverseCurve: Curves.fastOutSlowIn,
                      //             widget: GetDirectionsView(boardKey:widget.boardKey),
                      //           ),
                      //         );
                      //       }),
                      // ),
                    ],
                  ),
                ],
              ),
              Container(
                height: 200,
                padding: EdgeInsets.only(top: 10, bottom:10),
                child: GoogleMap(
                  initialCameraPosition: _initialLocation,
                  myLocationEnabled: true,
                  myLocationButtonEnabled: false,
                  mapType: MapType.normal,
                  zoomGesturesEnabled: true,
                  zoomControlsEnabled: false,
                  onMapCreated: (GoogleMapController controller) {
                    mapController = controller;
                  },
                  markers: Set.from(_markers),
                  onTap:  (pos) {
                    Navigator.of(ViewerTabsState.tabContext).push(
                      Transitions(
                        transitionType: TransitionType.slideUp,
                        duration: Duration(milliseconds: 200),
                        curve: Curves.bounceInOut,
                        reverseCurve: Curves.fastOutSlowIn,
                        widget: MapView(pos: pos),
                      ),
                    );
                    // showCupertinoModalBottomSheet(
                    //   expand: true,
                    //   context: TabsState.tabContext,
                    //   backgroundColor: Colors.transparent,
                    //   builder: (context) => MapView(pos),
                    // );


                  },
                ),
              ),


              //final button
              // Container(
              //   width: Constants.sizeWidth>400? 400: Constants.sizeWidth * 0.9,
              //   padding: EdgeInsets.fromLTRB(30, 30, 30, 20),
              //   child: FlatButton(
              //     minWidth: double.infinity,
              //     child: Text("연결하기", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
              //     onPressed: () async{
              //       List<SheetAction> sheetActionList = [];
              //       if(viewerState.boardModelMap[widget.boardKey]!.isCallChecked) {
              //         sheetActionList.add(SheetAction(
              //           label: '전화 연결',
              //           icon: Icons.call,
              //           key: 'callKey',
              //         ));
              //       }
              //       if(viewerState.boardModelMap[widget.boardKey]!.isMessageChecked) {
              //         sheetActionList.add(SheetAction(
              //           label: '문자 연결',
              //           icon: Icons.sms,
              //           key: 'messageKey',
              //         ));
              //       }
              //       if(viewerState.boardModelMap[widget.boardKey]!.isMailChecked) {
              //         sheetActionList.add(SheetAction(
              //           label: '메일 연결',
              //           icon: Icons.mail,
              //           key: 'mailKey',
              //         ));
              //       }
              //       if(viewerState.boardModelMap[widget.boardKey]!.isLinkChecked) {
              //         sheetActionList.add(SheetAction(
              //           label: '링크 연결',
              //           icon: Icons.link,
              //           key: 'linkKey',
              //         ));
              //       }
              //       if(viewerState.boardModelMap[widget.boardKey]!.isOnlineChecked) {
              //         sheetActionList.add(SheetAction(
              //           label: '온라인 연결',
              //           icon: Icons.online_prediction,
              //           key: 'onlineKey',
              //         ));
              //       }
              //
              //       final result = await showModalActionSheet(
              //         context: context,
              //         actions: sheetActionList,
              //       );
              //       if(result == 'callKey'){
              //         Constants.isMoblie?
              //         await FlutterPhoneDirectCaller.callNumber(_userPhoneNum.toString()):
              //         await showOkAlertDialog(
              //           context: context,
              //           title: '핸드폰 번호',
              //           message: _userPhoneNum.toString(),
              //           okLabel: '확인',
              //         );
              //       }
              //       else if(result == 'messageKey'){
              //         List<String> recipents = [_userPhoneNum.toString()];
              //         await sendSMS(message: 'Name : $_userName\nEmail : $_userEamil\nPhoneNumber : $_userPhoneNum\n', recipients: recipents)
              //             .catchError((onError) {
              //           print(onError);
              //         });
              //       }
              //       else if(result == 'mailKey'){
              //         await Constants.launchEmail(context, "magnate0@naver.com", 'connect');
              //       }
              //       else if(result == 'linkKey'){
              //         Constants.launchInWebViewOrVC(viewerState.boardModelMap[widget.boardKey]!.linkStr);
              //       }
              //       else if(result == 'onlineKey'){
              //         Navigator.of(context).push(
              //           Transitions(
              //             transitionType: TransitionType.slideLeft,
              //             duration: Duration(milliseconds: 200),
              //             curve: Curves.bounceInOut,
              //             reverseCurve: Curves.fastOutSlowIn,
              //             widget: SupportView(boardKey: widget.boardKey, userMade: widget.userMade, userKey:viewerState.userModel.userKey),
              //           ),
              //         );
              //       }
              //     },
              //     color: Color(settingState.entireModel.defaultColor) ,
              //     textColor: Colors.white,
              //     padding: EdgeInsets.symmetric(vertical: 22),
              //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
              //   ),
              // ),
            ],
          )

      );
    });
  }
}
