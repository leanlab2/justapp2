import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:provider/provider.dart';

class MapView extends StatefulWidget {
  final LatLng pos;

  MapView({required this.pos});

  @override
  _MapViewState createState() => _MapViewState();
}

class _MapViewState extends State<MapView> {
  late GoogleMapController mapController;
  late CameraPosition _initialLocation;
  List<Marker> _markers = [];

  @override
  void initState() {
    super.initState();
    _initialLocation = CameraPosition(target: LatLng(widget.pos.latitude, widget.pos.longitude),zoom: 15.0);

    _markers.add(Marker(
        markerId: MarkerId("1"),
        draggable: true,
        position: LatLng(widget.pos.latitude, widget.pos.longitude)));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<SettingState>(builder: (BuildContext context, SettingState settingState, child) {
      return Scaffold(
          appBar: AppBar(
            title: Text('위치보기', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
            centerTitle: true,
            leading: CloseButton(
                color: Colors.black
            ),
            backgroundColor: Colors.white,
          ),
          body: Stack(children: [
            GoogleMap(
              initialCameraPosition: _initialLocation,
              myLocationEnabled: true,
              myLocationButtonEnabled: false,
              mapType: MapType.normal,
              zoomGesturesEnabled: true,
              zoomControlsEnabled: false,
              onMapCreated: (GoogleMapController controller) {
                mapController = controller;
              },
              markers: Set.from(_markers),
              // onTap:  (pos) {
              // },
            ),
          ]),
      );
    });
  }
}
