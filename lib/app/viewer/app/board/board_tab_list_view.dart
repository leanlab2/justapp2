import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/firestore/board_model.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/board/write/create_board_view.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:justapp/app/viewer/app/others/notification_list_view.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:justapp/app/viewer/app/tabs/viewer_tabs.dart';
import 'package:justapp/utils/transitions.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:rubber/rubber.dart';

class BoardTabListView extends StatefulWidget {
  final GlobalKey<NavigatorState> navigatorKey;
  final tabNum;
  final userMade;

  BoardTabListView({required this.navigatorKey,required this.tabNum,required this.userMade});

  @override
  _BoardTabListViewState createState() => _BoardTabListViewState();
}

class _BoardTabListViewState extends State<BoardTabListView> with SingleTickerProviderStateMixin {
  late RubberAnimationController _controller;
  CameraPosition _initialLocation = CameraPosition(target: LatLng(37.0, 126.0)); //현재위치로 변경
  late GoogleMapController mapController;
  late LatLng _curBoundNE; // 현재 보이는 화면의 북동쪽 위경도
  late LatLng _curBoundSW; // 현재 보이는 화면의 남서쪽 위경도

  double _dampingValue = DampingRatio.LOW_BOUNCY;
  double _stiffnessValue = Stiffness.LOW;

  List<Marker> _markers = [];
  late Map<String, BoardModel> updateMarkers = {};
  int markerLength = 0;
  Map<String, String> _todayBoardModelMap = {}, _today1BoardModelMap = {}, _today2BoardModelMap = {}, _today3BoardModelMap = {}, _today4BoardModelMap = {};

  @override
  void initState() {
    super.initState();
    termCalculator(Provider.of<ViewerState>(context, listen: false));
    _controller = RubberAnimationController(
        vsync: this,
        lowerBoundValue: AnimationControllerValue(pixel: 220),
        upperBoundValue: AnimationControllerValue(percentage: 0.95),
        springDescription: SpringDescription.withDampingRatio(mass: 1, stiffness: _stiffnessValue, ratio: _dampingValue),
        duration: Duration(milliseconds: 300));
    initMapView(Provider.of<SettingState>(context, listen: false), Provider.of<ViewerState>(context, listen: false));

    print(Provider.of<ViewerState>(context, listen: false).boardModelMap);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
        key: widget.tabNum == 1 ? null : widget.navigatorKey,
        onGenerateRoute: (routeSettings) {
          return MaterialPageRoute(
              builder: (context) =>
                  Consumer2<ViewerState, SettingState>(builder: (BuildContext context, ViewerState viewerState, SettingState settingState, child) {
                    return DefaultTabController(
                      length: 4,
                      child: Scaffold(
                        appBar: AppBar(
                          title: Text(selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_BOARD, KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE),
                              style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
                          centerTitle: true,
                          bottom:
                              selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_BOARD, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW)
                                  ? null:const TabBar(
                                      // labelColor: Colors.black,
                                      tabs: [
                                        Tab(child: Text('전체', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black))),
                                        Tab(child: Text('최신', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black))),
                                        Tab(child: Text('일주일', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black))),
                                        Tab(child: Text('한달', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black))),
                                      ],
                                    ),
                          actions: [
                            IconButton(
                                icon: Icon(Icons.notifications_none_outlined, color: Colors.black),
                                onPressed: () => {
                                      Navigator.of(context).push(
                                        Transitions(
                                          transitionType: TransitionType.slideLeft,
                                          duration: Duration(milliseconds: 200),
                                          curve: Curves.bounceInOut,
                                          reverseCurve: Curves.fastOutSlowIn,
                                          widget: NotificationListView(),
                                        ),
                                      ),
                                    }),
                          ],
                          backgroundColor: Colors.white,
                        ),
                        backgroundColor: selectTabBgColor(widget.tabNum, settingState),
                        floatingActionButton: viewerState.userModel.userPosition == KEY_CUSTOMAPP_USERS_CONSUMER
                            ? null
                            : Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: FloatingActionButton(
                                  onPressed: () => {
                                    Navigator.of(ViewerTabsState.tabContext).push(
                                      Transitions(
                                        transitionType: TransitionType.slideUp,
                                        duration: Duration(milliseconds: 200),
                                        curve: Curves.bounceInOut,
                                        reverseCurve: Curves.fastOutSlowIn,
                                        widget: CreateBoardView(userMade: widget.userMade, tabNum: widget.tabNum, boardKey: ""),
                                      ),
                                    ),
                                  },
                                  child: Icon(Icons.add),
                                  backgroundColor: Color(settingState.entireModel.defaultColor),
                                ),
                              ),
                        body: widget.tabNum == 1 && settingState.tab1BoardModel.listType == "map"
                            ? mapList(settingState, viewerState)
                            : widget.tabNum == 2 && settingState.tab2BoardModel.listType == "map"
                                ? mapList(settingState, viewerState)
                                : widget.tabNum == 3 && settingState.tab3BoardModel.listType == "map"
                                    ? mapList(settingState, viewerState)
                                    : widget.tabNum == 4 && settingState.tab4BoardModel.listType == "map"
                                        ? mapList(settingState, viewerState)
                                        : widget.tabNum == 5 && settingState.tab5BoardModel.listType == "map"
                                            ? mapList(settingState, viewerState)
                                            : selectGetFuncAllType(
                                                    settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_BOARD, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW)
                                                ? Column(
                                                    children: [
                                                      SizedBox(height: 20),
                                                      Expanded(child: eachBoardList(settingState, viewerState, viewerState.boardModelMap)),
                                                    ],
                                                  )
                                                : TabBarView(
                                                    children: [
                                                      Column(
                                                        children: [
                                                          SizedBox(height: 20),
                                                          Expanded(child: eachBoardList(settingState, viewerState, viewerState.boardModelMap)),
                                                        ],
                                                      ),
                                                      Column(
                                                        children: [
                                                          SizedBox(height: 20),
                                                          Expanded(child: eachBoardList(settingState, viewerState, _todayBoardModelMap)),
                                                        ],
                                                      ),
                                                      Column(
                                                        children: [
                                                          SizedBox(height: 20),
                                                          Expanded(child: eachBoardList(settingState, viewerState, _today2BoardModelMap)),
                                                        ],
                                                      ),
                                                      Column(
                                                        children: [
                                                          SizedBox(height: 20),
                                                          Expanded(child: eachBoardList(settingState, viewerState, _today3BoardModelMap)),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                      ),
                    );
                  }));
        });
  }

  eachBoardList(SettingState settingState, ViewerState viewerState, boardMap) {
    return widget.tabNum == 1
        ? selectBoardList(settingState.tab1BoardModel.listType, settingState, viewerState, boardMap, widget.userMade,
        KEY_CUSTOMAPP_SETTINGS_BOARD, widget.tabNum)
        : widget.tabNum == 2
            ? selectBoardList(settingState.tab2BoardModel.listType, settingState, viewerState, boardMap, widget.userMade,
                KEY_CUSTOMAPP_SETTINGS_BOARD, widget.tabNum)
            : widget.tabNum == 3
                ? selectBoardList(settingState.tab3BoardModel.listType, settingState, viewerState, boardMap, widget.userMade,
                    KEY_CUSTOMAPP_SETTINGS_BOARD, widget.tabNum)
                : widget.tabNum == 4
                    ? selectBoardList(settingState.tab4BoardModel.listType, settingState, viewerState, boardMap, widget.userMade,
                        KEY_CUSTOMAPP_SETTINGS_BOARD, widget.tabNum)
                    : selectBoardList(settingState.tab5BoardModel.listType, settingState, viewerState, boardMap, widget.userMade,
                        KEY_CUSTOMAPP_SETTINGS_BOARD, widget.tabNum);
  }

//map
  Stack mapList(SettingState settingState, ViewerState viewerState) {
    return Stack(children: [
      GoogleMap(
        initialCameraPosition: _initialLocation,
        myLocationEnabled: true,
        myLocationButtonEnabled: false,
        mapType: MapType.normal,
        zoomGesturesEnabled: true,
        zoomControlsEnabled: false,
        onMapCreated: (GoogleMapController controller) {
          mapController = controller;
          _currentLocation();
        },
        onCameraMove: (CameraPosition position) {
          _updateMarkers(viewerState);
        },
        markers: Set.from(_markers),
        // onTap:  (pos) {
        // },
      ),
      Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: EdgeInsets.only(top: 15, right: 15),
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
                boxShadow: [
                  BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.2), blurRadius: 5, spreadRadius: 5),
                ],
              ),
              child: InkWell(
                splashColor: Color(Constants.defaultColor), // inkwell color
                child: SizedBox(
                  width: 40,
                  height: 40,
                  child: Icon(Icons.my_location),
                ),
                onTap: () {
                  // TODO: Add the operation to be performed
                  _currentLocation();
                },
              ),
            ),
          ),
          Expanded(
            child: RubberBottomSheet(
              lowerLayer: Container(),
              upperLayer: _getUpperLayer(settingState, viewerState),
              animationController: _controller,
            ),
          ),
        ],
      ),
    ]);
  }

  Widget _getUpperLayer(settingState, viewerState) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          // borderRadius: BorderRadius.all(Radius.circular(30)),
          boxShadow: [
            BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.2), blurRadius: 5, spreadRadius: 5),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 5),
          child: Column(
            children: [
              Divider(
                color: Colors.grey,
                height: 30,
                thickness: 5,
                indent: Constants.sizeWidth * 0.45,
                endIndent: Constants.sizeWidth * 0.45,
              ),
              Expanded(
                  child: selectBoardList(
                      'list', settingState, viewerState, updateMarkers, widget.userMade, KEY_CUSTOMAPP_SETTINGS_BOARD, widget.tabNum)),
            ],
          ),
        ));
    // return Container(
    //   decoration: BoxDecoration(color: Colors.cyan),
    // );
  }

  void _currentLocation() async {
    LocationData currentLocation;
    var location = new Location();
    currentLocation = await location.getLocation();

    mapController.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
        bearing: 0,
        target: LatLng(currentLocation.latitude!.toDouble(), currentLocation.longitude!.toDouble()),
        zoom: 18.0,
      ),
    ));
  }

  Future<void> _getLatLngBound() async {
    //getVisibleRegion 메소드로 현재 경계를 찾는다.
    final LatLngBounds _currentBound = await mapController.getVisibleRegion().then((value) => value);
    _curBoundNE = _currentBound.northeast; //현재 경계의 북동쪽 위경도를 전역 변수에 넣는다.
    _curBoundSW = _currentBound.southwest; //현재 경계의 남서쪽 위경도를 전역 변수에 넣는다.
  }

  Future<void> _updateMarkers(ViewerState viewerState) async {
    await _getLatLngBound(); // 현재 경계의 위경도를 찾는다.
    // 기타 메소드 생략
    final markersInBound = _markers.where((element) =>
        element.position.latitude < _curBoundNE.latitude &&
        element.position.latitude > _curBoundSW.latitude &&
        element.position.longitude < _curBoundNE.longitude &&
        element.position.longitude > _curBoundSW.longitude);

    updateMarkers = {};
    BoardModel? boardModel;
    Map<String, BoardModel> boardModelMap = {};
    if (viewerState.currentMarkerId == "") {
      if (markerLength != markersInBound.length) {
        markerLength = markersInBound.length;
        for (int i = 0; i < markersInBound.length; i++) {
          Marker marker = markersInBound.elementAt(i);
          boardModel = viewerState.boardModelMap[marker.markerId.value];
          boardModelMap[marker.markerId.value] = boardModel!;
        }
        setState(() {
          updateMarkers = boardModelMap;
        });
      }
    } else {
      boardModelMap[viewerState.currentMarkerId] = viewerState.boardModelMap[viewerState.currentMarkerId]!;
      // markerLength = markersInBound.length;
      for (int i = 0; i < markersInBound.length; i++) {
        Marker marker = markersInBound.elementAt(i);
        if (viewerState.currentMarkerId != marker.markerId.value) {
          boardModel = viewerState.boardModelMap[marker.markerId.value];
          boardModelMap[marker.markerId.value] = boardModel!;
        }
      }
      setState(() {
        updateMarkers = boardModelMap;
      });
    }
  }

  void initMapView(SettingState settingState, ViewerState viewerState) async {
    GeoPoint location;
    for (int i = 0; i < viewerState.boardModelMap.length; i++) {
      location = viewerState.boardModelMap.values.elementAt(i).location;
      // final icon = await BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(24, 24)), 'assets/auth/icons/marker.png');

      _markers.add(Marker(
          infoWindow: InfoWindow(
              title: viewerState.boardModelMap[viewerState.boardModelMap.keys.elementAt(i)]!.boardTitle,
              snippet: viewerState.boardModelMap[viewerState.boardModelMap.keys.elementAt(i)]!.boardSubTItle),
          markerId: MarkerId(viewerState.boardModelMap.keys.elementAt(i)),
          draggable: true,
          // icon: BitmapDescriptor.defaultMarkerWithHue(_showBlue?BitmapDescriptor.hueGreen:BitmapDescriptor.hueBlue),
          onTap: () => {
                setState(() {}),
                viewerState.currentMarkerId = viewerState.boardModelMap.keys.elementAt(i),
                _updateMarkers(viewerState),
              },
          position: LatLng(location.latitude, location.longitude)));
    }
  }

  termCalculator(ViewerState viewState) {
    final today = DateTime.now();
    viewState.boardModelMap.values.forEach((element) {
      final difference = today.difference(element.date.toDate()).inDays;

      if(difference <= 5){
        _todayBoardModelMap[element.boardKey] = "";
      }
      if(difference <= 7){
        _today2BoardModelMap[element.boardKey] = "";
      }
      if(difference <= 30){
        _today3BoardModelMap[element.boardKey] = "";
      }
    });
  }
}
