import 'package:flutter/material.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:provider/provider.dart';

class NotificationListView extends StatefulWidget {

  @override
  _NotificationListViewState createState() => _NotificationListViewState();
}

class _NotificationListViewState extends State<NotificationListView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<SettingState>(builder: (BuildContext context, SettingState settingState, child) {
      return Scaffold(
          appBar: AppBar(
            title: Text('알림', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
            centerTitle: true,
            leading: BackButton(
                color: Colors.black
            ),
            backgroundColor: Colors.white,
          ),
          body: Center(child: Text("전달된 알림이 없습니다.", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)))
      );
    });
  }
}
