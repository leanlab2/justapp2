import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:provider/provider.dart';

class ReadReceiveUserView extends StatefulWidget {
  final boardKey;
  final Map<String, dynamic> valueMap;
  final tabNum;
  final userMade;

  ReadReceiveUserView({required this.boardKey, required this.valueMap, required this.tabNum, required this.userMade});

  @override
  _ReadReceiveUserViewState createState() => _ReadReceiveUserViewState();
}

class _ReadReceiveUserViewState extends State<ReadReceiveUserView> {

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<ViewerState, SettingState>(
        builder: (BuildContext context, ViewerState viewerState, SettingState settingState, child) {
          return Scaffold(
            appBar: AppBar(
              title: Text('지원한 사람', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
              centerTitle: true,
              leading: BackButton(
                  color: Colors.black
              ),
              backgroundColor: Colors.white,
            ),
            backgroundColor: selectTabBgColor(widget.tabNum, settingState),
            body: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.only(top: 20),
                    child: Text(
                      viewerState.userModelMap[widget.valueMap![KEY_CUSTOMAPP_USERS_RECEIVE_MAP_USERKEY]]!.userName+ ' ' +
                          widget.valueMap![KEY_CUSTOMAPP_USERS_RECEIVE_MAP_DATE].toDate().year.toString() + '-' +
                          widget.valueMap![KEY_CUSTOMAPP_USERS_RECEIVE_MAP_DATE].toDate().month.toString() + '-' +
                          widget.valueMap![KEY_CUSTOMAPP_USERS_RECEIVE_MAP_DATE].toDate().day.toString(),
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(height:5),
                  Container(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: Text(
                      widget.valueMap![KEY_CUSTOMAPP_USERS_RECEIVE_MAP_TITLE].toString(),
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Divider(
                    color: Colors.black.withOpacity(0.1),
                    thickness: 1,
                  ),

                  Container(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text("휴대폰         " + viewerState.userModelMap[widget.valueMap![KEY_CUSTOMAPP_USERS_RECEIVE_MAP_USERKEY]]!.userPhoneNum.toString())),
                  Container(
                      padding: const EdgeInsets.only(top:10, bottom: 10),
                      child: Text("이메일         " + viewerState.userModelMap[widget.valueMap![KEY_CUSTOMAPP_USERS_RECEIVE_MAP_USERKEY]]!.userEmail)),
                  Divider(
                    color: Colors.black.withOpacity(0.1),
                    thickness: 1,
                  ),

                  Container(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text("전달메세지", style: TextStyle(fontWeight: FontWeight.bold))),
                  Container(
                      padding: const EdgeInsets.only(left: 10, top: 20),
                      child: Text(widget.valueMap![KEY_CUSTOMAPP_USERS_RECEIVE_MAP_MESSAGE].toString().replaceAll('\\n', '\n'))),
                ],
              ),
            ),
          );
        });
  }
}
