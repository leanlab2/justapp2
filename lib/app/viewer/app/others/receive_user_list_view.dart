import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:provider/provider.dart';

class ReceiveUserListView extends StatefulWidget {
  final boardKey;
  final tabNum;
  final userMade;

  ReceiveUserListView({required this.boardKey, required this.tabNum, required this.userMade});

  @override
  _ReceiveUserListViewState createState() => _ReceiveUserListViewState();
}

class _ReceiveUserListViewState extends State<ReceiveUserListView> {

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<ViewerState, SettingState>(
        builder: (BuildContext context, ViewerState viewerState, SettingState settingState, child) {
          return Scaffold(
            appBar: AppBar(
              title: Text('유저 리스트', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
              centerTitle: true,
              leading: BackButton(
                  color: Colors.black
              ),
              backgroundColor: Colors.white,
            ),
            backgroundColor: selectTabBgColor(widget.tabNum, settingState),
            body: Column(
              children: [
                Expanded(
                    child: selectUserList(settingState, viewerState.userModel.receiveMap[widget.boardKey],
                        widget.userMade, KEY_CUSTOMAPP_SETTINGS_RECEIVE, widget.tabNum)),
              ],
            )
          );
        });
  }
}
