import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/board/read/read_board_view.dart';
import 'package:justapp/app/viewer/app/others/read_receive_user_view.dart';
import 'package:justapp/utils/my_progress_indicator.dart';
import 'package:justapp/utils/transitions.dart';
import 'package:provider/provider.dart';

class ReceiveMessageItem extends StatefulWidget {
  final String boardKey;
  final Map<String, dynamic> valueMap;
  final String userMade;
  final String funcType;
  final int tabNum;

  ReceiveMessageItem({required this.boardKey, required this.valueMap, required this.userMade, required this.funcType, required this.tabNum});

  @override
  _ReceiveMessageItemState createState() => _ReceiveMessageItemState();
}

class _ReceiveMessageItemState extends State<ReceiveMessageItem> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<ViewerState, SettingState>(builder: (BuildContext context, ViewerState viewerState, SettingState settingState, child) {
      print("dsnjklasdf");
      print(widget.valueMap![KEY_CUSTOMAPP_USERS_RECEIVE_MAP_USERKEY]);
      return Container(
        margin: const EdgeInsets.only(left: 5, right: 5, top: 20, bottom:10),
        child: ListTile(
          leading: Padding(
            padding: EdgeInsets.only(top:10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                    viewerState.userModelMap[widget.valueMap![KEY_CUSTOMAPP_USERS_RECEIVE_MAP_USERKEY]]!.userName+ ' ' +
                    widget.valueMap![KEY_CUSTOMAPP_USERS_RECEIVE_MAP_DATE].toDate().year.toString() + '-' +
                    widget.valueMap![KEY_CUSTOMAPP_USERS_RECEIVE_MAP_DATE].toDate().month.toString() + '-' +
                    widget.valueMap![KEY_CUSTOMAPP_USERS_RECEIVE_MAP_DATE].toDate().day.toString(),
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(height:5),
                Text(
                  widget.valueMap![KEY_CUSTOMAPP_USERS_RECEIVE_MAP_TITLE].toString(),
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),

              ],
            ),
          ),
          trailing: Container(width: 30, height: 30, child: SvgPicture.asset('assets/profile/icons/profileArrow.svg')),
          onTap: () =>
          {
            Navigator.of(context).push(
              Transitions(
                transitionType: TransitionType.slideLeft,
                duration: Duration(milliseconds: 200),
                curve: Curves.bounceInOut,
                reverseCurve: Curves.fastOutSlowIn,
                widget: ReadReceiveUserView(boardKey: widget.boardKey, valueMap: widget.valueMap, tabNum: widget.tabNum, userMade: widget.userMade),
              ),
            ),
          },
        ),
      );
    });
  }
}


