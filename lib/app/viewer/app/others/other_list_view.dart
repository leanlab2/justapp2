import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/common/common_viewer.dart';
import 'package:provider/provider.dart';

class OtherListView extends StatefulWidget {
  final tabNum;
  final userMade;
  final funcType;

  OtherListView({required this.tabNum, required this.userMade, required this.funcType});

  @override
  _OtherListViewState createState() => _OtherListViewState();
}

class _OtherListViewState extends State<OtherListView> {

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<ViewerState, SettingState>(
        builder: (BuildContext context, ViewerState viewerState, SettingState settingState, child) {
          return Scaffold(
            appBar: AppBar(
              title: Text(selectGetFuncAllType(settingState, widget.tabNum, widget.funcType,
                  KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE), style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
              centerTitle: true,
              leading: BackButton(
                  color: Colors.black
              ),
              backgroundColor: Colors.white,
            ),
            backgroundColor: selectTabBgColor(widget.tabNum, settingState),
            body: Column(
              children: [
                Expanded(
                    child: selectBoardList(
                        selectGetFuncAllType(settingState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE),
                        settingState, viewerState,
                        widget.funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE? viewerState.userModel.favoriteMap:
                        widget.funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE? viewerState.userModel.receiveMap:
                        widget.funcType == KEY_CUSTOMAPP_SETTINGS_SEND? viewerState.userModel.sendMap:
                        widget.funcType == KEY_CUSTOMAPP_SETTINGS_MADE? viewerState.userModel.madeMap:viewerState.userModel.madeMap,
                        widget.userMade, widget.funcType, widget.tabNum)),
              ],
            )
          );
        });
  }
}
