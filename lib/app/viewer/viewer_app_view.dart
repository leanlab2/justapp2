import 'package:flutter/material.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/tabs/viewer_tabs.dart';
import 'package:justapp/app/viewer/screens/splash_view.dart';
import 'package:justapp/utils/init_model.dart';
import 'package:provider/provider.dart';

class ViewerAppView extends StatefulWidget {
  final String userMade;
  final String userKey;
  ViewerAppView({required this.userMade, required this.userKey});

  @override
  _ViewerAppViewState createState() => _ViewerAppViewState();
}

class _ViewerAppViewState extends State<ViewerAppView> {
  bool isComplete = false;
  late Future _init1,_init2;
  bool isa = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ViewerState>(
          create: (_) => ViewerState(),
        ),
        ChangeNotifierProvider<SettingState>(
          create: (_) => SettingState(),
        ),
      ],
      child: MaterialApp(
        theme: ThemeData(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          hoverColor: Colors.transparent,
          focusColor: Colors.transparent,
        ),
        home: Consumer2<ViewerState, SettingState>(builder: (context,
            ViewerState viewerState, SettingState settingState,child) {
          if(isa) {
            _init1 = initViewerModel(viewerState, widget.userMade, widget.userKey);
            _init2 = initSettingModel(settingState, widget.userMade);
            isa = false;
          }
          return FutureBuilder(
              future: Future.wait([
                _init1,
                _init2
              ]),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (!isComplete) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                      return SplashView();
                    case ConnectionState.waiting:
                      return SplashView();
                    default:
                      isComplete = true;
                      return ViewerTabs(userMade: widget.userMade);
                  }
                } else {
                  return ViewerTabs(userMade: widget.userMade);
                }
              });
        }),
      ),
    );
  }
}
