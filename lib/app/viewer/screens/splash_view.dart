import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:justapp/app/const/constants.dart';

class SplashView extends StatefulWidget {

  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: Constants.size.width,
      child: Stack(
        children: [
          //background image
          Constants.prefs.getString(Constants.splashKey)  == null || Constants.prefs.getString(Constants.splashKey) == "splashImg"?
          Image.asset(
            'assets/auth/images/splashBg.png',
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.cover,
          ):
           CachedNetworkImage(
              imageUrl: Constants.prefs.getString(Constants.splashKey).toString(),
              fit: BoxFit.cover,
              width: double.infinity,
              height: double.infinity,
            ),

              // placeholder: (BuildContext context, String url) {
              //   return MyProgressIndicator(containerSize: 30);
              // })

          // Image.network( Constants.prefs.getString(Constants.splashKey).toString(),
          //   width: double.infinity,
          //   height: double.infinity,
          //   fit: BoxFit.cover,)
          // https://firebasestorage.googleapis.com/v0/b/justapp-1b25e.appspot.com/o/default%2FsplashImg.png?alt=media&token=bf93a727-52b4-47c8-8c38-045cb80a33d4

          // Provider.of<SettingState>(context, listen: false).entireModel.splashImg == 'splashImg'?
          // Image.asset(
          //   'assets/auth/images/splashBg.png',
          //   width: double.infinity,
          //   height: double.infinity,
          //   fit: BoxFit.cover,
          // ):
          // Image.network(
          //   Provider.of<SettingState>(context, listen: false).entireModel.splashImg,
          //   width: double.infinity,
          //   height: double.infinity,
          //   fit: BoxFit.cover,)

          // SvgPicture.asset(
          //   'assets/auth/images/splashBg.svg',
          //   width: double.infinity,
          //   height: double.infinity,
          //   fit: BoxFit.cover,
          // ),
        ],
      ));
  }
}
