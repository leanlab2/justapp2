import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/viewer/viewer_app_view.dart';
import 'package:permission_handler/permission_handler.dart';

class SelectViewerAppView extends StatefulWidget {
  final String userMade;
  final String template;
  SelectViewerAppView({required this.userMade, required this.template});

  @override
  _SelectViewerAppViewState createState() => _SelectViewerAppViewState();
}

class _SelectViewerAppViewState extends State<SelectViewerAppView> {

  @override
  void initState() {
    checkPermission();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: ListView(
        children: [
          Column(
              children: [
                //logo
                Container(
                  padding: EdgeInsets.only(top: 50),
                  child: Image.asset(
                    'assets/auth/images/appLogo.png',
                    width: Constants.size.width>300? 300: Constants.size.width * 0.8,
                    color: Colors.black,
                  ),
                ),
                Text(
                  "'제공자' 역할 또는 '사용자' 역할을 선택해 주세요.",
                  style: TextStyle(color: Colors.black, fontSize: 12),
                  overflow: TextOverflow.ellipsis,
                ),

                //button
                Container(
                  width: Constants.size.width>400? 400: Constants.size.width * 0.9,
                  padding: EdgeInsets.fromLTRB(30, 50, 30, 10),
                  child: FlatButton(
                    minWidth: double.infinity,
                    child: Text("사용자", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context)
                      {
                        // if (widget.template == 'community') {
                        return ViewerAppView(userMade: widget.userMade, userKey: KEY_CUSTOMAPP_USERS_CONSUMER);
                      }));
                    },
                    color: Color(Constants.defaultColor),
                    textColor: Colors.white,
                    padding: EdgeInsets.symmetric(vertical: 22),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                  ),
                ),

                //button
                Container(
                  width: Constants.size.width>400? 400: Constants.size.width * 0.9,
                  padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                  child: FlatButton(
                    minWidth: double.infinity,
                    child: Text("제공자", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                        // if(widget.template == 'community'){
                        return ViewerAppView(userMade: widget.userMade, userKey: KEY_CUSTOMAPP_USERS_PRODUCER);
                      }));
                    },
                    color: Color(Constants.defaultColor),
                    textColor: Colors.white,
                    padding: EdgeInsets.symmetric(vertical: 22),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                  ),
                ),
          ],
          ),
        ],
      ),);
  }

  Future<bool> checkPermission() async{
    Map<Permission, PermissionStatus> statuses =
    await[Permission.camera].request();
    bool per = true;
    statuses.forEach((permission, permissionStatus) {
      if(!permissionStatus.isGranted){
        per = false;
      }
    });
    return per;
  }
}