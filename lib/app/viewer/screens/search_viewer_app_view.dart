import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/viewer/viewer_app_view.dart';

class SearchViewerAppView extends StatefulWidget {
  @override
  _SearchViewerAppViewState createState() => _SearchViewerAppViewState();
}

class _SearchViewerAppViewState extends State<SearchViewerAppView> {
  final myController = TextEditingController();
  bool isSearchResult = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: ListView(
        children: [
          Column(
            children: [
              //logo
              Container(
                padding: EdgeInsets.only(top: 50),
                child: Image.asset(
                  'assets/auth/images/appLogo.png',
                  width: Constants.size.width>300? 300: Constants.size.width * 0.8,
                  color: Colors.black,
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 20),
                child: Text(
                  "원하시는 앱을 정확히 검색해 주세요.",
                  style: TextStyle(color: Colors.black, fontSize: 12),
                  overflow: TextOverflow.ellipsis,
                ),
              ),


              Container(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                // padding: EdgeInsets.only(top: 50),
                width: Constants.size.width>400? 400: Constants.size.width * 0.8,
                child: TextField(
                  controller: myController,
                ),
              ),


              //button
              Container(
                width: Constants.size.width>400? 400: Constants.size.width * 0.9,
                padding: EdgeInsets.fromLTRB(30, 30, 30, 20),
                child: FlatButton(
                  minWidth: double.infinity,
                  child: Text("검색", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                  onPressed: () {
                    getDocs(myController.text).then((value) => {
                      if(value == "NONE"){
                        setState((){
                          isSearchResult = false;
                        }),
                      }
                      else
                        {
                          Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                            // return SelectViewerAppView(userMade: myController.text, template: value);

                            return ViewerAppView(userMade: myController.text, userKey: KEY_CUSTOMAPP_USERS_PRODUCER);

                          }))
                        }
                    });
                  },
                  color: Color(Constants.defaultColor),
                  textColor: Colors.white,
                  padding: EdgeInsets.symmetric(vertical: 22),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                ),
              ),

              Text(isSearchResult? '': "'" + myController.text + "' 앱은 존재하지 않습니다. 다시 검색해 주세요.",
                  style: TextStyle(color: Colors.black, fontSize: 12)),
            ],
          ),
        ],
      ),
    );
  }


  Future<String> getDocs(String customAppName) async {
    DocumentSnapshot snapshot = await FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(myController.text).get();
    if (snapshot.data() != null) {
      String templateStr = snapshot.data()![KEY_TEMPLATE].toString();
      return templateStr;
    }
    else {
      return "NONE";
    }
  }
}


  //검색
  // Future getDocs(String customAppName) async {
  //   QuerySnapshot querySnapshot = await FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).get();
  //   for (int i = 0; i < querySnapshot.docs.length; i++) {
  //     if(querySnapshot.docs[i].toString() == customAppName){
  //
  //     }
  //   }
  // }

