import 'package:flutter/foundation.dart';
import 'package:justapp/app/models/firestore/admin_user_model.dart';

class AdminUserState extends ChangeNotifier {
  late AdminUserModel _adminUserModel;

  //AdminUserModel
  AdminUserModel get adminUserModel => _adminUserModel;
  set adminUserModel(AdminUserModel value){
    _adminUserModel = value;
    notifyListeners();
  }
}
