import 'package:flutter/foundation.dart';
import 'package:justapp/app/models/firestore/app_info.dart';
import 'package:justapp/app/models/firestore/board_model.dart';
import 'package:justapp/app/models/firestore/setting/custom_app_model.dart';
import 'package:justapp/app/models/firestore/user_model.dart';

class ViewerState extends ChangeNotifier {
  late UserModel _userModel;
  late AppInfo _appInfo;
  late CustomAppModel _customAppModel;
  Map<String, Map<String,dynamic>> _categoryModelMap = {};
  Map<String, BoardModel> _boardModelMap = {};
  Map<String, UserModel> _userModelMap = {};
  // late Map<String, UserModel> _userModelMap;
  String currentMarkerId = "";

  //appInfo
  AppInfo get appInfo => _appInfo;
  set appInfo(AppInfo value) {
    _appInfo = value;
  }

  //UserModel
  UserModel get userModel => _userModel;
  set userModel(UserModel value){
    _userModel = value;
    notifyListeners();
  }


  // _categoryModelMap
  Map<String, Map<String,dynamic>> get categoryModelMap => _categoryModelMap;
  set categoryModelMap(Map<String, Map<String,dynamic>> value) {
    _categoryModelMap = value;
    notifyListeners();
  }

  //CustomAppModel
  CustomAppModel get customAppModel => _customAppModel;
  set customAppModel(CustomAppModel value) {
    _customAppModel = value;
    notifyListeners();
  }

  //boardModel
  Map<String, BoardModel> get boardModelMap => _boardModelMap;
  set boardModelMap(Map<String, BoardModel> value) {
    _boardModelMap = value;
    print("dsnkjlfkjlndsnkjlsf");
    notifyListeners();
  }

  // UserModelMap
  Map<String, UserModel> get userModelMap => _userModelMap;
  set userModelMap(Map<String, UserModel> value){
    _userModelMap = value;
    notifyListeners();
  }


}
