import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:justapp/app/repo/admin_user_network_repository.dart';
import 'package:justapp/utils/simple_snackbar.dart';

class FirebaseAuthState extends ChangeNotifier {
  FirebaseAuthStatus _firebaseAuthStatus = FirebaseAuthStatus.progress;
  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  late User _firebaseUser;

  void watchAuthChange() {
    _firebaseAuth.authStateChanges().listen((User? firebaseUser) {
      print(firebaseUser);
      if (firebaseUser == null) {
        changeFirebaseAuthStatus(FirebaseAuthStatus.signout);
      } else{
        _firebaseUser = firebaseUser;
        changeFirebaseAuthStatus(FirebaseAuthStatus.signin);
      }
    });
  }

  Future<void> registerUser(BuildContext context,
      {required String email, required String password}) async {
    changeFirebaseAuthStatus(FirebaseAuthStatus.progress);

    UserCredential authResult = await _firebaseAuth
        .createUserWithEmailAndPassword(
        email: email.trim(), password: password.trim())
        .catchError((error) {
      print("error");
      print(error);

      String _message = "";
      switch (error.code) {
        case 'ERROR_WEAK_PASSWORD':
          _message = "ERROR_WEAK_PASSWORD";
          break;
        case 'ERROR_INVALID_EMAIL':
          _message = "ERROR_INVALID_EMAIL";
          break;
        case 'ERROR_EMAIL_ALREADY_IN_USE':
          _message = "ERROR_EMAIL_ALREADY_IN_USE";
          break;
      }

      SnackBar snackBar = SnackBar(
        content: Text(_message),
      );
      Scaffold.of(context).showSnackBar(snackBar);
    });

    if(authResult.user == null) {
      SnackBar snackBar = SnackBar(
        content: Text('Please try again later!'),
      );
      Scaffold.of(context).showSnackBar(snackBar);
    }
    else{
      _firebaseUser = authResult.user!;
      changeFirebaseAuthStatus(FirebaseAuthStatus.signin);
      await adminUserNetworkRepository.attemptCreateUser(authResult.user!.uid,authResult.user!.email!);
      Navigator.of(context).pop();
    }
  }

  Future<void> login(BuildContext context, {required String email, required String password}) async {
    changeFirebaseAuthStatus(FirebaseAuthStatus.progress);

    UserCredential authResult = await _firebaseAuth
        .signInWithEmailAndPassword(
        email: email.trim(), password: password.trim())
        .catchError((error) {
      changeFirebaseAuthStatus(FirebaseAuthStatus.signout);
      String _message = "";
      switch (error.code) {
        case 'invalid-email':
          _message = "정확한 이메일 주소를 입력하세요.";
          break;
        case 'user-disabled':
          _message = 'user-disabled';
          break;
        case 'user-not-found':
          _message = 'user-not-found';
          break;
        case 'wrong-password':
          _message = "비밀번호가 틀렸습니다.";
          break;
      }

      SnackBar snackBar = SnackBar(
        content: Text(_message),
      );
      Scaffold.of(context).showSnackBar(snackBar);
    });

    if(authResult.user == null) {
      SnackBar snackBar = SnackBar(
        content: Text('Please try again later!'),
      );
      Scaffold.of(context).showSnackBar(snackBar);
    }
    else{
      _firebaseUser = authResult.user!;
      changeFirebaseAuthStatus(FirebaseAuthStatus.signin);
    }
  }

  Future<void> signOut() async {
    // changeFirebaseAuthStatus(FirebaseAuthStatus.progress);
    changeFirebaseAuthStatus(FirebaseAuthStatus.signout);
    await _firebaseAuth.signOut();
    notifyListeners();
  }

  void changeFirebaseAuthStatus([FirebaseAuthStatus? firebaseAuthStatus]) {
    _firebaseAuthStatus = firebaseAuthStatus!;
    notifyListeners();
  }

  void sendResetEmail(BuildContext context, String email, String msg, String failMsg) async {
    try{
      _firebaseAuth.setLanguageCode(Platform.localeName.substring(0,2));
      await _firebaseAuth.sendPasswordResetEmail(email: email);
      simpleSnackBar(context, msg);
    }catch(e){
      simpleSnackBar(context, failMsg);
    }
  }

  FirebaseAuthStatus get firebaseAuthStatus => _firebaseAuthStatus;

  User get firebaseUser => _firebaseUser;
}

enum FirebaseAuthStatus { signout, progress, signin, reset }
