// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:justapp/app/const/firestore_keys.dart';
//
// class BoardModel {
//   final String boardKey;
//   final String boardImage;
//   final String boardTitle;
//   final String boardContent;
//   final String boardDate;
//   final String category;
//   final String userKey;
//
//   BoardModel(
//       {required this.boardKey,
//         required this.boardImage,
//         required this.boardTitle,
//         required this.boardContent,
//         required this.boardDate,
//         required this.category,
//         required this.userKey});
//
//   factory BoardModel.fromJson(Map<dynamic, dynamic> json) => BoardModel(
//     boardKey: json[KEY_CUSTOMAPP_BOARD_BOARDKEY],
//     boardImage: json[KEY_CUSTOMAPP_BOARD_BOARDIMAGE],
//     boardTitle: json[KEY_CUSTOMAPP_BOARD_BOARDTITLE],
//     boardContent: json[KEY_CUSTOMAPP_BOARD_BOARDCONTENT],
//     boardDate: json[KEY_CUSTOMAPP_BOARD_BOARDDATE],
//     category: json[KEY_CUSTOMAPP_BOARD_CATEGORY],
//     userKey: json[KEY_CUSTOMAPP_BOARD_USERKEY],
//   );
//
// }