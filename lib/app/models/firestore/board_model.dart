import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_place/google_place.dart';
import 'package:justapp/app/const/firestore_keys.dart';
class BoardModel {
  final String boardKey;
  final List<dynamic> boardImage;
  final List<dynamic> boardThumbnailImage;
  final String boardTitle;
  final String boardSubTItle;
  final String boardContents;
  final dynamic date;
  final String category;
  final String userKey;
  final String time;
  final dynamic termStartDate;
  final dynamic termEndDate;
  final String price;
  final int phoneNum;
  final String address;
  final bool isHidden;
  final bool isCallChecked;
  final bool isMessageChecked;
  final bool isMailChecked;
  final bool isLinkChecked;
  final String linkStr;
  final bool isOnlineChecked;
  final int favoriteCnt;
  final dynamic location;
  final List<dynamic> optionList;
  DocumentReference reference;

  BoardModel.fromMap(Map<String, dynamic> map, this.boardKey, {required this.reference})
      : boardImage = map.containsKey(KEY_CUSTOMAPP_BOARD_BOARDIMAGE)?map[KEY_CUSTOMAPP_BOARD_BOARDIMAGE]:[],
        boardThumbnailImage = map.containsKey(KEY_CUSTOMAPP_BOARD_THUMBNAILIMAGE)?map[KEY_CUSTOMAPP_BOARD_THUMBNAILIMAGE]:[],
        boardTitle = map.containsKey(KEY_CUSTOMAPP_BOARD_BOARDTITLE)?map[KEY_CUSTOMAPP_BOARD_BOARDTITLE]:"",
        boardSubTItle = map.containsKey(KEY_CUSTOMAPP_BOARD_BOARDSUBTITLE)?map[KEY_CUSTOMAPP_BOARD_BOARDSUBTITLE]:"",
        boardContents = map.containsKey(KEY_CUSTOMAPP_BOARD_BOARDCONTENTS)?map[KEY_CUSTOMAPP_BOARD_BOARDCONTENTS]:"",
        date = map.containsKey(KEY_CUSTOMAPP_BOARD_DATE)?map[KEY_CUSTOMAPP_BOARD_DATE]:DateTime.now(),
        category = map.containsKey(KEY_CUSTOMAPP_BOARD_CATEGORY)?map[KEY_CUSTOMAPP_BOARD_CATEGORY]:"",
        userKey = map.containsKey(KEY_CUSTOMAPP_BOARD_USERKEY)?map[KEY_CUSTOMAPP_BOARD_USERKEY]:"",
        location = map.containsKey(KEY_CUSTOMAPP_BOARD_LOCATION)?map[KEY_CUSTOMAPP_BOARD_LOCATION]:Location(lat:37.0,lng:128.0),
        time =  map.containsKey(KEY_CUSTOMAPP_BOARD_TIME)?map[KEY_CUSTOMAPP_BOARD_TIME]:"",
        termStartDate = map.containsKey(KEY_CUSTOMAPP_BOARD_TERM_START_DATE)?map[KEY_CUSTOMAPP_BOARD_TERM_START_DATE]:new DateTime.utc(1990,1,1),
        termEndDate = map.containsKey(KEY_CUSTOMAPP_BOARD_TERM_END_DATE)?map[KEY_CUSTOMAPP_BOARD_TERM_END_DATE]:new DateTime.utc(1990,1,1),
        price = map.containsKey(KEY_CUSTOMAPP_BOARD_PRICE)?map[KEY_CUSTOMAPP_BOARD_PRICE]:"",
        phoneNum = map.containsKey(KEY_CUSTOMAPP_BOARD_PHONENUM)?map[KEY_CUSTOMAPP_BOARD_PHONENUM]:0,
        address = map.containsKey(KEY_CUSTOMAPP_BOARD_ADDRESS)?map[KEY_CUSTOMAPP_BOARD_ADDRESS]:"",
        isHidden = map.containsKey(KEY_CUSTOMAPP_BOARD_ISHIDDEN)?map[KEY_CUSTOMAPP_BOARD_ISHIDDEN]:false,
        isCallChecked = map.containsKey(KEY_CUSTOMAPP_BOARD_IS_CALL_CHECKED)?map[KEY_CUSTOMAPP_BOARD_IS_CALL_CHECKED]:true,
        isMessageChecked = map.containsKey(KEY_CUSTOMAPP_BOARD_IS_MESSAGE_CHECKED)?map[KEY_CUSTOMAPP_BOARD_IS_MESSAGE_CHECKED]:false,
        isMailChecked = map.containsKey(KEY_CUSTOMAPP_BOARD_IS_MAIL_CHECKED)?map[KEY_CUSTOMAPP_BOARD_IS_MAIL_CHECKED]:false,
        isLinkChecked = map.containsKey(KEY_CUSTOMAPP_BOARD_IS_LINK_CHECKED)?map[KEY_CUSTOMAPP_BOARD_IS_LINK_CHECKED]:false,
        linkStr = map.containsKey(KEY_CUSTOMAPP_BOARD_IS_LINK_STR)?map[KEY_CUSTOMAPP_BOARD_IS_LINK_STR]:"",
        favoriteCnt = map.containsKey(KEY_CUSTOMAPP_BOARD_FAVORITE_CNT)?map[KEY_CUSTOMAPP_BOARD_FAVORITE_CNT]:0,
        isOnlineChecked = map.containsKey(KEY_CUSTOMAPP_BOARD_IS_ONLINE_CHECKED)?map[KEY_CUSTOMAPP_BOARD_IS_ONLINE_CHECKED]:false,
        optionList = map.containsKey(KEY_CUSTOMAPP_BOARD_OPTION_LIST) ? map[KEY_CUSTOMAPP_BOARD_OPTION_LIST] : [];

  BoardModel.fromSnapshot(DocumentSnapshot snapshot) : this.fromMap(snapshot.data()!, snapshot.id, reference: snapshot.reference);



  // BoardModel(
  //     {required this.boardKey,
  //       required this.boardImage,
  //       required this.boardTitle,
  //       required this.boardContent,
  //       required this.boardDate,
  //       required this.category,
  //       required this.userKey});
  //
  // factory BoardModel.fromJson(Map<dynamic, dynamic> json) => BoardModel(
  //   boardKey: json[KEY_CUSTOMAPP_BOARD_BOARDKEY],
  //   boardImage: json[KEY_CUSTOMAPP_BOARD_BOARDIMAGE],
  //   boardTitle: json[KEY_CUSTOMAPP_BOARD_BOARDTITLE],
  //   boardContent: json[KEY_CUSTOMAPP_BOARD_BOARDCONTENT],
  //   boardDate: json[KEY_CUSTOMAPP_BOARD_BOARDDATE],
  //   category: json[KEY_CUSTOMAPP_BOARD_CATEGORY],
  //   userKey: json[KEY_CUSTOMAPP_BOARD_USERKEY],
  // );

}