import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/const/firestore_keys.dart';

class AdminUserModel {
  String userKey;
  String userImg;
  String userEmail;
  String userName;
  String userBelong;
  String userMade;
  DocumentReference reference;

  AdminUserModel.fromMap(Map<String, dynamic> map, this.userKey, {required this.reference})
      : userImg = map[KEY_USERIMG],
        userEmail = map[KEY_USEREMAIL],
        userName = map[KEY_USERNAME],
        userBelong = map[KEY_USERBELONG],
        userMade = map[KEY_USERMADE];

  AdminUserModel.fromSnapshot(DocumentSnapshot snapshot) : this.fromMap(snapshot.data()!, snapshot.id, reference: snapshot.reference);

  static Map<String, dynamic> getMapForCreateUser(String email, String userKey) {
    Map<String, dynamic> map = Map();
    map[KEY_USEREMAIL] = email;
    map[KEY_USERKEY] = userKey;
    map[KEY_USERIMG] = '';
    map[KEY_USERNAME] = '';
    map[KEY_USERBELONG] = '';
    map[KEY_USERMADE] = '';
    return map;
  }
}
