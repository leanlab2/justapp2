import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/app/const/firestore_keys.dart';

class AppInfo {
  final int versionAndroid;
  final int versionIos;
  final int versionApp;
  final DocumentReference reference;

  AppInfo.fromMap(Map<String, dynamic> map, {required this.reference})
      : versionAndroid = map[KEY_CUSTOMAPP_APPINFO_VERSIONANDROID],
        versionIos = map[KEY_CUSTOMAPP_APPINFO_VERSIONIOS],
        versionApp = map[KEY_CUSTOMAPP_APPINFO_VERSIONAPP];

  AppInfo.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data()!, reference: snapshot.reference);

}
