import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/app/const/firestore_keys.dart';

class UserModel {
  String userKey;
  String userImg;
  String userEmail;
  String userName;
  String userBelong;
  String userPosition;
  int userPhoneNum;
  Map<String, dynamic> favoriteMap;
  Map<String, dynamic> sendMap;
  Map<String, dynamic> madeMap;
  Map<String, dynamic> receiveMap;
  DocumentReference reference;

  UserModel.fromMap(Map<String, dynamic> map, this.userKey, {required this.reference})
      : userImg = map.containsKey(KEY_CUSTOMAPP_USERS_USERIMG)?map[KEY_CUSTOMAPP_USERS_USERIMG]:"",
        userEmail = map.containsKey(KEY_CUSTOMAPP_USERS_USEREMAIL)?map[KEY_CUSTOMAPP_USERS_USEREMAIL]:"",
        userName = map.containsKey(KEY_CUSTOMAPP_USERS_USERNAME)?map[KEY_CUSTOMAPP_USERS_USERNAME]:"",
        userBelong = map.containsKey(KEY_CUSTOMAPP_USERS_USERBELONG)?map[KEY_CUSTOMAPP_USERS_USERBELONG]:"",
        userPosition = map.containsKey(KEY_CUSTOMAPP_USERS_POSITION)?map[KEY_CUSTOMAPP_USERS_POSITION]:"",
        userPhoneNum = map.containsKey(KEY_CUSTOMAPP_USERS_PHONE)?map[KEY_CUSTOMAPP_USERS_PHONE]:0,
        favoriteMap = map.containsKey(KEY_CUSTOMAPP_USERS_FAVORITE_MAP) ? map[KEY_CUSTOMAPP_USERS_FAVORITE_MAP] : {},
        sendMap = map.containsKey(KEY_CUSTOMAPP_USERS_SEND_MAP) ? map[KEY_CUSTOMAPP_USERS_SEND_MAP] : {},
        madeMap = map.containsKey(KEY_CUSTOMAPP_USERS_MADE_MAP) ? map[KEY_CUSTOMAPP_USERS_MADE_MAP] : {},
        receiveMap = map.containsKey(KEY_CUSTOMAPP_USERS_RECEIVE_MAP) ? map[KEY_CUSTOMAPP_USERS_RECEIVE_MAP] : {};

  UserModel.fromSnapshot(DocumentSnapshot snapshot) : this.fromMap(snapshot.data()!, snapshot.id, reference: snapshot.reference);

  static Map<String, dynamic> getMapForCreateUser(String email, String userKey) {
    Map<String, dynamic> map = Map();
    map[KEY_CUSTOMAPP_USERS_USEREMAIL] = email;
    map[KEY_CUSTOMAPP_USERS_USERKEY] = userKey;
    return map;
  }
}
