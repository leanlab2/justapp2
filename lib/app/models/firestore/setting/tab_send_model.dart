import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/app/const/firestore_keys.dart';

class TabSendModel {
  final String listType;
  final String connectType;
  final int listBgColor;
  final bool listShadow;
  final bool listDivision;
  final String naviTitle;
  final String naviReadTitle;
  final Map<dynamic, dynamic> fontTitle;
  final Map<dynamic, dynamic> fontSubTitle;
  final Map<dynamic, dynamic> fontContents;
  final DocumentReference reference;


  TabSendModel.fromMap(Map<String, dynamic> map, {required this.reference})
      : listType = map[KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE].toString(),
        connectType = map[KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE].toString(),
        listBgColor = map[KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR],
        listShadow = map[KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW],
        listDivision = map[KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION],
        naviTitle = map[KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE],
        naviReadTitle = map[KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE],
        fontTitle = map[KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE],
        fontSubTitle = map[KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE],
        fontContents = map[KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS];

  TabSendModel.fromSnapshot(DocumentSnapshot snapshot) : this.fromMap(snapshot.data()!, reference: snapshot.reference);

}