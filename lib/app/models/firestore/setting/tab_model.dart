import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/app/const/firestore_keys.dart';

class TabModel {
  final String funcPage;
  final int bgColor;
  final int tabIcon;
  final String tabTitle;
  final DocumentReference reference;


  TabModel.fromMap(Map<String, dynamic> map, {required this.reference})
      : funcPage = map[KEY_CUSTOMAPP_SETTINGS_TAB_FUNCPAGE].toString(),
        bgColor = map[KEY_CUSTOMAPP_SETTINGS_TAB_BGCOLOR],
        tabIcon = map[KEY_CUSTOMAPP_SETTINGS_TAB_ICON],
        tabTitle = map[KEY_CUSTOMAPP_SETTINGS_TAB_TITLE];

  TabModel.fromSnapshot(DocumentSnapshot snapshot) : this.fromMap(snapshot.data()!, reference: snapshot.reference);
}
