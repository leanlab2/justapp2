import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/app/const/firestore_keys.dart';

class StringModel {
  final String string;
  final DocumentReference reference;


  StringModel.fromMap(Map<String, dynamic> map, {required this.reference})
      : string = map[KEY_CUSTOMAPP_SETTINGS_STRING_STRING].toString();

  StringModel.fromSnapshot(DocumentSnapshot snapshot) : this.fromMap(snapshot.data()!, reference: snapshot.reference);

}