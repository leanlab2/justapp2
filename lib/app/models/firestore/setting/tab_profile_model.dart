import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/app/const/firestore_keys.dart';

class TabProfileModel {
  final int fontColor;
  final int fontSize;

  final bool isInfoActive;
  final String infoTitle;
  final int infoIcon;
  final bool isNameActive;
  final String nameTitle;
  final bool isPositionActive;
  final String positionTitle;

  final bool isMadeActive;
  final int madeIcon;
  final bool isSendActive;
  final int sendIcon;
  final bool isReceiveActive;
  final int receiveIcon;

  final bool isReportActive;
  final String reportTitle;
  final int reportIcon;
  final String reportEmail;

  final bool isRatingActive;
  final String ratingTitle;
  final int ratingIcon;
  final String ratingGoogleplayId;
  final String ratingAppsotreId;

  final bool isShareActive;
  final String shareTitle;
  final int shareIcon;
  final String shareMsg;

  final bool isSettingActive;
  final String settingTitle;
  final int settingIcon;
  final bool isAlramActive;
  final String alramTitle;
  final bool isPrivacyActive;
  final String privacyTitle;
  final String privacyUrl;
  final bool isProvisionActive;
  final String provisionTitle;
  final String provisionUrl;
  final bool isLicenseActive;
  final String licenseTitle;
  final String licenseUrl;
  final bool isAccountActive;
  final String accountAlign;
  final DocumentReference reference;


  TabProfileModel.fromMap(Map<String, dynamic> map, {required this.reference})
      : fontColor = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR],
        fontSize = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE],

        isInfoActive = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ISACTIVE],
        infoTitle = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_TITLE],
        infoIcon = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ICON],
        isNameActive = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_ISACTIVE],
        nameTitle = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_TITLE],
        isPositionActive = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_ISACTIVE],
        positionTitle = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_TITLE],

        isMadeActive = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ISACTIVE],
        madeIcon = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ICON],
        isSendActive = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ISACTIVE],
        sendIcon = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ICON],
        isReceiveActive = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ISACTIVE],
        receiveIcon = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ICON],

        isReportActive = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ISACTIVE],
        reportTitle = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_TITLE],
        reportIcon = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ICON],
        reportEmail = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_EMAIL],

        isRatingActive = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ISACTIVE],
        ratingTitle = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_TITLE],
        ratingIcon = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ICON],
        ratingGoogleplayId = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_GOOGLEPLAYID],
        ratingAppsotreId = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_APPSTOREID],

        isShareActive = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ISACTIVE],
        shareTitle = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_TITLE],
        shareIcon = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ICON],
        shareMsg = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_MSG],

        isSettingActive = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ISACTIVE],
        settingTitle = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_TITLE],
        settingIcon = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ICON],
        isAlramActive = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_ISACTIVE],
        alramTitle = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_TITLE],
        isPrivacyActive = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_ISACTIVE],
        privacyTitle = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_TITLE],
        privacyUrl = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_URL],
        isProvisionActive = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_ISACTIVE],
        provisionTitle = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_TITLE],
        provisionUrl = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_URL],
        isLicenseActive = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_ISACTIVE],
        licenseTitle = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_TITLE],
        licenseUrl = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_URL],
        isAccountActive = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ISACTIVE],
        accountAlign = map[KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ALIGN];

  TabProfileModel.fromSnapshot(DocumentSnapshot snapshot) : this.fromMap(snapshot.data()!, reference: snapshot.reference);

}


