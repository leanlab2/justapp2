import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/app/const/firestore_keys.dart';

class CustomAppModel {
  final String name;
  final String des;
  final String madeby;
  final String template;
  final String image;
  final String date;
  final DocumentReference reference;

  CustomAppModel.fromMap(Map<String, dynamic> map, {required this.reference})
      : name = map[KEY_NAME],
        des = map[KEY_DES],
        madeby = map[KEY_MADEBY],
        template = map[KEY_TEMPLATE],
        image = map[KEY_IMAGE],
        date = map[KEY_DATE];

  CustomAppModel.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data()!, reference: snapshot.reference);

}
