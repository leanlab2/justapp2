import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/app/const/firestore_keys.dart';

class TabWebModel {
  final String url;
  final DocumentReference reference;


  TabWebModel.fromMap(Map<String, dynamic> map, {required this.reference})
      : url = map[KEY_CUSTOMAPP_SETTINGS_WEB_URL].toString();

  TabWebModel.fromSnapshot(DocumentSnapshot snapshot) : this.fromMap(snapshot.data()!, reference: snapshot.reference);
}
