import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/app/const/firestore_keys.dart';

class EntireModel {
  final String splashImg;
  final int tabCount;
  final int categoryCount;
  final int defaultColor;
  final int tabColor;
  final int tabBgColor;
  final Map<String, dynamic> category1;
  final Map<String, dynamic> category2;
  final Map<String, dynamic> category3;
  final Map<String, dynamic> category4;
  final Map<String, dynamic> category5;
  final Map<String, dynamic> category6;
  final Map<String, dynamic> category7;
  final Map<String, dynamic> category8;
  final DocumentReference reference;


  EntireModel.fromMap(Map<String, dynamic> map, {required this.reference})
      : splashImg = map[KEY_CUSTOMAPP_SETTINGS_ENTIRE_SPLASHIMG].toString(),
        tabCount = map[KEY_CUSTOMAPP_SETTINGS_ENTIRE_TABCOUNT],
        categoryCount = map[KEY_CUSTOMAPP_SETTINGS_ENTIRE_CATEGORYCOUNT],
        defaultColor = map[KEY_CUSTOMAPP_SETTINGS_ENTIRE_DEFAULTCOLOR],
        tabColor = map[KEY_CUSTOMAPP_SETTINGS_ENTIRE_TABCOLOR],
        tabBgColor = map[KEY_CUSTOMAPP_SETTINGS_ENTIRE_TABBGCOLOR],
        category1 = map.containsKey(KEY_CUSTOMAPP_SETTINGS_CATEGORY1) ? map[KEY_CUSTOMAPP_SETTINGS_CATEGORY1] : {},
        category2 = map.containsKey(KEY_CUSTOMAPP_SETTINGS_CATEGORY2) ? map[KEY_CUSTOMAPP_SETTINGS_CATEGORY2] : {},
        category3 = map.containsKey(KEY_CUSTOMAPP_SETTINGS_CATEGORY3) ? map[KEY_CUSTOMAPP_SETTINGS_CATEGORY3] : {},
        category4 = map.containsKey(KEY_CUSTOMAPP_SETTINGS_CATEGORY4) ? map[KEY_CUSTOMAPP_SETTINGS_CATEGORY4] : {},
        category5 = map.containsKey(KEY_CUSTOMAPP_SETTINGS_CATEGORY5) ? map[KEY_CUSTOMAPP_SETTINGS_CATEGORY5] : {},
        category6 = map.containsKey(KEY_CUSTOMAPP_SETTINGS_CATEGORY6) ? map[KEY_CUSTOMAPP_SETTINGS_CATEGORY6] : {},
        category7 = map.containsKey(KEY_CUSTOMAPP_SETTINGS_CATEGORY7) ? map[KEY_CUSTOMAPP_SETTINGS_CATEGORY7] : {},
        category8 = map.containsKey(KEY_CUSTOMAPP_SETTINGS_CATEGORY8) ? map[KEY_CUSTOMAPP_SETTINGS_CATEGORY8] : {};

  EntireModel.fromSnapshot(DocumentSnapshot snapshot) : this.fromMap(snapshot.data()!, reference: snapshot.reference);

}