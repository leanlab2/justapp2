import 'package:flutter/material.dart';
import 'package:justapp/app/models/firestore/setting/string_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_board_model.dart';
import 'package:justapp/app/models/firestore/setting/entire_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_category_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_favorite_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_made_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_profile_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_receive_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_send_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_web_model.dart';


class SettingState extends ChangeNotifier {
  late EntireModel _entireModel;
  late StringModel _stringModel;
  late TabModel _tab1Model;
  late TabModel _tab2Model;
  late TabModel _tab3Model;
  late TabModel _tab4Model;
  late TabModel _tab5Model;

  //board
  late TabBoardModel _tab1BoardModel;
  late TabBoardModel _tab2BoardModel;
  late TabBoardModel _tab3BoardModel;
  late TabBoardModel _tab4BoardModel;
  late TabBoardModel _tab5BoardModel;

  //favorite
  late TabFavoriteModel _tab1FavoriteModel;
  late TabFavoriteModel _tab2FavoriteModel;
  late TabFavoriteModel _tab3FavoriteModel;
  late TabFavoriteModel _tab4FavoriteModel;
  late TabFavoriteModel _tab5FavoriteModel;

  //category
  late TabCategoryModel _tab1CategoryModel;
  late TabCategoryModel _tab2CategoryModel;
  late TabCategoryModel _tab3CategoryModel;
  late TabCategoryModel _tab4CategoryModel;
  late TabCategoryModel _tab5CategoryModel;

  //Made
  late TabMadeModel _tab1MadeModel;
  late TabMadeModel _tab2MadeModel;
  late TabMadeModel _tab3MadeModel;
  late TabMadeModel _tab4MadeModel;
  late TabMadeModel _tab5MadeModel;

  //Send
  late TabSendModel _tab1SendModel;
  late TabSendModel _tab2SendModel;
  late TabSendModel _tab3SendModel;
  late TabSendModel _tab4SendModel;
  late TabSendModel _tab5SendModel;

  //Receive
  late TabReceiveModel _tab1ReceiveModel;
  late TabReceiveModel _tab2ReceiveModel;
  late TabReceiveModel _tab3ReceiveModel;
  late TabReceiveModel _tab4ReceiveModel;
  late TabReceiveModel _tab5ReceiveModel;

  //web
  late TabWebModel _tab1WebModel;
  late TabWebModel _tab2WebModel;
  late TabWebModel _tab3WebModel;
  late TabWebModel _tab4WebModel;
  late TabWebModel _tab5WebModel;

  //profile
  late TabProfileModel _tab1ProfileModel;
  late TabProfileModel _tab2ProfileModel;
  late TabProfileModel _tab3ProfileModel;
  late TabProfileModel _tab4ProfileModel;
  late TabProfileModel _tab5ProfileModel;

  //EntireModel
  EntireModel get entireModel => _entireModel;
  set entireModel(EntireModel value) {
    _entireModel = value;
    notifyListeners();
  }

  //StringModel
  StringModel get stringModel => _stringModel;
  set stringModel(StringModel value) {
    _stringModel = value;
    notifyListeners();
  }

  //Tab1
  TabModel get tab1Model => _tab1Model;
  set tab1Model(TabModel value) {
    _tab1Model = value;
    notifyListeners();
  }
  //Tab2
  TabModel get tab2Model => _tab2Model;
  set tab2Model(TabModel value) {
    _tab2Model = value;
    notifyListeners();
  }
  //Tab3
  TabModel get tab3Model => _tab3Model;
  set tab3Model(TabModel value) {
    _tab3Model = value;
    notifyListeners();
  }
  //Tab4
  TabModel get tab4Model => _tab4Model;
  set tab4Model(TabModel value) {
    _tab4Model = value;
    notifyListeners();
  }
  //Tab5
  TabModel get tab5Model => _tab5Model;
  set tab5Model(TabModel value) {
    _tab5Model = value;
    notifyListeners();
  }


  //Tab1BoardModel
  TabBoardModel get tab1BoardModel => _tab1BoardModel;
  set tab1BoardModel(TabBoardModel value) {
    _tab1BoardModel = value;
    notifyListeners();
  }
  //Tab2BoardModel
  TabBoardModel get tab2BoardModel => _tab2BoardModel;
  set tab2BoardModel(TabBoardModel value) {
    _tab2BoardModel = value;
    notifyListeners();
  }
  //Tab3BoardModel
  TabBoardModel get tab3BoardModel => _tab3BoardModel;
  set tab3BoardModel(TabBoardModel value) {
    _tab3BoardModel = value;
    notifyListeners();
  }
  //Tab4BoardModel
  TabBoardModel get tab4BoardModel => _tab4BoardModel;
  set tab4BoardModel(TabBoardModel value) {
    _tab4BoardModel = value;
    notifyListeners();
  }
  //Tab5BoardModel
  TabBoardModel get tab5BoardModel => _tab5BoardModel;
  set tab5BoardModel(TabBoardModel value) {
    _tab5BoardModel = value;
    notifyListeners();
  }



  //Tab1FavoriteModel
  TabFavoriteModel get tab1FavoriteModel => _tab1FavoriteModel;
  set tab1FavoriteModel(TabFavoriteModel value) {
    _tab1FavoriteModel = value;
    notifyListeners();
  }
  //Tab2FavoriteModel
  TabFavoriteModel get tab2FavoriteModel => _tab2FavoriteModel;
  set tab2FavoriteModel(TabFavoriteModel value) {
    _tab2FavoriteModel = value;
    notifyListeners();
  }
  //Tab3FavoriteModel
  TabFavoriteModel get tab3FavoriteModel => _tab3FavoriteModel;
  set tab3FavoriteModel(TabFavoriteModel value) {
    _tab3FavoriteModel = value;
    notifyListeners();
  }
  //Tab4FavoriteModel
  TabFavoriteModel get tab4FavoriteModel => _tab4FavoriteModel;
  set tab4FavoriteModel(TabFavoriteModel value) {
    _tab4FavoriteModel = value;
    notifyListeners();
  }
  //Tab5FavoriteModel
  TabFavoriteModel get tab5FavoriteModel => _tab5FavoriteModel;
  set tab5FavoriteModel(TabFavoriteModel value) {
    _tab5FavoriteModel = value;
    notifyListeners();
  }


  //Tab1CategoryModel
  TabCategoryModel get tab1CategoryModel => _tab1CategoryModel;
  set tab1CategoryModel(TabCategoryModel value) {
    _tab1CategoryModel = value;
    notifyListeners();
  }
  //Tab2CategoryModel
  TabCategoryModel get tab2CategoryModel => _tab2CategoryModel;
  set tab2CategoryModel(TabCategoryModel value) {
    _tab2CategoryModel = value;
    notifyListeners();
  }
  //Tab3CategoryModel
  TabCategoryModel get tab3CategoryModel => _tab3CategoryModel;
  set tab3CategoryModel(TabCategoryModel value) {
    _tab3CategoryModel = value;
    notifyListeners();
  }
  //Tab4CategoryModel
  TabCategoryModel get tab4CategoryModel => _tab4CategoryModel;
  set tab4CategoryModel(TabCategoryModel value) {
    _tab4CategoryModel = value;
    notifyListeners();
  }
  //Tab5CategoryModel
  TabCategoryModel get tab5CategoryModel => _tab5CategoryModel;
  set tab5CategoryModel(TabCategoryModel value) {
    _tab5CategoryModel = value;
    notifyListeners();
  }


  //Tab1MadeModel
  TabMadeModel get tab1MadeModel => _tab1MadeModel;
  set tab1MadeModel(TabMadeModel value) {
    _tab1MadeModel = value;
    notifyListeners();
  }
  //Tab2MadeModel
  TabMadeModel get tab2MadeModel => _tab2MadeModel;
  set tab2MadeModel(TabMadeModel value) {
    _tab2MadeModel = value;
    notifyListeners();
  }
  //Tab3MadeModel
  TabMadeModel get tab3MadeModel => _tab3MadeModel;
  set tab3MadeModel(TabMadeModel value) {
    _tab3MadeModel = value;
    notifyListeners();
  }
  //Tab4MadeModel
  TabMadeModel get tab4MadeModel => _tab4MadeModel;
  set tab4MadeModel(TabMadeModel value) {
    _tab4MadeModel = value;
    notifyListeners();
  }
  //Tab5MadeModel
  TabMadeModel get tab5MadeModel => _tab5MadeModel;
  set tab5MadeModel(TabMadeModel value) {
    _tab5MadeModel = value;
    notifyListeners();
  }


  //Tab1SendModel
  TabSendModel get tab1SendModel => _tab1SendModel;
  set tab1SendModel(TabSendModel value) {
    _tab1SendModel = value;
    notifyListeners();
  }
  //Tab2SendModel
  TabSendModel get tab2SendModel => _tab2SendModel;
  set tab2SendModel(TabSendModel value) {
    _tab2SendModel = value;
    notifyListeners();
  }
  //Tab3SendModel
  TabSendModel get tab3SendModel => _tab3SendModel;
  set tab3SendModel(TabSendModel value) {
    _tab3SendModel = value;
    notifyListeners();
  }
  //Tab4SendModel
  TabSendModel get tab4SendModel => _tab4SendModel;
  set tab4SendModel(TabSendModel value) {
    _tab4SendModel = value;
    notifyListeners();
  }
  //Tab5SendModel
  TabSendModel get tab5SendModel => _tab5SendModel;
  set tab5SendModel(TabSendModel value) {
    _tab5SendModel = value;
    notifyListeners();
  }


  //Tab1ReceiveModel
  TabReceiveModel get tab1ReceiveModel => _tab1ReceiveModel;
  set tab1ReceiveModel(TabReceiveModel value) {
    _tab1ReceiveModel = value;
    notifyListeners();
  }
  //Tab2ReceiveModel
  TabReceiveModel get tab2ReceiveModel => _tab2ReceiveModel;
  set tab2ReceiveModel(TabReceiveModel value) {
    _tab2ReceiveModel = value;
    notifyListeners();
  }
  //Tab3ReceiveModel
  TabReceiveModel get tab3ReceiveModel => _tab3ReceiveModel;
  set tab3ReceiveModel(TabReceiveModel value) {
    _tab3ReceiveModel = value;
    notifyListeners();
  }
  //Tab4ReceiveModel
  TabReceiveModel get tab4ReceiveModel => _tab4ReceiveModel;
  set tab4ReceiveModel(TabReceiveModel value) {
    _tab4ReceiveModel = value;
    notifyListeners();
  }
  //Tab5ReceiveModel
  TabReceiveModel get tab5ReceiveModel => _tab5ReceiveModel;
  set tab5ReceiveModel(TabReceiveModel value) {
    _tab5ReceiveModel = value;
    notifyListeners();
  }


  //Tab1WebModel
  TabWebModel get tab1WebModel => _tab1WebModel;
  set tab1WebModel(TabWebModel value) {
    _tab1WebModel = value;
    notifyListeners();
  }
  //Tab2WebModel
  TabWebModel get tab2WebModel => _tab2WebModel;
  set tab2WebModel(TabWebModel value) {
    _tab2WebModel = value;
    notifyListeners();
  }
  //Tab3WebModel
  TabWebModel get tab3WebModel => _tab3WebModel;
  set tab3WebModel(TabWebModel value) {
    _tab3WebModel = value;
    notifyListeners();
  }
  //Tab4WebModel
  TabWebModel get tab4WebModel => _tab4WebModel;
  set tab4WebModel(TabWebModel value) {
    _tab4WebModel = value;
    notifyListeners();
  }
  //Tab5WebModel
  TabWebModel get tab5WebModel => _tab5WebModel;
  set tab5WebModel(TabWebModel value) {
    _tab5WebModel = value;
    notifyListeners();
  }


  //Tab1ProfileModel
  TabProfileModel get tab1ProfileModel => _tab1ProfileModel;
  set tab1ProfileModel(TabProfileModel value) {
    _tab1ProfileModel = value;
    notifyListeners();
  }
  //Tab2ProfileModel
  TabProfileModel get tab2ProfileModel => _tab2ProfileModel;
  set tab2ProfileModel(TabProfileModel value) {
    _tab2ProfileModel = value;
    notifyListeners();
  }
  //Tab3ProfileModel
  TabProfileModel get tab3ProfileModel => _tab3ProfileModel;
  set tab3ProfileModel(TabProfileModel value) {
    _tab3ProfileModel = value;
    notifyListeners();
  }
  //Tab4ProfileModel
  TabProfileModel get tab4ProfileModel => _tab4ProfileModel;
  set tab4ProfileModel(TabProfileModel value) {
    _tab4ProfileModel = value;
    notifyListeners();
  }
  //Tab5ProfileModel
  TabProfileModel get tab5ProfileModel => _tab5ProfileModel;
  set tab5ProfileModel(TabProfileModel value) {
    _tab5ProfileModel = value;
    notifyListeners();
  }






  //pageIndex
  late int _pageIndex = 0;
  int get pageIndex => _pageIndex;
  set pageIndex(int value) {
    _pageIndex = value;
    notifyListeners();
  }

  //simple viewer entire
  late bool _isEntireTabSimpleViewer = true;
  bool get isEntireTabSimpleViewer => _isEntireTabSimpleViewer;
  set isEntireTabSimpleViewer(bool value) {
    _isEntireTabSimpleViewer = value;
    notifyListeners();
  }
  late bool _isEntireStyleSimpleViewer = true;
  bool get isEntireStyleSimpleViewer => _isEntireStyleSimpleViewer;
  set isEntireStyleSimpleViewer(bool value) {
    _isEntireStyleSimpleViewer = value;
    notifyListeners();
  }

  //board
  late bool _isBoardTabSimpleViewer = true;
  bool get isBoardTabSimpleViewer => _isBoardTabSimpleViewer;
  set isBoardTabSimpleViewer(bool value) {
    _isBoardTabSimpleViewer = value;
    notifyListeners();
  }
  late bool _isBoardStyleSimpleViewer = true;
  bool get isBoardStyleSimpleViewer => _isBoardStyleSimpleViewer;
  set isBoardStyleSimpleViewer(bool value) {
    _isBoardStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isBoardListStyleSimpleViewer = true;
  bool get isBoardListStyleSimpleViewer => _isBoardListStyleSimpleViewer;
  set isBoardListStyleSimpleViewer(bool value) {
    _isBoardListStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isBoardNaviStyleSimpleViewer = true;
  bool get isBoardNaviStyleSimpleViewer => _isBoardNaviStyleSimpleViewer;
  set isBoardNaviStyleSimpleViewer(bool value) {
    _isBoardNaviStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isBoardFontStyleSimpleViewer = true;
  bool get isBoardFontStyleSimpleViewer => _isBoardFontStyleSimpleViewer;
  set isBoardFontStyleSimpleViewer(bool value) {
    _isBoardFontStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isBoardSubFontStyleSimpleViewer = true;
  bool get isBoardSubFontStyleSimpleViewer => _isBoardSubFontStyleSimpleViewer;
  set isBoardSubFontStyleSimpleViewer(bool value) {
    _isBoardSubFontStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isBoardContentsFontStyleSimpleViewer = true;
  bool get isBoardContentsFontStyleSimpleViewer => _isBoardContentsFontStyleSimpleViewer;
  set isBoardContentsFontStyleSimpleViewer(bool value) {
    _isBoardContentsFontStyleSimpleViewer = value;
    notifyListeners();
  }

  //favorite
  late bool _isFavoriteTabSimpleViewer = true;
  bool get isFavoriteTabSimpleViewer => _isFavoriteTabSimpleViewer;
  set isFavoriteTabSimpleViewer(bool value) {
    _isFavoriteTabSimpleViewer = value;
    notifyListeners();
  }
  late bool _isFavoriteStyleSimpleViewer = true;
  bool get isFavoriteStyleSimpleViewer => _isFavoriteStyleSimpleViewer;
  set isFavoriteStyleSimpleViewer(bool value) {
    _isFavoriteStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isFavoriteListStyleSimpleViewer = true;
  bool get isFavoriteListStyleSimpleViewer => _isFavoriteListStyleSimpleViewer;
  set isFavoriteListStyleSimpleViewer(bool value) {
    _isFavoriteListStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isFavoriteNaviStyleSimpleViewer = true;
  bool get isFavoriteNaviStyleSimpleViewer => _isFavoriteNaviStyleSimpleViewer;
  set isFavoriteNaviStyleSimpleViewer(bool value) {
    _isFavoriteNaviStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isFavoriteFontStyleSimpleViewer = true;
  bool get isFavoriteFontStyleSimpleViewer => _isFavoriteFontStyleSimpleViewer;
  set isFavoriteFontStyleSimpleViewer(bool value) {
    _isFavoriteFontStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isFavoriteSubFontStyleSimpleViewer = true;
  bool get isFavoriteSubFontStyleSimpleViewer => _isFavoriteSubFontStyleSimpleViewer;
  set isFavoriteSubFontStyleSimpleViewer(bool value) {
    _isFavoriteSubFontStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isFavoriteContentsFontStyleSimpleViewer = true;
  bool get isFavoriteContentsFontStyleSimpleViewer => _isFavoriteContentsFontStyleSimpleViewer;
  set isFavoriteContentsFontStyleSimpleViewer(bool value) {
    _isFavoriteContentsFontStyleSimpleViewer = value;
    notifyListeners();
  }

  //profile
  late bool _isProfileTabSimpleViewer = true;
  bool get isProfileTabSimpleViewer => _isProfileTabSimpleViewer;
  set isProfileTabSimpleViewer(bool value) {
    _isProfileTabSimpleViewer = value;
    notifyListeners();
  }
  late bool _isProfileStyleSimpleViewer = true;
  bool get isProfileStyleSimpleViewer => _isProfileStyleSimpleViewer;
  set isProfileStyleSimpleViewer(bool value) {
    _isProfileStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isProfileFontSimpleViewer = true;
  bool get isProfileFontSimpleViewer => _isProfileFontSimpleViewer;
  set isProfileFontSimpleViewer(bool value) {
    _isProfileFontSimpleViewer = value;
    notifyListeners();
  }
  late bool _isProfileListStyleSimpleViewer = true;
  bool get isProfileListStyleSimpleViewer => _isProfileListStyleSimpleViewer;
  set isProfileListStyleSimpleViewer(bool value) {
    _isProfileListStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isProfileNaviStyleSimpleViewer = true;
  bool get isProfileNaviStyleSimpleViewer => _isProfileNaviStyleSimpleViewer;
  set isProfileNaviStyleSimpleViewer(bool value) {
    _isProfileNaviStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isProfileFontStyleSimpleViewer = true;
  bool get isProfileFontStyleSimpleViewer => _isProfileFontStyleSimpleViewer;
  set isProfileFontStyleSimpleViewer(bool value) {
    _isProfileFontStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isProfileSubFontStyleSimpleViewer = true;
  bool get isProfileSubFontStyleSimpleViewer => _isProfileSubFontStyleSimpleViewer;
  set isProfileSubFontStyleSimpleViewer(bool value) {
    _isProfileSubFontStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isProfileContentsFontStyleSimpleViewer = true;
  bool get isProfileContentsFontStyleSimpleViewer => _isProfileContentsFontStyleSimpleViewer;
  set isProfileContentsFontStyleSimpleViewer(bool value) {
    _isProfileContentsFontStyleSimpleViewer = value;
    notifyListeners();
  }

  //made
  late bool _isMadeListStyleSimpleViewer = true;
  bool get isMadeListStyleSimpleViewer => _isMadeListStyleSimpleViewer;
  set isMadeListStyleSimpleViewer(bool value) {
    _isMadeListStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isMadeNaviStyleSimpleViewer = true;
  bool get isMadeNaviStyleSimpleViewer => _isMadeNaviStyleSimpleViewer;
  set isMadeNaviStyleSimpleViewer(bool value) {
    _isMadeNaviStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isMadeFontStyleSimpleViewer = true;
  bool get isMadeFontStyleSimpleViewer => _isMadeFontStyleSimpleViewer;
  set isMadeFontStyleSimpleViewer(bool value) {
    _isMadeFontStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isMadeSubFontStyleSimpleViewer = true;
  bool get isMadeSubFontStyleSimpleViewer => _isMadeSubFontStyleSimpleViewer;
  set isMadeSubFontStyleSimpleViewer(bool value) {
    _isMadeSubFontStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isMadeContentsFontStyleSimpleViewer = true;
  bool get isMadeContentsFontStyleSimpleViewer => _isMadeContentsFontStyleSimpleViewer;
  set isMadeContentsFontStyleSimpleViewer(bool value) {
    _isMadeContentsFontStyleSimpleViewer = value;
    notifyListeners();
  }


  //send
  late bool _isSendListStyleSimpleViewer = true;
  bool get isSendListStyleSimpleViewer => _isSendListStyleSimpleViewer;
  set isSendListStyleSimpleViewer(bool value) {
    _isSendListStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isSendNaviStyleSimpleViewer = true;
  bool get isSendNaviStyleSimpleViewer => _isSendNaviStyleSimpleViewer;
  set isSendNaviStyleSimpleViewer(bool value) {
    _isSendNaviStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isSendFontStyleSimpleViewer = true;
  bool get isSendFontStyleSimpleViewer => _isSendFontStyleSimpleViewer;
  set isSendFontStyleSimpleViewer(bool value) {
    _isSendFontStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isSendSubFontStyleSimpleViewer = true;
  bool get isSendSubFontStyleSimpleViewer => _isSendSubFontStyleSimpleViewer;
  set isSendSubFontStyleSimpleViewer(bool value) {
    _isSendSubFontStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isSendContentsFontStyleSimpleViewer = true;
  bool get isSendContentsFontStyleSimpleViewer => _isSendContentsFontStyleSimpleViewer;
  set isSendContentsFontStyleSimpleViewer(bool value) {
    _isSendContentsFontStyleSimpleViewer = value;
    notifyListeners();
  }


  //receive
  late bool _isReceiveListStyleSimpleViewer = true;
  bool get isReceiveListStyleSimpleViewer => _isReceiveListStyleSimpleViewer;
  set isReceiveListStyleSimpleViewer(bool value) {
    _isReceiveListStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isReceiveNaviStyleSimpleViewer = true;
  bool get isReceiveNaviStyleSimpleViewer => _isReceiveNaviStyleSimpleViewer;
  set isReceiveNaviStyleSimpleViewer(bool value) {
    _isReceiveNaviStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isReceiveFontStyleSimpleViewer = true;
  bool get isReceiveFontStyleSimpleViewer => _isReceiveFontStyleSimpleViewer;
  set isReceiveFontStyleSimpleViewer(bool value) {
    _isReceiveFontStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isReceiveSubFontStyleSimpleViewer = true;
  bool get isReceiveSubFontStyleSimpleViewer => _isReceiveSubFontStyleSimpleViewer;
  set isReceiveSubFontStyleSimpleViewer(bool value) {
    _isReceiveSubFontStyleSimpleViewer = value;
    notifyListeners();
  }
  late bool _isReceiveContentsFontStyleSimpleViewer = true;
  bool get isReceiveContentsFontStyleSimpleViewer => _isReceiveContentsFontStyleSimpleViewer;
  set isReceiveContentsFontStyleSimpleViewer(bool value) {
    _isReceiveContentsFontStyleSimpleViewer = value;
    notifyListeners();
  }


  clear() {
    print("CLEAER");
    // _currentStreamSub.cancel();
  }

}
