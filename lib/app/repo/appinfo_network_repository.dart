import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/firestore/app_info.dart';
import 'package:justapp/app/repo/helper/transformer.dart';

class AppInfoNetworkRepository with Transformers {

  Future<void> createAppInfo(int versionAndroid, int versionIos, int versionApp, String userMade) async {
    final DocumentReference boardRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_APPINFO).doc(userMade);
    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.set(boardRef, {KEY_CUSTOMAPP_APPINFO_VERSIONANDROID: versionAndroid, KEY_CUSTOMAPP_APPINFO_VERSIONIOS:versionIos, KEY_CUSTOMAPP_APPINFO_VERSIONAPP:versionApp});
    });
  }

  Future<AppInfo> getAppInfo(String userMade) async {
    final DocumentReference userRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_APPINFO).doc(userMade);
    DocumentSnapshot snapshot = await userRef.get();
    print("getAppInfo");
    print(snapshot.data());
    return AppInfo.fromSnapshot(snapshot);
  }
}

AppInfoNetworkRepository appInfoNetworkRepository = AppInfoNetworkRepository();
