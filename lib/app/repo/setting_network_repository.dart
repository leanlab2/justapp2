import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/firestore/setting/string_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_board_model.dart';
import 'package:justapp/app/models/firestore/setting/entire_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_category_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_favorite_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_made_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_profile_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_receive_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_send_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_web_model.dart';
import 'package:justapp/app/repo/helper/transformer.dart';

class SettingNetworkRepository with Transformers {

  Future<void> createSetting(String userMade) async {
    //Entire
    final DocumentReference entireRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(
        COLLECTION_CUSTOMAPP_SETTINGS)
        .doc(KEY_CUSTOMAPP_SETTINGS_ENTIRE);
    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.set(entireRef,
          {KEY_CUSTOMAPP_SETTINGS_ENTIRE_SPLASHIMG: 'splashImg', KEY_CUSTOMAPP_SETTINGS_ENTIRE_TABCOUNT: 3, KEY_CUSTOMAPP_SETTINGS_ENTIRE_CATEGORYCOUNT: 4,
            KEY_CUSTOMAPP_SETTINGS_ENTIRE_DEFAULTCOLOR: Constants.blackColor, KEY_CUSTOMAPP_SETTINGS_ENTIRE_TABCOLOR: Constants.blackColor, KEY_CUSTOMAPP_SETTINGS_ENTIRE_TABBGCOLOR: Constants.whiteColor,
            KEY_CUSTOMAPP_SETTINGS_CATEGORY1 : {KEY_CUSTOMAPP_SETTINGS_CATEGORY_ICON:58136, KEY_CUSTOMAPP_SETTINGS_CATEGORY_NAME:'category_1'},
            KEY_CUSTOMAPP_SETTINGS_CATEGORY2 : {KEY_CUSTOMAPP_SETTINGS_CATEGORY_ICON:58136, KEY_CUSTOMAPP_SETTINGS_CATEGORY_NAME:'category_2'},
            KEY_CUSTOMAPP_SETTINGS_CATEGORY3 : {KEY_CUSTOMAPP_SETTINGS_CATEGORY_ICON:58136, KEY_CUSTOMAPP_SETTINGS_CATEGORY_NAME:'category_3'},
            KEY_CUSTOMAPP_SETTINGS_CATEGORY4 : {KEY_CUSTOMAPP_SETTINGS_CATEGORY_ICON:58136, KEY_CUSTOMAPP_SETTINGS_CATEGORY_NAME:'category_4'},
            KEY_CUSTOMAPP_SETTINGS_CATEGORY5 : {KEY_CUSTOMAPP_SETTINGS_CATEGORY_ICON:58136, KEY_CUSTOMAPP_SETTINGS_CATEGORY_NAME:'category_5'},
            KEY_CUSTOMAPP_SETTINGS_CATEGORY6 : {KEY_CUSTOMAPP_SETTINGS_CATEGORY_ICON:58136, KEY_CUSTOMAPP_SETTINGS_CATEGORY_NAME:'category_6'},
            KEY_CUSTOMAPP_SETTINGS_CATEGORY7 : {KEY_CUSTOMAPP_SETTINGS_CATEGORY_ICON:58136, KEY_CUSTOMAPP_SETTINGS_CATEGORY_NAME:'category_7'},
            KEY_CUSTOMAPP_SETTINGS_CATEGORY8 : {KEY_CUSTOMAPP_SETTINGS_CATEGORY_ICON:58136, KEY_CUSTOMAPP_SETTINGS_CATEGORY_NAME:'category_8'}
          });
    });
    //String
    final DocumentReference stringRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(
        COLLECTION_CUSTOMAPP_SETTINGS).doc(KEY_CUSTOMAPP_SETTINGS_STRING);
    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.set(stringRef,{KEY_CUSTOMAPP_SETTINGS_STRING_STRING:'string'});
    });

    //tab
    selectSettingInit(KEY_CUSTOMAPP_SETTINGS_TAB1, userMade);
    selectSettingInit(KEY_CUSTOMAPP_SETTINGS_TAB2, userMade);
    selectSettingInit(KEY_CUSTOMAPP_SETTINGS_TAB3, userMade);
    selectSettingInit(KEY_CUSTOMAPP_SETTINGS_TAB4, userMade);
    selectSettingInit(KEY_CUSTOMAPP_SETTINGS_TAB5, userMade);
  }

  //entire,string,tab
  Future<void> selectSetTab(String userMade, int tabNum, setField, temp) async{
    final DocumentReference settingRef = FirebaseFirestore.instance
        .collection(COLLECTION_CUSTOMAPP)
        .doc(userMade)
        .collection(COLLECTION_CUSTOMAPP_SETTINGS)
        .doc(tabNum==-1?KEY_CUSTOMAPP_SETTINGS_ENTIRE:tabNum==0?KEY_CUSTOMAPP_SETTINGS_STRING:tabNum==1?KEY_CUSTOMAPP_SETTINGS_TAB1:tabNum==2?KEY_CUSTOMAPP_SETTINGS_TAB2:
              tabNum==3?KEY_CUSTOMAPP_SETTINGS_TAB3:tabNum==4?KEY_CUSTOMAPP_SETTINGS_TAB4:KEY_CUSTOMAPP_SETTINGS_TAB5);
    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.update(settingRef, {
        setField: temp
      });
    });
  }

  Future<dynamic> selectGetTab(String userMade, String tabNumStr, tabModel) async{
    final DocumentReference userRef = FirebaseFirestore.instance
        .collection(COLLECTION_CUSTOMAPP)
        .doc(userMade)
        .collection(COLLECTION_CUSTOMAPP_SETTINGS)
        .doc(tabNumStr);
    DocumentSnapshot snapshot = await userRef.get();

    return tabModel==EntireModel?EntireModel.fromSnapshot(snapshot):tabModel==TabModel?TabModel.fromSnapshot(snapshot):StringModel.fromSnapshot(snapshot);

  }
  selectGetTabStream(String userMade, String tabNumStr, toTabStreamStr) {
    return FirebaseFirestore.instance
        .collection(COLLECTION_CUSTOMAPP)
        .doc(userMade)
        .collection(COLLECTION_CUSTOMAPP_SETTINGS)
        .doc(tabNumStr)
        .snapshots()
        .transform(toTabStreamStr=='entire'?toEntire:toTabStreamStr=='tab'?toTab:toTabString);
  }

  //tab_func
  Future<void> selectSetTabFunc(String userMade, int tabNum, funcType, setField, temp) async{
    final DocumentReference settingRef = FirebaseFirestore.instance
        .collection(COLLECTION_CUSTOMAPP)
        .doc(userMade)
        .collection(COLLECTION_CUSTOMAPP_SETTINGS)
        .doc(tabNum==1?KEY_CUSTOMAPP_SETTINGS_TAB1:tabNum==2?KEY_CUSTOMAPP_SETTINGS_TAB2:tabNum==3?KEY_CUSTOMAPP_SETTINGS_TAB3:
             tabNum==4?KEY_CUSTOMAPP_SETTINGS_TAB4:KEY_CUSTOMAPP_SETTINGS_TAB5)
        .collection(KEY_CUSTOMAPP_SETTINGS_COLLECTION_FUNC)
        .doc(funcType);
    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.update(settingRef, {
        setField: temp
      });
    });
  }
  selectGetTabFunc(String userMade, String tabNumStr, funcType, tabModel) async{
    final DocumentReference userRef = FirebaseFirestore.instance
        .collection(COLLECTION_CUSTOMAPP)
        .doc(userMade)
        .collection(COLLECTION_CUSTOMAPP_SETTINGS)
        .doc(tabNumStr)
        .collection(KEY_CUSTOMAPP_SETTINGS_COLLECTION_FUNC)
        .doc(funcType);
    DocumentSnapshot snapshot = await userRef.get();
    print(snapshot);
    return tabModel==TabBoardModel?TabBoardModel.fromSnapshot(snapshot): tabModel==TabFavoriteModel?TabFavoriteModel.fromSnapshot(snapshot):
    tabModel==TabCategoryModel?TabCategoryModel.fromSnapshot(snapshot):tabModel==TabMadeModel?TabMadeModel.fromSnapshot(snapshot):
    tabModel==TabSendModel?TabSendModel.fromSnapshot(snapshot):tabModel==TabReceiveModel?TabReceiveModel.fromSnapshot(snapshot):
    tabModel==TabWebModel?TabWebModel.fromSnapshot(snapshot):TabProfileModel.fromSnapshot(snapshot);

  }
  selectGetTabFuncStream(String userMade, String tabNumStr, funcType) {
    return FirebaseFirestore.instance
        .collection(COLLECTION_CUSTOMAPP)
        .doc(userMade)
        .collection(COLLECTION_CUSTOMAPP_SETTINGS)
        .doc(tabNumStr)
        .collection(KEY_CUSTOMAPP_SETTINGS_COLLECTION_FUNC)
        .doc(funcType)
        .snapshots()
        .transform(funcType==KEY_CUSTOMAPP_SETTINGS_BOARD?toTabBoard:funcType==KEY_CUSTOMAPP_SETTINGS_FAVORITE?toTabFavorite:
                    funcType==KEY_CUSTOMAPP_SETTINGS_CATEGORY?toTabCategory:funcType==KEY_CUSTOMAPP_SETTINGS_MADE?toTabMade:
                    funcType==KEY_CUSTOMAPP_SETTINGS_SEND?toTabSend:funcType==KEY_CUSTOMAPP_SETTINGS_RECEIVE?toTabReceive:
                    funcType==KEY_CUSTOMAPP_SETTINGS_WEB?toTabWeb:toTabProfile);
  }
}

selectSettingInit(String tabNum, String userMade){
  //tab
  final DocumentReference tabRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_SETTINGS)
      .doc(tabNum);
  FirebaseFirestore.instance.runTransaction((tx) async {
    tx.set(tabRef, {KEY_CUSTOMAPP_SETTINGS_TAB_FUNCPAGE: tabNum==KEY_CUSTOMAPP_SETTINGS_TAB1? KEY_CUSTOMAPP_SETTINGS_BOARD:
    tabNum==KEY_CUSTOMAPP_SETTINGS_TAB2? KEY_CUSTOMAPP_SETTINGS_FAVORITE : tabNum==KEY_CUSTOMAPP_SETTINGS_TAB3? KEY_CUSTOMAPP_SETTINGS_PROFILE : KEY_CUSTOMAPP_SETTINGS_BOARD,
      KEY_CUSTOMAPP_SETTINGS_TAB_ICON: 58136, KEY_CUSTOMAPP_SETTINGS_TAB_BGCOLOR: Constants.whiteColor, KEY_CUSTOMAPP_SETTINGS_TAB_TITLE: 'tab'});
  });
  Map<dynamic, dynamic> fontTitle = {KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN: 'left', KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR: Constants.blackColor, KEY_CUSTOMAPP_SETTINGS_FUNC_ISACTIVE:true, KEY_CUSTOMAPP_SETTINGS_FUNC_LINE:1, KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE:15};
  Map<dynamic, dynamic> fontSubTitle = {KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN: 'left', KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR: Constants.blackColor, KEY_CUSTOMAPP_SETTINGS_FUNC_ISACTIVE:true, KEY_CUSTOMAPP_SETTINGS_FUNC_LINE:2, KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE:15};
  Map<dynamic, dynamic> fontContents = {KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN: 'left', KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR: Constants.blackColor, KEY_CUSTOMAPP_SETTINGS_FUNC_ISACTIVE:true, KEY_CUSTOMAPP_SETTINGS_FUNC_LINE:3, KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE:15};
  //tab_board
  final DocumentReference tabBoardRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_SETTINGS)
      .doc(tabNum).collection(KEY_CUSTOMAPP_SETTINGS_COLLECTION_FUNC).doc(KEY_CUSTOMAPP_SETTINGS_BOARD);
  FirebaseFirestore.instance.runTransaction((tx) async {
    tx.set(tabBoardRef, {KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE: 'list', KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE : 'work', KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR : Constants.whiteColor,
      KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW : false, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION : true,
      KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE: '리스트', KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE: '글 보기', KEY_CUSTOMAPP_SETTINGS_BOARD_NAVI_CREATETITLE: '글 쓰기',
      KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE: fontTitle, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE : fontSubTitle, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS : fontContents,});
  });
  //tab_favorite
  final DocumentReference tabFavoriteRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_SETTINGS)
      .doc(tabNum).collection(KEY_CUSTOMAPP_SETTINGS_COLLECTION_FUNC).doc(KEY_CUSTOMAPP_SETTINGS_FAVORITE);
  FirebaseFirestore.instance.runTransaction((tx) async {
    tx.set(tabFavoriteRef, {KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE: 'list', KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE : 'work', KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR : Constants.whiteColor,
      KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW : false, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION : true,
      KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE: 'title', KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE: 'title',
      KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE: fontTitle, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE : fontSubTitle, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS : fontContents});
  });
  //tab_category
  final DocumentReference tabCategoryRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_SETTINGS)
      .doc(tabNum).collection(KEY_CUSTOMAPP_SETTINGS_COLLECTION_FUNC).doc(KEY_CUSTOMAPP_SETTINGS_CATEGORY);
  FirebaseFirestore.instance.runTransaction((tx) async {
    tx.set(tabCategoryRef, {KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE: 'list', KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE : 'work', KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR : Constants.whiteColor,
      KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW : false, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION : true,
      KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE: '카테고리', KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE: '글 보기',
      KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE: fontTitle, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE : fontSubTitle, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS : fontContents});
  });
  //tab_made
  final DocumentReference tabMadeRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_SETTINGS)
      .doc(tabNum).collection(KEY_CUSTOMAPP_SETTINGS_COLLECTION_FUNC).doc(KEY_CUSTOMAPP_SETTINGS_MADE);
  FirebaseFirestore.instance.runTransaction((tx) async {
    tx.set(tabMadeRef, {KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE: 'list', KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE : 'work', KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR : Constants.whiteColor,
      KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW : false, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION : true,
      KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE: '내 글 리스트', KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE: '글 보기', KEY_CUSTOMAPP_SETTINGS_MADE_NAVI_UPDATETITLE: '글 수정',
      KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE: fontTitle, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE : fontSubTitle, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS : fontContents});
  });
  //tab_send
  final DocumentReference tabSendRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_SETTINGS)
      .doc(tabNum).collection(KEY_CUSTOMAPP_SETTINGS_COLLECTION_FUNC).doc(KEY_CUSTOMAPP_SETTINGS_SEND);
  FirebaseFirestore.instance.runTransaction((tx) async {
    tx.set(tabSendRef, {KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE: 'list', KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE : 'work', KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR : Constants.whiteColor,
      KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW : false, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION : true,
      KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE: '요청 리스트', KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE: '글 보기',
      KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE: fontTitle, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE : fontSubTitle, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS : fontContents});
  });
  //tab_receive
  final DocumentReference tabReceiveRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_SETTINGS)
      .doc(tabNum).collection(KEY_CUSTOMAPP_SETTINGS_COLLECTION_FUNC).doc(KEY_CUSTOMAPP_SETTINGS_RECEIVE);
  FirebaseFirestore.instance.runTransaction((tx) async {
    tx.set(tabReceiveRef, {KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE: 'list', KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE : 'work', KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR : Constants.whiteColor,
      KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW : false, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION : true,
      KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE: '요청 받은 리스트', KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE: '글 보기',
      KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE: fontTitle, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE : fontSubTitle, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS : fontContents});
  });
  //tab_web
  final DocumentReference tabWebRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_SETTINGS)
      .doc(tabNum).collection(KEY_CUSTOMAPP_SETTINGS_COLLECTION_FUNC).doc(KEY_CUSTOMAPP_SETTINGS_WEB);
  FirebaseFirestore.instance.runTransaction((tx) async {
    tx.set(tabWebRef, {KEY_CUSTOMAPP_SETTINGS_WEB_URL : "https://leanlab.dev"});
  });
  //tab_profile
  final DocumentReference tabProfileRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_SETTINGS)
      .doc(tabNum).collection(KEY_CUSTOMAPP_SETTINGS_COLLECTION_FUNC).doc(KEY_CUSTOMAPP_SETTINGS_PROFILE);
  FirebaseFirestore.instance.runTransaction((tx) async {
    tx.set(tabProfileRef, {KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR:4278190080, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE: 15,
      KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ISACTIVE: true, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_TITLE: '내 정보', KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ICON : 62754,
      KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_ISACTIVE: true, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_TITLE: '이름',
      KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_ISACTIVE: true, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_TITLE: '포지션',
      KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ISACTIVE: true, KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ICON : 59428,
      KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ISACTIVE: true, KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ICON : 60520,
      KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ISACTIVE: true, KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ICON : 57644,
      KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ISACTIVE: true, KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_TITLE: '버그신고 및 문의사항',
      KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ICON : 58913, KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_EMAIL: 'hepl@leanlab.dev',
      KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ISACTIVE: true, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_TITLE: '앱 평가하기',
      KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ICON : 983508, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_GOOGLEPLAYID: '', KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_APPSTOREID: '',
      KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ISACTIVE: true, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_TITLE: '앱 공유하기',
      KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ICON : 58771, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_MSG: '',
      KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ISACTIVE: true, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_TITLE: '설정', KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ICON : 58751,
      KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_ISACTIVE: true, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_TITLE: '알림 설정',
      KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_ISACTIVE: true, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_TITLE: '개인정보처리방침', KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_URL:'',
      KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_ISACTIVE: true, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_TITLE: '서비스 약관', KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_URL:'',
      KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_ISACTIVE: true, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_TITLE: '라이센스', KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_URL:'',
      KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ISACTIVE: true, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ALIGN: 'left',
      });
  });
}

SettingNetworkRepository settingNetworkRepository = SettingNetworkRepository();
