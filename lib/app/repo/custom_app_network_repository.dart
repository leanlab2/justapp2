import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/firestore/setting/custom_app_model.dart';
import 'package:justapp/app/repo/helper/transformer.dart';

class CustomAppNetworkRepository with Transformers {
  Future<void> createCustomAppModel(String date, String des, String image, String madeby, String name, String template) async {
    final DocumentReference customAppRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(name);
    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.set(customAppRef, {KEY_DATE: date, KEY_DES: des, KEY_IMAGE: image, KEY_MADEBY: madeby, KEY_NAME: name, KEY_TEMPLATE: template});
    });
  }

  Future<void> setCustomAppModel(String date, String des, String image, String madeby, String name, String template) async {
    final DocumentReference customAppRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(name);
    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.update(customAppRef, {KEY_DATE: date, KEY_DES: des, KEY_IMAGE: image, KEY_MADEBY: madeby, KEY_NAME: name, KEY_TEMPLATE: template});
    });
  }

  Future<CustomAppModel> getCustomAppModel(String userMade) async {
    final DocumentReference customAppRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade);
    DocumentSnapshot snapshot = await customAppRef.get();
    return CustomAppModel.fromSnapshot(snapshot);
  }
}

CustomAppNetworkRepository customAppNetworkRepository = CustomAppNetworkRepository();
