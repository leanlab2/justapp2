import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/app/models/firestore/admin_user_model.dart';
import 'package:justapp/app/models/firestore/board_model.dart';
import 'package:justapp/app/models/firestore/setting/string_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_board_model.dart';
import 'package:justapp/app/models/firestore/setting/entire_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_category_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_favorite_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_made_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_profile_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_receive_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_send_model.dart';
import 'package:justapp/app/models/firestore/setting/tab_web_model.dart';
import 'package:justapp/app/models/firestore/user_model.dart';

class Transformers {
  final toAdminUser = StreamTransformer<DocumentSnapshot, AdminUserModel>.fromHandlers(
      handleData: (snapshot, sink) async {
        if(snapshot.data() != null){
          sink.add(AdminUserModel.fromSnapshot(snapshot));
        }else{
          //이상하게 회원가입시 널로 한번 이벤트가 발생됨.
        }
  });

  final toUser = StreamTransformer<DocumentSnapshot, UserModel>.fromHandlers(
      handleData: (snapshot, sink) async {
        if(snapshot.data() != null){
          sink.add(UserModel.fromSnapshot(snapshot));
        }else{
          //이상하게 회원가입시 널로 한번 이벤트가 발생됨.
        }
      });

  final toUserMap = StreamTransformer<QuerySnapshot,  Map<String, UserModel>>.fromHandlers(
      handleData: (snapshot, sink) async {
        Map<String, UserModel> userModeMap = {};
        for(int i=0;i<snapshot.docs.length;i++){
          userModeMap[snapshot.docs[i].id] = UserModel.fromSnapshot(snapshot.docs[i]);
        }
        sink.add(userModeMap);
      });

  final toBoard = StreamTransformer<DocumentSnapshot, BoardModel>.fromHandlers(
      handleData: (snapshot, sink) async {
        if(snapshot.data() != null){
          sink.add(BoardModel.fromSnapshot(snapshot));
        }else{
        }
      });









  // final toBoardMap = StreamTransformer<QuerySnapshot, Map<String, BoardModel>>.fromHandlers(
  //     handleData: (snapshot, sink) async {
  //       Map<String, BoardModel> boardModeMap = {};
  //       for(int i=0;i<snapshot.docs.length;i++){
  //         boardModeMap[snapshot.docs[i].id] = BoardModel.fromSnapshot(snapshot.docs[i]);
  //       }
  //       sink.add(boardModeMap);
  //     });
  //
  // final toReservationMap = StreamTransformer<QuerySnapshot, Map<String, ReservationModel>>.fromHandlers(
  //     handleData: (snapshot, sink) async {
  //       Map<String, ReservationModel> reservationModelMap = {};
  //       for(int i=0;i<snapshot.docs.length;i++){
  //         reservationModelMap[snapshot.docs[i].id] = ReservationModel.fromSnapshot(snapshot.docs[i]);
  //       }
  //       sink.add(reservationModelMap);
  //     });





  final toEntire = StreamTransformer<DocumentSnapshot, EntireModel>.fromHandlers(handleData: (snapshot, sink) async {
    if (snapshot.data() != null) {
      sink.add(EntireModel.fromSnapshot(snapshot));
    } else {}
  });

  final toTabString = StreamTransformer<DocumentSnapshot, StringModel>.fromHandlers(handleData: (snapshot, sink) async {
    if (snapshot.data() != null) {
      sink.add(StringModel.fromSnapshot(snapshot));
    } else {}
  });

  final toTab = StreamTransformer<DocumentSnapshot, TabModel>.fromHandlers(handleData: (snapshot, sink) async {
    if (snapshot.data() != null) {
      sink.add(TabModel.fromSnapshot(snapshot));
    } else {}
  });

  final toTabBoard = StreamTransformer<DocumentSnapshot, TabBoardModel>.fromHandlers(handleData: (snapshot, sink) async {
    if (snapshot.data() != null) {
      sink.add(TabBoardModel.fromSnapshot(snapshot));
    } else {}
  });

  final toTabFavorite = StreamTransformer<DocumentSnapshot, TabFavoriteModel>.fromHandlers(handleData: (snapshot, sink) async {
    if (snapshot.data() != null) {
      sink.add(TabFavoriteModel.fromSnapshot(snapshot));
    } else {}
  });

  final toTabCategory = StreamTransformer<DocumentSnapshot, TabCategoryModel>.fromHandlers(handleData: (snapshot, sink) async {
    if (snapshot.data() != null) {
      sink.add(TabCategoryModel.fromSnapshot(snapshot));
    } else {}
  });

  final toTabMade = StreamTransformer<DocumentSnapshot, TabMadeModel>.fromHandlers(handleData: (snapshot, sink) async {
    if (snapshot.data() != null) {
      sink.add(TabMadeModel.fromSnapshot(snapshot));
    } else {}
  });

  final toTabSend = StreamTransformer<DocumentSnapshot, TabSendModel>.fromHandlers(handleData: (snapshot, sink) async {
    if (snapshot.data() != null) {
      sink.add(TabSendModel.fromSnapshot(snapshot));
    } else {}
  });

  final toTabReceive = StreamTransformer<DocumentSnapshot, TabReceiveModel>.fromHandlers(handleData: (snapshot, sink) async {
    if (snapshot.data() != null) {
      sink.add(TabReceiveModel.fromSnapshot(snapshot));
    } else {}
  });

  final toTabWeb = StreamTransformer<DocumentSnapshot, TabWebModel>.fromHandlers(handleData: (snapshot, sink) async {
    if (snapshot.data() != null) {
      sink.add(TabWebModel.fromSnapshot(snapshot));
    } else {}
  });


  final toTabProfile = StreamTransformer<DocumentSnapshot, TabProfileModel>.fromHandlers(handleData: (snapshot, sink) async {
    if (snapshot.data() != null) {
      sink.add(TabProfileModel.fromSnapshot(snapshot));
    } else {}
  });

}
