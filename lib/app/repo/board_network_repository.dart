import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/firestore/board_model.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/repo/helper/transformer.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:justapp/app/repo/user_network_repository.dart';

class BoardNetworkRepository with Transformers {
  Future<String> setBoard(viewerState, String userMade, String userKey, date, List<XFile> imageList, boardTitle, boardSubTitle, category,
      time,termStartDate,termEndDate,price,phoneNum, address, location, optionList, boardContents,
      isCallChecked, isMessageChecked, isMailChecked, isLinkChecked, linkStr, isOnlineChecked, List<String> editImageList) async {
    final DocumentReference boardRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_BOARD).doc();
    String boardKey = boardRef.id;
    List<String> imgList = [];
    GeoPoint geoPoint = GeoPoint(location.lat,location.lng);
    linkStr.replaceAll('http://', "");
    linkStr.replaceAll('https://', "");
    linkStr = 'https://' + linkStr;

    //image
    for(int i=0;i<editImageList.length;i++){
      imgList.add(editImageList[i]);
    }
    for(int i=0 + editImageList.length; i<imageList.length + editImageList.length;i++){
      Uint8List dbBytes = await imageList[i].readAsBytes();
      firebase_storage.Reference ref = firebase_storage.FirebaseStorage.instance.ref().child('board/image/' + boardKey + "_" + i.toString());
      final metadata = firebase_storage.SettableMetadata(contentType: 'image/png');
      firebase_storage.UploadTask uploadTask = ref.putData(dbBytes, metadata);
      imgList.add(await (await uploadTask).ref.getDownloadURL());
    }
    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.set(boardRef, {
        KEY_CUSTOMAPP_BOARD_BOARDIMAGE : imgList,
        KEY_CUSTOMAPP_BOARD_THUMBNAILIMAGE : imgList,
        KEY_CUSTOMAPP_BOARD_BOARDKEY: boardKey,
        KEY_CUSTOMAPP_BOARD_DATE : date,
        KEY_CUSTOMAPP_BOARD_BOARDTITLE: boardTitle,
        KEY_CUSTOMAPP_BOARD_BOARDSUBTITLE: boardSubTitle,
        KEY_CUSTOMAPP_BOARD_BOARDCONTENTS : boardContents,
        KEY_CUSTOMAPP_BOARD_CATEGORY : category,
        KEY_CUSTOMAPP_BOARD_USERKEY : userKey,
        KEY_CUSTOMAPP_BOARD_LOCATION: geoPoint,
        KEY_CUSTOMAPP_BOARD_TIME: time,
        KEY_CUSTOMAPP_BOARD_TERM_START_DATE: termStartDate,
        KEY_CUSTOMAPP_BOARD_TERM_END_DATE: termEndDate,
        KEY_CUSTOMAPP_BOARD_PRICE: price,
        KEY_CUSTOMAPP_BOARD_PHONENUM: phoneNum==""?0:int.parse(phoneNum),
        KEY_CUSTOMAPP_BOARD_ADDRESS: address,
        KEY_CUSTOMAPP_BOARD_ISHIDDEN: false,
        KEY_CUSTOMAPP_BOARD_IS_CALL_CHECKED: isCallChecked,
        KEY_CUSTOMAPP_BOARD_IS_MESSAGE_CHECKED: isMessageChecked,
        KEY_CUSTOMAPP_BOARD_IS_MAIL_CHECKED: isMailChecked,
        KEY_CUSTOMAPP_BOARD_IS_LINK_CHECKED: isLinkChecked,
        KEY_CUSTOMAPP_BOARD_IS_LINK_STR: linkStr,
        KEY_CUSTOMAPP_BOARD_FAVORITE_CNT: 0,
        KEY_CUSTOMAPP_BOARD_IS_ONLINE_CHECKED: isOnlineChecked,
        KEY_CUSTOMAPP_BOARD_OPTION_LIST: optionList});
    }).then((value) => {
      //board Event
      boardNetworkRepository.getBoardModelStream(userMade, boardRef.id).listen((boardModel) async {
        viewerState.boardModelMap[boardRef.id] = boardModel;
        // viewerState.boardModelMap = viewerState.boardModelMap;

        //처음에 카테고리 지우기
        viewerState.categoryModelMap!.values.forEach((element1) {
          element1.remove(boardRef.id);
        });
        Map<String,dynamic>? categoryMap = viewerState.categoryModelMap[boardModel.category]==null?{}:
        viewerState.categoryModelMap[boardModel.category];
        categoryMap![boardRef.id] = "";
        viewerState.categoryModelMap[boardRef.id]  = categoryMap;
        // viewerState.categoryModelMap = viewerState.categoryModelMap;
      }),

      //Made list
      userNetworkRepository.setUserOtherMap(userMade, viewerState.userModel.userKey, boardRef.id, KEY_CUSTOMAPP_USERS_MADE_MAP, "")

    });

    return boardRef.id;
  }


  Future<void> updateBoard(String userMade, String userKey, String boardKey, date, List<XFile> imageList, boardTitle, boardSubTitle, category,
      time,termStartDate,termEndDate,price,phoneNum, address, location, optionList, boardContents,
      isCallChecked, isMessageChecked, isMailChecked, isLinkChecked, linkStr, isOnlineChecked, List<String> editImageList) async {
    final DocumentReference boardRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_BOARD).doc(boardKey);
    List<String> imgList = [];
    GeoPoint geoPoint = GeoPoint(location.lat,location.lng);
    linkStr.replaceAll('http://', "");
    linkStr.replaceAll('https://', "");
    linkStr = 'https://' + linkStr;

    //image
    for(int i=0;i<editImageList.length;i++){
      imgList.add(editImageList[i]);
    }
    for(int i=0 + editImageList.length; i<imageList.length + editImageList.length;i++){
      Uint8List dbBytes = await imageList[i].readAsBytes();
      firebase_storage.Reference ref = firebase_storage.FirebaseStorage.instance.ref().child('board/image/' + boardKey + "_" + i.toString());
      final metadata = firebase_storage.SettableMetadata(contentType: 'image/png');
      firebase_storage.UploadTask uploadTask = ref.putData(dbBytes, metadata);
      imgList.add(await (await uploadTask).ref.getDownloadURL());
    }
    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.update(boardRef, {
        KEY_CUSTOMAPP_BOARD_BOARDIMAGE : imgList,
        KEY_CUSTOMAPP_BOARD_THUMBNAILIMAGE : imgList,
        KEY_CUSTOMAPP_BOARD_BOARDKEY: boardKey,
        KEY_CUSTOMAPP_BOARD_DATE : date,
        KEY_CUSTOMAPP_BOARD_BOARDTITLE: boardTitle,
        KEY_CUSTOMAPP_BOARD_BOARDSUBTITLE: boardSubTitle,
        KEY_CUSTOMAPP_BOARD_BOARDCONTENTS : boardContents,
        KEY_CUSTOMAPP_BOARD_CATEGORY : category,
        KEY_CUSTOMAPP_BOARD_USERKEY : userKey,
        KEY_CUSTOMAPP_BOARD_LOCATION: geoPoint,
        KEY_CUSTOMAPP_BOARD_TIME: time,
        KEY_CUSTOMAPP_BOARD_TERM_START_DATE: termStartDate,
        KEY_CUSTOMAPP_BOARD_TERM_END_DATE: termEndDate,
        KEY_CUSTOMAPP_BOARD_PRICE: price,
        KEY_CUSTOMAPP_BOARD_PHONENUM: phoneNum==""?0:int.parse(phoneNum),
        KEY_CUSTOMAPP_BOARD_ADDRESS: address,
        KEY_CUSTOMAPP_BOARD_ISHIDDEN: false,
        KEY_CUSTOMAPP_BOARD_IS_CALL_CHECKED: isCallChecked,
        KEY_CUSTOMAPP_BOARD_IS_MESSAGE_CHECKED: isMessageChecked,
        KEY_CUSTOMAPP_BOARD_IS_MAIL_CHECKED: isMailChecked,
        KEY_CUSTOMAPP_BOARD_IS_LINK_CHECKED: isLinkChecked,
        KEY_CUSTOMAPP_BOARD_IS_LINK_STR: linkStr,
        KEY_CUSTOMAPP_BOARD_IS_ONLINE_CHECKED: isOnlineChecked,
        KEY_CUSTOMAPP_BOARD_OPTION_LIST: optionList});
    });
  }

  //hiddenUpdate
  Future<void> updateHiddenBoard(String userMade, String userKey, String boardKey, bool isHidden) async {
    final DocumentReference boardRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(
        COLLECTION_CUSTOMAPP_BOARD).doc(boardKey);

    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.update(boardRef, {
        KEY_CUSTOMAPP_BOARD_ISHIDDEN: isHidden});
    });
  }

  //hiddenUpdate
  Future<void> updateFavoriteCntBoard(String userMade, String userKey, String boardKey, bool isFavorite) async {
    final DocumentReference boardRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(
        COLLECTION_CUSTOMAPP_BOARD).doc(boardKey);
    DocumentSnapshot snapshot = await boardRef.get();
    int number = snapshot.data()![KEY_CUSTOMAPP_BOARD_FAVORITE_CNT];
    isFavorite? number-- : number++;

    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.update(boardRef, {
        KEY_CUSTOMAPP_BOARD_FAVORITE_CNT: number});
    });
  }

  Future<void> deleteBoard(String boardKey, String userMade, String userKey, ViewerState viewerState) async {
    final DocumentReference boardRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_BOARD).doc(boardKey);

    //category
    viewerState.categoryModelMap!.values.forEach((element1) {
      element1.remove(boardKey);
    });
    //favorite, send, made
    if(viewerState.userModel.favoriteMap.containsKey(boardKey)) userNetworkRepository.deleteUserOtherMap(userMade, userKey, boardKey, KEY_CUSTOMAPP_USERS_FAVORITE_MAP);
    if(viewerState.userModel.sendMap.containsKey(boardKey)) userNetworkRepository.deleteUserOtherMap(userMade, userKey, boardKey, KEY_CUSTOMAPP_USERS_SEND_MAP);
    if(viewerState.userModel.madeMap.containsKey(boardKey)) userNetworkRepository.deleteUserOtherMap(userMade, userKey, boardKey, KEY_CUSTOMAPP_USERS_MADE_MAP);
    if(viewerState.userModel.madeMap.containsKey(boardKey)) userNetworkRepository.deleteUserOtherMap(userMade, userKey, boardKey, KEY_CUSTOMAPP_USERS_RECEIVE_MAP);
    //board
    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.delete(boardRef);
    });
  }


  Future<void> getBoardModelMap(String userMade, ViewerState viewerState) async {
    final CollectionReference boardCollRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_BOARD);
    QuerySnapshot querySnapshot = await boardCollRef.get();

    querySnapshot.docs.forEach((element) {
      viewerState.boardModelMap[element.id] = BoardModel.fromSnapshot(element);
      viewerState.boardModelMap = viewerState.boardModelMap;

      Map<String,dynamic>? categoryMap = viewerState.categoryModelMap[viewerState.boardModelMap[element.id]!.category]==null?{}:
                                          viewerState.categoryModelMap[viewerState.boardModelMap[element.id]!.category];
      categoryMap![element.id] = "";
      viewerState.categoryModelMap[viewerState.boardModelMap[element.id]!.category]  = categoryMap;
      viewerState.categoryModelMap = viewerState.categoryModelMap;

      Map<String,dynamic>? dummyCategoryMap = {};
      viewerState.categoryModelMap[KEY_CUSTOMAPP_SETTINGS_CATEGORY1]= dummyCategoryMap;
      viewerState.categoryModelMap[KEY_CUSTOMAPP_SETTINGS_CATEGORY2]= dummyCategoryMap;
      viewerState.categoryModelMap[KEY_CUSTOMAPP_SETTINGS_CATEGORY3]= dummyCategoryMap;
      viewerState.categoryModelMap[KEY_CUSTOMAPP_SETTINGS_CATEGORY4]= dummyCategoryMap;
      viewerState.categoryModelMap[KEY_CUSTOMAPP_SETTINGS_CATEGORY5]= dummyCategoryMap;
      viewerState.categoryModelMap[KEY_CUSTOMAPP_SETTINGS_CATEGORY6]= dummyCategoryMap;
      viewerState.categoryModelMap[KEY_CUSTOMAPP_SETTINGS_CATEGORY7]= dummyCategoryMap;
      viewerState.categoryModelMap[KEY_CUSTOMAPP_SETTINGS_CATEGORY8]= dummyCategoryMap;

      //board Event
      boardNetworkRepository.getBoardModelStream(userMade, element.id).listen((boardModel) async {
        print("dsnjkllsnjkddsfasd");
        viewerState.boardModelMap[element.id] = boardModel;
        viewerState.boardModelMap = viewerState.boardModelMap;

        viewerState.categoryModelMap!.values.forEach((element1) {
          element1.remove(element.id);
        });
        Map<String,dynamic>? categoryMap = viewerState.categoryModelMap[boardModel.category]==null?{}:
                                          viewerState.categoryModelMap[boardModel.category];
        categoryMap![element.id] = "";
        viewerState.categoryModelMap[boardModel.category] = categoryMap;
        viewerState.categoryModelMap = viewerState.categoryModelMap;
      });
    });
  }

  Stream<BoardModel> getBoardModelStream(String userMade, String boardKey) {
    return FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_BOARD).doc(boardKey).snapshots().transform(toBoard);
  }



  // Stream<Map<String, BoardModel>> getBoardModelMapStream(String userMade) {
  //   final CollectionReference boardCollRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_BOARD);
  //   return boardCollRef.snapshots().transform(toBoardMap);
  // }

}

BoardNetworkRepository boardNetworkRepository = BoardNetworkRepository();












// Future<String> setBoard(String boardTitle, String boardContent, String currentDate, String userMade, String userKey, String category, String image, List<BoardModel> boardList) async {
//   final DocumentReference boardRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(KEY_CUSTOMAPP_BOARD).doc(KEY_CUSTOMAPP_BOARD);
//   DocumentSnapshot snapshot = await boardRef.get();
//   int number = snapshot.data()![KEY_CUSTOMAPP_BOARD_NUMBER];
//   String boardId = 'board_' + number.toString();
//
//   //중복처리
//   List<dynamic> boardSetList = [];
//   for(int i=0;i<boardList.length;i++){
//     Map<dynamic,dynamic> boardSubMap =  {
//       KEY_CUSTOMAPP_BOARD_ID: boardList[i].boardId,
//       KEY_CUSTOMAPP_BOARD_BOARDTITLE: boardList[i].boardTitle,
//       KEY_CUSTOMAPP_BOARD_BOARDCONTENT: boardList[i].boardContent,
//       KEY_CUSTOMAPP_BOARD_BOARDDATE : boardList[i].boardDate,
//       KEY_CUSTOMAPP_BOARD_CATEGORY : boardList[i].category,
//       KEY_CUSTOMAPP_BOARD_USERKEY : boardList[i].userKey,
//       KEY_CUSTOMAPP_BOARD_BOARDIMAGE : boardList[i].boardImage,
//     };
//     boardSetList.add(boardSubMap);
//   }
//   boardSetList.add({
//     KEY_CUSTOMAPP_BOARD_ID: boardId,
//     KEY_CUSTOMAPP_BOARD_BOARDTITLE: boardTitle,
//     KEY_CUSTOMAPP_BOARD_BOARDCONTENT: boardContent,
//     KEY_CUSTOMAPP_BOARD_BOARDDATE : currentDate,
//     KEY_CUSTOMAPP_BOARD_CATEGORY : category,
//     KEY_CUSTOMAPP_BOARD_USERKEY : userKey,
//     KEY_CUSTOMAPP_BOARD_BOARDIMAGE : image,
//   });
//
//   FirebaseFirestore.instance.runTransaction((tx) async {
//     tx.update(boardRef, {KEY_CUSTOMAPP_BOARD_LIST: boardSetList, KEY_CUSTOMAPP_BOARD_NUMBER : number + 1});
//   });
//   return boardId;
// }
// Future<void> updateBoard(int index, String boardTitle, String boardContent, String currentDate, String userMade, String userKey, String category, String image, List<BoardModel> boardList) async {
//   final DocumentReference boardRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(KEY_CUSTOMAPP_BOARD).doc(KEY_CUSTOMAPP_BOARD);
//   DocumentSnapshot snapshot = await boardRef.get();
//   int number = snapshot.data()![KEY_CUSTOMAPP_BOARD_NUMBER] - 1;
//   String boardId = 'board_' + number.toString();
//
//   List<dynamic> boardSetList = [];
//   for(int i=0;i<boardList.length;i++){
//     Map<dynamic,dynamic> boardSubMap =  {
//       KEY_CUSTOMAPP_BOARD_ID: i == index?  boardId : boardList[i].boardId,
//       KEY_CUSTOMAPP_BOARD_BOARDTITLE: i == index?  boardTitle : boardList[i].boardTitle,
//       KEY_CUSTOMAPP_BOARD_BOARDCONTENT: i == index?  boardContent : boardList[i].boardContent,
//       KEY_CUSTOMAPP_BOARD_BOARDDATE : i == index?  currentDate : boardList[i].boardDate,
//       KEY_CUSTOMAPP_BOARD_CATEGORY : i == index?  category : boardList[i].category,
//       KEY_CUSTOMAPP_BOARD_USERKEY : i == index?  userKey : boardList[i].userKey,
//       KEY_CUSTOMAPP_BOARD_BOARDIMAGE : i == index?  image : boardList[i].boardImage,
//     };
//     boardSetList.add(boardSubMap);
//   }
//
//   FirebaseFirestore.instance.runTransaction((tx) async {
//     tx.update(boardRef, {KEY_CUSTOMAPP_BOARD_LIST: boardSetList});
//   });
// }
// Future<String> deleteBoard(int index, String userMade, List<BoardModel> boardList) async {
//   final DocumentReference boardRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(KEY_CUSTOMAPP_BOARD).doc(KEY_CUSTOMAPP_BOARD);
//   DocumentSnapshot snapshot = await boardRef.get();
//   int number = snapshot.data()![KEY_CUSTOMAPP_BOARD_NUMBER];
//   String boardId = 'board_' + number.toString();
//
//   List<dynamic> boardSetList = [];
//   for(int i=0;i<boardList.length;i++){
//     Map<dynamic,dynamic> boardSubMap =  {
//       KEY_CUSTOMAPP_BOARD_ID: boardList[i].boardId,
//       KEY_CUSTOMAPP_BOARD_BOARDTITLE: boardList[i].boardTitle,
//       KEY_CUSTOMAPP_BOARD_BOARDCONTENT: boardList[i].boardContent,
//       KEY_CUSTOMAPP_BOARD_BOARDDATE : boardList[i].boardDate,
//       KEY_CUSTOMAPP_BOARD_CATEGORY : boardList[i].category,
//       KEY_CUSTOMAPP_BOARD_USERKEY : boardList[i].userKey,
//       KEY_CUSTOMAPP_BOARD_BOARDIMAGE : boardList[i].boardImage,
//     };
//     if(i != index)
//       boardSetList.add(boardSubMap);
//   }
//
//   FirebaseFirestore.instance.runTransaction((tx) async {
//     tx.update(boardRef, {KEY_CUSTOMAPP_BOARD_LIST: boardSetList, KEY_CUSTOMAPP_BOARD_NUMBER : number - 1});
//   });
//   return boardId;
// }
// //페이지 별로 가져오는거
// Future<List<BoardModel>> getBoard(String userMade) async {
//   final DocumentReference userRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(KEY_CUSTOMAPP_BOARD).doc(KEY_CUSTOMAPP_BOARD);
//   DocumentSnapshot snapshot = await userRef.get();
//
//   List<BoardModel> boardModels = List<BoardModel>.from(snapshot.data()![KEY_CUSTOMAPP_BOARD_LIST].map((boardModel) {
//     return BoardModel.fromJson(boardModel);
//   }));
//   return boardModels;
// }
// Stream<List<BoardModel>> getBoardStream(String userMade) {
//   return FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(KEY_CUSTOMAPP_BOARD).doc(KEY_CUSTOMAPP_BOARD).snapshots().transform(toBoard);
// }