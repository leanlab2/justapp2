import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/firestore/user_model.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/repo/helper/transformer.dart';

class UserNetworkRepository with Transformers {
  Future<void> createUserMade(String userMade) async {
    //CONSUMER
    final DocumentReference userConsumerRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_USERS).doc(KEY_CUSTOMAPP_USERS_CONSUMER);
    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.set(userConsumerRef, {KEY_CUSTOMAPP_USERS_USERKEY: KEY_CUSTOMAPP_USERS_CONSUMER,
        KEY_CUSTOMAPP_USERS_POSITION: KEY_CUSTOMAPP_USERS_CONSUMER,
        KEY_CUSTOMAPP_USERS_USERIMG: 'noimage',
        KEY_CUSTOMAPP_USERS_USERBELONG: '',
        KEY_CUSTOMAPP_USERS_PHONE: 010,
        KEY_CUSTOMAPP_USERS_USEREMAIL: '',
        KEY_CUSTOMAPP_USERS_USERNAME: 'consumer_guest',
        KEY_CUSTOMAPP_USERS_FAVORITE_MAP: {},
        KEY_CUSTOMAPP_USERS_SEND_MAP: {},
        KEY_CUSTOMAPP_USERS_MADE_MAP: {},
        KEY_CUSTOMAPP_USERS_RECEIVE_MAP: {},
      });
    });


    //PRODUCER
    final DocumentReference userProducerRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_USERS).doc(KEY_CUSTOMAPP_USERS_PRODUCER);
    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.set(userProducerRef, {KEY_CUSTOMAPP_USERS_USERKEY: KEY_CUSTOMAPP_USERS_PRODUCER,
        KEY_CUSTOMAPP_USERS_POSITION: KEY_CUSTOMAPP_USERS_PRODUCER,
        KEY_CUSTOMAPP_USERS_USERIMG: 'noimage',
        KEY_CUSTOMAPP_USERS_USERBELONG: '',
        KEY_CUSTOMAPP_USERS_PHONE: 010,
        KEY_CUSTOMAPP_USERS_USEREMAIL: '',
        KEY_CUSTOMAPP_USERS_USERNAME: 'producer_guest',
        KEY_CUSTOMAPP_USERS_FAVORITE_MAP: {},
        KEY_CUSTOMAPP_USERS_SEND_MAP: {},
        KEY_CUSTOMAPP_USERS_MADE_MAP: {},
        KEY_CUSTOMAPP_USERS_RECEIVE_MAP: {},
      });
    });
  }

  Stream<UserModel> getUserModelStream(String userMade, String userKey) {
    return FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_USERS).doc(userKey).snapshots().transform(toUser);
  }
  Future<UserModel> getUserModel(String userMade, String userKey) async {
    final DocumentReference userRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade)
        .collection(COLLECTION_CUSTOMAPP_USERS)
        .doc(userKey);
    DocumentSnapshot snapshot = await userRef.get();
    return UserModel.fromSnapshot(snapshot);
  }


  //update list
  Future<void> updateUserProfile(String userMade, String userKey, String userName, String position) async {
    final DocumentReference userRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_USERS).doc(userKey);
    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.update(userRef, {KEY_CUSTOMAPP_USERS_USERNAME: userName, KEY_CUSTOMAPP_USERS_POSITION: position});
    });
  }


  //ohter List
  Future<void> setUserOtherMap(String userMade, String userKey, String otherId, String funcType, messageMap) async {
    final DocumentReference userRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_USERS).doc(userKey);
    DocumentSnapshot snapshot = await userRef.get();

    Map<String, dynamic> otherMap = snapshot.data()![funcType];
    if(funcType==KEY_CUSTOMAPP_USERS_RECEIVE_MAP){
      Map<String, dynamic> receiveBoardMap = otherMap[otherId] == null? {}:otherMap[otherId];
      receiveBoardMap[userKey] = messageMap;
      otherMap[otherId] = receiveBoardMap;
    }
    else{
      otherMap[otherId] = {};
    }

    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.update(userRef, {funcType: otherMap});
    });
  }

  //other delete
  Future<void> deleteUserOtherMap(String userMade, String userKey, String otherId, String funcType) async {
    final DocumentReference userRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_USERS).doc(userKey);
    DocumentSnapshot snapshot = await userRef.get();

    Map<String, dynamic> otherMap = snapshot.data()![funcType];
    otherMap.remove(otherId);
    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.update(userRef, {funcType: otherMap});
    });
  }

  Future<void> getUserModelMap(String userMade, ViewerState viewerState) async {
    final CollectionReference userCollRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_USERS);
    QuerySnapshot querySnapshot = await userCollRef.get();

    querySnapshot.docs.forEach((element) {
      viewerState.userModelMap[element.id] = UserModel.fromSnapshot(element);
      viewerState.userModelMap = viewerState.userModelMap;

      //user Event
      userNetworkRepository.getUserModelStream(userMade, element.id).listen((userModel) async {
        viewerState.userModelMap[element.id] = userModel;
        viewerState.userModelMap = viewerState.userModelMap;
      });
    });
  }


// Stream<Map<String, UserModel>> getUserModelMapStream(String userMade) {
//   final CollectionReference userCollRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_USERS);
//   return userCollRef.snapshots().transform(toUserMap);
// }
// Future<Map<String, UserModel>> getUserMapModel(String userMade) async{
//   final CollectionReference userCollRef = FirebaseFirestore.instance.collection(COLLECTION_CUSTOMAPP).doc(userMade).collection(COLLECTION_CUSTOMAPP_USERS);
//
//   Map<String, UserModel> userModelMap = {};
//   userCollRef.snapshots().forEach((element) {
//     for(int i=0;i<element.docs.length;i++){
//       userModelMap[element.docs[i].id] = UserModel.fromSnapshot(element.docs[i]);
//     }
//   });
//   return userModelMap;
// }

}

UserNetworkRepository userNetworkRepository = UserNetworkRepository();
