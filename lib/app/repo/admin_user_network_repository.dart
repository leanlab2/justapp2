import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/app/models/firestore/admin_user_model.dart';
import 'package:justapp/app/repo/helper/transformer.dart';
import 'package:justapp/const/firestore_keys.dart';

class AdminUserNetworkRepository with Transformers {
  Future<void> attemptCreateUser(String userKey, String email) async {
    final DocumentReference userRef = FirebaseFirestore.instance.collection(COLLECTION_USERS).doc(userKey);
    DocumentSnapshot snapshot = await userRef.get();

    if (!snapshot.exists) {
      return await userRef.set(AdminUserModel.getMapForCreateUser(email, userKey));
    }
  }

  //admin
  Future<void> setAdminUserMade(String userKey, String userMade) async {
    final DocumentReference userRef = FirebaseFirestore.instance.collection(COLLECTION_USERS).doc(userKey);

    FirebaseFirestore.instance.runTransaction((tx) async {
      tx.update(userRef, {KEY_USERMADE: userMade});
    });
  }
  Stream<AdminUserModel> getAdminUserModelStream(String userKey) {
    return FirebaseFirestore.instance.collection(COLLECTION_USERS).doc(userKey).snapshots().transform(toAdminUser);
  }
  Future<AdminUserModel> getAdminUserModel(String userKey) async{
    final DocumentReference userRef = FirebaseFirestore.instance.collection(COLLECTION_USERS).doc(userKey);
    DocumentSnapshot snapshot = await userRef.get();
    return AdminUserModel.fromSnapshot(snapshot);
  }
}

AdminUserNetworkRepository adminUserNetworkRepository = AdminUserNetworkRepository();
