import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/builder/app/common/interface/common_setting_list_func.dart';
import 'package:justapp/app/builder/app/common/interface/common_setting_tab.dart';
import 'package:justapp/app/builder/app/common/widget/builder_setting_widget.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/admin_user_state.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:provider/provider.dart';

class CategorySettingView extends StatefulWidget {
  final int tabNum;

  CategorySettingView({required this.tabNum});

  @override
  _CategorySettingViewState createState() => _CategorySettingViewState();
}

class _CategorySettingViewState extends State<CategorySettingView> {
  TextEditingController _category1Controller = TextEditingController();
  TextEditingController _category2Controller = TextEditingController();
  TextEditingController _category3Controller = TextEditingController();
  TextEditingController _category4Controller = TextEditingController();
  TextEditingController _category5Controller = TextEditingController();
  TextEditingController _category6Controller = TextEditingController();
  TextEditingController _category7Controller = TextEditingController();
  TextEditingController _category8Controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _category1Controller.text = selectGetType(Provider.of<SettingState>(context, listen: false), -1, KEY_CUSTOMAPP_SETTINGS_CATEGORY1) == {}? "":
                                selectGetType(Provider.of<SettingState>(context, listen: false), -1, KEY_CUSTOMAPP_SETTINGS_CATEGORY1)["name"];
    _category2Controller.text = selectGetType(Provider.of<SettingState>(context, listen: false), -1, KEY_CUSTOMAPP_SETTINGS_CATEGORY2)? "":
                                selectGetType(Provider.of<SettingState>(context, listen: false), -1, KEY_CUSTOMAPP_SETTINGS_CATEGORY2)["name"];
    _category3Controller.text = selectGetType(Provider.of<SettingState>(context, listen: false), -1, KEY_CUSTOMAPP_SETTINGS_CATEGORY3)? "":
                                selectGetType(Provider.of<SettingState>(context, listen: false), -1, KEY_CUSTOMAPP_SETTINGS_CATEGORY3)["name"];
    _category4Controller.text = selectGetType(Provider.of<SettingState>(context, listen: false), -1, KEY_CUSTOMAPP_SETTINGS_CATEGORY4)? "":
                                selectGetType(Provider.of<SettingState>(context, listen: false), -1, KEY_CUSTOMAPP_SETTINGS_CATEGORY4)["name"];
    _category5Controller.text = selectGetType(Provider.of<SettingState>(context, listen: false), -1, KEY_CUSTOMAPP_SETTINGS_CATEGORY5)? "":
                                selectGetType(Provider.of<SettingState>(context, listen: false), -1, KEY_CUSTOMAPP_SETTINGS_CATEGORY5)["name"];
    _category6Controller.text = selectGetType(Provider.of<SettingState>(context, listen: false), -1, KEY_CUSTOMAPP_SETTINGS_CATEGORY6)? "":
                                selectGetType(Provider.of<SettingState>(context, listen: false), -1, KEY_CUSTOMAPP_SETTINGS_CATEGORY6)["name"];
    _category7Controller.text = selectGetType(Provider.of<SettingState>(context, listen: false), -1, KEY_CUSTOMAPP_SETTINGS_CATEGORY7)? "":
                                selectGetType(Provider.of<SettingState>(context, listen: false), -1, KEY_CUSTOMAPP_SETTINGS_CATEGORY7)["name"];
    _category8Controller.text = selectGetType(Provider.of<SettingState>(context, listen: false), -1, KEY_CUSTOMAPP_SETTINGS_CATEGORY8)? "":
                                selectGetType(Provider.of<SettingState>(context, listen: false), -1, KEY_CUSTOMAPP_SETTINGS_CATEGORY8)["name"];
  }
  @override
  Widget build(BuildContext context) {
    return Consumer2<SettingState, AdminUserState>(builder: (BuildContext context, SettingState settingState, AdminUserState adminUserState, child){
      return Scaffold(
        backgroundColor: Colors.white,
        body: Padding(
          padding: const EdgeInsets.all(5.0),
          child: ListView(
            children: [
              CommonSettingTab(tabNum: widget.tabNum, funcType: KEY_CUSTOMAPP_SETTINGS_CATEGORY),
              Text("CATEGORY", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 15)),
              dropDownListTile('Category 갯수', settingState,adminUserState, -1, '', KEY_CUSTOMAPP_SETTINGS_ENTIRE_CATEGORYCOUNT, ''),
              categorySetting(selectGetType(settingState, -1, KEY_CUSTOMAPP_SETTINGS_ENTIRE_CATEGORYCOUNT),
                  settingState,adminUserState),
              CommonSettingListFunc(tabNum: widget.tabNum, funcType: KEY_CUSTOMAPP_SETTINGS_CATEGORY),
            ],
          ),
        ),
      );
    });
  }
  categorySetting(categoryNum, SettingState settingState, AdminUserState adminUserState){
    return Column(
      children: [
        categoryNum == 1?textEdit('카테고리1 제목', context, adminUserState, _category1Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY1):
        categoryNum == 2?Column( children: [
          textEdit('카테고리1 제목', context, adminUserState, _category1Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY1),
          textEdit('카테고리2 제목', context, adminUserState, _category2Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY2),
        ]):
        categoryNum == 3?Column( children: [
          textEdit('카테고리1 제목', context, adminUserState, _category1Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY1),
          textEdit('카테고리2 제목', context, adminUserState, _category2Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY2),
          textEdit('카테고리3 제목', context, adminUserState, _category3Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY3),
        ]):
        categoryNum == 4?Column( children: [
          textEdit('카테고리1 제목', context, adminUserState, _category1Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY1),
          textEdit('카테고리2 제목', context, adminUserState, _category2Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY2),
          textEdit('카테고리3 제목', context, adminUserState, _category3Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY3),
          textEdit('카테고리4 제목', context, adminUserState, _category4Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY4),
        ]):
        categoryNum == 5?Column( children: [
          textEdit('카테고리1 제목', context, adminUserState, _category1Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY1),
          textEdit('카테고리2 제목', context, adminUserState, _category2Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY2),
          textEdit('카테고리3 제목', context, adminUserState, _category3Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY3),
          textEdit('카테고리4 제목', context, adminUserState, _category4Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY4),
          textEdit('카테고리5 제목', context, adminUserState, _category5Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY5),
        ]):
        categoryNum == 6?Column( children: [
          textEdit('카테고리1 제목', context, adminUserState, _category1Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY1),
          textEdit('카테고리2 제목', context, adminUserState, _category2Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY2),
          textEdit('카테고리3 제목', context, adminUserState, _category3Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY3),
          textEdit('카테고리4 제목', context, adminUserState, _category4Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY4),
          textEdit('카테고리5 제목', context, adminUserState, _category5Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY5),
          textEdit('카테고리6 제목', context, adminUserState, _category6Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY6),
        ]):
        categoryNum == 7?Column( children: [
          textEdit('카테고리1 제목', context, adminUserState, _category1Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY1),
          textEdit('카테고리2 제목', context, adminUserState, _category2Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY2),
          textEdit('카테고리3 제목', context, adminUserState, _category3Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY3),
          textEdit('카테고리4 제목', context, adminUserState, _category4Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY4),
          textEdit('카테고리5 제목', context, adminUserState, _category5Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY5),
          textEdit('카테고리6 제목', context, adminUserState, _category6Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY6),
          textEdit('카테고리7 제목', context, adminUserState, _category7Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY7),
        ]):
        Column( children: [
          textEdit('카테고리1 제목', context, adminUserState, _category1Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY1),
          textEdit('카테고리2 제목', context, adminUserState, _category2Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY2),
          textEdit('카테고리3 제목', context, adminUserState, _category3Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY3),
          textEdit('카테고리4 제목', context, adminUserState, _category4Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY4),
          textEdit('카테고리5 제목', context, adminUserState, _category5Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY5),
          textEdit('카테고리6 제목', context, adminUserState, _category6Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY6),
          textEdit('카테고리7 제목', context, adminUserState, _category7Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY7),
          textEdit('카테고리8 제목', context, adminUserState, _category8Controller, -1, '', KEY_CUSTOMAPP_SETTINGS_CATEGORY8),
        ]),
        divide(1.0,0.1),
      ],
    );
  }
}
