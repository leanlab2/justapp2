import 'package:flutter/material.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/viewer/app/tabs/viewer_tabs.dart';

class MobileAppView extends StatefulWidget {
  final String userMade;

  MobileAppView({required this.userMade});

  @override
  _MobileAppViewState createState() => _MobileAppViewState();
}

class _MobileAppViewState extends State<MobileAppView> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        alignment: Alignment.topCenter,
        padding: const EdgeInsets.only(top: 50),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: Color(Constants.defaultColor),
            ),
          ), //
          height: Constants.builderMobileHeight,
          width: Constants.builderMobileWidth,
          child: ViewerTabs(userMade: widget.userMade),
        ),
      ),
    );
  }
}
