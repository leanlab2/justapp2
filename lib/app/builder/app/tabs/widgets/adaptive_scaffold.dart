
import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/common/widget/builder_setting_widget.dart';
import 'package:justapp/app/const/constants.dart';

bool _isLargeScreen(BuildContext context) {
  return Constants.size.width > 960.0;
}

bool _isMediumScreen(BuildContext context) {
  return Constants.size.width > 640.0;
}

/// See bottomNavigationBarItem or NavigationRailDestination
class AdaptiveScaffoldDestination {
  final String title;
  final IconData icon;

  const AdaptiveScaffoldDestination({
    required this.title,
    required this.icon,
  });
}

/// A widget that adapts to the current display size, displaying a [Drawer],
/// [NavigationRail], or [BottomNavigationBar]. Navigation destinations are
/// defined in the [destinations] parameter.
class AdaptiveScaffold extends StatefulWidget {
  final Widget title;
  final Widget qrCode;
  final List<Widget> actions;
  final Widget execution;
  final Widget body;
  final int currentIndex;
  final List<AdaptiveScaffoldDestination> destinations;
  final ValueChanged<int> onNavigationIndexChange;
  // final FloatingActionButton floatingActionButton;

  const AdaptiveScaffold({
    required this.title,
    required this.qrCode,
    required this.body,
    required this.execution,
    this.actions = const [],
    required this.currentIndex,
    required this.destinations,
    required this.onNavigationIndexChange,
    // required this.floatingActionButton,
  });

  @override
  _AdaptiveScaffoldState createState() => _AdaptiveScaffoldState();
}

class _AdaptiveScaffoldState extends State<AdaptiveScaffold> {
  @override
  Widget build(BuildContext context) {
    // Show a Drawer
    if (_isLargeScreen(context)) {
      return Row(
        children: [
          Drawer(
            child: Column(
              children: [
                DrawerHeader(
                  child: Center(
                    child: widget.title,
                  ),
                ),
                for (var d in widget.destinations)
                  ListTile(
                    leading: Icon(d.icon),
                    title: Text(d.title),
                    selected:
                        widget.destinations.indexOf(d) == widget.currentIndex,
                    onTap: () => _destinationTapped(d),
                  ),
                Spacer(),
                divide(1.0,0.1),
                Center(
                  child: widget.qrCode,
                ),
                Padding(
                  padding: const EdgeInsets.only(top:10, bottom: 20),
                  child: Text('QR코드를 통해 실제앱을 확인해보세요.', style:TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),

          VerticalDivider(
            width: 1,
            thickness: 1,
            color: Colors.grey[300],
          ),
          Expanded(
            child: Container(
              child:widget.execution,
            ),
          ),

          VerticalDivider(
            width: 1,
            thickness: 1,
            color: Colors.grey[300],
          ),
          Expanded(
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: Color(Constants.defaultColor),
                actions: widget.actions,
              ),
              body: Container(
                  padding: const EdgeInsets.all(8.0),
                  child: widget.body),
              // floatingActionButton: widget.floatingActionButton,
            ),
          ),

        ],
      );
    }

    // Show a navigation rail
    if (_isMediumScreen(context)) {
      return Scaffold(
        appBar: AppBar(
          title: widget.title,
          actions: widget.actions,
          backgroundColor: Color(Constants.defaultColor),
        ),
        body: Row(
          children: [
            NavigationRail(
              backgroundColor: Color(Constants.defaultColor),
              // leading: widget.floatingActionButton,
              destinations: [
                ...widget.destinations.map(
                  (d) => NavigationRailDestination(
                    icon: Icon(d.icon),
                    label: Text(d.title),
                  ),
                ),
              ],
              selectedIndex: widget.currentIndex,
              onDestinationSelected: widget.onNavigationIndexChange,
            ),
            VerticalDivider(
              width: 1,
              thickness: 1,
              color: Colors.grey[300],
            ),
            Expanded(
              child: widget.body,
            ),
          ],
        ),
      );
    }

    // Show a bottom app bar
    return Scaffold(
      body: widget.body,
      appBar: AppBar(
        title: widget.title,
        actions: widget.actions,
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          ...widget.destinations.map(
            (d) => BottomNavigationBarItem(
              icon: Icon(d.icon),
              label: d.title,
              backgroundColor: Color(Constants.defaultColor),
            ),
          ),
        ],
        backgroundColor: Color(Constants.defaultColor),
        currentIndex: widget.currentIndex,
        onTap: widget.onNavigationIndexChange,
      ),
      // floatingActionButton: widget.floatingActionButton,
    );
  }

  void _destinationTapped(AdaptiveScaffoldDestination destination) {
    var idx = widget.destinations.indexOf(destination);
    if (idx != widget.currentIndex) {
      widget.onNavigationIndexChange(idx);
    }
  }
}
