import 'package:flutter/material.dart';
import 'package:justapp/app/builder/screens/no_result_view.dart';
import 'package:justapp/app/builder/app/tabs/widgets/adaptive_scaffold.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/models/admin_user_state.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/firebase_auth_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/viewer/app/tabs/viewer_tabs.dart';
import 'package:provider/provider.dart';

class BuildTabs extends StatefulWidget {
  final String userMade;

  BuildTabs({required this.userMade});

  @override
  _BuildTabsState createState() => _BuildTabsState();
}

class _BuildTabsState extends State<BuildTabs> {
  // int pageIndex = 0;
  @override
  void initState() {
    Provider.of<SettingState>(context, listen: false).pageIndex = -1;
  }

  @override
  Widget build(BuildContext context) {
    return Provider.of<AdminUserState>(context, listen: false).adminUserModel.userMade == ''? NoResultView():
    Consumer2<SettingState, ViewerState>(builder: (BuildContext context, SettingState settingState, ViewerState viewerState, child) {
      Constants.prefs.setString(Constants.splashKey,settingState.entireModel.splashImg);
      return AdaptiveScaffold(
              title: Container(
                child: Image.asset(
                  'assets/auth/images/appLogo.png',
                  width: 150,
                  color: Colors.black,
                ),
              ),
              qrCode : Container(
                child: Image.asset(
                  'assets/auth/images/qrCode.png',
                  width: 150,
                  height: 150,
                ),
              ),
              actions: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextButton(
                    style: TextButton.styleFrom(primary: Colors.white),
                    onPressed: () => Provider.of<FirebaseAuthState>(context, listen: false).signOut(),
                    child: Text('Sign Out'),
                  ),
                )
              ],
              currentIndex: settingState.pageIndex + 1,
              destinations: selectTabDestination(settingState.entireModel.tabCount),
              execution: Container(
                alignment: Alignment.topCenter,
                padding: const EdgeInsets.only(top: 50),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: Color(Constants.defaultColor),
                    ),
                  ), //
                  height: Constants.builderMobileHeight,
                  width: Constants.builderMobileWidth,
                  child: ViewerTabs(userMade: widget.userMade),
                ),
              ),
              // MobileAppView(userMade:adminUserState.adminUserModel.userMade),
              body: selectBodySetting(settingState.pageIndex + 1, settingState),
              onNavigationIndexChange: (newIndex) {
                // setState(() {
                  settingState.pageIndex = newIndex - 1;
                  if(newIndex!=0) ViewerTabsState.pageController.jumpToPage(settingState.pageIndex);
                // });
              },
              // floatingActionButton: ,
            );
    });
  }

  selectTabDestination(tabCount) {
    switch (tabCount) {
      case 2:
        {
          return [
            AdaptiveScaffoldDestination(title: 'Settings', icon: Icons.settings),
            AdaptiveScaffoldDestination(title: 'tab1', icon: Icons.list),
            AdaptiveScaffoldDestination(title: 'tab2', icon: Icons.list),
          ];
        }
      case 3:
        {
          return [
            AdaptiveScaffoldDestination(title: 'Settings', icon: Icons.settings),
            AdaptiveScaffoldDestination(title: 'tab1', icon: Icons.list),
            AdaptiveScaffoldDestination(title: 'tab2', icon: Icons.list),
            AdaptiveScaffoldDestination(title: 'tab3', icon: Icons.list),
          ];
        }
      case 4:
        {
          return [
            AdaptiveScaffoldDestination(title: 'Settings', icon: Icons.settings),
            AdaptiveScaffoldDestination(title: 'tab1', icon: Icons.list),
            AdaptiveScaffoldDestination(title: 'tab2', icon: Icons.list),
            AdaptiveScaffoldDestination(title: 'tab3', icon: Icons.list),
            AdaptiveScaffoldDestination(title: 'tab4', icon: Icons.list),
          ];
        }
      case 5:
        {
          return [
            AdaptiveScaffoldDestination(title: 'Settings', icon: Icons.settings),
            AdaptiveScaffoldDestination(title: 'tab1', icon: Icons.list),
            AdaptiveScaffoldDestination(title: 'tab2', icon: Icons.list),
            AdaptiveScaffoldDestination(title: 'tab3', icon: Icons.list),
            AdaptiveScaffoldDestination(title: 'tab4', icon: Icons.list),
            AdaptiveScaffoldDestination(title: 'tab5', icon: Icons.list),
          ];
        }
    }
  }


}
