import 'dart:typed_data';
import 'dart:ui';

import 'package:animate_icons/animate_icons.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:justapp/app/builder/app/common/widget/builder_setting_widget.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/admin_user_state.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/repo/setting_network_repository.dart';
import 'package:justapp/utils/simple_snackbar.dart';
import 'package:provider/provider.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

class EntireView extends StatefulWidget {
  @override
  _EntireViewState createState() => _EntireViewState();
}

class _EntireViewState extends State<EntireView> {
  final ImagePicker _picker = ImagePicker();
  late AnimateIconController _ani1Controller,_ani2Controller;

  @override
  void initState() {
    _ani1Controller = AnimateIconController();
    _ani2Controller = AnimateIconController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<SettingState, AdminUserState>(builder: (BuildContext context, SettingState settingState, AdminUserState adminUserState, child) {
      return Scaffold(
        backgroundColor: Colors.white,
        body: Padding(
          padding: const EdgeInsets.all(5.0),
          child: ListView(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("TAB", style:TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 15)),
                  arrowSimpleViewer('isEntireTabViewer', settingState,_ani1Controller),
                ],
              ),
              settingState.isEntireTabSimpleViewer?Column(
                children: [
                  dropDownListTile('Tab 갯수', settingState,adminUserState, -1, '', KEY_CUSTOMAPP_SETTINGS_ENTIRE_TABCOUNT, ''),
                  colorPickerListTile('Tab 색상', context, settingState, adminUserState, -1, '', KEY_CUSTOMAPP_SETTINGS_ENTIRE_TABCOLOR, ''),
                  colorPickerListTile('Tab 배경색상', context, settingState, adminUserState, -1, '', KEY_CUSTOMAPP_SETTINGS_ENTIRE_TABBGCOLOR, ''),
                ],
              ):SizedBox.shrink(),
              divide(1.0,0.1),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("STYLE", style:TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 15)),
                  arrowSimpleViewer('isEntireStyleViewer', settingState,_ani2Controller),
                ],
              ),

              settingState.isEntireStyleSimpleViewer?Column(
                children: [
                  colorPickerListTile('전체 기본 색상', context, settingState, adminUserState, -1, '', KEY_CUSTOMAPP_SETTINGS_ENTIRE_DEFAULTCOLOR, ''),
                  ListTile(
                    leading: Text('Splash 이미지', style:TextStyle(color: Colors.black,fontSize: 13)),
                    trailing: OutlineButton(
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 0.5,
                      ),
                      child: Container(
                        alignment: Alignment.center,
                        height: 30,
                        width: 30,
                        child: Icon(Icons.add_a_photo),
                      ),
                      onPressed: () async{
                        settingNetworkRepository.selectSetTab(adminUserState.adminUserModel.userMade, -1,
                            KEY_CUSTOMAPP_SETTINGS_ENTIRE_SPLASHIMG, await getImage()).then((value) => {
                          simpleLongSnackBar(context, 'splash이미지 등록이 완료되었습니다.')
                        });
                      },
                    ),
                  ),
                ]
              ):SizedBox.shrink(),
            ]
          ),
        ),
      );
    });
  }


  getImage() async {
    var image = await _picker.pickImage(source: ImageSource.gallery);
    Uint8List dbBytes = await image!.readAsBytes();
    firebase_storage.Reference ref = firebase_storage.FirebaseStorage.instance.ref().child('default/splashImg');
    final metadata = firebase_storage.SettableMetadata(contentType: 'image/jpeg');
    firebase_storage.UploadTask uploadTask = ref.putData(dbBytes, metadata);
    return await (await uploadTask).ref.getDownloadURL();
  }
}