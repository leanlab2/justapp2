import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/common/interface/common_setting_list_func.dart';
import 'package:justapp/app/builder/app/common/interface/common_setting_tab.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:provider/provider.dart';

class FavoriteSettingView extends StatefulWidget {
  final int tabNum;

  FavoriteSettingView({required this.tabNum});

  @override
  _FavoriteSettingViewState createState() => _FavoriteSettingViewState();
}

class _FavoriteSettingViewState extends State<FavoriteSettingView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.all(5.0),
        child: ListView(
          children: [
            CommonSettingTab(tabNum: widget.tabNum, funcType: KEY_CUSTOMAPP_SETTINGS_FAVORITE),
            CommonSettingListFunc(tabNum: widget.tabNum, funcType: KEY_CUSTOMAPP_SETTINGS_FAVORITE),
          ],
        ),
      ),
    );
  }
}
