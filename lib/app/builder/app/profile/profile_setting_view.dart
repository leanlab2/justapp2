import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/builder/app/common/interface/common_setting_list_func.dart';
import 'package:justapp/app/builder/app/common/interface/common_setting_tab.dart';
import 'package:justapp/app/builder/app/common/widget/builder_setting_widget.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/admin_user_state.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:provider/provider.dart';

class ProfileSettingView extends StatefulWidget {
  final int tabNum;

  ProfileSettingView({required this.tabNum});

  @override
  _ProfileSettingViewState createState() => _ProfileSettingViewState();
}

class _ProfileSettingViewState extends State<ProfileSettingView> {
  //info
  TextEditingController _infoTitleController = TextEditingController();
  TextEditingController _infoNameTitleController = TextEditingController();
  TextEditingController _infoPositionTitleController = TextEditingController();
  //report
  TextEditingController _reportTitleController = TextEditingController();
  TextEditingController _reportEmailController = TextEditingController();
  //rating
  TextEditingController _ratingTitleController = TextEditingController();
  TextEditingController _ratingGoogleplayIdController = TextEditingController();
  TextEditingController _ratingAppstoreIdController = TextEditingController();
  //share
  TextEditingController _shareTitleController = TextEditingController();
  TextEditingController _shareMsgController = TextEditingController();
  //setting
  TextEditingController _settingController = TextEditingController();
  TextEditingController _settingAlramTitleController = TextEditingController();
  TextEditingController _settingPrivacyController = TextEditingController();
  TextEditingController _settingPrivacyUrlController = TextEditingController();
  TextEditingController _settingProvisionTitleController = TextEditingController();
  TextEditingController _settingProvisionUrlController = TextEditingController();
  TextEditingController _settingLicenseTitleController = TextEditingController();
  TextEditingController _settingLicenseUrlController = TextEditingController();

  @override
  void initState() {
    super.initState();
    //info
    _infoTitleController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_TITLE);
    _infoNameTitleController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_TITLE);
    _infoPositionTitleController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_TITLE);

    //report
    _reportTitleController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_TITLE);
    _reportEmailController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_EMAIL);

    //rating
    _ratingTitleController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_TITLE);
    _ratingGoogleplayIdController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_GOOGLEPLAYID);
    _ratingAppstoreIdController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_APPSTOREID);

    //share
    _shareTitleController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_TITLE);
    _shareMsgController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_MSG);

    //setting
    _settingController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_TITLE);
    _settingAlramTitleController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_TITLE);
    _settingPrivacyController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_TITLE);
    _settingPrivacyUrlController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_URL);
    _settingProvisionTitleController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_TITLE);
    _settingProvisionUrlController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_URL);
    _settingLicenseTitleController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_TITLE);
    _settingLicenseUrlController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_URL);
  }
  @override
  Widget build(BuildContext context) {
    return Consumer2<SettingState, AdminUserState>(builder: (BuildContext context, SettingState settingState, AdminUserState adminUserState, child){
      return Scaffold(
        backgroundColor: Colors.white,
        body: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: CommonSettingTab(tabNum: widget.tabNum, funcType: KEY_CUSTOMAPP_SETTINGS_PROFILE),
            ),

            ListTile(
                leading: Text('LIST1 켜기/끄기', style:TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic, fontSize: 16)),
                trailing: dropDown(settingState, adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ISACTIVE, '')
            ),
            // dropDownListTile('내 정보 켜기/끄기', settingState, userModelState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ISACTIVE, ''),
            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ISACTIVE)?
            Padding(
              padding: const EdgeInsets.only(top: 10, left:10),
              child: Column(
                children: [
                  textEdit('LIST1 제목', context, adminUserState, _infoTitleController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_TITLE),
                  iconPickerListTile('LIST1 아이콘', context, settingState,adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ICON),
                  dropDownListTile('LIST1 이름 켜기/끄기', settingState, adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_ISACTIVE, ''),
                  selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_ISACTIVE)?
                  Padding(
                      padding: const EdgeInsets.only(left:10),
                      child:textEdit('LIST1-LIST1 제목', context, adminUserState, _infoNameTitleController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_TITLE)):SizedBox.shrink(),

                  dropDownListTile('LIST1-LIST2 켜기/끄기', settingState,adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_ISACTIVE, ''),
                  selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_ISACTIVE)?
                  Padding(
                      padding: const EdgeInsets.only(left:10),
                      child:textEdit('LIST1-LIST2 제목', context, adminUserState, _infoPositionTitleController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_TITLE)):SizedBox.shrink(),
                  ]
              ),
            ):SizedBox.shrink(),
            divide(1.0,0.1),


            ListTile(
                leading: Text('LIST2 켜기/끄기', style:TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic, fontSize: 16)),
                trailing: dropDown(settingState,adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ISACTIVE, '')
            ),
            // dropDownListTile('만든 list 켜기/끄기', settingState,userModelState,widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ISACTIVE, ''),
            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ISACTIVE)?
            Padding(
              padding: const EdgeInsets.only(top: 20, left:30),
              child: Column(
                children: [
                  iconPickerListTile('LIST2 list 아이콘', context, settingState, adminUserState,widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ICON),
                  CommonSettingListFunc(tabNum: widget.tabNum, funcType: KEY_CUSTOMAPP_SETTINGS_MADE),
                ],
              ),
            ):SizedBox.shrink(),
            divide(1.0,0.1),

            ListTile(
                leading: Text('LIST3 켜기/끄기', style:TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic, fontSize: 16)),
                trailing: dropDown(settingState,adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ISACTIVE, ''),
            ),
            // dropDownListTile('보낸 list 켜기/끄기', settingState,userModelState,widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ISACTIVE, ''),
            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ISACTIVE)?
            Padding(
              padding: const EdgeInsets.only(top: 20, left:30),
              child: Column(
                children: [
                  iconPickerListTile('LIST3 아이콘', context, settingState,adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ICON),
                  CommonSettingListFunc(tabNum: widget.tabNum, funcType: KEY_CUSTOMAPP_SETTINGS_SEND),
                ],
              ),
            ):SizedBox.shrink(),
            divide(1.0,0.1),

            ListTile(
              leading: Text('LIST4 켜기/끄기', style:TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic, fontSize: 16)),
              trailing: dropDown(settingState,adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ISACTIVE, ''),
            ),
            //dropDownListTile('받은 list 켜기/끄기', settingState,userModelState,widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ISACTIVE, ''),
            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ISACTIVE)?
            Padding(
              padding: const EdgeInsets.only(top: 20, left:30),
              child: Column(
                children: [
                  iconPickerListTile('LIST4 아이콘', context, settingState, adminUserState,widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ICON),
                  CommonSettingListFunc(tabNum: widget.tabNum, funcType: KEY_CUSTOMAPP_SETTINGS_RECEIVE),
                ],
              ),
            ):SizedBox.shrink(),
            divide(1.0,0.1),




            ListTile(
                leading: Text('LIST5 켜기/끄기', style:TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic, fontSize: 16)),
                trailing: dropDown(settingState,adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ISACTIVE, ''),
            ),
            // dropDownListTile('신고 켜기/끄기', settingState,userModelState,widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ISACTIVE, ''),
            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ISACTIVE)?
            Padding(
              padding: const EdgeInsets.only(top: 10, left:10),
              child: Column(
                  children: [
                    textEdit('LIST5 제목', context, adminUserState, _reportTitleController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_TITLE),
                    iconPickerListTile('LIST5 아이콘', context, settingState, adminUserState,widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ICON),
                    textEdit('LIST5 이메일', context, adminUserState, _reportEmailController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_EMAIL),
                  ]
              ),
            ):SizedBox.shrink(),
            divide(1.0,0.1),





            ListTile(
                leading: Text('LIST6 켜기/끄기', style:TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic, fontSize: 16)),
                trailing: dropDown(settingState,adminUserState,widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ISACTIVE, ''),
            ),
            // dropDownListTile('평점 켜기/끄기', settingState,userModelState,widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ISACTIVE, ''),
            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ISACTIVE)?
            Padding(
              padding: const EdgeInsets.only(top: 10, left:10),
              child: Column(
                  children: [
                    textEdit('LIST6 제목', context, adminUserState, _ratingTitleController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_TITLE),
                    iconPickerListTile('LIST6 아이콘', context, settingState, adminUserState,widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ICON),
                    textEdit('LIST6 구글플레이 Id', context, adminUserState, _ratingGoogleplayIdController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_GOOGLEPLAYID),
                    textEdit('LIST6 appstoreId', context, adminUserState, _ratingAppstoreIdController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_APPSTOREID),
                  ]
              ),
            ):SizedBox.shrink(),
            divide(1.0,0.1),




            ListTile(
              leading: Text('LIST7 켜기/끄기', style:TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic, fontSize: 16)),
              trailing: dropDown(settingState,adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ISACTIVE, ''),
            ),
            // dropDownListTile('공유 켜기/끄기', settingState,userModelState,widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ISACTIVE, ''),
            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ISACTIVE)?
            Padding(
              padding: const EdgeInsets.only(top: 10, left:10),
              child: Column(
                  children: [
                    textEdit('LIST7 제목', context, adminUserState, _shareTitleController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_TITLE),
                    iconPickerListTile('LIST7 아이콘', context, settingState, adminUserState,widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ICON),
                    textEdit('LIST7 메세지', context, adminUserState, _shareMsgController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_MSG),
                  ]
              ),
            ):SizedBox.shrink(),
            divide(1.0,0.1),





            ListTile(
              leading: Text('LIST8 켜기/끄기', style:TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic, fontSize: 16)),
              trailing: dropDown(settingState,adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ISACTIVE, ''),
            ),
            //dropDownListTile('설정 켜기/끄기', settingState,userModelState,widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ISACTIVE, ''),
            selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ISACTIVE)?
            Padding(
              padding: const EdgeInsets.only(top: 10, left:10),
              child: Column(
                  children: [
                    textEdit('LIST8 제목', context, adminUserState, _settingController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_TITLE),
                    iconPickerListTile('LIST8 아이콘', context, settingState, adminUserState,widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ICON),

                    dropDownListTile('LIST8-LIST1 켜기/끄기', settingState,adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_ISACTIVE, ''),
                    selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_ISACTIVE)?
                    Padding(
                        padding: const EdgeInsets.only(top: 10, left:10),
                        child:textEdit('LIST8-LIST1 제목', context, adminUserState, _settingAlramTitleController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_TITLE)):SizedBox.shrink(),

                    dropDownListTile('LIST8-LIST2 켜기/끄기', settingState,adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_ISACTIVE, ''),
                    selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_ISACTIVE)?
                    Padding(
                        padding: const EdgeInsets.only(top: 10, left:10),
                        child: textEdit('LIST8-LIST2 제목', context, adminUserState, _settingPrivacyController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_TITLE)):SizedBox.shrink(),
                    selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_ISACTIVE)?
                    Padding(
                        padding: const EdgeInsets.only(top: 10, left:10),
                        child: textEdit('LIST8-LIST2 url', context, adminUserState, _settingPrivacyUrlController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_URL)):SizedBox.shrink(),

                    dropDownListTile('LIST8-LIST3 켜기/끄기', settingState,adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_ISACTIVE, ''),
                    selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_ISACTIVE)?
                    Padding(
                        padding: const EdgeInsets.only(top: 10, left:10),
                        child: textEdit('LIST8-LIST3 제목', context, adminUserState, _settingProvisionTitleController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_TITLE)):SizedBox.shrink(),
                    selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_ISACTIVE)?
                    Padding(
                        padding: const EdgeInsets.only(top: 10, left:10),
                        child: textEdit('LIST8-LIST3 url', context, adminUserState, _settingProvisionUrlController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_URL)):SizedBox.shrink(),

                    dropDownListTile('LIST8-LIST4 켜기/끄기', settingState,adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_ISACTIVE, ''),
                    selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_ISACTIVE)?
                    Padding(
                        padding: const EdgeInsets.only(top: 10, left:10),
                        child: textEdit('LIST8-LIST4 제목', context, adminUserState, _settingLicenseTitleController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_TITLE)):SizedBox.shrink(),
                    selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_ISACTIVE)?
                    Padding(
                        padding: const EdgeInsets.only(top: 10, left:10),
                        child: textEdit('LIST8-LIST4 url', context, adminUserState, _settingLicenseUrlController, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_URL)):SizedBox.shrink(),

                    dropDownListTile('LIST8-LIST5 켜기/끄기', settingState, adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ISACTIVE, ''),
                    selectGetFuncAllType(settingState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ISACTIVE)?
                    Padding(
                        padding: const EdgeInsets.only(top: 10, left:10),
                        child: dropDownListTile('LIST8-LIST5 정렬', settingState, adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ALIGN, '')):SizedBox.shrink(),
                  ]
              ),
            ):SizedBox.shrink(),
          ],
        ),
      );
    });
  }

}
