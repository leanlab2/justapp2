import 'package:animate_icons/animate_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_iconpicker/flutter_iconpicker.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/admin_user_state.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/repo/setting_network_repository.dart';
import 'package:justapp/utils/simple_snackbar.dart';

arrowSimpleViewer(viewerStr, settingState, AnimateIconController _aniController){
  return AnimateIcons(
    startIcon: selectSimpleViewerType(viewerStr, settingState)?Icons.keyboard_arrow_down_outlined:Icons.keyboard_arrow_right_outlined,
    endIcon: selectSimpleViewerType(viewerStr, settingState)?Icons.keyboard_arrow_down_outlined:Icons.keyboard_arrow_right_outlined,
    controller: _aniController,
    size: 20.0,
    onStartIconPress: () {
      selectSimpleViewerType(viewerStr, settingState)?selectSimpleViewer(viewerStr, settingState, false):selectSimpleViewer(viewerStr, settingState, true);
      return true;
    },
    onEndIconPress: () {
      selectSimpleViewerType(viewerStr, settingState)?selectSimpleViewer(viewerStr, settingState, false):selectSimpleViewer(viewerStr, settingState, true);
      return true;
    },
    duration: Duration(milliseconds: 200),
    startIconColor: Colors.black,
    endIconColor: Colors.black,
    clockwise: false,
  );
}

dropDownListTile(String textStr, SettingState settingState, AdminUserState adminUserState, tabNum, funcType, field, mapField){
  return ListTile(
      leading: Text(textStr, style:TextStyle(color: Colors.black,fontSize: 13)),
      trailing: dropDown(settingState, adminUserState, tabNum, funcType, field, mapField)
  );
}
dropDown(SettingState settingState, AdminUserState adminUserState, tabNum, funcType, field, mapField) {
  return DropdownButton(
      value: mapField == '' && funcType == ''? selectGetType(settingState, tabNum, field):
      mapField == ''?selectGetFuncAllType(settingState, tabNum, funcType, field):
      selectGetFuncAllType(settingState, tabNum, funcType, field)[mapField],
      items: selectDropDownType(mapField==''?field:mapField),
      onChanged: (value) {
        if(mapField == '' && funcType == ''){
          settingNetworkRepository.selectSetTab(adminUserState.adminUserModel.userMade, tabNum, field, value);
        }
        else if(mapField == ''){
          settingNetworkRepository.selectSetTabFunc(adminUserState.adminUserModel.userMade, tabNum, funcType, field, value);
        }
        else {
          Map<dynamic, dynamic> fontTitleMap = {
            KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN: selectGetFuncAllType(settingState, tabNum, funcType, field)[KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN],
            KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR: selectGetFuncAllType(settingState, tabNum, funcType, field)[KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR],
            KEY_CUSTOMAPP_SETTINGS_FUNC_ISACTIVE: selectGetFuncAllType(settingState, tabNum, funcType, field)[KEY_CUSTOMAPP_SETTINGS_FUNC_ISACTIVE],
            KEY_CUSTOMAPP_SETTINGS_FUNC_LINE: selectGetFuncAllType(settingState, tabNum, funcType, field)[KEY_CUSTOMAPP_SETTINGS_FUNC_LINE],
            KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE: selectGetFuncAllType(settingState, tabNum, funcType, field)[KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE]};
          fontTitleMap[mapField] = value;

          settingNetworkRepository.selectSetTabFunc(adminUserState.adminUserModel.userMade, tabNum, funcType, field, fontTitleMap);
        }
      });
}

colorPickerListTile(String textStr, BuildContext context, SettingState settingState, AdminUserState adminUserState, tabNum, funcType, field, mapField){
  return ListTile(
      leading: Text(textStr, style:TextStyle(color: Colors.black,fontSize: 13)),
      trailing: colorPicker(context, settingState, adminUserState, tabNum, funcType, field, mapField)
  );
}
colorPicker(BuildContext context, SettingState settingState, AdminUserState adminUserState, tabNum, funcType, field, mapField) {

  Color currentColor = Colors.limeAccent;
  void changeColor(Color color) => currentColor = color;

  return RaisedButton(
    elevation: 3.0,
    onPressed: () {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('색상을 선택해 주세요.', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
            content: SingleChildScrollView(
              child: Column(
                children: [
                  // BlockPicker(
                  //   pickerColor: funcType== '' && mapField == ''? Color(selectGetType(settingState, tabNum, field)):
                  //                 mapField == ''? Color(selectGetFuncAllType(settingState, tabNum, funcType, field)):
                  //                 Color(selectGetFuncAllType(settingState, tabNum, funcType, field)[mapField]),
                  //   onColorChanged: changeColor,
                  // ),
                  ColorPicker(
                    pickerColor:funcType== '' && mapField == ''? Color(selectGetType(settingState, tabNum, field)):
                    mapField == ''? Color(selectGetFuncAllType(settingState, tabNum, funcType, field)):
                    Color(selectGetFuncAllType(settingState, tabNum, funcType, field)[mapField]),
                    onColorChanged: changeColor,
                    colorPickerWidth: 300.0,
                    pickerAreaHeightPercent: 0.7,
                    enableAlpha: true,
                    displayThumbColor: true,
                    showLabel: true,
                    paletteType: PaletteType.hsv,
                    pickerAreaBorderRadius: const BorderRadius.only(
                      topLeft: const Radius.circular(2.0),
                      topRight: const Radius.circular(2.0),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top:30, bottom:10),
                    child: FlatButton(
                      minWidth: 220,
                      child: Text("변경", style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
                      onPressed: () {
                        if(funcType== '' && mapField == ''){
                          settingNetworkRepository.selectSetTab(adminUserState.adminUserModel.userMade, tabNum, field, currentColor.value);
                        }
                        else if(mapField == ''){
                          settingNetworkRepository.selectSetTabFunc(
                              adminUserState.adminUserModel.userMade, tabNum, funcType, field, currentColor.value);
                        }
                        else {
                          Map<dynamic, dynamic> fontTitleMap = {
                            KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR: selectGetFuncAllType(
                                settingState, tabNum, funcType, field)[KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR],
                            KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN: selectGetFuncAllType(
                                settingState, tabNum, funcType, field)[KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN],
                            KEY_CUSTOMAPP_SETTINGS_FUNC_ISACTIVE: selectGetFuncAllType(
                                settingState, tabNum, funcType, field)[KEY_CUSTOMAPP_SETTINGS_FUNC_ISACTIVE],
                            KEY_CUSTOMAPP_SETTINGS_FUNC_LINE: selectGetFuncAllType(
                                settingState, tabNum, funcType, field)[KEY_CUSTOMAPP_SETTINGS_FUNC_LINE],
                            KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE: selectGetFuncAllType(
                                settingState, tabNum, funcType, field)[KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE]};
                          fontTitleMap[mapField] = currentColor.value;

                          settingNetworkRepository.selectSetTabFunc(adminUserState.adminUserModel.userMade, tabNum, funcType, field, fontTitleMap);
                        }
                        Navigator.pop(context);
                      },
                      color: Color(Constants.defaultColor),
                      textColor: Colors.white,
                      padding: EdgeInsets.symmetric(vertical: 18),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                    ),
                  ),
                  FlatButton(
                    minWidth: 220,
                    child: Text("취소", style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    color: Color(Constants.defaultColor),
                    textColor: Colors.white,
                    padding: EdgeInsets.symmetric(vertical: 18),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                  ),
                ],
              ),
            ),
          );
        },
      );
    },
    color: funcType== '' && mapField == ''? Color(selectGetType(settingState, tabNum, field)):
    mapField == ''? Color(selectGetFuncAllType(settingState, tabNum, funcType, field)):
    Color(selectGetFuncAllType(settingState, tabNum, funcType, field)[mapField]),
    shape: StadiumBorder(
      side: BorderSide(
        color: Colors.black,
        width: 0.5,
      ),
    ),
  );
}

textEdit(textStr, context, AdminUserState adminUserState, TextEditingController _textController, tabNum, funcType, field) {
  return ListTile(
      leading: Container(
          padding: const EdgeInsets.only(top: 3),
          child: Text(textStr, style:TextStyle(color: Colors.black,fontSize: 13))),
      subtitle: Container(
        child: TextField(
          controller: _textController,
          style: TextStyle(color: Color(Constants.defaultColor), fontWeight: FontWeight.bold, fontSize: 15),
          decoration: InputDecoration(
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            hintText: '',
          ),
          keyboardType: TextInputType.name,
          onChanged: (value) {},
        ),
      ),
      trailing: OutlineButton(
        borderSide: BorderSide(
          color: Colors.black,
          width: 0.5,
        ),
        child: Container(
          alignment: Alignment.center,
          height: 30,
          width: 30,
          child: Icon(Icons.edit),
        ),
        onPressed: () async{
          funcType == ''?settingNetworkRepository.selectSetTab(
              adminUserState.adminUserModel.userMade, tabNum, field, _textController.text).then((value) => {
                simpleLongSnackBar(context, '텍스트가 변경되었습니다.')}):
          settingNetworkRepository.selectSetTabFunc(
              adminUserState.adminUserModel.userMade, tabNum, funcType, field, _textController.text).then((value) => {
            simpleLongSnackBar(context, '텍스트가 변경되었습니다.')});
        },
      ),
  );
}

iconPickerListTile(textStr, context, settingState, userModelState, tabNum, funcType, field){
  IconData? iconPickerData;
  return ListTile(
    leading: Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Text(textStr, style: TextStyle(color: Colors.black, fontSize: 13)),
    ),
    subtitle: Container(
      alignment: Alignment.topRight,
      child: Icon(IconData(funcType== ''? selectGetType(settingState, tabNum, field):
            selectGetFuncAllType(settingState, tabNum, funcType, field), fontFamily: 'MaterialIcons'), color: Colors.black,)
    ),
    trailing: Padding(
      padding: const EdgeInsets.only(top: 10),
      child: ElevatedButton(
        onPressed: () async =>{
          iconPickerData = await FlutterIconPicker.showIconPicker(
            context,
              title: funcType == '' ?Text('변결하실 아이콘을 선택해 주세요.', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)):
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('변결하실 아이콘을 선택해 주세요.', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                  ElevatedButton(
                    onPressed: () async =>{
                        funcType == '' ? settingNetworkRepository.selectSetTab(userModelState.adminUserModel.userMade, tabNum, field, 0) :
                        settingNetworkRepository.selectSetTabFunc(userModelState.adminUserModel.userMade, tabNum, funcType, field, 0),
                    },
                    child: Text('아이콘 없애기', style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
                  ),
                ],
              ),
            showTooltips: true,
            iconPickerShape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            iconPackMode: IconPack.material
          ),
          if(iconPickerData != null){
            print("dsnjkldsafasdfas"),
            print(iconPickerData!.codePoint),
            funcType == '' ? settingNetworkRepository.selectSetTab(userModelState.adminUserModel.userMade, tabNum, field, iconPickerData!.codePoint) :
            settingNetworkRepository.selectSetTabFunc(userModelState.adminUserModel.userMade, tabNum, funcType, field, iconPickerData!.codePoint),
          },
          // print(iconPickerData!.codePoint)
        },
        child: Text('아이콘 변경', style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
      ),
    ),
  );
}

divide(thickness, opacity) {
  return Padding(
    padding: const EdgeInsets.only(bottom: 5),
    child: Divider(
      color: Colors.black.withOpacity(opacity),
      thickness: thickness,
    ),
  );
}