import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/board/board_setting_view.dart';
import 'package:justapp/app/builder/app/category/category_setting_view.dart';
import 'package:justapp/app/builder/app/entire/entire_view.dart';
import 'package:justapp/app/builder/app/favorite/favorite_setting_view.dart';
import 'package:justapp/app/builder/app/profile/profile_setting_view.dart';
import 'package:justapp/app/const/builder_keys.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';

//viewer(
selectSimpleViewerType(String viewerStr, SettingState settingState){
  return viewerStr == IS_ENTIRE_TAB_SIMPLE_VIEWER? settingState.isEntireTabSimpleViewer:
  viewerStr == IS_ENTIRE_STYLE_SIMPLE_VIEWER? settingState.isEntireStyleSimpleViewer:
  viewerStr == IS_BOARD_TAB_SIMPLE_VIEWER? settingState.isBoardTabSimpleViewer:
  viewerStr == IS_BOARD_STYLE_SIMPLE_VIEWER? settingState.isBoardStyleSimpleViewer:
  viewerStr == IS_BOARD_LIST_STYLE_SIMPLE_VIEWER? settingState.isBoardListStyleSimpleViewer:
  viewerStr == IS_BOARD_NAVI_STYLE_SIMPLE_VIEWER? settingState.isBoardNaviStyleSimpleViewer:
  viewerStr == IS_BOARD_FONT_STYLE_SIMPLE_VIEWER? settingState.isBoardFontStyleSimpleViewer:
  viewerStr == IS_BOARD_SUB_FONT_STYLE_SIMPLE_VIEWER? settingState.isBoardSubFontStyleSimpleViewer:
  viewerStr == IS_BOARD_CONTENTS_FONT_STYLE_SIMPLE_VIEWER? settingState.isBoardContentsFontStyleSimpleViewer:
  viewerStr == IS_FAVORITE_TAB_SIMPLE_VIEWER? settingState.isFavoriteTabSimpleViewer:
  viewerStr == IS_FAVORITE_STYLE_SIMPLE_VIEWER? settingState.isFavoriteStyleSimpleViewer:
  viewerStr == IS_FAVORITE_LIST_STYLE_SIMPLE_VIEWER? settingState.isFavoriteListStyleSimpleViewer:
  viewerStr == IS_FAVORITE_NAVI_STYLE_SIMPLE_VIEWER? settingState.isFavoriteNaviStyleSimpleViewer:
  viewerStr == IS_FAVORITE_FONT_STYLE_SIMPLE_VIEWER? settingState.isFavoriteFontStyleSimpleViewer:
  viewerStr == IS_FAVORITE_SUB_FONT_STYLE_SIMPLE_VIEWER? settingState.isFavoriteSubFontStyleSimpleViewer:
  viewerStr == IS_FAVORITE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER? settingState.isFavoriteContentsFontStyleSimpleViewer:
  viewerStr == IS_PROFILE_TAB_SIMPLE_VIEWER? settingState.isProfileTabSimpleViewer:
  viewerStr == IS_PROFILE_STYLE_SIMPLE_VIEWER? settingState.isProfileStyleSimpleViewer:
  viewerStr == IS_PROFILE_FONT_SIMPLE_VIEWER? settingState.isProfileFontSimpleViewer:
  viewerStr == IS_PROFILE_LIST_STYLE_SIMPLE_VIEWER? settingState.isProfileListStyleSimpleViewer:
  viewerStr == IS_PROFILE_NAVI_STYLE_SIMPLE_VIEWER? settingState.isProfileNaviStyleSimpleViewer:
  viewerStr == IS_PROFILE_FONT_STYLE_SIMPLE_VIEWER? settingState.isProfileFontStyleSimpleViewer:
  viewerStr == IS_PROFILE_SUB_FONT_STYLE_SIMPLE_VIEWER? settingState.isProfileSubFontStyleSimpleViewer:
  viewerStr == IS_PROFILE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER? settingState.isProfileContentsFontStyleSimpleViewer:
  viewerStr == IS_MADE_LIST_STYLE_SIMPLE_VIEWER? settingState.isMadeListStyleSimpleViewer:
  viewerStr == IS_MADE_NAVI_STYLE_SIMPLE_VIEWER? settingState.isMadeNaviStyleSimpleViewer:
  viewerStr == IS_MADE_FONT_STYLE_SIMPLE_VIEWER? settingState.isMadeFontStyleSimpleViewer:
  viewerStr == IS_MADE_SUB_FONT_STYLE_SIMPLE_VIEWER? settingState.isMadeSubFontStyleSimpleViewer:
  viewerStr == IS_MADE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER? settingState.isMadeContentsFontStyleSimpleViewer:
  viewerStr == IS_SEND_LIST_STYLE_SIMPLE_VIEWER? settingState.isSendListStyleSimpleViewer:
  viewerStr == IS_SEND_NAVI_STYLE_SIMPLE_VIEWER? settingState.isSendNaviStyleSimpleViewer:
  viewerStr == IS_SEND_FONT_STYLE_SIMPLE_VIEWER? settingState.isSendFontStyleSimpleViewer:
  viewerStr == IS_SEND_SUB_FONT_STYLE_SIMPLE_VIEWER? settingState.isSendSubFontStyleSimpleViewer:
  viewerStr == IS_SEND_CONTENTS_FONT_STYLE_SIMPLE_VIEWER? settingState.isSendContentsFontStyleSimpleViewer:
  viewerStr == IS_RECEIVE_LIST_STYLE_SIMPLE_VIEWER? settingState.isReceiveListStyleSimpleViewer:
  viewerStr == IS_RECEIVE_NAVI_STYLE_SIMPLE_VIEWER? settingState.isReceiveNaviStyleSimpleViewer:
  viewerStr == IS_RECEIVE_FONT_STYLE_SIMPLE_VIEWER? settingState.isReceiveFontStyleSimpleViewer:
  viewerStr == IS_RECEIVE_SUB_FONT_STYLE_SIMPLE_VIEWER? settingState.isReceiveSubFontStyleSimpleViewer:
  viewerStr == IS_RECEIVE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER? settingState.isReceiveContentsFontStyleSimpleViewer:settingState.isReceiveContentsFontStyleSimpleViewer;

}
selectSimpleViewer(String viewerStr, SettingState settingState, bool viewerBool){
  switch(viewerStr){
    case IS_ENTIRE_TAB_SIMPLE_VIEWER: settingState.isEntireTabSimpleViewer = viewerBool; break;
    case IS_ENTIRE_TAB_SIMPLE_VIEWER: settingState.isEntireStyleSimpleViewer = viewerBool; break;
    case IS_BOARD_TAB_SIMPLE_VIEWER: settingState.isBoardTabSimpleViewer = viewerBool; break;
    case IS_BOARD_STYLE_SIMPLE_VIEWER: settingState.isBoardStyleSimpleViewer = viewerBool; break;
    case IS_BOARD_LIST_STYLE_SIMPLE_VIEWER: settingState.isBoardListStyleSimpleViewer = viewerBool; break;
    case IS_BOARD_NAVI_STYLE_SIMPLE_VIEWER: settingState.isBoardNaviStyleSimpleViewer = viewerBool; break;
    case IS_BOARD_FONT_STYLE_SIMPLE_VIEWER: settingState.isBoardFontStyleSimpleViewer = viewerBool; break;
    case IS_BOARD_SUB_FONT_STYLE_SIMPLE_VIEWER: settingState.isBoardSubFontStyleSimpleViewer = viewerBool; break;
    case IS_BOARD_CONTENTS_FONT_STYLE_SIMPLE_VIEWER: settingState.isBoardContentsFontStyleSimpleViewer = viewerBool; break;
    case IS_FAVORITE_TAB_SIMPLE_VIEWER: settingState.isFavoriteTabSimpleViewer = viewerBool; break;
    case IS_FAVORITE_STYLE_SIMPLE_VIEWER: settingState.isFavoriteStyleSimpleViewer = viewerBool; break;
    case IS_FAVORITE_LIST_STYLE_SIMPLE_VIEWER: settingState.isFavoriteListStyleSimpleViewer = viewerBool; break;
    case IS_FAVORITE_NAVI_STYLE_SIMPLE_VIEWER: settingState.isFavoriteNaviStyleSimpleViewer = viewerBool; break;
    case IS_FAVORITE_FONT_STYLE_SIMPLE_VIEWER: settingState.isFavoriteFontStyleSimpleViewer = viewerBool; break;
    case IS_FAVORITE_SUB_FONT_STYLE_SIMPLE_VIEWER: settingState.isFavoriteSubFontStyleSimpleViewer = viewerBool; break;
    case IS_FAVORITE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER: settingState.isFavoriteContentsFontStyleSimpleViewer = viewerBool; break;
    case IS_PROFILE_TAB_SIMPLE_VIEWER: settingState.isProfileTabSimpleViewer = viewerBool; break;
    case IS_PROFILE_STYLE_SIMPLE_VIEWER: settingState.isProfileStyleSimpleViewer = viewerBool; break;
    case IS_PROFILE_FONT_SIMPLE_VIEWER: settingState.isProfileFontSimpleViewer = viewerBool; break;
    case IS_PROFILE_LIST_STYLE_SIMPLE_VIEWER: settingState.isProfileListStyleSimpleViewer = viewerBool; break;
    case IS_PROFILE_NAVI_STYLE_SIMPLE_VIEWER: settingState.isProfileNaviStyleSimpleViewer = viewerBool; break;
    case IS_PROFILE_FONT_STYLE_SIMPLE_VIEWER: settingState.isProfileFontStyleSimpleViewer = viewerBool; break;
    case IS_PROFILE_SUB_FONT_STYLE_SIMPLE_VIEWER: settingState.isProfileSubFontStyleSimpleViewer = viewerBool; break;
    case IS_PROFILE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER: settingState.isProfileContentsFontStyleSimpleViewer = viewerBool; break;
    case IS_MADE_LIST_STYLE_SIMPLE_VIEWER: settingState.isMadeListStyleSimpleViewer = viewerBool; break;
    case IS_MADE_NAVI_STYLE_SIMPLE_VIEWER: settingState.isMadeNaviStyleSimpleViewer = viewerBool; break;
    case IS_MADE_FONT_STYLE_SIMPLE_VIEWER: settingState.isMadeFontStyleSimpleViewer = viewerBool; break;
    case IS_MADE_SUB_FONT_STYLE_SIMPLE_VIEWER: settingState.isMadeSubFontStyleSimpleViewer = viewerBool; break;
    case IS_MADE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER: settingState.isMadeContentsFontStyleSimpleViewer = viewerBool; break;
    case IS_SEND_LIST_STYLE_SIMPLE_VIEWER: settingState.isSendListStyleSimpleViewer = viewerBool; break;
    case IS_SEND_NAVI_STYLE_SIMPLE_VIEWER: settingState.isSendNaviStyleSimpleViewer = viewerBool; break;
    case IS_SEND_FONT_STYLE_SIMPLE_VIEWER: settingState.isSendFontStyleSimpleViewer = viewerBool; break;
    case IS_SEND_SUB_FONT_STYLE_SIMPLE_VIEWER: settingState.isSendSubFontStyleSimpleViewer = viewerBool; break;
    case IS_SEND_CONTENTS_FONT_STYLE_SIMPLE_VIEWER: settingState.isSendContentsFontStyleSimpleViewer = viewerBool; break;
    case IS_RECEIVE_LIST_STYLE_SIMPLE_VIEWER: settingState.isReceiveListStyleSimpleViewer = viewerBool; break;
    case IS_RECEIVE_NAVI_STYLE_SIMPLE_VIEWER: settingState.isReceiveNaviStyleSimpleViewer = viewerBool; break;
    case IS_RECEIVE_FONT_STYLE_SIMPLE_VIEWER: settingState.isReceiveFontStyleSimpleViewer = viewerBool; break;
    case IS_RECEIVE_SUB_FONT_STYLE_SIMPLE_VIEWER: settingState.isReceiveSubFontStyleSimpleViewer = viewerBool; break;
    case IS_RECEIVE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER: settingState.isReceiveContentsFontStyleSimpleViewer = viewerBool; break;
  }
}

//dropdowntype
selectDropDownType(field){
  return
    field == KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN || field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ALIGN?
    Constants.valueFontAlignType.map(
          (value) {
        return DropdownMenuItem(
          value: value,
          child: Text(value.toString()),
        );
      },
    ).toList():
    field == KEY_CUSTOMAPP_SETTINGS_FUNC_LINE ?
    Constants.valueFontLineType.map(
          (value) {
        return DropdownMenuItem(
          value: value,
          child: Text(value.toString()),
        );
      },
    ).toList():
    field == KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE || field == KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE?
    Constants.valueFontSizeType.map(
          (value) {
        return DropdownMenuItem(
          value: value,
          child: Text(value.toString()),
        );
      },
    ).toList():
    field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE?
    Constants.valueShapeType.map(
          (value) {
        return DropdownMenuItem(
          value: value,
          child: Text(value.toString()),
        );
      },
    ).toList():
    field == KEY_CUSTOMAPP_SETTINGS_ENTIRE_TABCOUNT?
    Constants.valueListTabCount.map(
          (value) {
        return DropdownMenuItem(
          value: value,
          child: Text(value.toString()),
        );
      },
    ).toList():
    field == KEY_CUSTOMAPP_SETTINGS_ENTIRE_CATEGORYCOUNT ?
    Constants.valueListCategoryCount.map(
          (value) {
        return DropdownMenuItem(
          value: value,
          child: Text(value.toString()),
        );
      },
    ).toList():
    field == KEY_CUSTOMAPP_SETTINGS_TAB_FUNCPAGE ?
    Constants.valueFuncPage.map(
          (value) {
        return DropdownMenuItem(
          value: value,
          child: Text(value.toString()),
        );
      },
    ).toList():
    Constants.valueListBool.map(
          (value) {
        return DropdownMenuItem(
          value: value,
          child: Text(value.toString()),
        );
      },
    ).toList();
}

//tab
selectBodySetting(int index, SettingState settingState) {
  switch (index) {
    case 1:
      return settingState.tab1Model.funcPage == KEY_CUSTOMAPP_SETTINGS_BOARD
          ? BoardSettingView(tabNum: 1)
          : settingState.tab1Model.funcPage == KEY_CUSTOMAPP_SETTINGS_FAVORITE
              ? FavoriteSettingView(tabNum: 1)
              : settingState.tab1Model.funcPage == KEY_CUSTOMAPP_SETTINGS_CATEGORY
                  ? CategorySettingView(tabNum: 1)
                  : ProfileSettingView(tabNum: 1);
    case 2:
      return settingState.tab2Model.funcPage == KEY_CUSTOMAPP_SETTINGS_BOARD
          ? BoardSettingView(tabNum: 2)
          : settingState.tab2Model.funcPage == KEY_CUSTOMAPP_SETTINGS_FAVORITE
              ? FavoriteSettingView(tabNum: 2)
              : settingState.tab2Model.funcPage == KEY_CUSTOMAPP_SETTINGS_CATEGORY
                  ? CategorySettingView(tabNum: 2)
                  : ProfileSettingView(tabNum: 2);
    case 3:
      return settingState.tab3Model.funcPage == KEY_CUSTOMAPP_SETTINGS_BOARD
          ? BoardSettingView(tabNum: 3)
          : settingState.tab3Model.funcPage == KEY_CUSTOMAPP_SETTINGS_FAVORITE
              ? FavoriteSettingView(tabNum: 3)
              : settingState.tab3Model.funcPage == KEY_CUSTOMAPP_SETTINGS_CATEGORY
                  ? CategorySettingView(tabNum: 3)
                  : ProfileSettingView(tabNum: 3);
    case 4:
      return settingState.tab4Model.funcPage == KEY_CUSTOMAPP_SETTINGS_BOARD
          ? BoardSettingView(tabNum: 4)
          : settingState.tab4Model.funcPage == KEY_CUSTOMAPP_SETTINGS_FAVORITE
              ? FavoriteSettingView(tabNum: 4)
              : settingState.tab4Model.funcPage == KEY_CUSTOMAPP_SETTINGS_CATEGORY
                  ? CategorySettingView(tabNum: 4)
                  : ProfileSettingView(tabNum: 4);
    case 5:
      return settingState.tab5Model.funcPage == KEY_CUSTOMAPP_SETTINGS_BOARD
          ? BoardSettingView(tabNum: 5)
          : settingState.tab5Model.funcPage == KEY_CUSTOMAPP_SETTINGS_FAVORITE
              ? FavoriteSettingView(tabNum: 5)
              : settingState.tab5Model.funcPage == KEY_CUSTOMAPP_SETTINGS_CATEGORY
                  ? CategorySettingView(tabNum: 5)
                  : ProfileSettingView(tabNum: 5);
    default:
      return EntireView();
  }
}

//allType(without string)
selectGetType(SettingState settingState, int tabNum, String field) {
  if(tabNum == -1){
    if(field == KEY_CUSTOMAPP_SETTINGS_ENTIRE_SPLASHIMG) return settingState.entireModel.splashImg;
    else if(field == KEY_CUSTOMAPP_SETTINGS_ENTIRE_TABCOUNT) return settingState.entireModel.tabCount;
    else if(field == KEY_CUSTOMAPP_SETTINGS_ENTIRE_CATEGORYCOUNT) return settingState.entireModel.categoryCount;
    else if(field == KEY_CUSTOMAPP_SETTINGS_ENTIRE_DEFAULTCOLOR) return settingState.entireModel.defaultColor;
    else if(field == KEY_CUSTOMAPP_SETTINGS_ENTIRE_TABCOLOR) return settingState.entireModel.tabColor;
    else if(field == KEY_CUSTOMAPP_SETTINGS_ENTIRE_TABBGCOLOR) return settingState.entireModel.tabBgColor;
    else if(field == KEY_CUSTOMAPP_SETTINGS_CATEGORY1) return settingState.entireModel.category1;
    else if(field == KEY_CUSTOMAPP_SETTINGS_CATEGORY2) return settingState.entireModel.category2;
    else if(field == KEY_CUSTOMAPP_SETTINGS_CATEGORY3) return settingState.entireModel.category3;
    else if(field == KEY_CUSTOMAPP_SETTINGS_CATEGORY4) return settingState.entireModel.category4;
    else if(field == KEY_CUSTOMAPP_SETTINGS_CATEGORY5) return settingState.entireModel.category5;
    else if(field == KEY_CUSTOMAPP_SETTINGS_CATEGORY6) return settingState.entireModel.category6;
    else if(field == KEY_CUSTOMAPP_SETTINGS_CATEGORY7) return settingState.entireModel.category7;
    else if(field == KEY_CUSTOMAPP_SETTINGS_CATEGORY8) return settingState.entireModel.category8;
    else return settingState.entireModel.tabBgColor;
  }
  if(tabNum ==1){
    if(field == KEY_CUSTOMAPP_SETTINGS_TAB_FUNCPAGE) return settingState.tab1Model.funcPage;
    else if(field == KEY_CUSTOMAPP_SETTINGS_TAB_BGCOLOR) return settingState.tab1Model.bgColor;
    else if(field == KEY_CUSTOMAPP_SETTINGS_TAB_ICON) return settingState.tab1Model.tabIcon;
    else if(field == KEY_CUSTOMAPP_SETTINGS_TAB_TITLE) return settingState.tab1Model.tabTitle;
    else return settingState.tab1Model.tabIcon;
  }
  else if(tabNum ==2){
    if(field == KEY_CUSTOMAPP_SETTINGS_TAB_FUNCPAGE) return settingState.tab2Model.funcPage;
    else if(field == KEY_CUSTOMAPP_SETTINGS_TAB_BGCOLOR) return settingState.tab2Model.bgColor;
    else if(field == KEY_CUSTOMAPP_SETTINGS_TAB_ICON) return settingState.tab2Model.tabIcon;
    else if(field == KEY_CUSTOMAPP_SETTINGS_TAB_TITLE) return settingState.tab2Model.tabTitle;
    else return settingState.tab2Model.tabIcon;
  }
  else if(tabNum ==3){
    if(field == KEY_CUSTOMAPP_SETTINGS_TAB_FUNCPAGE) return settingState.tab3Model.funcPage;
    else if(field == KEY_CUSTOMAPP_SETTINGS_TAB_BGCOLOR) return settingState.tab3Model.bgColor;
    else if(field == KEY_CUSTOMAPP_SETTINGS_TAB_ICON) return settingState.tab3Model.tabIcon;
    else if(field == KEY_CUSTOMAPP_SETTINGS_TAB_TITLE) return settingState.tab3Model.tabTitle;
    else return settingState.tab3Model.tabIcon;
  }
  else if(tabNum ==4){
    if(field == KEY_CUSTOMAPP_SETTINGS_TAB_FUNCPAGE) return settingState.tab4Model.funcPage;
    else if(field == KEY_CUSTOMAPP_SETTINGS_TAB_BGCOLOR) return settingState.tab4Model.bgColor;
    else if(field == KEY_CUSTOMAPP_SETTINGS_TAB_ICON) return settingState.tab4Model.tabIcon;
    else if(field == KEY_CUSTOMAPP_SETTINGS_TAB_TITLE) return settingState.tab4Model.tabTitle;
    else return settingState.tab4Model.tabIcon;
  }
  else if(tabNum ==5){
    if(field == KEY_CUSTOMAPP_SETTINGS_TAB_FUNCPAGE) return settingState.tab5Model.funcPage;
    else if(field == KEY_CUSTOMAPP_SETTINGS_TAB_BGCOLOR) return settingState.tab5Model.bgColor;
    else if(field == KEY_CUSTOMAPP_SETTINGS_TAB_ICON) return settingState.tab5Model.tabIcon;
    else if(field == KEY_CUSTOMAPP_SETTINGS_TAB_TITLE) return settingState.tab5Model.tabTitle;
    else return settingState.tab5Model.tabIcon;
  }
}

//funcAllType
selectGetFuncAllType(SettingState settingState, int tabNum, String funcType, String field) {
    if(tabNum ==1){
    if(funcType == KEY_CUSTOMAPP_SETTINGS_BOARD){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab1BoardModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab1BoardModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab1BoardModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab1BoardModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab1BoardModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab1BoardModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab1BoardModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_BOARD_NAVI_CREATETITLE) return settingState.tab1BoardModel.naviCreateTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab1BoardModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab1BoardModel.fontSubTitle;
      else return settingState.tab1BoardModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab1FavoriteModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab1FavoriteModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab1FavoriteModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab1FavoriteModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab1FavoriteModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab1FavoriteModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab1FavoriteModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab1FavoriteModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab1FavoriteModel.fontSubTitle;
      else return settingState.tab1FavoriteModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_CATEGORY){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab1CategoryModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab1CategoryModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab1CategoryModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab1CategoryModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab1CategoryModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab1CategoryModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab1CategoryModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab1CategoryModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab1CategoryModel.fontSubTitle;
      else return settingState.tab1CategoryModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_MADE){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab1MadeModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab1MadeModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab1MadeModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab1MadeModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab1MadeModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab1MadeModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab1MadeModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_MADE_NAVI_UPDATETITLE) return settingState.tab1MadeModel.naviUpdateTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab1MadeModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab1MadeModel.fontSubTitle;
      else return settingState.tab1MadeModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_SEND){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab1SendModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab1SendModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab1SendModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab1SendModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab1SendModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab1SendModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab1SendModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab1SendModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab1SendModel.fontSubTitle;
      else return settingState.tab1SendModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab1ReceiveModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab1ReceiveModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab1ReceiveModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab1ReceiveModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab1ReceiveModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab1ReceiveModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab1ReceiveModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab1ReceiveModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab1ReceiveModel.fontSubTitle;
      else return settingState.tab1ReceiveModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else{ //KEY_CUSTOMAPP_SETTINGS_PROFILE
      if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR) return settingState.tab1ProfileModel.fontColor;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE) return settingState.tab1ProfileModel.fontSize;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ISACTIVE) return settingState.tab1ProfileModel.isInfoActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_TITLE) return settingState.tab1ProfileModel.infoTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ICON) return settingState.tab1ProfileModel.infoIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_ISACTIVE) return settingState.tab1ProfileModel.isNameActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_TITLE) return settingState.tab1ProfileModel.nameTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_ISACTIVE) return settingState.tab1ProfileModel.isPositionActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_TITLE) return settingState.tab1ProfileModel.positionTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ISACTIVE) return settingState.tab1ProfileModel.isMadeActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ICON) return settingState.tab1ProfileModel.madeIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ISACTIVE) return settingState.tab1ProfileModel.isSendActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ICON) return settingState.tab1ProfileModel.sendIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ISACTIVE) return settingState.tab1ProfileModel.isReceiveActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ICON) return settingState.tab1ProfileModel.receiveIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ISACTIVE) return settingState.tab1ProfileModel.isReportActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_TITLE) return settingState.tab1ProfileModel.reportTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ICON) return settingState.tab1ProfileModel.reportIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_EMAIL) return settingState.tab1ProfileModel.reportEmail;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ISACTIVE) return settingState.tab1ProfileModel.isRatingActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_TITLE) return settingState.tab1ProfileModel.ratingTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ICON) return settingState.tab1ProfileModel.ratingIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_GOOGLEPLAYID) return settingState.tab1ProfileModel.ratingGoogleplayId;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_APPSTOREID) return settingState.tab1ProfileModel.ratingAppsotreId;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ISACTIVE) return settingState.tab1ProfileModel.isShareActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_TITLE) return settingState.tab1ProfileModel.shareTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ICON) return settingState.tab1ProfileModel.shareIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_MSG) return settingState.tab1ProfileModel.shareMsg;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ISACTIVE) return settingState.tab1ProfileModel.isSettingActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_TITLE) return settingState.tab1ProfileModel.settingTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ICON) return settingState.tab1ProfileModel.settingIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_ISACTIVE) return settingState.tab1ProfileModel.isAlramActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_TITLE) return settingState.tab1ProfileModel.alramTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_ISACTIVE) return settingState.tab1ProfileModel.isPrivacyActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_TITLE) return settingState.tab1ProfileModel.privacyTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_URL) return settingState.tab1ProfileModel.privacyUrl;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_ISACTIVE) return settingState.tab1ProfileModel.isProvisionActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_TITLE) return settingState.tab1ProfileModel.provisionTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_URL) return settingState.tab1ProfileModel.provisionUrl;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_ISACTIVE) return settingState.tab1ProfileModel.isLicenseActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_TITLE) return settingState.tab1ProfileModel.licenseTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_URL) return settingState.tab1ProfileModel.licenseUrl;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ISACTIVE) return settingState.tab1ProfileModel.isAccountActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ALIGN) return settingState.tab1ProfileModel.accountAlign;
      else return settingState.tab1ProfileModel.accountAlign;
    }
  }
  else if(tabNum ==2){
    if(funcType == KEY_CUSTOMAPP_SETTINGS_BOARD){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab2BoardModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab2BoardModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab2BoardModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab2BoardModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab2BoardModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab2BoardModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab2BoardModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_BOARD_NAVI_CREATETITLE) return settingState.tab2BoardModel.naviCreateTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab2BoardModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab2BoardModel.fontSubTitle;
      else return settingState.tab2BoardModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab2FavoriteModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab2FavoriteModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab2FavoriteModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab2FavoriteModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab2FavoriteModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab2FavoriteModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab2FavoriteModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab2FavoriteModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab2FavoriteModel.fontSubTitle;
      else return settingState.tab2FavoriteModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_CATEGORY){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab2CategoryModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab2CategoryModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab2CategoryModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab2CategoryModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab2CategoryModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab2CategoryModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab2CategoryModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab2CategoryModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab2CategoryModel.fontSubTitle;
      else return settingState.tab2CategoryModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_MADE){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab2MadeModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab2MadeModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab2MadeModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab2MadeModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab2MadeModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab2MadeModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab2MadeModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_MADE_NAVI_UPDATETITLE) return settingState.tab2MadeModel.naviUpdateTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab2MadeModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab2MadeModel.fontSubTitle;
      else return settingState.tab2MadeModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_SEND){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab2SendModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab2SendModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab2SendModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab2SendModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab2SendModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab2SendModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab2SendModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab2SendModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab2SendModel.fontSubTitle;
      else return settingState.tab2SendModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab2ReceiveModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab2ReceiveModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab2ReceiveModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab2ReceiveModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab2ReceiveModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab2ReceiveModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab2ReceiveModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab2ReceiveModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab2ReceiveModel.fontSubTitle;
      else return settingState.tab2ReceiveModel.fontContents; //KEY_Y_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else{ //KEY_CUSTOMAPP_SETTINGS_PROFILE
      if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR) return settingState.tab2ProfileModel.fontColor;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE) return settingState.tab2ProfileModel.fontSize;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ISACTIVE) return settingState.tab2ProfileModel.isInfoActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_TITLE) return settingState.tab2ProfileModel.infoTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ICON) return settingState.tab2ProfileModel.infoIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_ISACTIVE) return settingState.tab2ProfileModel.isNameActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_TITLE) return settingState.tab2ProfileModel.nameTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_ISACTIVE) return settingState.tab2ProfileModel.isPositionActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_TITLE) return settingState.tab2ProfileModel.positionTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ISACTIVE) return settingState.tab2ProfileModel.isMadeActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ICON) return settingState.tab2ProfileModel.madeIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ISACTIVE) return settingState.tab2ProfileModel.isSendActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ICON) return settingState.tab2ProfileModel.sendIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ISACTIVE) return settingState.tab2ProfileModel.isReceiveActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ICON) return settingState.tab2ProfileModel.receiveIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ISACTIVE) return settingState.tab2ProfileModel.isReportActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_TITLE) return settingState.tab2ProfileModel.reportTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ICON) return settingState.tab2ProfileModel.reportIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_EMAIL) return settingState.tab2ProfileModel.reportEmail;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ISACTIVE) return settingState.tab2ProfileModel.isRatingActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_TITLE) return settingState.tab2ProfileModel.ratingTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ICON) return settingState.tab2ProfileModel.ratingIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_GOOGLEPLAYID) return settingState.tab2ProfileModel.ratingGoogleplayId;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_APPSTOREID) return settingState.tab2ProfileModel.ratingAppsotreId;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ISACTIVE) return settingState.tab2ProfileModel.isShareActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_TITLE) return settingState.tab2ProfileModel.shareTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ICON) return settingState.tab2ProfileModel.shareIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_MSG) return settingState.tab2ProfileModel.shareMsg;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ISACTIVE) return settingState.tab2ProfileModel.isSettingActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_TITLE) return settingState.tab2ProfileModel.settingTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ICON) return settingState.tab2ProfileModel.settingIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_ISACTIVE) return settingState.tab2ProfileModel.isAlramActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_TITLE) return settingState.tab2ProfileModel.alramTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_ISACTIVE) return settingState.tab2ProfileModel.isPrivacyActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_TITLE) return settingState.tab2ProfileModel.privacyTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_URL) return settingState.tab2ProfileModel.privacyUrl;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_ISACTIVE) return settingState.tab2ProfileModel.isProvisionActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_TITLE) return settingState.tab2ProfileModel.provisionTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_URL) return settingState.tab2ProfileModel.provisionUrl;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_ISACTIVE) return settingState.tab2ProfileModel.isLicenseActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_TITLE) return settingState.tab2ProfileModel.licenseTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_URL) return settingState.tab2ProfileModel.licenseUrl;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ISACTIVE) return settingState.tab2ProfileModel.isAccountActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ALIGN) return settingState.tab2ProfileModel.accountAlign;
      else return settingState.tab2ProfileModel.accountAlign;
    }
  }
  else if(tabNum ==3){
    if(funcType == KEY_CUSTOMAPP_SETTINGS_BOARD){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab3BoardModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab3BoardModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab3BoardModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab3BoardModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab3BoardModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab3BoardModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab3BoardModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_BOARD_NAVI_CREATETITLE) return settingState.tab3BoardModel.naviCreateTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab3BoardModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab3BoardModel.fontSubTitle;
      else return settingState.tab3BoardModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab3FavoriteModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab3FavoriteModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab3FavoriteModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab3FavoriteModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab3FavoriteModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab3FavoriteModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab3FavoriteModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab3FavoriteModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab3FavoriteModel.fontSubTitle;
      else return settingState.tab3FavoriteModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_CATEGORY){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab3CategoryModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab3CategoryModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab3CategoryModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab3CategoryModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab3CategoryModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab3CategoryModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab3CategoryModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab3CategoryModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab3CategoryModel.fontSubTitle;
      else return settingState.tab3CategoryModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_MADE){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab3MadeModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab3MadeModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab3MadeModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab3MadeModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab3MadeModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab3MadeModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab3MadeModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_MADE_NAVI_UPDATETITLE) return settingState.tab3MadeModel.naviUpdateTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab3MadeModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab3MadeModel.fontSubTitle;
      else return settingState.tab3MadeModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_SEND){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab3SendModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab3SendModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab3SendModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab3SendModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab3SendModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab3SendModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab3SendModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab3SendModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab3SendModel.fontSubTitle;
      else return settingState.tab3SendModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab3ReceiveModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab3ReceiveModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab3ReceiveModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab3ReceiveModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab3ReceiveModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab3ReceiveModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab3ReceiveModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab3ReceiveModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab3ReceiveModel.fontSubTitle;
      else return settingState.tab3ReceiveModel.fontContents; //EY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else{ //KEY_CUSTOMAPP_SETTINGS_PROFILE
      if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR) return settingState.tab3ProfileModel.fontColor;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE) return settingState.tab3ProfileModel.fontSize;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ISACTIVE) return settingState.tab3ProfileModel.isInfoActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_TITLE) return settingState.tab3ProfileModel.infoTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ICON) return settingState.tab3ProfileModel.infoIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_ISACTIVE) return settingState.tab3ProfileModel.isNameActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_TITLE) return settingState.tab3ProfileModel.nameTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_ISACTIVE) return settingState.tab3ProfileModel.isPositionActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_TITLE) return settingState.tab3ProfileModel.positionTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ISACTIVE) return settingState.tab3ProfileModel.isMadeActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ICON) return settingState.tab3ProfileModel.madeIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ISACTIVE) return settingState.tab3ProfileModel.isSendActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ICON) return settingState.tab3ProfileModel.sendIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ISACTIVE) return settingState.tab3ProfileModel.isReceiveActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ICON) return settingState.tab3ProfileModel.receiveIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ISACTIVE) return settingState.tab3ProfileModel.isReportActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_TITLE) return settingState.tab3ProfileModel.reportTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ICON) return settingState.tab3ProfileModel.reportIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_EMAIL) return settingState.tab3ProfileModel.reportEmail;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ISACTIVE) return settingState.tab3ProfileModel.isRatingActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_TITLE) return settingState.tab3ProfileModel.ratingTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ICON) return settingState.tab3ProfileModel.ratingIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_GOOGLEPLAYID) return settingState.tab3ProfileModel.ratingGoogleplayId;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_APPSTOREID) return settingState.tab3ProfileModel.ratingAppsotreId;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ISACTIVE) return settingState.tab3ProfileModel.isShareActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_TITLE) return settingState.tab3ProfileModel.shareTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ICON) return settingState.tab3ProfileModel.shareIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_MSG) return settingState.tab3ProfileModel.shareMsg;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ISACTIVE) return settingState.tab3ProfileModel.isSettingActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_TITLE) return settingState.tab3ProfileModel.settingTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ICON) return settingState.tab3ProfileModel.settingIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_ISACTIVE) return settingState.tab3ProfileModel.isAlramActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_TITLE) return settingState.tab3ProfileModel.alramTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_ISACTIVE) return settingState.tab3ProfileModel.isPrivacyActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_TITLE) return settingState.tab3ProfileModel.privacyTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_URL) return settingState.tab3ProfileModel.privacyUrl;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_ISACTIVE) return settingState.tab3ProfileModel.isProvisionActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_TITLE) return settingState.tab3ProfileModel.provisionTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_URL) return settingState.tab3ProfileModel.provisionUrl;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_ISACTIVE) return settingState.tab3ProfileModel.isLicenseActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_TITLE) return settingState.tab3ProfileModel.licenseTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_URL) return settingState.tab3ProfileModel.licenseUrl;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ISACTIVE) return settingState.tab3ProfileModel.isAccountActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ALIGN) return settingState.tab3ProfileModel.accountAlign;
      else return settingState.tab3ProfileModel.accountAlign;
    }
  }
  else if(tabNum ==4){
    if(funcType == KEY_CUSTOMAPP_SETTINGS_BOARD){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab4BoardModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab4BoardModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab4BoardModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab4BoardModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab4BoardModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab4BoardModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab4BoardModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_BOARD_NAVI_CREATETITLE) return settingState.tab4BoardModel.naviCreateTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab4BoardModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab4BoardModel.fontSubTitle;
      else return settingState.tab4BoardModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab4FavoriteModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab4FavoriteModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab4FavoriteModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab4FavoriteModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab4FavoriteModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab4FavoriteModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab4FavoriteModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab4FavoriteModel.fontSubTitle;
      else return settingState.tab4FavoriteModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_CATEGORY){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab4CategoryModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab4CategoryModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab4CategoryModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab4CategoryModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab4CategoryModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab4CategoryModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab4CategoryModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab4CategoryModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab4CategoryModel.fontSubTitle;
      else return settingState.tab4CategoryModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_MADE){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab4MadeModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab4MadeModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab4MadeModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab4MadeModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab4MadeModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab4MadeModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab4MadeModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_MADE_NAVI_UPDATETITLE) return settingState.tab4MadeModel.naviUpdateTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab4MadeModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab4MadeModel.fontSubTitle;
      else return settingState.tab4MadeModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_SEND){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab4SendModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab4SendModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab4SendModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab4SendModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab4SendModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab4SendModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab4SendModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab4SendModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab4SendModel.fontSubTitle;
      else return settingState.tab4SendModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab4ReceiveModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab4ReceiveModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab4ReceiveModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab4ReceiveModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab4ReceiveModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab4ReceiveModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab4ReceiveModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab4ReceiveModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab4ReceiveModel.fontSubTitle;
      else return settingState.tab4ReceiveModel.fontContents;
    }
    else{ //KEY_CUSTOMAPP_SETTINGS_PROFILE
      if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR) return settingState.tab4ProfileModel.fontColor;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE) return settingState.tab4ProfileModel.fontSize;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ISACTIVE) return settingState.tab4ProfileModel.isInfoActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_TITLE) return settingState.tab4ProfileModel.infoTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ICON) return settingState.tab4ProfileModel.infoIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_ISACTIVE) return settingState.tab4ProfileModel.isNameActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_TITLE) return settingState.tab4ProfileModel.nameTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_ISACTIVE) return settingState.tab4ProfileModel.isPositionActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_TITLE) return settingState.tab4ProfileModel.positionTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ISACTIVE) return settingState.tab4ProfileModel.isMadeActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ICON) return settingState.tab4ProfileModel.madeIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ISACTIVE) return settingState.tab4ProfileModel.isSendActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ICON) return settingState.tab4ProfileModel.sendIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ISACTIVE) return settingState.tab4ProfileModel.isReceiveActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ICON) return settingState.tab4ProfileModel.receiveIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ISACTIVE) return settingState.tab4ProfileModel.isReportActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_TITLE) return settingState.tab4ProfileModel.reportTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ICON) return settingState.tab4ProfileModel.reportIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_EMAIL) return settingState.tab4ProfileModel.reportEmail;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ISACTIVE) return settingState.tab4ProfileModel.isRatingActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_TITLE) return settingState.tab4ProfileModel.ratingTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ICON) return settingState.tab4ProfileModel.ratingIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_GOOGLEPLAYID) return settingState.tab4ProfileModel.ratingGoogleplayId;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_APPSTOREID) return settingState.tab4ProfileModel.ratingAppsotreId;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ISACTIVE) return settingState.tab4ProfileModel.isShareActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_TITLE) return settingState.tab4ProfileModel.shareTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ICON) return settingState.tab4ProfileModel.shareIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_MSG) return settingState.tab4ProfileModel.shareMsg;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ISACTIVE) return settingState.tab4ProfileModel.isSettingActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_TITLE) return settingState.tab4ProfileModel.settingTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ICON) return settingState.tab4ProfileModel.settingIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_ISACTIVE) return settingState.tab4ProfileModel.isAlramActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_TITLE) return settingState.tab4ProfileModel.alramTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_ISACTIVE) return settingState.tab4ProfileModel.isPrivacyActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_TITLE) return settingState.tab4ProfileModel.privacyTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_URL) return settingState.tab4ProfileModel.privacyUrl;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_ISACTIVE) return settingState.tab4ProfileModel.isProvisionActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_TITLE) return settingState.tab4ProfileModel.provisionTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_URL) return settingState.tab4ProfileModel.provisionUrl;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_ISACTIVE) return settingState.tab4ProfileModel.isLicenseActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_TITLE) return settingState.tab4ProfileModel.licenseTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_URL) return settingState.tab4ProfileModel.licenseUrl;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ISACTIVE) return settingState.tab4ProfileModel.isAccountActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ALIGN) return settingState.tab4ProfileModel.accountAlign;
      else return settingState.tab4ProfileModel.accountAlign;
    }
  }
  else{ //tabNum ==5
    if(funcType == KEY_CUSTOMAPP_SETTINGS_BOARD){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab5BoardModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab5BoardModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab5BoardModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab5BoardModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab5BoardModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab5BoardModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab5BoardModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_BOARD_NAVI_CREATETITLE) return settingState.tab5BoardModel.naviCreateTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab5BoardModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab5BoardModel.fontSubTitle;
      else return settingState.tab5BoardModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab5FavoriteModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab5FavoriteModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab5FavoriteModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab5FavoriteModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab5FavoriteModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab5FavoriteModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab5FavoriteModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab5FavoriteModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab5FavoriteModel.fontSubTitle;
      else return settingState.tab5FavoriteModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_CATEGORY){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab5CategoryModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab5CategoryModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab5CategoryModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab5CategoryModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab5CategoryModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab5CategoryModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab5CategoryModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab5CategoryModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab5CategoryModel.fontSubTitle;
      else return settingState.tab5CategoryModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_MADE){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab5MadeModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab5MadeModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab5MadeModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab5MadeModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab5MadeModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab5MadeModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab5MadeModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_MADE_NAVI_UPDATETITLE) return settingState.tab5MadeModel.naviUpdateTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab5MadeModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab5MadeModel.fontSubTitle;
      else return settingState.tab5MadeModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_SEND){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab5SendModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab5SendModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab5SendModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab5SendModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab5SendModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab5SendModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab5SendModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab5SendModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab5SendModel.fontSubTitle;
      else return settingState.tab5SendModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else if(funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE){
      if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE) return settingState.tab5ReceiveModel.listType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_CONNECTTYPE) return settingState.tab5ReceiveModel.connectType;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR) return settingState.tab5ReceiveModel.listBgColor;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION) return settingState.tab5ReceiveModel.listDivision;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW) return settingState.tab5ReceiveModel.listShadow;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE) return settingState.tab5ReceiveModel.naviTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE) return settingState.tab5ReceiveModel.naviReadTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE) return settingState.tab5ReceiveModel.fontTitle;
      else if(field == KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE) return settingState.tab5ReceiveModel.fontSubTitle;
      else return settingState.tab5ReceiveModel.fontContents; //KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS
    }
    else{ //KEY_CUSTOMAPP_SETTINGS_PROFILE
      if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR) return settingState.tab5ProfileModel.fontColor;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE) return settingState.tab5ProfileModel.fontSize;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ISACTIVE) return settingState.tab5ProfileModel.isInfoActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_TITLE) return settingState.tab5ProfileModel.infoTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_ICON) return settingState.tab5ProfileModel.infoIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_ISACTIVE) return settingState.tab5ProfileModel.isNameActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_NAME_TITLE) return settingState.tab5ProfileModel.nameTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_ISACTIVE) return settingState.tab5ProfileModel.isPositionActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_INFO_POSITION_TITLE) return settingState.tab5ProfileModel.positionTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ISACTIVE) return settingState.tab5ProfileModel.isMadeActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_MADE_ICON) return settingState.tab5ProfileModel.madeIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ISACTIVE) return settingState.tab5ProfileModel.isSendActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SEND_ICON) return settingState.tab5ProfileModel.sendIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ISACTIVE) return settingState.tab5ProfileModel.isReceiveActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RECEIVE_ICON) return settingState.tab5ProfileModel.receiveIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ISACTIVE) return settingState.tab5ProfileModel.isReportActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_TITLE) return settingState.tab5ProfileModel.reportTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_ICON) return settingState.tab5ProfileModel.reportIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_REPORT_EMAIL) return settingState.tab5ProfileModel.reportEmail;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ISACTIVE) return settingState.tab5ProfileModel.isRatingActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_TITLE) return settingState.tab5ProfileModel.ratingTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_ICON) return settingState.tab5ProfileModel.ratingIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_GOOGLEPLAYID) return settingState.tab5ProfileModel.ratingGoogleplayId;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_RATING_APPSTOREID) return settingState.tab5ProfileModel.ratingAppsotreId;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ISACTIVE) return settingState.tab5ProfileModel.isShareActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_TITLE) return settingState.tab5ProfileModel.shareTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_ICON) return settingState.tab5ProfileModel.shareIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SHARE_MSG) return settingState.tab5ProfileModel.shareMsg;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ISACTIVE) return settingState.tab5ProfileModel.isSettingActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_TITLE) return settingState.tab5ProfileModel.settingTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ICON) return settingState.tab5ProfileModel.settingIcon;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_ISACTIVE) return settingState.tab5ProfileModel.isAlramActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ALRAM_TITLE) return settingState.tab5ProfileModel.alramTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_ISACTIVE) return settingState.tab5ProfileModel.isPrivacyActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_TITLE) return settingState.tab5ProfileModel.privacyTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PRIVACY_URL) return settingState.tab5ProfileModel.privacyUrl;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_ISACTIVE) return settingState.tab5ProfileModel.isProvisionActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_TITLE) return settingState.tab5ProfileModel.provisionTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_PROVISION_URL) return settingState.tab5ProfileModel.provisionUrl;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_ISACTIVE) return settingState.tab5ProfileModel.isLicenseActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_TITLE) return settingState.tab5ProfileModel.licenseTitle;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_LICENSE_URL) return settingState.tab5ProfileModel.licenseUrl;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ISACTIVE) return settingState.tab5ProfileModel.isAccountActive;
      else if (field == KEY_CUSTOMAPP_SETTINGS_PROFILE_SETTING_ACCOUNT_ALIGN) return settingState.tab5ProfileModel.accountAlign;
      else return settingState.tab5ProfileModel.accountAlign;
    }
  }
}