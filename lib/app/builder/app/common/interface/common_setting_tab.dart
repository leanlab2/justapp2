import 'package:animate_icons/animate_icons.dart';
import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/common/widget/builder_setting_widget.dart';
import 'package:justapp/app/builder/app/utils/icon_notifier.dart';
import 'package:justapp/app/const/builder_keys.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/admin_user_state.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:provider/provider.dart';

class CommonSettingTab extends StatefulWidget {
  final int tabNum;
  final funcType;

  CommonSettingTab({required this.tabNum, required this.funcType});

  @override
  _CommonSettingTabState createState() => _CommonSettingTabState();
}

class _CommonSettingTabState extends State<CommonSettingTab> {
  TextEditingController _titleController = TextEditingController();
  late AnimateIconController _ani1Controller,_ani2Controller,_ani3Controller;

  late IconNotifier notifier;
  late IconData icon = IconData(60970, fontFamily: 'MaterialIcons'); //Icons.flight;



  @override
  void initState() {
    super.initState();
    widget.tabNum == 1?_titleController.text = Provider.of<SettingState>(context, listen: false).tab1Model.tabTitle:
    widget.tabNum == 2?_titleController.text = Provider.of<SettingState>(context, listen: false).tab2Model.tabTitle:
    widget.tabNum == 3?_titleController.text = Provider.of<SettingState>(context, listen: false).tab3Model.tabTitle:
    widget.tabNum == 4?_titleController.text = Provider.of<SettingState>(context, listen: false).tab4Model.tabTitle:
                      _titleController.text = Provider.of<SettingState>(context, listen: false).tab5Model.tabTitle;

    _ani1Controller = AnimateIconController();
    _ani2Controller = AnimateIconController();
    _ani3Controller = AnimateIconController();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<SettingState, AdminUserState>(builder: (BuildContext context, SettingState settingState, AdminUserState adminUserState, child) {
      return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("TAB" + widget.tabNum.toString(), style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 15)),
            arrowSimpleViewer(widget.funcType == KEY_CUSTOMAPP_SETTINGS_BOARD? IS_BOARD_TAB_SIMPLE_VIEWER:
                        widget.funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE? IS_FAVORITE_TAB_SIMPLE_VIEWER:
                        widget.funcType == KEY_CUSTOMAPP_SETTINGS_PROFILE? IS_PROFILE_TAB_SIMPLE_VIEWER:IS_PROFILE_TAB_SIMPLE_VIEWER, settingState,_ani1Controller),
          ],
        ),

        widget.funcType == KEY_CUSTOMAPP_SETTINGS_BOARD?
          settingState.isBoardTabSimpleViewer?tabFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE?
          settingState.isFavoriteTabSimpleViewer?tabFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_PROFILE?
          settingState.isProfileTabSimpleViewer?tabFunc(settingState,adminUserState):SizedBox.shrink():SizedBox.shrink(),
        divide(1.0, 0.1),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("STYLE", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 15)),
            arrowSimpleViewer(widget.funcType == KEY_CUSTOMAPP_SETTINGS_BOARD?IS_BOARD_STYLE_SIMPLE_VIEWER:
                        widget.funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE?IS_FAVORITE_STYLE_SIMPLE_VIEWER:
                        widget.funcType == KEY_CUSTOMAPP_SETTINGS_PROFILE?IS_PROFILE_STYLE_SIMPLE_VIEWER:IS_PROFILE_STYLE_SIMPLE_VIEWER, settingState,_ani2Controller)
          ],
        ),

        widget.funcType == KEY_CUSTOMAPP_SETTINGS_BOARD?
          settingState.isBoardStyleSimpleViewer?styleFunc(settingState ,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE?
          settingState.isFavoriteStyleSimpleViewer?styleFunc(settingState, adminUserState ):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_PROFILE?
          settingState.isProfileStyleSimpleViewer?styleFunc(settingState, adminUserState):SizedBox.shrink():SizedBox.shrink(),
        divide(1.0, 0.1),

        widget.funcType == KEY_CUSTOMAPP_SETTINGS_PROFILE?
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("FONT", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 15)),
                arrowSimpleViewer('isProfileFontViewer', settingState, _ani3Controller),
              ],
            ),
            settingState.isProfileFontSimpleViewer?Column(
              children:[
                colorPickerListTile('font 색상', context, settingState, adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_COLOR, ''),
                dropDownListTile('font 크기', settingState, adminUserState, widget.tabNum, KEY_CUSTOMAPP_SETTINGS_PROFILE, KEY_CUSTOMAPP_SETTINGS_PROFILE_FONT_SIZE, ''),
              ]
            ):SizedBox.shrink(),
            divide(1.0, 0.1),
          ],
        ):SizedBox.shrink(),

      ]);
    });
  }

  tabFunc(SettingState settingState, AdminUserState adminUserState){
    return Column(
        children:[
          dropDownListTile('Tab 기능 설정 : ', settingState, adminUserState, widget.tabNum, '', KEY_CUSTOMAPP_SETTINGS_TAB_FUNCPAGE, ''),
          textEdit('tab 제목', context,  adminUserState, _titleController, widget.tabNum, '',KEY_CUSTOMAPP_SETTINGS_TAB_TITLE),
          iconPickerListTile('tab 아이콘', context, settingState, adminUserState,widget.tabNum, '', KEY_CUSTOMAPP_SETTINGS_TAB_ICON),
        ]
    );
  }
  styleFunc(SettingState settingState, AdminUserState adminUserState){
    return colorPickerListTile('배경 색상 : ', context, settingState, adminUserState, widget.tabNum, '', KEY_CUSTOMAPP_SETTINGS_TAB_BGCOLOR, '');
  }

}
