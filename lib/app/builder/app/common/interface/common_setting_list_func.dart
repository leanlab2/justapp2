import 'package:animate_icons/animate_icons.dart';
import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/builder/app/common/widget/builder_setting_widget.dart';
import 'package:justapp/app/const/builder_keys.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/admin_user_state.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:provider/provider.dart';

class CommonSettingListFunc extends StatefulWidget {
  final int tabNum;
  final funcType;

  CommonSettingListFunc({required this.tabNum, required this.funcType});

  @override
  _CommonSettingListFuncState createState() => _CommonSettingListFuncState();
}

class _CommonSettingListFuncState extends State<CommonSettingListFunc> {
  TextEditingController _titleController = TextEditingController();
  TextEditingController _readTitleController = TextEditingController();
  TextEditingController _createTitleController = TextEditingController();
  TextEditingController _updateTitleController = TextEditingController();
  late AnimateIconController _ani1Controller,_ani2Controller,_ani3Controller,_ani4Controller,_ani5Controller;

  @override
  void initState() {
    super.initState();
    _titleController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE);
    _readTitleController.text = selectGetFuncAllType(
        Provider.of<SettingState>(context, listen: false), widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE);
    if(widget.funcType == KEY_CUSTOMAPP_SETTINGS_BOARD)
      _createTitleController.text = selectGetFuncAllType(
          Provider.of<SettingState>(context, listen: false), widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_BOARD_NAVI_CREATETITLE);
    if(widget.funcType == KEY_CUSTOMAPP_SETTINGS_MADE)
      _updateTitleController.text = selectGetFuncAllType(
          Provider.of<SettingState>(context, listen: false), widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_MADE_NAVI_UPDATETITLE);

    _ani1Controller = AnimateIconController();
    _ani2Controller = AnimateIconController();
    _ani3Controller = AnimateIconController();
    _ani4Controller = AnimateIconController();
    _ani5Controller = AnimateIconController();
  }


  @override
  Widget build(BuildContext context) {
    return Consumer2<SettingState, AdminUserState>(builder: (BuildContext context, SettingState settingState, AdminUserState adminUserState, child) {
      return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("LIST STYLE", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 15)),
            arrowSimpleViewer(widget.funcType == KEY_CUSTOMAPP_SETTINGS_BOARD? IS_BOARD_LIST_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE? IS_FAVORITE_LIST_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_PROFILE? IS_PROFILE_LIST_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_MADE? IS_MADE_LIST_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_SEND? IS_SEND_LIST_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE? IS_RECEIVE_LIST_STYLE_SIMPLE_VIEWER:IS_RECEIVE_LIST_STYLE_SIMPLE_VIEWER, settingState,_ani1Controller),
          ],
        ),
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_BOARD?
          settingState.isBoardListStyleSimpleViewer?listFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE?
          settingState.isFavoriteListStyleSimpleViewer?listFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_PROFILE?
          settingState.isProfileListStyleSimpleViewer?listFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_MADE?
          settingState.isMadeListStyleSimpleViewer?listFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_SEND?
          settingState.isSendListStyleSimpleViewer?listFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE?
          settingState.isReceiveListStyleSimpleViewer?listFunc(settingState,adminUserState):SizedBox.shrink():SizedBox.shrink(),
        divide(1.0,0.1),


        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("NAVI STYLE", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 15)),
            arrowSimpleViewer(widget.funcType == KEY_CUSTOMAPP_SETTINGS_BOARD? IS_BOARD_NAVI_STYLE_SIMPLE_VIEWER:
                    widget.funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE? IS_FAVORITE_NAVI_STYLE_SIMPLE_VIEWER:
                    widget.funcType == KEY_CUSTOMAPP_SETTINGS_PROFILE? IS_PROFILE_NAVI_STYLE_SIMPLE_VIEWER:
                    widget.funcType == KEY_CUSTOMAPP_SETTINGS_MADE? IS_MADE_NAVI_STYLE_SIMPLE_VIEWER:
                    widget.funcType == KEY_CUSTOMAPP_SETTINGS_SEND? IS_SEND_NAVI_STYLE_SIMPLE_VIEWER:
                    widget.funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE? IS_RECEIVE_NAVI_STYLE_SIMPLE_VIEWER:IS_RECEIVE_NAVI_STYLE_SIMPLE_VIEWER, settingState,_ani2Controller),
          ],
        ),
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_BOARD?
          settingState.isBoardNaviStyleSimpleViewer?naviFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE?
          settingState.isFavoriteNaviStyleSimpleViewer?naviFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_PROFILE?
          settingState.isProfileNaviStyleSimpleViewer?naviFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_MADE?
          settingState.isMadeNaviStyleSimpleViewer?naviFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_SEND?
          settingState.isSendNaviStyleSimpleViewer?naviFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE?
          settingState.isReceiveNaviStyleSimpleViewer?naviFunc(settingState,adminUserState):SizedBox.shrink():SizedBox.shrink(),
        divide(1.0,0.1),


        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("FONT STYLE(TITLE)", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 15)),
            arrowSimpleViewer(widget.funcType == KEY_CUSTOMAPP_SETTINGS_BOARD? IS_BOARD_FONT_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE? IS_FAVORITE_FONT_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_PROFILE? IS_PROFILE_FONT_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_MADE? IS_MADE_FONT_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_SEND? IS_SEND_FONT_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE? IS_RECEIVE_FONT_STYLE_SIMPLE_VIEWER:IS_RECEIVE_FONT_STYLE_SIMPLE_VIEWER, settingState,_ani3Controller),
          ],
        ),
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_BOARD?
          settingState.isBoardFontStyleSimpleViewer?fontFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE?
          settingState.isFavoriteFontStyleSimpleViewer?fontFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_PROFILE?
          settingState.isProfileFontStyleSimpleViewer?fontFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_MADE?
          settingState.isMadeFontStyleSimpleViewer?fontFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_SEND?
          settingState.isSendFontStyleSimpleViewer?fontFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE?
          settingState.isReceiveFontStyleSimpleViewer?fontFunc(settingState,adminUserState):SizedBox.shrink():SizedBox.shrink(),
        divide(1.0,0.1),


        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("FONT STYLE(SUBTITLE)", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 15)),
            arrowSimpleViewer(widget.funcType == KEY_CUSTOMAPP_SETTINGS_BOARD? IS_BOARD_SUB_FONT_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE? IS_FAVORITE_SUB_FONT_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_PROFILE? IS_PROFILE_SUB_FONT_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_MADE? IS_MADE_SUB_FONT_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_SEND? IS_SEND_SUB_FONT_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE? IS_RECEIVE_SUB_FONT_STYLE_SIMPLE_VIEWER:IS_RECEIVE_SUB_FONT_STYLE_SIMPLE_VIEWER, settingState,_ani4Controller),
          ],
        ),
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_BOARD?
          settingState.isBoardSubFontStyleSimpleViewer?subFontFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE?
          settingState.isFavoriteSubFontStyleSimpleViewer?subFontFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_PROFILE?
          settingState.isProfileSubFontStyleSimpleViewer?subFontFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_MADE?
          settingState.isMadeSubFontStyleSimpleViewer?subFontFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_SEND?
          settingState.isSendSubFontStyleSimpleViewer?subFontFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE?
          settingState.isReceiveSubFontStyleSimpleViewer?subFontFunc(settingState,adminUserState):SizedBox.shrink():SizedBox.shrink(),
        divide(1.0,0.1),


        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("FONT STYLE(CONTENTS)", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 15)),
            arrowSimpleViewer(widget.funcType == KEY_CUSTOMAPP_SETTINGS_BOARD? IS_BOARD_CONTENTS_FONT_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE? IS_FAVORITE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_PROFILE? IS_PROFILE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_MADE? IS_MADE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_SEND? IS_SEND_CONTENTS_FONT_STYLE_SIMPLE_VIEWER:
                      widget.funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE? IS_RECEIVE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER:IS_RECEIVE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER, settingState,_ani5Controller),
          ],
        ),
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_BOARD?
          settingState.isBoardContentsFontStyleSimpleViewer?contentsFontFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_FAVORITE?
          settingState.isFavoriteContentsFontStyleSimpleViewer?contentsFontFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_PROFILE?
          settingState.isProfileContentsFontStyleSimpleViewer?contentsFontFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_MADE?
          settingState.isMadeContentsFontStyleSimpleViewer?contentsFontFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_SEND?
          settingState.isSendContentsFontStyleSimpleViewer?contentsFontFunc(settingState,adminUserState):SizedBox.shrink():
        widget.funcType == KEY_CUSTOMAPP_SETTINGS_RECEIVE?
          settingState.isReceiveContentsFontStyleSimpleViewer?contentsFontFunc(settingState,adminUserState):SizedBox.shrink():SizedBox.shrink(),
      ]);
    });
  }


  listFunc(SettingState settingState, AdminUserState adminUserState){
    return Column(
        children: [
          widget.funcType != KEY_CUSTOMAPP_SETTINGS_RECEIVE
              ? dropDownListTile('list 모양', settingState, adminUserState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTTYPE, '')
              : SizedBox.shrink(),
          colorPickerListTile('list 배경 색상', context, settingState, adminUserState,widget.tabNum, widget.funcType,
              KEY_CUSTOMAPP_SETTINGS_FUNC_LISTBGCOLOR, ''),
          dropDownListTile('list 구분 선', settingState, adminUserState,widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTDIVISION, ''),
          dropDownListTile('list 그림자', settingState, adminUserState,widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_LISTSHADOW, ''),
        ],
      );
  }
  naviFunc(SettingState settingState, AdminUserState adminUserState){
    return Column(
        children: [
          textEdit('navi 제목', context, adminUserState, _titleController, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_NAVITITLE),
          //navi_read_title
          textEdit('navi read 제목', context, adminUserState, _readTitleController, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_NAVI_READTITLE),
          //navi_create_title
          widget.funcType == KEY_CUSTOMAPP_SETTINGS_BOARD
              ? textEdit('navi create 제목', context, adminUserState, _createTitleController, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_BOARD_NAVI_CREATETITLE)
              : SizedBox.shrink(),
          //navi_update_title
          widget.funcType == KEY_CUSTOMAPP_SETTINGS_MADE
              ? textEdit('navi update 제목', context, adminUserState, _updateTitleController, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_MADE_NAVI_UPDATETITLE)
              : SizedBox.shrink(),
        ]
    );
  }
  fontFunc(SettingState settingState, AdminUserState adminUserState){
    return Column(
        children: [
          colorPickerListTile('font 색상', context, settingState, adminUserState,widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE,
              KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR),
          dropDownListTile('font 정렬', settingState, adminUserState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE,
              KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN),
          dropDownListTile('title 켜기/끄기', settingState, adminUserState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE,
              KEY_CUSTOMAPP_SETTINGS_FUNC_ISACTIVE),
          dropDownListTile('최대길이', settingState, adminUserState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE,
              KEY_CUSTOMAPP_SETTINGS_FUNC_LINE),
          dropDownListTile('font 크기', settingState, adminUserState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTTITLE,
              KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE),
        ]
    );
  }
  subFontFunc(SettingState settingState, AdminUserState adminUserState){
    return Column(
        children: [
          colorPickerListTile('font 색상', context, settingState, adminUserState, widget.tabNum, widget.funcType,
              KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE, KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR),
          dropDownListTile('font 정렬', settingState, adminUserState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE,
              KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN),
          dropDownListTile('title 켜기/끄기', settingState, adminUserState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE,
              KEY_CUSTOMAPP_SETTINGS_FUNC_ISACTIVE),
          dropDownListTile('최대길이', settingState, adminUserState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE,
              KEY_CUSTOMAPP_SETTINGS_FUNC_LINE),
          dropDownListTile('font 크기', settingState, adminUserState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTSUBTITLE,
              KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE),
        ]
    );
  }
  contentsFontFunc(SettingState settingState, AdminUserState adminUserState){
    return Column(
        children: [
          colorPickerListTile('font 색상', context, settingState, adminUserState, widget.tabNum, widget.funcType,
              KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS, KEY_CUSTOMAPP_SETTINGS_FUNC_COLOR),
          dropDownListTile('font 정렬', settingState, adminUserState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS,
              KEY_CUSTOMAPP_SETTINGS_FUNC_ALIGN),
          dropDownListTile('title 켜기/끄기', settingState, adminUserState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS,
              KEY_CUSTOMAPP_SETTINGS_FUNC_ISACTIVE),
          dropDownListTile('최대길이', settingState, adminUserState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS,
              KEY_CUSTOMAPP_SETTINGS_FUNC_LINE),
          dropDownListTile('font 크기', settingState, adminUserState, widget.tabNum, widget.funcType, KEY_CUSTOMAPP_SETTINGS_FUNC_FONTCONTENTS,
              KEY_CUSTOMAPP_SETTINGS_FUNC_SIZE),
        ]
    );
  }

}
