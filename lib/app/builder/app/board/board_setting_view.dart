import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/common/common_builder_protocal.dart';
import 'package:justapp/app/builder/app/common/interface/common_setting_list_func.dart';
import 'package:justapp/app/builder/app/common/interface/common_setting_tab.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:provider/provider.dart';

class BoardSettingView extends StatefulWidget {
  final int tabNum;

  BoardSettingView({required this.tabNum});

  @override
  _BoardSettingViewState createState() => _BoardSettingViewState();
}

class _BoardSettingViewState extends State<BoardSettingView> {
  TextEditingController _titleController = TextEditingController();
  @override
  void initState() {
    super.initState();
    _titleController.text = selectGetFuncAllType(Provider.of<SettingState>(context, listen: false), widget.tabNum, KEY_CUSTOMAPP_SETTINGS_BOARD, KEY_CUSTOMAPP_SETTINGS_BOARD_NAVI_CREATETITLE);
  }
  @override
  Widget build(BuildContext context) {

    return Consumer<SettingState>(builder: (BuildContext context, SettingState settingState, child)
    {
      return Scaffold(
          backgroundColor: Colors.white,
          body: Padding(
            padding: const EdgeInsets.all(5.0),
            child: ListView(
                children: [
                  CommonSettingTab(tabNum: widget.tabNum, funcType: KEY_CUSTOMAPP_SETTINGS_BOARD),
                  CommonSettingListFunc(tabNum: widget.tabNum, funcType: KEY_CUSTOMAPP_SETTINGS_BOARD),
                ]
            ),
          )
      );
    });
  }
}

