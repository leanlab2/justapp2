import 'dart:async';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:justapp/app/builder/app/tabs/build_tabs.dart';
import 'package:justapp/app/builder/screens/login/login_view.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/const/firestore_keys.dart';
import 'package:justapp/app/models/admin_user_state.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/firebase_auth_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/utils/init_model.dart';
import 'package:justapp/utils/my_progress_indicator.dart';
import 'package:provider/provider.dart';

class BuilderAppView extends StatefulWidget {
  @override
  _BuilderAppViewState createState() => _BuilderAppViewState();
}

class _BuilderAppViewState extends State<BuilderAppView> {
  FirebaseAuthState _firebaseAuthState = FirebaseAuthState();
  late Widget _currentWidget;
  bool isFirstReset = true;
  bool isComplete = false;
  bool isa = true;

  MultiProvider nextWidget() {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<FirebaseAuthState>.value(
          value: _firebaseAuthState,
        ),
        ChangeNotifierProvider<ViewerState>(
          create: (_) => ViewerState(),
        ),
        ChangeNotifierProvider<AdminUserState>(
          create: (_) => AdminUserState(),
        ),
        ChangeNotifierProvider<SettingState>(
          create: (_) => SettingState(),
        ),
      ],
      child: MaterialApp(
        theme: ThemeData(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          hoverColor: Colors.transparent,
          focusColor: Colors.transparent,
          primarySwatch: createMaterialColor(Color(Constants.defaultColor)),
        ),
        home: Consumer4<FirebaseAuthState, ViewerState, AdminUserState, SettingState>(builder: (BuildContext context,
            FirebaseAuthState firebaseAuthState,
            ViewerState viewerState,
            AdminUserState adminUserState,
            SettingState settingState,
            child) {
          if (firebaseAuthState.firebaseAuthStatus == FirebaseAuthStatus.signout) {
            print("_SIGNOUT");
            isa = true;
            _currentWidget = LoginView();
          } else if (firebaseAuthState.firebaseAuthStatus == FirebaseAuthStatus.signin ||
              firebaseAuthState.firebaseAuthStatus == FirebaseAuthStatus.reset) {
            print("_SIGNIN");
            if (firebaseAuthState.firebaseAuthStatus == FirebaseAuthStatus.reset && isFirstReset) {
              isa = true;
              isFirstReset = false;
            }
            if (isa) {
              isa = false;
              _currentWidget = FutureBuilder(
                  future: isFirstReset
                      ? initAdminUserModel(firebaseAuthState, adminUserState).then((value) async => {
                        if(value.userMade != ''){
                            await Future.wait([
                              initViewerModel(viewerState, value.userMade, KEY_CUSTOMAPP_USERS_PRODUCER),
                              initSettingModel(settingState, value.userMade),
                            ])
                          }
                        })
                      : Future.wait([
                          initViewerModel(viewerState, adminUserState.adminUserModel.userMade, KEY_CUSTOMAPP_USERS_PRODUCER),
                          initSettingModel(settingState, adminUserState.adminUserModel.userMade),
                        ]),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    print(snapshot.connectionState);

                    if (!isComplete) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                          return MyProgressIndicator(containerSize: 60);
                        case ConnectionState.waiting:
                          return MyProgressIndicator(containerSize: 60);
                        default:
                          isComplete = true;
                          return BuildTabs(userMade:adminUserState.adminUserModel.userMade );
                      }
                    }
                    else{
                      return BuildTabs(userMade:adminUserState.adminUserModel.userMade );
                    }
                  });
            }
          } else {
            _currentWidget = MyProgressIndicator(containerSize: 60);
          }
          return AnimatedSwitcher(child: _currentWidget, duration: Duration(milliseconds: 300));
        }),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _firebaseAuthState.watchAuthChange();
    return nextWidget();
  }
}
