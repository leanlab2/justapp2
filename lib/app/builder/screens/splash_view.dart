import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/utils/my_progress_indicator.dart';

class SplashView extends StatefulWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: Constants.size.width,
      child: Stack(
        children: [
          Constants.prefs.getString(Constants.splashKey)  == null?
          Image.asset(
            'assets/auth/images/splashBg.png',
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.cover,
          ):
          CachedNetworkImage(
              imageUrl: Constants.prefs.getString(Constants.splashKey).toString(),
              fit: BoxFit.cover,
              width: double.infinity,
              height: double.infinity,
              placeholder: (BuildContext context, String url) {
                return MyProgressIndicator(containerSize: 30);
              })
        ],
      ));
  }
}
