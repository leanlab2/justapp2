import 'package:flutter/material.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/models/admin_user_state.dart';
import 'package:justapp/app/models/firebase_auth_state.dart';
import 'package:justapp/app/models/setting_state.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:justapp/app/repo/admin_user_network_repository.dart';
import 'package:justapp/app/repo/appinfo_network_repository.dart';
import 'package:justapp/app/repo/custom_app_network_repository.dart';
import 'package:justapp/app/repo/setting_network_repository.dart';
import 'package:justapp/app/repo/user_network_repository.dart';
import 'package:provider/provider.dart';

class NoResultView extends StatefulWidget {
  @override
  _NoResultViewState createState() => _NoResultViewState();
}

class _NoResultViewState extends State<NoResultView> {
  String _tamplateValue = 'community';
  final myController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Consumer3<ViewerState, AdminUserState, SettingState>(builder: (BuildContext context, ViewerState viewerState, AdminUserState adminUserState, SettingState settingState, child) {
      return Scaffold(
        backgroundColor: Colors.white,
        body: ListView(children: [
          ConstrainedBox(
            constraints: new BoxConstraints(minHeight: Constants.size.height),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    padding: EdgeInsets.only(top: 50, bottom: 20),
                    child: Text("만든앱이 없습니다.", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20))),
                Container(
                  padding: EdgeInsets.only(bottom: 50),
                  child: Text(
                    "'앱 만들기'를 통해 앱을 만들어 주세요.",
                    style: TextStyle(color: Colors.black, fontSize: 12),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),

                Container(
                  width: Constants.size.width > 400 ? 400 : Constants.size.width * 0.9,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('템플릿을 선택하세요', style:TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 13)),
                      DropdownButton(
                          value: _tamplateValue,
                          items: Constants.valueTemplate.map(
                            (value) {
                              return DropdownMenuItem(
                                value: value,
                                child: Text(value.toString()),
                              );
                            },
                          ).toList(),
                          onChanged: (value) {
                            setState(() {
                              _tamplateValue = value.toString();
                            });
                          }),
                    ],
                  ),
                ),

                Container(
                  width: Constants.size.width > 400 ? 400 : Constants.size.width * 0.9,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('앱 이름을 적어주세요.', style:TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 13)),
                      Container(
                        width: Constants.size.width > 200 ? 200 : Constants.size.width * 0.45,
                        child: TextFormField(
                          controller: myController,
                          onChanged: (text) {
                            setState(() {});
                          },
                          style: TextStyle(color: Colors.black, fontSize: 20),
                          cursorColor: Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: Constants.size.width > 400 ? 400 : Constants.size.width * 0.9,
                  padding: EdgeInsets.fromLTRB(30, 30, 30, 20),
                  child: FlatButton(
                    minWidth: double.infinity,
                    child: Text("앱 만들기", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                    onPressed: () {
                      Future.wait([
                        customAppNetworkRepository
                            .createCustomAppModel(
                            DateTime.now().year.toString() + '-' + DateTime.now().month.toString() + '-' + DateTime.now().day.toString(),
                            '',
                            '',
                            adminUserState.adminUserModel.userKey,
                            myController.text,
                            _tamplateValue)
                            .then((value) => {
                          appInfoNetworkRepository.createAppInfo(1, 1, 1, myController.text),
                          // boardNetworkRepository.createBoard(myController.text),
                          settingNetworkRepository.createSetting(myController.text),
                          userNetworkRepository.createUserMade(myController.text),
                          adminUserNetworkRepository.setAdminUserMade(adminUserState.adminUserModel.userKey, myController.text),
                          adminUserState.adminUserModel.userMade = myController.text,
                        })
                            .then((value) => {
                          Provider.of<FirebaseAuthState>(context, listen: false).changeFirebaseAuthStatus(FirebaseAuthStatus.reset),
                        }),
                      ]);
                    },
                    color: Color(Constants.defaultColor),
                    textColor: Colors.white,
                    padding: EdgeInsets.symmetric(vertical: 22),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                  ),
                ),
              ],
            ),
          ),
        ]),
      );
    });
  }
}
