import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:future_progress_dialog/future_progress_dialog.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/models/firebase_auth_state.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class SignupView extends StatefulWidget {
  @override
  _SignupViewState createState() => _SignupViewState();
}

class _SignupViewState extends State<SignupView> {

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confimePasswordController = TextEditingController();

  late FocusNode passwordNode;
  late FocusNode confirmPasswordNode;

  @override
  void initState() {
    super.initState();
    passwordNode = FocusNode();
    confirmPasswordNode = FocusNode();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _confimePasswordController.dispose();
    passwordNode.dispose();
    confirmPasswordNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Builder(
              builder: (context) => Form(
                  key: _formKey,
                  child: ListView(
                    children: [
                      ConstrainedBox(
                        constraints: new BoxConstraints(minHeight: Constants.size.height),
                        child: Stack(
                          children: [
                            Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.fromLTRB(28, 10, 0, 10),
                              child: InkWell(
                                focusColor: Colors.transparent,
                                hoverColor: Colors.transparent,
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
                                  child: Icon(
                                    Icons.arrow_back,
                                    color: Colors.black,
                                    size: 28,
                                  ),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                            Column(
                              children: [
                                //logo
                                Container(
                                  padding: EdgeInsets.only(top: 50),
                                  child: Image.asset(
                                    'assets/auth/images/appLogo.png',
                                    width: Constants.size.width>300? 300: Constants.size.width * 0.8,
                                    color: Colors.black,
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(bottom: 20),
                                  child: Text(
                                    "회원정보를 입력해주세요.",
                                    style: TextStyle(color: Colors.black, fontSize: 12),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),

                                //email
                                Container(
                                  width: Constants.size.width>400? 400: Constants.size.width * 0.9,
                                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                                  child: TextFormField(
                                    controller: _emailController,
                                    onChanged: (text) {
                                      setState(() {});
                                    },
                                    onFieldSubmitted: (_textController) {
                                      FocusScope.of(context).requestFocus(passwordNode);
                                    },
                                    style: TextStyle(color: Colors.black, fontSize: 20),
                                    cursorColor: Colors.black,
                                    decoration: textInputDecor("이메일", _emailController),
                                    validator: (text) {
                                      if (text!.isNotEmpty && text.contains("@")) {
                                        return null;
                                      } else {
                                        return "정확한 이메일 주소를 입력하세요.";
                                      }
                                    },
                                    keyboardType: TextInputType.emailAddress,
                                  ),
                                ),//password
                                Container(
                                  width: Constants.size.width>400? 400: Constants.size.width * 0.9,
                                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                                  child: TextFormField(
                                    controller: _passwordController,
                                    obscureText: true,
                                    onChanged: (text) {
                                      setState(() {});
                                    },
                                    onFieldSubmitted: (_textController) {
                                      FocusScope.of(context).requestFocus(confirmPasswordNode);
                                    },
                                    focusNode: passwordNode,
                                    style: TextStyle(color: Colors.black, fontSize: 20),
                                    cursorColor: Colors.black,
                                    decoration: textInputDecor("비밀번호", _passwordController),
                                    validator: (text) {
                                      if (text!.isNotEmpty && text!.length > 5) {
                                        return null;
                                      } else {
                                        return "비밀번호가 틀렸습니다.";
                                      }
                                    },
                                    keyboardType: TextInputType.text,
                                  ),
                                ),
                                Container(
                                  width: Constants.size.width>400? 400: Constants.size.width * 0.9,
                                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                                  child: TextFormField(
                                    controller: _confimePasswordController,
                                    obscureText: true,
                                    onChanged: (text) {
                                      setState(() {});
                                    },
                                    onFieldSubmitted: (controller) {
                                      signUp(context);
                                    },
                                    focusNode: confirmPasswordNode,
                                    style: TextStyle(color: Colors.blueGrey, fontSize: 20),
                                    cursorColor: Color.fromRGBO(172, 180, 215, 1),
                                    decoration: textInputDecor("비밀번호 확인", _confimePasswordController),
                                    validator: (text) {
                                      if (text!.isNotEmpty && _passwordController.text == text) {
                                        return null;
                                      } else {
                                        return "입력한 값이 비밀번호와 다릅니다.";
                                      }
                                    },
                                    keyboardType: TextInputType.text,
                                  ),
                                ),
                                //button
                                Container(
                                  width: Constants.size.width>400? 400: Constants.size.width * 0.9,
                                  padding: EdgeInsets.fromLTRB(30, 30, 30, 20),
                                  child: FlatButton(
                                    minWidth: double.infinity,
                                    child: Text("회원가입", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                                    onPressed: () {
                                      signUp(context);
                                    },
                                    color: Color(Constants.defaultColor),
                                    textColor: Colors.white,
                                    padding: EdgeInsets.symmetric(vertical: 22),
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                                  ),
                                ),

                                Padding(
                                    padding: EdgeInsets.fromLTRB(60, 0, 60, 45),
                                    child: Center(
                                        child: RichText(
                                            textAlign: TextAlign.center,
                                            text: TextSpan(children: [
                                              TextSpan(
                                                text: "회원가입을 진행하면 ",
                                                style: TextStyle(color: Colors.blueGrey, fontSize: 10),
                                              ),
                                              TextSpan(
                                                text: "개인정보 처리방침",
                                                style: TextStyle(color: Colors.blueGrey, fontSize: 10, decoration: TextDecoration.underline),
                                                recognizer: TapGestureRecognizer()
                                                  ..onTap = () async {
                                                    final url = Constants.toPrivacy;
                                                    if (await canLaunch(url)) {
                                                      await launch(
                                                        url,
                                                        forceSafariVC: false,
                                                      );
                                                    }
                                                  },
                                              ),
                                              TextSpan(
                                                text: "과 ",
                                                style: TextStyle(color: Colors.blueGrey, fontSize: 10),
                                              ),
                                              TextSpan(
                                                text: "서비스 이용 약관",
                                                style: TextStyle(color: Colors.blueGrey, fontSize: 10, decoration: TextDecoration.underline),
                                                recognizer: TapGestureRecognizer()
                                                  ..onTap = () async {
                                                    final url = Constants.toProvision;
                                                    if (await canLaunch(url)) {
                                                      await launch(
                                                        url,
                                                        forceSafariVC: false,
                                                      );
                                                    }
                                                  },
                                              ),
                                              TextSpan(
                                                text: "에 동의한 것으로 간주합니다.",
                                                style: TextStyle(color: Colors.blueGrey, fontSize: 10),
                                              ),
                                            ])))),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ))),
        ));
  }

  void signUp(BuildContext context) async {
    if (_formKey.currentState!.validate()) {
      await showDialog(
        context: context,
        builder: (context) =>
            FutureProgressDialog(Future(() async {
              await Future.delayed(Duration(seconds: 4));
            }), message: Text('회원가입을 하는 중 입니다. 잠시만 기다려 주세요...')),
      );
      await Provider.of<FirebaseAuthState>(context, listen: false)
          .registerUser(context, email: _emailController.text, password: _passwordController.text);
    }
  }

  InputDecoration textInputDecor(String hint, TextEditingController textController) {
    return InputDecoration(
      labelText: hint,
      labelStyle: TextStyle(color: Colors.blueGrey, fontSize: 14),
      suffixIcon: textController.text.isNotEmpty
          ? Padding(
              padding: const EdgeInsetsDirectional.only(start: 0.0),
              child: IconButton(
                iconSize: 20.0,
                icon: Icon(
                  Icons.cancel,
                  color: Colors.blueGrey,
                ),
                onPressed: () {
                  setState(() {
                    textController.clear();
                  });
                },
              ),
            )
          : null,
      enabledBorder: activeInputBorder(),
      focusedBorder: activeInputBorder(),
      errorBorder: errorInputborder(),
      focusedErrorBorder: errorInputborder(),
    );
  }

  UnderlineInputBorder errorInputborder() {
    return UnderlineInputBorder(borderSide: BorderSide(color: Colors.redAccent));
  }

  UnderlineInputBorder activeInputBorder() {
    return UnderlineInputBorder(borderSide: BorderSide(color: Colors.blueGrey));
  }
}
