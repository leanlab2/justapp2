import 'package:flutter/material.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/models/firebase_auth_state.dart';
import 'package:provider/provider.dart';

class FindPasswordView extends StatefulWidget {
  @override
  _FindPasswordViewState createState() => _FindPasswordViewState();
}

class _FindPasswordViewState extends State<FindPasswordView> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController _emailController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Builder(
              builder: (context) => Form(
                  key: _formKey,
                  child: ListView(children: [
                    ConstrainedBox(
                      constraints: new BoxConstraints(minHeight: Constants.size.height),
                      child: Stack(
                        children: [
                          Container(
                            alignment: Alignment.topLeft,
                            padding: EdgeInsets.fromLTRB(28, 10, 0, 10),
                            child: InkWell(
                              focusColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
                                child: Icon(
                                  Icons.arrow_back,
                                  color: Colors.black,
                                  size: 28,
                                ),
                              ),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          Column(
                            children: [
                              //logo
                              Container(
                                padding: EdgeInsets.only(top: 50),
                                child: Image.asset(
                                  'assets/auth/images/appLogo.png',
                                  width: Constants.size.width>300? 300: Constants.size.width * 0.8,
                                  color: Colors.black,
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(bottom: 20),
                                child: Text(
                                  "회원가입한 이메일을 입력해주세요.",
                                  style: TextStyle(color: Colors.black, fontSize: 12),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),

                              //email
                              Container(
                                width: Constants.size.width>400? 400: Constants.size.width * 0.9,
                                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                                child: TextFormField(
                                  controller: _emailController,
                                  onChanged: (text) {
                                    setState(() {});
                                  },
                                  onFieldSubmitted: (_textController) {
                                    sendResetMail(context);
                                  },
                                  style: TextStyle(color: Colors.black, fontSize: 20),
                                  cursorColor: Colors.black,
                                  decoration: textInputDecor("이메일", _emailController),
                                  validator: (text) {
                                    if (text!.isNotEmpty && text.contains("@")) {
                                      return null;
                                    } else {
                                      return "정확한 이메일 주소를 입력하세요.";
                                    }
                                  },
                                  keyboardType: TextInputType.emailAddress,
                                ),
                              ),
                              //button
                              Container(
                                width: Constants.size.width>400? 400: Constants.size.width * 0.9,
                                padding: EdgeInsets.fromLTRB(30, 30, 30, 20),
                                child: FlatButton(
                                  minWidth: double.infinity,
                                  child: Text("초기화 메일 전송", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                                  onPressed: () {
                                    sendResetMail(context);
                                  },
                                  color: Color(Constants.defaultColor),
                                  textColor: Colors.white,
                                  padding: EdgeInsets.symmetric(vertical: 22),
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                                ),
                              ),

                              Padding(
                                  padding: EdgeInsets.fromLTRB(60, 0, 60, 45),
                                  child: Center(
                                      child: RichText(
                                          textAlign: TextAlign.center,
                                          text: TextSpan(children: [
                                            TextSpan(
                                              text: "이메일로 초기화 메일을 전송해드립니다.",
                                              style: TextStyle(color: Colors.blueGrey, fontSize: 10),
                                            ),
                                          ])))),



                            ],
                          ),
                        ],
                      ),
                    ),
                  ]))),
        ));
  }

  void sendResetMail(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      Provider.of<FirebaseAuthState>(context, listen: false).sendResetEmail(context, _emailController.text, "이메일이 전송되었습니다.", "정확한 이메일 주소를 입력하세요.");
    }
  }

  InputDecoration textInputDecor(String hint, TextEditingController textController) {
    return InputDecoration(
      labelText: hint,
      labelStyle: TextStyle(color: Colors.blueGrey, fontSize: 14),
      suffixIcon: textController.text.isNotEmpty
          ? Padding(
              padding: const EdgeInsetsDirectional.only(start: 0.0),
              child: IconButton(
                iconSize: 20.0,
                icon: Icon(
                  Icons.cancel,
                  color: Colors.blueGrey,
                ),
                onPressed: () {
                  setState(() {
                    textController.clear();
                  });
                },
              ),
            )
          : null,
      enabledBorder: activeInputBorder(),
      focusedBorder: activeInputBorder(),
      errorBorder: errorInputborder(),
      focusedErrorBorder: errorInputborder(),
    );
  }

  UnderlineInputBorder errorInputborder() {
    return UnderlineInputBorder(borderSide: BorderSide(color: Colors.redAccent));
  }

  UnderlineInputBorder activeInputBorder() {
    return UnderlineInputBorder(borderSide: BorderSide(color: Colors.blueGrey));
  }
}
