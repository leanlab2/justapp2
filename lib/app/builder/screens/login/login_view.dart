import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:justapp/app/builder/screens/login/find_password_view.dart';
import 'package:justapp/app/builder/screens/login/signup_view.dart';
import 'package:justapp/app/const/constants.dart';
import 'package:justapp/app/models/firebase_auth_state.dart';
import 'package:provider/provider.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  late FocusNode passwordNode;
  late DateTime _lastPressedAt;

  @override
  void initState() {
    super.initState();
    passwordNode = FocusNode();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    passwordNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          if (!Platform.isAndroid) {
            return false;
          }
          if (_lastPressedAt == null || DateTime.now().difference(_lastPressedAt) > Duration(seconds: 2)) {
            _lastPressedAt = DateTime.now();
            Fluttertoast.showToast(msg: "'뒤로' 버튼을 한 번 더 누르시면 앱이 종료됩니다.");
            return false;
          } else {
            return true;
          }
        },
        child: Scaffold(
          backgroundColor: Colors.white,
          resizeToAvoidBottomInset: true,
          body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Builder(
                builder: (context) => Form(
                    key: _formKey,
                    child: ListView(
                      children: [
                        ConstrainedBox(
                          constraints: new BoxConstraints(minHeight: Constants.size.height),
                          child: Column(
                            children: [
                              //logo
                              Container(
                                padding: EdgeInsets.only(top: 50, bottom: 20),
                                child: Image.asset(
                                  'assets/auth/images/appLogo.png',
                                  width: Constants.size.width>300? 300: Constants.size.width * 0.8,
                                  color: Colors.black,
                                ),
                              ),
                              //email
                              Container(
                                width: Constants.size.width>400? 400: Constants.size.width * 0.9,
                                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                                child: TextFormField(
                                  controller: _emailController,
                                  onChanged: (text) {
                                    setState(() {});
                                  },
                                  onFieldSubmitted: (_textController) {
                                    FocusScope.of(context).requestFocus(passwordNode);
                                  },
                                  style: TextStyle(color: Colors.black, fontSize: 20),
                                  cursorColor: Colors.black,
                                  decoration: textInputDecor("이메일", _emailController),
                                  validator: (text) {
                                    if (text!.isNotEmpty && text!.contains("@")) {
                                      return null;
                                    } else {
                                      return "정확한 이메일 주소를 입력하세요.";
                                    }
                                  },
                                  keyboardType: TextInputType.emailAddress,
                                ),
                              ),
                              //password
                              Container(
                                width: Constants.size.width>400? 400: Constants.size.width * 0.9,
                                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                                child: TextFormField(
                                  controller: _passwordController,
                                  obscureText: true,
                                  onChanged: (text) {
                                    setState(() {});
                                  },
                                  onFieldSubmitted: (_textController) {
                                    signInWithEmail(context);
                                  },
                                  focusNode: passwordNode,
                                  style: TextStyle(color: Colors.black, fontSize: 20),
                                  cursorColor: Colors.black,
                                  decoration: textInputDecor("비밀번호", _passwordController),
                                  validator: (text) {
                                    if (text!.isNotEmpty && text!.length > 5) {
                                      return null;
                                    } else {
                                      return "비밀번호가 틀렸습니다.";
                                    }
                                  },
                                  keyboardType: TextInputType.text,
                                ),
                              ),
                              //button
                              Container(
                                width: Constants.size.width>400? 400: Constants.size.width * 0.9,
                                padding: EdgeInsets.fromLTRB(30, 30, 30, 20),
                                child: FlatButton(
                                  minWidth: double.infinity,
                                  child: Text("로그인", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                                  onPressed: () {
                                    signInWithEmail(context);
                                  },
                                  color: Color(Constants.defaultColor),
                                  textColor: Colors.white,
                                  padding: EdgeInsets.symmetric(vertical: 22),
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                                ),
                              ),


                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  InkWell(
                                    child: Text("회원가입", style: TextStyle(color: Colors.black, fontSize: 12)),
                                    onTap: () async {
                                      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                        return SignupView();
                                      }));
                                    },
                                  ),
                                  SizedBox(width: 20),
                                  Container(
                                    color: Colors.black54,
                                    height: 10,
                                    width: 1,
                                  ),
                                  SizedBox(width: 20),
                                  InkWell(
                                    child: Text("비밀번호 찾기", style: TextStyle(color: Colors.black, fontSize: 12)),
                                    onTap: () {
                                      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                        return FindPasswordView();
                                      }));
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ))),
          ),
        ));
  }
  void signInWithEmail(BuildContext context) async {
    if (_formKey.currentState!.validate()) {
      // ProgressDialog pr = await showLoading(context, "로딩중...");
      await Provider.of<FirebaseAuthState>(context, listen: false).login(context, email: _emailController.text, password: _passwordController.text);
      // dismissLoading(pr);
    }
  }

  InputDecoration textInputDecor(String hint, TextEditingController textController) {
    return InputDecoration(
      labelText: hint,
      labelStyle: TextStyle(color: Colors.blueGrey, fontSize: 14),
      suffixIcon: textController.text.isNotEmpty
          ? Padding(
              padding: const EdgeInsetsDirectional.only(start: 0.0),
              child: IconButton(
                iconSize: 20.0,
                icon: Icon(
                  Icons.cancel,
                  color: Colors.blueGrey,
                ),
                onPressed: () {
                  setState(() {
                    textController.clear();
                  });
                },
              ),
            )
          : null,
      enabledBorder: activeInputBorder(),
      focusedBorder: activeInputBorder(),
      errorBorder: errorInputborder(),
      focusedErrorBorder: errorInputborder(),
    );
  }

  UnderlineInputBorder errorInputborder() {
    return UnderlineInputBorder(borderSide: BorderSide(color: Colors.redAccent));
  }

  UnderlineInputBorder activeInputBorder() {
    return UnderlineInputBorder(borderSide: BorderSide(color: Colors.blueGrey));
  }
}
