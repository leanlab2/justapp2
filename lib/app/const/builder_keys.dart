//SimpleBuilder
const IS_ENTIRE_TAB_SIMPLE_VIEWER = 'isEntireTabSimpleViewer';
const IS_ENTIRE_STYLE_SIMPLE_VIEWER = 'isEntireStyleSimpleViewer';
const IS_BOARD_TAB_SIMPLE_VIEWER = 'isBoardTabSimpleViewer';
const IS_BOARD_STYLE_SIMPLE_VIEWER = 'isBoardStyleSimpleViewer';
const IS_BOARD_LIST_STYLE_SIMPLE_VIEWER = 'isBoardListStyleSimpleViewer';
const IS_BOARD_NAVI_STYLE_SIMPLE_VIEWER = 'isBoardNaviStyleSimpleViewer';
const IS_BOARD_FONT_STYLE_SIMPLE_VIEWER = 'isBoardFontStyleSimpleViewer';
const IS_BOARD_SUB_FONT_STYLE_SIMPLE_VIEWER = 'isBoardSubFontStyleSimpleViewer';
const IS_BOARD_CONTENTS_FONT_STYLE_SIMPLE_VIEWER = 'isBoardContentsFontStyleSimpleViewer';
const IS_FAVORITE_TAB_SIMPLE_VIEWER = 'isFavoriteTabSimpleViewer';
const IS_FAVORITE_STYLE_SIMPLE_VIEWER = 'isFavoriteStyleSimpleViewer';
const IS_FAVORITE_LIST_STYLE_SIMPLE_VIEWER = 'isFavoriteListStyleSimpleViewer';
const IS_FAVORITE_NAVI_STYLE_SIMPLE_VIEWER = 'isFavoriteNaviStyleSimpleViewer';
const IS_FAVORITE_FONT_STYLE_SIMPLE_VIEWER = 'isFavoriteFontStyleSimpleViewer';
const IS_FAVORITE_SUB_FONT_STYLE_SIMPLE_VIEWER = 'isFavoriteSubFontStyleSimpleViewer';
const IS_FAVORITE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER = 'isFavoriteContentsFontStyleSimpleViewer';
const IS_PROFILE_TAB_SIMPLE_VIEWER = 'isProfileTabSimpleViewer';
const IS_PROFILE_STYLE_SIMPLE_VIEWER = 'isProfileStyleSimpleViewer';
const IS_PROFILE_FONT_SIMPLE_VIEWER = 'isProfileFontSimpleViewer';
const IS_PROFILE_LIST_STYLE_SIMPLE_VIEWER = 'isProfileListStyleSimpleViewer';
const IS_PROFILE_NAVI_STYLE_SIMPLE_VIEWER = 'isProfileNaviStyleSimpleViewer';
const IS_PROFILE_FONT_STYLE_SIMPLE_VIEWER = 'isProfileFontStyleSimpleViewer';
const IS_PROFILE_SUB_FONT_STYLE_SIMPLE_VIEWER = 'isProfileSubFontStyleSimpleViewer';
const IS_PROFILE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER = 'isProfileContentsFontStyleSimpleViewer';
const IS_MADE_LIST_STYLE_SIMPLE_VIEWER = 'isMadeListStyleSimpleViewer';
const IS_MADE_NAVI_STYLE_SIMPLE_VIEWER = 'isMadeNaviStyleSimpleViewer';
const IS_MADE_FONT_STYLE_SIMPLE_VIEWER = 'isMadeFontStyleSimpleViewer';
const IS_MADE_SUB_FONT_STYLE_SIMPLE_VIEWER = 'isMadeSubFontStyleSimpleViewer';
const IS_MADE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER = 'isMadeContentsFontStyleSimpleViewer';
const IS_SEND_LIST_STYLE_SIMPLE_VIEWER = 'isSendListStyleSimpleViewer';
const IS_SEND_NAVI_STYLE_SIMPLE_VIEWER = 'isSendNaviStyleSimpleViewer';
const IS_SEND_FONT_STYLE_SIMPLE_VIEWER = 'isSendFontStyleSimpleViewer';
const IS_SEND_SUB_FONT_STYLE_SIMPLE_VIEWER = 'isSendSubFontStyleSimpleViewer';
const IS_SEND_CONTENTS_FONT_STYLE_SIMPLE_VIEWER = 'isSendContentsFontStyleSimpleViewer';
const IS_RECEIVE_LIST_STYLE_SIMPLE_VIEWER = 'isReceiveListStyleSimpleViewer';
const IS_RECEIVE_NAVI_STYLE_SIMPLE_VIEWER = 'isReceiveNaviStyleSimpleViewer';
const IS_RECEIVE_FONT_STYLE_SIMPLE_VIEWER = 'isReceiveFontStyleSimpleViewer';
const IS_RECEIVE_SUB_FONT_STYLE_SIMPLE_VIEWER = 'isReceiveSubFontStyleSimpleViewer';
const IS_RECEIVE_CONTENTS_FONT_STYLE_SIMPLE_VIEWER = 'isReceiveContentsFontStyleSimpleViewer';



