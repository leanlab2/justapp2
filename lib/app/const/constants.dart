import 'dart:io';
import 'package:flutter/material.dart';
import 'package:justapp/app/models/viewer_state.dart';
import 'package:mailto/mailto.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:store_redirect/store_redirect.dart';
import 'package:flutter/foundation.dart' as foundation;

class Constants {

  //초반 setting local
  static const valueListTabCount = [2,3,4,5];
  static const valueListCategory1 = [1];
  static const valueListCategory2 = [1,2];
  static const valueListCategory3 = [1,2,3];
  static const valueListCategory4 = [1,2,3,4];
  static const valueListCategory5 = [1,2,3,4,5];
  static const valueListCategory6 = [1,2,3,4,5,6];
  static const valueListCategory7 = [1,2,3,4,5,6,7];
  static const valueListCategoryCount = [1,2,3,4,5,6,7,8];
  static const valueFuncPage = ['Board','Favorite','Category','Profile','Reservation'];
  static const valueShapeType = ['list','collection','image','map'];
  static const valueTemplate = ['community','commerce'];
  static const valueListBool = [true,false];
  static const valueFontAlignType = ['left','center','right'];
  static const valuePositionType = ['producer','consumer'];
  static const valueConnectType = ['Work','RealEstate','Recommend'];
  static const valueFontLineType = [1,2,3];
  static const valueFontSizeType = [5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30];

  static var size;
  static currentSize(context){
    size = MediaQuery.of(context).size;
  }
  static var sizeWidth = foundation.defaultTargetPlatform==foundation.TargetPlatform.iOS || foundation.defaultTargetPlatform==foundation.TargetPlatform.android?
  size.width : builderMobileWidth;
  static var sizeHeight = foundation.defaultTargetPlatform==foundation.TargetPlatform.iOS || foundation.defaultTargetPlatform==foundation.TargetPlatform.android?
  size.width : builderMobileHeight;
  static bool isMoblie = foundation.defaultTargetPlatform==foundation.TargetPlatform.iOS || foundation.defaultTargetPlatform==foundation.TargetPlatform.android?true:false;


  //custom color
  static const defaultGradient = LinearGradient(
    colors: [Color(0xfff85032), Color.fromRGBO(255, 82, 106,1)],
  );
  // static const defaultColor = Color.fromRGBO(255, 82, 106,1);
  // static const defaultPadding = 40.0;
  static var defaultColor = 4286337785;
  static var whiteColor = 4294967295;
  static var blackColor = 4278190080;
  static var updateUIForTab;
  static var tabHeight = 100;
  static double builderMobileHeight = 700;
  static double builderMobileWidth = 385;
  // static const kDefaultPadding = 30.0;

  static String splashKey = 'splashKey';
  static late SharedPreferences prefs;
  static prefsClear() {
    prefs.clear();
  }

  static Future<void> launchEmail(BuildContext context, String email, String purpose) async {
    String buildNumber = "";
    String buildVersion = "";
    if(isMoblie){
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      buildNumber = packageInfo.buildNumber;
      buildVersion = packageInfo.version;
    }
    String userKey = Provider.of<ViewerState>(context, listen: false).userModel.userKey;
    String userName = Provider.of<ViewerState>(context, listen: false).userModel.userName;
    String userEmail = Provider.of<ViewerState>(context, listen: false).userModel.userEmail;
    String userPhoneNum = Provider.of<ViewerState>(context, listen: false).userModel.userPhoneNum.toString();
    String os = Platform.operatingSystem;
    String version = Platform.version;
    String osVersion = Platform.operatingSystemVersion;
    String localeName = Platform.localeName;

    final mailtoLink = Mailto(
      to: [email],
      // cc: ['cc1@example.com', 'cc2@example.com'],
      subject: purpose == "support"?'Bug & App Feedback':'conntect',
      body:
      purpose == "support"?'\n\n\n\n\n\n\n\n\n\n\n\n\nDo not delete\nkey : $userKey\nos : $os\nbuildNumber : $buildNumber\nbuildVersion : $buildVersion\nversion : $version\nosVersion : $osVersion\nlocaleName : $localeName':
      '\n\n\n\n\n\n\n\n\n\n\n\n\nDo not delete\nkey : $userKey\nName : $userName\nEmail : $userEmail\nPhoneNumber : $userPhoneNum',
    );
    print(mailtoLink);

    if (await canLaunch('$mailtoLink')) {
      await launch('$mailtoLink');
    } else {
      throw 'Could not send email';
    }
  }

  static openWithStore() {
    StoreRedirect.redirect(
        androidAppId: googlePlayIdentifier,
        iOSAppId: appStoreIdentifier);
  }

  static Future<void> launchInWebViewOrVC(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: true,
        forceWebView: true,
        headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }


  //edit
  static const String googlePlayIdentifier = 'io.sleepinglion.sleepingbaby';
  static const String appStoreIdentifier = '1518639460';
  static String toPrivacy = 'https://www.bebesleep.com/privacy.html?hl=';
  static String toProvision = 'https://www.bebesleep.com/provision.html?hl=';
  static String toLicense = 'https://www.bebesleep.com/license.html';
  static String googleApiKey = 'AIzaSyDj9i_G9NwWZ-cWaMhAYoI3xMvfMFEdokg';
  static const API_KEY = 'AIzaSyDj9i_G9NwWZ-cWaMhAYoI3xMvfMFEdokg';
}