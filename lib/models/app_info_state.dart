import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:justapp/models/firestore/app_info.dart';

class AppInfoState extends ChangeNotifier {
  late AppInfo _appInfo;

  //appInfo
  AppInfo get appInfo => _appInfo;
  set appInfo(AppInfo value) {
    _appInfo = value;
  }
}
