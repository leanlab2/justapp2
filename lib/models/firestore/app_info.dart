import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:justapp/const/firestore_keys.dart';

class AppInfo {
  final int versionAndroid;
  final int versionIos;
  final int versionApp;
  final DocumentReference reference;

  AppInfo.fromMap(Map<String, dynamic> map, {required this.reference})
      : versionAndroid = map[KEY_VERSIONANDROID],
        versionIos = map[KEY_VERSIONIOS],
        versionApp = map[KEY_VERSIONAPP];

  AppInfo.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data()!, reference: snapshot.reference);

}
