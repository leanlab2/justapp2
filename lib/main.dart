import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/services.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:justapp/app_view.dart';
import 'package:justapp/screens/splash_view.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app/const/constants.dart';

void main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    systemNavigationBarColor: Colors.black, // navigation bar color
  ));

  //비동기로 데이터를 다룬다음 runapp할 경우 사용
  WidgetsFlutterBinding.ensureInitialized();

  //화면이 가로/ 세로 돌아가지 않고 유지
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  runZonedGuarded(() {
    initializeDateFormatting().then((_) => runApp(
      Phoenix(
        child: MyApp(),
      ),
    ));
  }, (error, stackTrace) {
    print('runZonedGuarded: Caught error in my root zone.');
  });
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<String> availableLocaleList = ["en", "ko"];

  late Future<void> _initializeFlutterFireFuture;
  Future<void> _initializeFlutterFire() async {
    await Firebase.initializeApp();
  }


  Future _setPref() async {
    Constants.prefs = await SharedPreferences.getInstance();
  }

  Future initSetting() async {
    await _initializeFlutterFire();
    await _setPref();
  }

  @override
  void initState() {
    super.initState();
    _initializeFlutterFireFuture = initSetting();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          hoverColor: Colors.transparent,
          focusColor: Colors.transparent,
        ),
        builder: (context, child) {
          return MediaQuery(
            child: child!,
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
          );
        },
        home: FutureBuilder(
          future: _initializeFlutterFireFuture,
          builder: (context, snapshot) {
            // Check for errors
            if (snapshot.hasError) {
              print('hasError');
              return SplashView();
            }

            if (snapshot.connectionState == ConnectionState.done) {
              return AppView();
            }

            return SplashView();
          },
        ));
  }
}
